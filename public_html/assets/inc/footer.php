	
	<!-- Footer -->
	<footer id="footer" class="clearfix">
		      <div id="footer-widgets">
						<div class="container">
							<div id="footer-wrapper">
							  <div class="row">

								<div class="col-sm-6 col-md-6">
									<div id="meta-3" class="widget widgetFooter widget_meta">
									  <div>
										  <ul>
											  <li><i class="fa fa-phone"></i> (21) 2147-3010</li>
											  <li><a href="mailto:name@example.com"><i class="fa fa-envelope-o"></i>  ab@canalouvidoria.com.br</a></li>
											  <li><i class="fa fa-clock-o"></i>  Segunda a Sexta-feira: 9:00 as 18:00</li>
										 </ul>
										</div>      
									</div> 
								</div>

							<div class="col-sm-6 col-md-6">
								<div id="recent-posts-3" class="widget widgetFooter widget_recent_entries">
										<div>
											<ul>
												<li>
													<a href="https://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i> Facebook</a>
												</li>
												<li>
													<a href="https://www.twitter.com" target="_blank"><i class="fa fa-linkedin"></i> LinkedIn</a>
												</li>
												<li>
													<a href="http://www.youtube.com/user/sonsofye" target="_blank"><i class="fa fa-youtube"></i> Youtube</a>
											   </li>
											</ul>
										</div>            
								</div> <!-- end widget1 -->
							</div>
						</div>
					</div>
				</div>
			</div>
		
		<div id="direitos">
			<div class="container">
			  <div class="row">
				<div class="col-md-4 copyright">
					Copyright © 2017 | CANALOUVIDORIA
				</div>
				<div class="col-md-4 col-md-offset-4 attribution">
					Desenvolvido por Webmanager
				</div>
			  </div> <!-- end .row -->
			</div>
		  </div>
		</footer>
	<script src="<?php echo BASEURL; ?>js/jquery.js"></script>
	<script src="<?php echo BASEURL; ?>js/bootstrap.min.js"></script>
	<script src="<?php echo BASEURL; ?>js/navbar-animation-fix.js"></script>
	<script src="<?php echo BASEURL; ?>js/bootstrap.js"></script>
	<script src="<?php echo BASEURL; ?>js/contact_me.js"></script>
	<script src="<?php echo BASEURL; ?>js/jqBootstrapValidation.js"></script>

</body>
</html>