<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta name="keywords" content="Conformidade, Ética, Auditoria, Recursos Humanos, Corrupção, Assédio Moral, Assédio Sexual, Fraude, Fornecedor, Favorecimento, Clima Organizacional, Pesquisa, Engajamento, Risco, Insatisfação, Network, Confidencial, Pesquisa Anônima, Serviço de Ouvidoria, Ouvidoria Terceirizada, Queixas de Empregados, Demissão, Entrevista de Demissão, Autoritarismo, Reclamação, CLT, Reclamação Trabalhista, Justa Causa">
    <meta name="description" content="Se você quer se aventurar com o HTML5, entenda aqui a sua estrutura básica, a semântica das principais marcações novas e algumas ferramentas.">



    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Ageu Barros">

    <title>CanalOuvidoria</title>

    <!-- Link Bootstrap-->
    <link href="<?php echo BASEURL; ?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo BASEURL; ?>css/bootstrap.css" rel="stylesheet">
	

    <!-- Customização -->
    <link href="<?php echo BASEURL; ?>css/estilos-categorias.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo BASEURL; ?>css/style-footer.css">

    <!-- Customização Fontes -->
    <link href="<?php echo BASEURL; ?>css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
</head>

<body>

    <header>
	  	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	        <div class="container">
	            <div class="navbar-header">
	                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse-navbar">
						<span class="sr-only"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                </button>
					<a class="navbar-brand logo navbar-left" href="<?php echo BASEURL; ?>index.php">CanalOuvidoria</a>
	            </div>
	            <!-- menu -->
	            <div class="collapse navbar-collapse" id="collapse-navbar">
	                <ul class="nav navbar-nav navbar-right">
							<li><a href="<?php echo BASEURL; ?>#sobre-nos">Quem Somos</a></li>
							<li><a href="<?php echo BASEURL; ?>#servicos">Serviços</a></li>
							<li><a href="<?php echo BASEURL; ?>#depoimentos">E-Books</a></li>
							<li><a href="http://www.grc.blog.br">Blog</a></li>
							<li><a href="<?php echo BASEURL; ?>naturezas.php">Naturezas</a></li>
							<li><a href="<?php echo BASEURL; ?>#contato">Contato</a></li>
							<li><a href="<?php echo BASEURL; ?>denunciar.php">Denuncie</a></li>		
	                </ul>
	            </div>
	        </div>      
	    </nav>

	</header>

   	
   	 

