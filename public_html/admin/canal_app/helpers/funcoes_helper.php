<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Base Project Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/url_helper.html
**/


date_default_timezone_set('America/Sao_Paulo');


/**
 * project_url
**/
if ( ! function_exists('project_url'))
{
	function project_url($uri = '', $protocol = NULL)
	{
		$ci =& get_instance();
		if( !empty($ci->bairro) ):
			$uri = "bairro:". $ci->bairro ."/". $uri;
		endif;
		return get_instance()->config->site_url($uri, $protocol);
	}
};


/**
 * admin_url
**/
if ( ! function_exists('admin_url'))
{
	function admin_url($uri = ''){
		//$ci =& get_instance();
		//$ci->config->load('admin_settings');
		//$ci->cfg = $ci->config->item('cfg_admin');
		if ($uri != ''){
			return fct_get_config("SITE_URL") . $uri;
		}else{
			return fct_get_config("SITE_URL");
		}
	}
};


/**
 * fct_get_config
**/
if ( ! function_exists('fct_get_config'))
{
	function fct_get_config($field){
		$ci =& get_instance();
		$ci->config->load('admin_settings');
		$ci->cfg = $ci->config->item('cfg_admin');
		if( !empty($field) ):
			return $ci->cfg[$field];
		else:
			return "default";
		endif;
	}
};


/**
 * fct_print_debug : 
**/
if ( ! function_exists('fct_print_debug'))
{
	function fct_print_debug($value) {
		print '<pre style="margin:5px;">'; print_r($value); print '</pre>';
	}
};


/**
 * fct_Money2db : 
 */
if ( ! function_exists('fct_Money2db'))
{
	function fct_Money2db($pNum){
		$pNum = (empty($pNum)?0:$pNum);
		//If(strLen(Trim($pNum)) == 0 ) return 0;
		$pNum = str_replace(".", "", $pNum);
		$pNum = str_replace(",", ".", $pNum);
		return $pNum;
	}
};


/**
 * fct_date2bd: formata data
 */
if ( ! function_exists('fct_date2bd')){
	function fct_date2bd($pDate="", $format="Y-m-d") {
		if(empty($pDate)) return "";
		list($sDia, $sMes, $sAno) = preg_split('/[\/.-]+/', $pDate);
		$dteReturn = date($format, strtotime($sAno ."-".$sMes ."-".$sDia));
		return $dteReturn;
	}
};


/**
 * fct_formatdate: formata data
**/
if ( ! function_exists('fct_formatdate'))
{
	function fct_formatdate($pDate="", $format="Y-m-d", $template="", $return_array=false) {
		date_default_timezone_set('America/Sao_Paulo');
		$ci =& get_instance();

		$ci->config->load('cfg_variaveis');
		$arr_meses = $ci->config->item('cfg_meses');
		$arr_day_week = $ci->config->item('cfg_day_week');

		if(empty($pDate)) return "";
		
		$type_return = "";
		$type_return = ( !empty($template)		? "template" : $type_return );
		$type_return = ( ($return_array==true)	? "array" : $type_return );

		$intDayOfWeek		= date('w',strtotime($pDate));	// Descobre o dia da semana			
		$intDayOfMonth		= date('d',strtotime($pDate));	// Descobre o dia do mês
		$intMonthOfYear		= date('n',strtotime($pDate));	// Descobre o mês
		$intYear			= date('Y',strtotime($pDate));	// Descobre o ano

		$_week			= isset($arr_day_week[$intDayOfWeek][0]) ? $arr_day_week[$intDayOfWeek][0] : "";
		$_week_full		= isset($arr_day_week[$intDayOfWeek][1]) ? $arr_day_week[$intDayOfWeek][1] : "";
		$_month			= isset($arr_meses[$intMonthOfYear][0]) ? $arr_meses[$intMonthOfYear][0] : "";
		$_month_full	= isset($arr_meses[$intMonthOfYear][1]) ? $arr_meses[$intMonthOfYear][1] : "";

		switch ($type_return):
			case "template":
				// modelo de formatacao de template
				// {semana}, {dia} de {mes} de {ano}
				$template = str_replace('{semana}', utf8_encode($_week_full), $template);
				$template = str_replace('{semanaabrev}', utf8_encode($_week), $template);
				$template = str_replace('{dia}', $intDayOfMonth, $template);
				$template = str_replace('{mes}', utf8_encode($_month_full), $template);
				$template = str_replace('{mesabrev}', utf8_encode($_month), $template);
				$template = str_replace('{ano}', $intYear, $template);			

				$dteReturn = $template;

			break;
			case "array":
				$dteReturn = array(
					"week"		=> $_week,
					"week_full"	=> $_week_full,
					"day"		=> $intDayOfMonth,
					"month"		=> $_month,
					"month_full"=> $_month_full,
					"year"		=> $intYear,
				);
				
			break;
			default:
				$dateTemp = preg_replace('/[\/.-]+/','/', $pDate);
				$dteReturn = date($format, strtotime($dateTemp));

			break;
		endswitch;

		return $dteReturn;
	}
};


/**
 * fct_encripta : encripta uma string
**/
if ( ! function_exists('fct_encripta'))
{
	function fct_encripta($pString="", $pTipo="sha512") {
		if(!empty($pString)):
			$ci =& get_instance();
			$passGerada	= hash($pTipo, sha1($pString . $ci->config->item('encryption_key')));		
		else:
			$passGerada = "";
		endif;

		return $passGerada;
	}
};


/**
 * fct_gerahashkey : gera uma hash
**/
if ( ! function_exists('fct_gerahashkey')){
	function fct_gerahashkey($pRandSize=30, $pTipo="md5") {
		$ci =& get_instance();
		$hashkey = $ci->config->item('cfg_site_name') . date("dmYHis") . random_string('alnum', $pRandSize) . date("Y");
		return do_hash($hashkey, $pTipo);
	}
};


/**
 * fct_upload_files_unico : faz upload de um arquivo
**/
if ( ! function_exists('fct_upload_files_unico'))
{
	function fct_upload_files_unico($field='userfile', $config_upl){
		$ci =& get_instance();
		$file = ''; // reset

		/*
		// RE-SERIALIZE $_FILES ARRAY
		foreach($_FILES[$field] as $key => $file){
			$i = 0;
			foreach ($file as $item){
				$data[$i][$key] = $item;
				$i++;
			}
		}// END RE-SERIALIZE
		$_FILES = $data; // re-declarate
		*/
		
		//for($j=0;$j<count($data);$j++){
			$name		= utf8_decode(Trim($_FILES[$field]["name"]));
			$extensao	= strtolower(substr(strrchr($name, "."),0));
			$name		= str_replace($extensao, "", $name);
			
			$name		= fct_RemoveAcentos($name, "_");
			$name		= date("dmYHis") ."__". random_string('alnum', 6) ."__". $name;
			$name		= strtolower($name). $extensao;

			/*
			$ci->load->library('ftp'); 
			$config_ftp['hostname']	= trim( "ftp.formatualbabykids.com.br" );
			$config_ftp['username']	= trim( "formatualbabykids" );
			$config_ftp['password']	= trim( "b@by@99" );
			$config_ftp['port']		= trim( "21" );
			$config_ftp['folder']	= trim( "/formatualbabykids/Web/dev/arquivos_upload" );
			$folder = $config_ftp["folder"];

			// grava as ações no arquivo de log.txt
			$ci->ftp->connect($config_ftp);
			$ci->ftp->mkdir($folder, 0775);
			$ci->ftp->mkdir($folder ."/posts", 0775);
			$ci->ftp->mkdir($folder, 0777);
			$ci->ftp->mkdir($folder ."/posts", 0777);

			$folder = "/web/dev/arquivos_upload";
			$ci->ftp->mkdir($folder, 0777);
			$ci->ftp->mkdir($folder ."/posts", 0777);

			$folder = "../arquivos_upload";
			$ci->ftp->mkdir($folder, 0777);
			$ci->ftp->mkdir($folder ."/posts", 0777);

			//$ci->ftp->upload($local, $file_destino, 'ascii', 0775);
			$ci->ftp->close();
			*/

			//$config_upl['file_name'] =  random_string('alnum', 6).'_image_'. $j;
			$config_upl['file_name'] =  $name;
			$ci->upload->initialize($config_upl); 
			if($ci->upload->do_upload( $field )){
				$file = $ci->upload->data();
			}/*else{
				$error = array('error' => $ci->upload->display_errors(), 'path' => $config_upl);
				$_log = utf8_encode('erros upload: ') . json_encode($error);
				fct_debug( $_log, $print=true, $log="");
				exit();
			}*/

		//}
		return $file;
	}
};


/**
 * fct_delete_arquivo : 
**/
if ( ! function_exists('fct_delete_arquivo'))
{
	function fct_delete_arquivo($path)
	{
		$return = false;
		if( !empty($path) )
		{
			if( is_file($path) And file_exists($path) )
			{
				unlink($path);
				$return = true; 
			}
		}
		return $return;
	}
}




/**
 * fct_RemoveAcentos : remove acentuação
**/
if ( ! function_exists('fct_RemoveAcentos'))
{
	function fct_RemoveAcentos($string, $slug = false) {
		$string = strtolower($string);
		// Código ASCII das vogais
		$ascii['a'] = range(224, 230);
		$ascii['e'] = range(232, 235);
		$ascii['i'] = range(236, 239);
		$ascii['o'] = array_merge(range(242, 246), array(240, 248));
		$ascii['u'] = range(249, 252);
		// Código ASCII dos outros caracteres
		$ascii['b'] = array(223);
		$ascii['c'] = array(231);
		$ascii['d'] = array(208);
		$ascii['n'] = array(241);
		$ascii['y'] = array(253, 255);
		foreach ($ascii as $key=>$item) {
			$acentos = '';
			foreach ($item AS $codigo) $acentos .= chr($codigo);
			$troca[$key] = '/['.$acentos.']/i';
		}
		$string = preg_replace(array_values($troca), array_keys($troca), $string);
		// Slug?
		if ($slug) {
			// Troca tudo que não for letra ou número por um caractere ($slug)
			$string = preg_replace('/[^a-z0-9]/i', $slug, $string);
			// Tira os caracteres ($slug) repetidos
			$string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
			$string = trim($string, $slug);
		}
		return $string;
	}
};


/**
 * fct_replace_url : arruma a url temporária para a url correta
**/
if ( ! function_exists('fct_replace_url'))
{
	function fct_replace_url($text = ''){
		if(  empty($text) ) return '';
		$text = str_replace(base_url('dev'), base_url(), $text);
		$text = str_replace(base_url('outros'), base_url(), $text);
		return $text;
	}
};


/**
 * fct_validate :
**/
if ( ! function_exists('fct_validate'))
{
	function fct_validate($cfg_config=""){
		if( empty($cfg_config) ) return false;
		$ci =& get_instance();
		$ci->load->helper(array('form', 'url'));
		$ci->load->library('form_validation');
		$ci->config->load('form_validation');
		$cfg_validate = $ci->config->item('cfg_validate');

		/*
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[12]|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[passconf]|md5');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		*/
		// if ($this->form_validation->run('contatofaleconosco')):
		
		//print_r( $cfg_validate );
		//print_r( $cfg_config );

		$ci->form_validation->set_rules( $cfg_validate[$cfg_config] );
		/*
		if ($this->form_validation->run() == TRUE):
			//self::save();
			//self::save_address();
		else:
			$error		= 1;
			$error_msg	= utf8_encode('Preencha corretamente todos os campos');
			$error_msg	.= validation_errors();
		endif;
		*/
		return $ci->form_validation->run();
	}
};


/**
 * fct_get_view : 
**/
if ( ! function_exists('fct_get_view'))
{
	function fct_get_view($pView) {
		$ci =& get_instance();
		return '{{>'. $pView .'}}';
		//return $ci->load->view('gadgets/'. $pView , '', TRUE);
	}
};


/**
 * fct_array_column : 
**/
if( ! function_exists('fct_array_column'))
{
	class customMapper {
		private $customMap = NULL;
		public function __construct($customMap){
			$this->customMap = $customMap;
		}
		public function map($data){
			return $data[$this->customMap];
		}
	}
	function fct_array_column($array,$column_name){
		$ids = array_map(array(new customMapper($column_name), 'map'), $array);
		return $ids;
	}
};


/**
 * fct_cache
 */
if ( ! function_exists('fct_cache'))
{
	function fct_cache( $horas="" ){
		return;
		/*$horas = empty($horas) ? 3 : $horas;
		$minutos = 60*$horas;
		$ci =& get_instance();
		$minify = $ci->config->item('minify_output');
		if( $minify ):
			$ci->output->cache( $minutos );
		endif;
		*/
	}
}


/**
 * fct_limpa_cache
 */
if ( ! function_exists('fct_limpa_cache'))
{
	function fct_limpa_cache($arquivo=""){
		$ci =& get_instance();
		//$path = APPPATH . "cache/"; //"arquivos/"; 
	
		$path = $ci->config->item('cache_path');
		$cache_path = ($path == '') ? APPPATH.'cache/' : $path;

		$diretorio = dir($cache_path); 
		//echo "Lista de Arquivos do diretório '<strong>". $path ."</strong>':<br />"; 
		while($arquivo = $diretorio->read()){
			$arq_pathfile = $path . $arquivo;
			//echo "<a href='". $path . $arquivo ."'>". $arquivo ."</a><br />";
			if( $arquivo == "index.html" OR $arquivo == ".htaccess" ):
				//não deletarás
			else:
				if( file_exists($arq_pathfile) and is_file($arq_pathfile) ) :
					unlink($arq_pathfile); $arq_pathfile = '';
				endif;
			endif;
		} 
		$diretorio->close();
	}
}


/**
 * fct_set_links_pages
 */
if ( ! function_exists('fct_set_links_pages'))
{
	function fct_set_links_pages($args=""){

		$_PAG_COUNT	= isset( $args["page_count"] ) ? $args["page_count"] : "" ;
		$_PAG_COUNT = (int)$_PAG_COUNT;

		$_NUM_ROWS	= isset( $args["num_rows"] ) ? $args["num_rows"] : "" ;
		$_NUM_ROWS	= (int)$_NUM_ROWS;

		$_PAGE_LINK	= isset( $args["page_link"] ) ? $args["page_link"] : "" ;

		$_PAGE_CUR	= isset( $args["page_cur"] ) ? $args["page_cur"] : "" ;
		$_PAGE_CUR	= (int)$_PAGE_CUR;
		$_PAGE_CUR	= ($_PAGE_CUR == 0 ? 1 : $_PAGE_CUR);

		$p_prev		= (int)($_PAGE_CUR - 1);
		$prev_link	= $_PAGE_LINK ."/pg:". $p_prev;
		if( $p_prev < 1 OR $p_prev == $_PAGE_CUR) :
			$p_prev = "";
			$prev_link = "";
		endif;

		$p_next		= (int)($_PAGE_CUR + 1);
		$next_link	= $_PAGE_LINK ."/pg:". $p_next;
		if( $p_next == $_PAGE_CUR OR $p_next > $_PAG_COUNT ) :
			$p_next = "";
			$next_link = "";
		endif;
		
		$arr_retorno = array(
			"page" => array(
				"page_count"	=> $_PAG_COUNT,
				"current"		=> $_PAGE_CUR,
				"prev"			=> $p_prev,
				"prev_link"		=> $prev_link,
				//"next"			=> (($p_next >= $_PAG_COUNT) ? "" : $p_next),
				"next"			=> $p_next,
				"next_link"		=> $next_link,
			)	
		);
		return $arr_retorno;
	}
}


/**
 * fct_paginacaov3 : mostra paginacao em uma listagem
 */
if ( ! function_exists('fct_paginacaov3'))
{
	function fct_paginacaov3($pPAG_NUM, $pPAG_COUNT, $pPAG_LIMIT, $pPAG_URL, $pClass="", $pNum_Rows="", $pForm="", $args="") {
		$htmlReturn = "";
		$htmlNumPages = "";
		$urlParameter = "";

		$show_text		= true;
		$css_notlink	= "notlink";
		$lbl_prev		= " &laquo; ";
		$lbl_next		= " &raquo; ";
		$lbl_textkey	= "";
		if( !empty($args) ):
			$show_text		= isset($args["show_text"])		? $args["show_text"] : true;
			$css_notlink	= isset($args["css_notlink"])	? $args["css_notlink"] : "notlink";
			$lbl_prev		= isset($args["prev"])			? utf8_encode($args["prev"]) : "";
			$lbl_next		= isset($args["next"])			? utf8_encode($args["next"]) : "";
			$lbl_textkey	= isset($args["textkey"])		? $args["textkey"] : "";
		endif;

		$urlParameter = "";
		if( !empty($lbl_textkey) ):
			if( is_array($lbl_textkey) ):
				foreach($lbl_textkey as $key => $val):
					$urlParameter .= (!empty($val) ? "/". $key .":". $val : "");
				endforeach;			
			endif;
		endif;
		$urlParameter = ( !empty($urlParameter) ? "/search". $urlParameter : "" );

		if ($pPAG_COUNT > 1){
			$pIni = 1;
			$pFim = $pPAG_COUNT;
			if($pPAG_COUNT > $pPAG_LIMIT){
				$page_loop = $pPAG_LIMIT;
				$pMeio = floor($pPAG_LIMIT/2);
				$pIni = ($pPAG_NUM - $pMeio);
				if($pIni < 1) $pIni = 1;
				$pFim = ($pIni + ($pMeio*2));
				if($pFim > $pPAG_COUNT) {
					$pFim = $pPAG_COUNT;
					$pIni = ($pFim - ($pMeio*2));
				}
			}
			$_js = "javascript:;";

			if( $show_text == true ):
				//$htmlNumPages .= '<li class="textofixo">Página '. $pPAG_NUM .' de '. $pPAG_COUNT .'</li>';
				//$htmlNumPages .= "\n";
				//$htmlNumPages .= '<li class="'. $css_notlink .'"><span>'. $pNum_Rows .' registros / página: '. $pPAG_NUM .' de '. $pPAG_COUNT .'</span></li>';
				//$htmlNumPages .= '<li><a href="javascript:return false;">Qtd. de Registros: '. $pNum_Rows .'</a></li>';
			endif;

			$prev = ( ($pIni-1)<=1?1:($pIni-1) );
			$pHREF = (!empty($pClass)) ? $_js : $pPAG_URL .'/pg:'. ($prev) . $urlParameter;
			$htmlNumPages .= '<li class="notborder"><a href="'. $pHREF .'" class="paginacao"> '. $lbl_prev .' </a></li>';
			// «
			$htmlNumPages .= "\n";

			if($pIni > 1){
				//$pHREF = (!empty($pClass)) ? $_js : $pPAG_URL .'pg/'. ($pIni-1);
				//$htmlNumPages .= '<li><a href="'. $pHREF .'" rel="'. $pPAG_URL .'pg/'. ($pIni-1) .'" class="paginacao '. $pClass .'">...</a></li>';
				//$htmlNumPages .= "\n";
			}
			for ($i=$pIni;$i<=$pFim;$i++){ 
				if ($pPAG_NUM == $i){// página atual
					$htmlNumPages .= '<li class="active '. $css_notlink .'"><a href="javascript:;" class="paginacao">'. $i .'</a></li>';
					$htmlNumPages .= "\n";
				}else{
					//$pHREF = (!empty($pClass)) ? $_js : $pPAG_URL .'pagina/'. ($i);
					$pHREF = (!empty($pClass)) ? $_js : $pPAG_URL .'/pg:'. ($i) . $urlParameter;
					$htmlNumPages .= '<li><a href="'. $pHREF .'" class="paginacao">'. $i .'</a></li>';
					$htmlNumPages .= "\n";
				}
			}
			if($pFim < $pPAG_COUNT){
				//$pHREF = (!empty($pClass)) ? $_js : $pPAG_URL .'pg/'. ($pFim+1);
				//$htmlNumPages .= '<li><a href="'. $pHREF .'" rel="'. $pPAG_URL .'pg/'. ($pFim+1) .'" class="paginacao '. $pClass .'">...</a></li>';
				//$htmlNumPages .= "\n";
			}
			$next = ( ($pFim+1)>=$pPAG_COUNT?$pPAG_COUNT:($pFim+1) ); 
			$pHREF = (!empty($pClass)) ? $_js : $pPAG_URL .'/pg:'. ($next) . $urlParameter;
			$htmlNumPages .= '<li class="notborder"><a href="'. $pHREF .'" class="paginacao"> '. $lbl_next .' </a></li>';
			// »
			$htmlNumPages .= "\n";

			/*
			$htmlReturn .= '
			<div style="clear:both !important;"></div>
			<div id="paginacao">
				<ul>'. $htmlNumPages .'</ul>
			</div><!--// paginacao -->';
			*/
			$htmlReturn .= '
			<div style="clear:both !important;"></div>
			<div style="margin:5px 0;">
				<ul class="pagination pagination-sm no-margin pull-left">
					'. $htmlNumPages .'
				</ul><!--// paginacao -->
				<div style="clear:both !important;"></div>
			</div>';
		}
		return $htmlReturn;
	}
}


/**
 * fct_paginacaov3 : mostra paginacao em uma listagem
 */
if ( ! function_exists('fct_paginacao_admin'))
{
	function fct_paginacao_admin($pPAG_NUM, $pPAG_COUNT, $pPAG_LIMIT, $pPAG_URL, $pClass="", $pNum_Rows="", $pForm="", $args="") {
		$htmlReturn = "";
		$htmlNumPages = "";
		$urlParameter = "";

		$show_text		= true;
		$css_notlink	= "notlink";
		$lbl_prev		= " &laquo; ";
		$lbl_next		= " &raquo; ";
		$lbl_textkey	= "";
		if( !empty($args) ):
			$show_text		= isset($args["show_text"])		? $args["show_text"] : true;
			$css_notlink	= isset($args["css_notlink"])	? $args["css_notlink"] : "notlink";
			$lbl_prev			= isset($args["prev"])			? utf8_encode($args["prev"]) : "";
			$lbl_next			= isset($args["next"])			? utf8_encode($args["next"]) : "";
			$lbl_textkey	= isset($args["textkey"])		? $args["textkey"] : "";
		endif;

		$urlParameter = "";
		if( !empty($lbl_textkey) ):
			if( is_array($lbl_textkey) ):
				foreach($lbl_textkey as $key => $val):
					$urlParameter .= (!empty($val) ? "/". $key ."/". $val : "");
				endforeach;			
			endif;
		endif;
		$urlParameter = ( !empty($urlParameter) ? "/search". $urlParameter : "" );

		if ($pPAG_COUNT > 1){
			$pIni = 1;
			$pFim = $pPAG_COUNT;
			if($pPAG_COUNT > $pPAG_LIMIT){
				$page_loop = $pPAG_LIMIT;
				$pMeio = floor($pPAG_LIMIT/2);
				$pIni = ($pPAG_NUM - $pMeio);
				if($pIni < 1) $pIni = 1;
				$pFim = ($pIni + ($pMeio*2));
				if($pFim > $pPAG_COUNT) {
					$pFim = $pPAG_COUNT;
					$pIni = ($pFim - ($pMeio*2));
				}
			}
			$_js = "javascript:;";

			if( $show_text == true ):
				//$htmlNumPages .= '<li class="textofixo">Página '. $pPAG_NUM .' de '. $pPAG_COUNT .'</li>';
				//$htmlNumPages .= "\n";
				//$htmlNumPages .= '<li class="'. $css_notlink .'"><span>'. $pNum_Rows .' registros / página: '. $pPAG_NUM .' de '. $pPAG_COUNT .'</span></li>';
				//$htmlNumPages .= '<li><a href="javascript:return false;">Qtd. de Registros: '. $pNum_Rows .'</a></li>';
			endif;

			$prev = ( ($pIni-1)<=1?1:($pIni-1) );
			$pHREF = (!empty($pClass)) ? $_js : $pPAG_URL .'/pg/'. ($prev) . $urlParameter;
			$htmlNumPages .= '<li class="notborder"><a href="'. $pHREF .'" class="paginacao"> '. $lbl_prev .' </a></li>';
			// «
			$htmlNumPages .= "\n";

			if($pIni > 1){
				//$pHREF = (!empty($pClass)) ? $_js : $pPAG_URL .'pg/'. ($pIni-1);
				//$htmlNumPages .= '<li><a href="'. $pHREF .'" rel="'. $pPAG_URL .'pg/'. ($pIni-1) .'" class="paginacao '. $pClass .'">...</a></li>';
				//$htmlNumPages .= "\n";
			}
			for ($i=$pIni;$i<=$pFim;$i++){ 
				if ($pPAG_NUM == $i){// página atual
					$htmlNumPages .= '<li class="active '. $css_notlink .'"><a href="javascript:;" class="paginacao">'. $i .'</a></li>';
					$htmlNumPages .= "\n";
				}else{
					//$pHREF = (!empty($pClass)) ? $_js : $pPAG_URL .'pagina/'. ($i);
					$pHREF = (!empty($pClass)) ? $_js : $pPAG_URL .'/pg/'. ($i) . $urlParameter;
					$htmlNumPages .= '<li><a href="'. $pHREF .'" class="paginacao">'. $i .'</a></li>';
					$htmlNumPages .= "\n";
				}
			}
			if($pFim < $pPAG_COUNT){
				//$pHREF = (!empty($pClass)) ? $_js : $pPAG_URL .'pg/'. ($pFim+1);
				//$htmlNumPages .= '<li><a href="'. $pHREF .'" rel="'. $pPAG_URL .'pg/'. ($pFim+1) .'" class="paginacao '. $pClass .'">...</a></li>';
				//$htmlNumPages .= "\n";
			}
			$next = ( ($pFim+1)>=$pPAG_COUNT?$pPAG_COUNT:($pFim+1) ); 
			$pHREF = (!empty($pClass)) ? $_js : $pPAG_URL .'/pg/'. ($next) . $urlParameter;
			$htmlNumPages .= '<li class="notborder"><a href="'. $pHREF .'" class="paginacao"> '. $lbl_next .' </a></li>';
			// »
			$htmlNumPages .= "\n";

			/*
			$htmlReturn .= '
			<div style="clear:both !important;"></div>
			<div id="paginacao">
				<ul>'. $htmlNumPages .'</ul>
			</div><!--// paginacao -->';
			*/
			$htmlReturn .= '
			<div style="clear:both !important;"></div>
			<div style="margin:5px 0;">
				<ul class="pagination pagination-sm no-margin pull-left">
					'. $htmlNumPages .'
				</ul><!--// paginacao -->
				<div style="clear:both !important;"></div>
			</div>';
		}
		return $htmlReturn;
	}
}


/**
 * fct_create_mail : envia o email
**/
if ( ! function_exists('fct_create_mail'))
{
	function fct_create_mail($args, $pMultiple=false, $pDebug=false) {
		$ci =& get_instance();

		/*
		 * CONFIG PHPMAILER
		**/
		//$ci->config->load('cfg_settings');
		//$cfgVar = $ci->config->item('cfg_settings');
		//$config['sender_name']	= $cfgVar["CFG_INFOS_SENDMAIL"]["sender_name"];

		//$ci->config->load('cfg_settings');
		//$ci->cfg = $ci->config->item('cfg_settings');
		//$arrEmailTo = $ci->cfg["CFG_SEND_MAIL"]["emails"];
		$arrEmailTo = array(
			'listasguardiao@gmail.com',
		);
		$arrEmailTo = array();
		$nome_project = "EAGLE HOUSE";

		$dataTpl			= isset($args["data_tpl"]) ? $args["data_tpl"] : '';
		$file_parser	= isset($args["file_parser"]) ? $args["file_parser"] : '';
		$subject			= isset($args["subject"]) ? $args["subject"] : '';
		$attach				= isset($args["attach"]) ? $args["attach"] : '';
		$moreEmailTo	= isset($args["mail_to"]) ? $args["mail_to"] : array();

		if( is_array($moreEmailTo) and count($moreEmailTo)>0 ):
			$arrEmailTo = array_merge($arrEmailTo, $moreEmailTo);
		endif;
		$arrEmailTo = array_map('trim',$arrEmailTo);
		$arrEmailTo = array_unique($arrEmailTo);

		if( empty($file_parser)): return; endif;

		$ci->load->library( array('email', 'parser') );
		$body = $ci->parser->parse($file_parser, $dataTpl, TRUE);
		$arrInfos = array(
			//'from_nome'		=> $nome_project,
			//'from_email'	=> 'sacmerzbiolab@merzbrasil.com.br',		// senha: noRP3dg2016
			//'from_email'	=> 'no-reply@merzbrasil.com.br',		// senha: noRP3dg2016		
			'to'			=> $arrEmailTo,
			'to_cc'			=> '',
			'to_bcc'		=> '',
			//'subject'		=> utf8_encode('['. $nome_project .'] '). ' : '. $subject,
			'subject'		=> '['. $nome_project .'] : '. $subject,
			//'subject'		=> 'CONTATO - DMAULI',
			'message'		=> $body, //." || ". json_encode( $arrEmailTo )
			'attach'		=> $attach			
		);
		//fct_sendmail($arrInfos, $pMultiple, $pDebug);
		fct_sendmail_smtp($arrInfos, true, false);
		//fct_sendmail_ci($arrInfos, true, false);
	}
}


/**
 * fct_sendmail_smtp : envia o email
**/
if ( ! function_exists('fct_sendmail_smtp'))
{
	function fct_sendmail_smtp($arrInfos, $pMultiple=false, $pDebug=false) {
		if($_SERVER['SERVER_NAME'] != "localhost"):
			$ci =& get_instance();

			if(is_array($arrInfos)):
				$strFromName	= (isset($arrInfos["from_nome"]) ? $arrInfos["from_nome"] : "");
				$strFromEmail	= (isset($arrInfos["from_email"]) ? $arrInfos["from_email"] : "");
				$strTo				= (isset($arrInfos["to"]) ? $arrInfos["to"] : "");
				$strToCC			= (isset($arrInfos["to_cc"]) ? $arrInfos["to_cc"] : "");
				$strToBCC			= (isset($arrInfos["to_bcc"]) ? $arrInfos["to_bcc"] : "");
				$strSubject		= (isset($arrInfos["subject"]) ? $arrInfos["subject"] : "");
				$strMessage		= (isset($arrInfos["message"]) ? $arrInfos["message"] : "");
				$strAttach		= (isset($arrInfos["attach"]) ? $arrInfos["attach"] : "");

				/*
				 * CONFIG PHPMAILER
				**/
				$ci->config->load('cfg_settings');
				$cfgVar = $ci->config->item('cfg_settings');

				$ci->load->library( array('phpmailerlib') );
				$pMail = new $ci->phpmailerlib();

				$config['sender_name']	= $cfgVar["CFG_INFOS_SENDMAIL"]["sender_name"];
				$config['sender_mail']	= $cfgVar["CFG_INFOS_SENDMAIL"]["sender_mail"];
				$config['smtp_auth']		= $cfgVar["CFG_INFOS_SENDMAIL"]["smtp_auth"];
				$config['smtp_host']		= $cfgVar["CFG_INFOS_SENDMAIL"]["smtp_host"];
				$config['smtp_port']		= $cfgVar["CFG_INFOS_SENDMAIL"]["smtp_port"];
				$config['smtp_user']		= $cfgVar["CFG_INFOS_SENDMAIL"]["smtp_user"];
				$config['smtp_pass']		= $cfgVar["CFG_INFOS_SENDMAIL"]["smtp_pass"];
				$config['smtp_tls']			= $cfgVar["CFG_INFOS_SENDMAIL"]["smtp_tls"];

				//print '<pre>';
				//print_r( $config );
				//print '</pre>';
				////exit();

				if($pMultiple):
					foreach ($strTo as $userEmail) :
						$mail = $pMail->PHPMail();
						$mail->clearAllRecipients();
						//$mail->CharSet = 'iso-8859-1';
						$mail->CharSet = 'UTF-8';
						//$mail->isHTML = true;

						if( $config['smtp_auth'] == true ){
							$mail->isSMTP();
							$mail->SMTPAuth = true;
							if( $config['smtp_tls'] == true){ $mail->SMTPSecure = 'tls'; };
							$mail->Host = $config['smtp_host'];
							$mail->Port = $config['smtp_port'];

							$mail->Username = $config['smtp_user'];
							$mail->Password = $config['smtp_pass'];
						}else{
							$mail->isMail();
						}

						//Enable SMTP debugging
						// 0 = off (for production use)
						// 1 = client messages
						// 2 = client and server messages
						$mail->SMTPDebug = 2;	//(($pDebug == true) ? 2 : 0);
						$mail->Debugoutput = 'html';

						$mail->setFrom($config['sender_mail'], $config['sender_name']);
						//$mail->setFrom($strFromEmail, $strFromName);
						//$mail->addReplyTo($strFromEmail, $strFromName);
						$mail->addReplyTo($userEmail, $userEmail);

						$mail->addAddress($userEmail, $userEmail);
						//if(!empty($strToCC)): $ci->email->cc($strToCC); endif;
						//if(!empty($strToBCC)): $ci->email->bcc($strToBCC); endif;

						$mail->Subject = $strSubject;
						$mail->Body = $strMessage;
						$mail->AltBody = $strMessage;		//Replace the plain text body with one created manually

						if( !empty($strAttach) ):
							$mail->addAttachment($strAttach);
						endif;

						if (!$mail->send()) {
								//echo "Mailer Error: " . $mail->ErrorInfo;
								//echo "<hr>";
						} else {
								//echo "<h2>Message sent! ". $userEmail ."</h2>";
								//echo "<hr>";
						}
						unset($mail);
					endforeach;
				else:
					$mail = $pMail->PHPMail();
					$mail->clearAllRecipients();
					$mail->isHTML = true;

					if( $config['smtp_auth'] == true ){
						$mail->isSMTP();
						$mail->SMTPAuth = true;
						//if( $config['smtp_tls'] == true){ $mail->SMTPSecure = 'tls'; };
						$mail->Host = $config['smtp_host'];
						$mail->Port = $config['smtp_port'];

						$mail->Username = $config['smtp_user'];
						$mail->Password = $config['smtp_pass'];
					}else{
						$mail->isMail();
					}					

					//Enable SMTP debugging
					// 0 = off (for production use)
					// 1 = client messages
					// 2 = client and server messages

					$mail->SMTPDebug = 0; // (($pDebug == true) ? 2 : 0);
					$mail->Debugoutput = 'html';

					$mail->setFrom($strFromEmail, $strFromName);
					$mail->addReplyTo($strFromEmail, $strFromName);
					$mail->addAddress($strTo, $strTo);
					//if(!empty($strToCC)): $ci->email->cc($strToCC); endif;
					//if(!empty($strToBCC)): $ci->email->bcc($strToBCC); endif;

					$mail->Subject = $strSubject;
					$mail->Body = $strMessage;
					$mail->AltBody = $strMessage;		//Replace the plain text body with one created manually

					if( !empty($strAttach) ):
						$mail->addAttachment($strAttach);
					endif;

					if (!$mail->send()) {
							//echo "Mailer Error: " . $mail->ErrorInfo;
					} else {
							//echo "Message sent!";
					}
				endif;
				//print_r( $strTo );
				if($pDebug == true)
				{
					//print 'config: ';
					//print_r( $config );
				}


			endif;
		endif; // SERVER_NAME : LOCALHOST
	}
}

/**
 * fct_sendmail : envia o email
**/
if ( ! function_exists('fct_sendmail'))
{
	function fct_sendmail($arrInfos, $pMultiple=false, $pDebug=false) {
		if($_SERVER['SERVER_NAME'] != "localhost"):
			$ci =& get_instance();

			if(is_array($arrInfos)):
				$strFromName	= (isset($arrInfos["from_nome"]) ? $arrInfos["from_nome"] : "");
				$strFromEmail	= (isset($arrInfos["from_email"]) ? $arrInfos["from_email"] : "");
				$strTo				= (isset($arrInfos["to"]) ? $arrInfos["to"] : "");
				$strToCC			= (isset($arrInfos["to_cc"]) ? $arrInfos["to_cc"] : "");
				$strToBCC			= (isset($arrInfos["to_bcc"]) ? $arrInfos["to_bcc"] : "");
				$strSubject		= (isset($arrInfos["subject"]) ? $arrInfos["subject"] : "");
				$strMessage		= (isset($arrInfos["message"]) ? $arrInfos["message"] : "");
				$strAttach		= (isset($arrInfos["attach"]) ? $arrInfos["attach"] : "");

				/*
				 * CONFIG PHPMAILER
				**/
				$ci->config->load('cfg_variaveis');
				$cfgVar = $ci->config->item('cfg_info_smpt');

				$ci->load->library( array('phpmailerlib') );
				$pMail = new $ci->phpmailerlib();

				//$config['sender_name']	= $cfgVar["CFG_INFOS_SENDMAIL"]["sender_name"];
				//$config['sender_mail']	= $cfgVar["CFG_INFOS_SENDMAIL"]["sender_mail"];
				//$config['smtp_host']		= $cfgVar["CFG_INFOS_SENDMAIL"]["smtp_host"];
				//$config['smtp_port']		= $cfgVar["CFG_INFOS_SENDMAIL"]["smtp_port"];
				//$config['smtp_user']		= $cfgVar["CFG_INFOS_SENDMAIL"]["smtp_user"];
				//$config['smtp_pass']		= $cfgVar["CFG_INFOS_SENDMAIL"]["smtp_pass"];
				//$config['smtp_tls']			= $cfgVar["CFG_INFOS_SENDMAIL"]["smtp_tls"];

				$config['sender_name']	= $cfgVar["sender_name"];
				$config['sender_mail']	= $cfgVar["sender_mail"];
				$config['smtp_host']		= $cfgVar["smtp_host"];
				$config['smtp_port']		= $cfgVar["smtp_port"];
				$config['smtp_user']		= $cfgVar["smtp_user"];
				$config['smtp_pass']		= $cfgVar["smtp_pass"];
				$config['smtp_tls']			= $cfgVar["smtp_tls"];

				//fct_print_debug( $config );
				//print '<pre>';
				//print_r( $config );
				//print '</pre>';
				//exit();

				if($pMultiple):
					foreach ($strTo as $userEmail) :
						//fct_print_debug( $userEmail );
						$mail = $pMail->PHPMail();
						$mail->clearAllRecipients();
						//$mail->CharSet = 'iso-8859-1';
						$mail->CharSet = 'UTF-8';
						$mail->IsMail();
						$mail->isHTML = true;

						//Enable SMTP debugging
						// 0 = off (for production use)
						// 1 = client messages
						// 2 = client and server messages
						$mail->SMTPDebug = 2;	//(($pDebug == true) ? 2 : 0);
						$mail->Debugoutput = 'html';

						$mail->setFrom($config['sender_mail'], $config['sender_name']);
						//$mail->setFrom($strFromEmail, $strFromName);
						//$mail->addReplyTo($strFromEmail, $strFromName);
						$mail->addReplyTo($userEmail, $userEmail);

						$mail->addAddress($userEmail, $userEmail);
						//if(!empty($strToCC)): $ci->email->cc($strToCC); endif;
						//if(!empty($strToBCC)): $ci->email->bcc($strToBCC); endif;

						$mail->Subject = $strSubject;
						$mail->Body = $strMessage;
						$mail->AltBody = $strMessage;		//Replace the plain text body with one created manually

						if( !empty($strAttach) ):
							$mail->addAttachment($strAttach);
						endif;

						if (!$mail->send()) {
								echo "Mailer Error: " . $mail->ErrorInfo;
								echo "<hr>";
						} else {
								echo "<h2>Message sent! ". $userEmail ."</h2>";
								echo "<hr>";
						}
						unset($mail);
					endforeach;
				else:
					$mail = $pMail->PHPMail();
					$mail->clearAllRecipients();
					$mail->IsMail();
					$mail->SMTPAuth = true;
					//$mail->SMTPSecure = 'tls';
					$mail->isHTML = true;

					//Enable SMTP debugging
					// 0 = off (for production use)
					// 1 = client messages
					// 2 = client and server messages

					//$mail->SMTPDebug = 2; // (($pDebug == true) ? 2 : 0);
					$mail->SMTPDebug = (($pDebug == true) ? 2 : 0);
					$mail->Debugoutput = 'html';

					$mail->setFrom($strFromEmail, $strFromName);
					$mail->addReplyTo($strFromEmail, $strFromName);
					$mail->addAddress($strTo, $strTo);
					//if(!empty($strToCC)): $ci->email->cc($strToCC); endif;
					//if(!empty($strToBCC)): $ci->email->bcc($strToBCC); endif;

					$mail->Subject = $strSubject;
					$mail->Body = $strMessage;
					$mail->AltBody = $strMessage;		//Replace the plain text body with one created manually

					if( !empty($strAttach) ):
						$mail->addAttachment($strAttach);
					endif;

					if (!$mail->send()) {
							//echo "Mailer Error: " . $mail->ErrorInfo;
					} else {
							//echo "Message sent!";
					}
				endif;
				//print_r( $strTo );
				if($pDebug == true)
				{
					//print 'config: ';
					//print_r( $config );
				}


			endif;
		endif; // SERVER_NAME : LOCALHOST
	}
}


/**
 * fct_sendmail_ci : envia o email
 */
if ( ! function_exists('fct_sendmail_ci'))
{
	function fct_sendmail_ci($arrInfos, $pMultiple=false, $pDebug=false) {
		if($_SERVER['SERVER_NAME'] != "localhost"):
			$ci =& get_instance();

			if(is_array($arrInfos)):
				//$strFromName	= $arrInfos["from_nome"];
				//$strFromEmail	= $arrInfos["from_email"];
				//$strTo			= $arrInfos["to"];
				//$strToCC		= $arrInfos["to_cc"];
				//$strToBCC		= $arrInfos["to_bcc"];
				//$strSubject		= $arrInfos["subject"];
				//$strMessage		= $arrInfos["message"];

				$strFromName	= (isset($arrInfos["from_nome"]) ? $arrInfos["from_nome"] : "");
				$strFromEmail	= (isset($arrInfos["from_email"]) ? $arrInfos["from_email"] : "");
				$strTo				= (isset($arrInfos["to"]) ? $arrInfos["to"] : "");
				$strToCC			= (isset($arrInfos["to_cc"]) ? $arrInfos["to_cc"] : "");
				$strToBCC			= (isset($arrInfos["to_bcc"]) ? $arrInfos["to_bcc"] : "");
				$strSubject		= (isset($arrInfos["subject"]) ? $arrInfos["subject"] : "");
				$strMessage		= (isset($arrInfos["message"]) ? $arrInfos["message"] : "");
				//$strAttach		= (isset($arrInfos["attach"]) ? $arrInfos["attach"] : "");

				/*
				 * CONFIG PHPMAILER
				**/
				$ci->config->load('cfg_settings');
				$cfgVar = $ci->config->item('cfg_settings');

				$config['sender_name']	= $cfgVar["CFG_INFOS_SENDMAIL"]["sender_name"];
				$config['sender_mail']	= $cfgVar["CFG_INFOS_SENDMAIL"]["sender_mail"];
				$config['smtp_host']		= $cfgVar["CFG_INFOS_SENDMAIL"]["smtp_host"];
				$config['smtp_port']		= $cfgVar["CFG_INFOS_SENDMAIL"]["smtp_port"];
				$config['smtp_user']		= $cfgVar["CFG_INFOS_SENDMAIL"]["smtp_user"];
				$config['smtp_pass']		= $cfgVar["CFG_INFOS_SENDMAIL"]["smtp_pass"];
				$config['smtp_tls']			= $cfgVar["CFG_INFOS_SENDMAIL"]["smtp_tls"];

				// ENVIAR EMAIL SIMPLES
				$config['protocol'] = 'mail'; // mail, sendmail, or smtp    The mail sending protocol.
				$config['validate'] = FALSE;
			
				$config['useragent'] = 'CodeIgniter'; // The "user agent".
				$config['wordwrap'] = TRUE;			// TRUE or FALSE (boolean)    Enable word-wrap.
				$config['wrapchars'] = 76;			// Character count to wrap at.
				$config['mailtype'] = 'html';		// text or html Type of mail. If you send HTML email you must send it as a complete web page. Make sure you don't have any relative links or relative image paths otherwise they will not work.
				$config['charset'] = 'utf-8';		// Character set (utf-8, iso-8859-1, etc.).
				$config['priority'] = 3;			// 1, 2, 3, 4, 5    Email Priority. 1 = highest. 5 = lowest. 3 = normal.
				$config['crlf'] = '\r\n';			// "\r\n" or "\n" or "\r" Newline character. (Use "\r\n" to comply with RFC 822).
				$config['newline'] = '\r\n';		// "\r\n" or "\n" or "\r"    Newline character. (Use "\r\n" to comply with RFC 822).
				$config['bcc_batch_mode'] = FALSE;	// TRUE or FALSE (boolean)    Enable BCC Batch Mode.
				$config['bcc_batch_size'] = 200;	// Number of emails in each BCC batch.

				//$mail->setFrom($config['sender_mail'], $config['sender_name']);

				if($pMultiple):
					foreach ($strTo as $userEmail) : 
						$ci->email->clear();
						$ci->email->initialize($config);
						$ci->email->from( $config['sender_mail'], $config['sender_name'] );
						$ci->email->reply_to($userEmail, $userEmail);
						$ci->email->to( $userEmail );
						$ci->email->subject( $strSubject);
						$ci->email->message( $strMessage );	
						$ci->email->send();
					endforeach;
				else:
					$ci->email->clear();
					$ci->email->initialize($config);
					$ci->email->from( $config['sender_mail'], $config['sender_name'] );
					$ci->email->reply_to($strTo, $strTo);
					$ci->email->to( $strTo );
					if(!empty($strToCC)): $ci->email->cc($strToCC); endif;
					if(!empty($strToBCC)): $ci->email->bcc($strToBCC); endif;
					$ci->email->subject( $strSubject);
					$ci->email->message( $strMessage );	
					$ci->email->send();
				endif;
				//print( '<br>'. $strFromEmail .' - ' );
				//print_r( $strToBCC );
				if($pDebug):
					echo '<hr>'. $ci->email->print_debugger();
				endif;
			endif;
		endif; // SERVER_NAME : LOCALHOST
	}
}


/**
 * fct_create_log : envia o email
 * cria log com dados enviados pelos formulários
**/
if ( ! function_exists('fct_create_log'))
{
	function fct_create_log($args="")
	{
		if( empty($args) ){ return; }

		$ci =& get_instance();

		$filename = (isset($args["filename"]) ? $args["filename"] : date("m") ."_". date("Y") .".log"); 
		// ". $baseAcao ."_". date("m") ."_". date("Y") .".log"

		/*
		 * CRIA LOG
		 * -------------------------------------------------------------
		 * cria log com dados enviados pelos formulários
		 * -------------------------------------------------------------
		**/
		//$folder		= "agilecom/dados/logs";
		////$dir_root	= self::get_path_server() . $folder . DIRECTORY_SEPARATOR; 

		//$folder		= "dados/logs";
		//$dir_root	= dirname( $_SERVER['DOCUMENT_ROOT'] ) . DIRECTORY_SEPARATOR . $folder;
		//$dir_root	= dirname( $_SERVER['DOCUMENT_ROOT'] ) . DIRECTORY_SEPARATOR . $folder;
		//$dir_root	= dirname('/home/') . DIRECTORY_SEPARATOR . $folder;
		////if( !is_dir($dir_root) ) mkdir($dir_root, 0777, TRUE);

		$dataPOST = $ci->input->post(NULL, TRUE);
		$dataJSON = json_encode($dataPOST);

		$folder		= "_BACKUP_DADOS";
		$folder		= "__xx-CI_DATA_BACKUP-xx_";
		$dir_root	= dirname( $_SERVER['DOCUMENT_ROOT'] ) . DIRECTORY_SEPARATOR . $folder;
		if( !is_dir($dir_root) ) mkdir($dir_root, 0777, TRUE);
		
		//$file_name	= $baseAcao .'_'. date("d-m-Y__H-i-s") .'.gz';
		//$file_path	= $dir_root .'/'. $file_name;

		$fp = fopen($dir_root . "/". $filename,"a+");
		fwrite($fp,"\n---- ". date("d/m/Y H:i:s")  ." - informacoes da mensagem ---- \n");
		fwrite($fp,$dataJSON);
	}
}


/**
 * fct_prepara_dados : 
**/
if ( ! function_exists('fct_prepara_dados'))
{
	function fct_prepara_dados($post) {
		if ('array' !== gettype($post)) $post=array();
		$retorno=array();
		foreach ($post as $key=>$value){
			if('string'!==gettype($value)) $post[$key]='';
			$value=urlencode(stripslashes($value));
			$retorno[]="{$key}={$value}";
		}
		return implode('&', $retorno);
	}
}

/**
 * fct_filesize_convert : 
**/
if ( ! function_exists('fct_filesize_convert'))
{
	function fct_filesize_convert($bytes)
	{
			$bytes = floatval($bytes);
					$arBytes = array(
							0 => array(
									"UNIT" => "TB",
									"VALUE" => pow(1024, 4)
							),
							1 => array(
									"UNIT" => "GB",
									"VALUE" => pow(1024, 3)
							),
							2 => array(
									"UNIT" => "MB",
									"VALUE" => pow(1024, 2)
							),
							3 => array(
									"UNIT" => "KB",
									"VALUE" => 1024
							),
							4 => array(
									"UNIT" => "B",
									"VALUE" => 1
							),
					);

			foreach($arBytes as $arItem)
			{
					if($bytes >= $arItem["VALUE"])
					{
							$result = $bytes / $arItem["VALUE"];
							$result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
							break;
					}
			}
			return $result;
	}
}

/**
 * fct_sendmailexec : 
**/
if ( ! function_exists('fct_sendmailexec'))
{
	function fct_sendmailexec($arrInfos)
	{
		$ci =& get_instance();

		$_CONST_SERVER_NAME = $_SERVER['SERVER_NAME'];
		if($_CONST_SERVER_NAME == "localhost")
		{
		}
		else
		{
			// Enviando email para o administrador
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			//$subject		= utf8_decode('[Canal Ouvidoria] : Nova Denúncia');
			//$emailsender	= "CanalOuvidoria<no-reply@webmanager.com.br>";
			//$corpo = "TESTE : ". date("Y-m-d H:i:s");

			//$_br = "\n";	if(PHP_OS == "WINNT") { $_br = "\r\n"; }
			//$headers = "MIME-Version: 1.1". $_br;
			//$headers .= "Content-type: text/html; charset=iso-8859-1". $_br;
			//$headers .= "From: ". $emailsender . $_br;
			//$headers .= "Return-Path: ". $emailsender . $_br;
			//$headers .= "Reply-To: ". $emailsender . $_br;

			//@mail("Marcio<listasguardiao@gmail.com>", $subject, $corpo, $headers);
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

			/*
			$config['charset'] = 'utf-8';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = 'html';
			$this->email->initialize($config);
			$this->email->from('marcio@3dgarage.com.br', 'Marcio');
			$this->email->to('marcio@3dgarage.com.br'); 
			//$this->email->cc('sergio@3dgarage.com.br'); 
			//$this->email->bcc('them@their-example.com'); 
			$this->email->subject('[Albuka] : Status do seu pedido : '. $idPedido);
			$this->email->message($body);	
			$this->email->send();
			echo $this->email->print_debugger();
			*/
			if(is_array($arrInfos)){
				$strFromName	= $arrInfos["from_nome"];
				$strFromEmail	= $arrInfos["from_email"];
				$strReplyTo		= isset($arrInfos["replyto"]) ? $arrInfos["replyto"] : "";
				$strTo				= $arrInfos["to"];
				$strToCC			= $arrInfos["to_cc"];
				$strToBCC			= $arrInfos["to_bcc"];
				$strSubject		= $arrInfos["subject"];
				$strMessage		= $arrInfos["message"];
				$strAttach		= isset($arrInfos["attach"]) ? $arrInfos["attach"] : '';

				foreach ($strTo as $userEmail) : 
					//$config['charset']		= 'utf-8';
					$config['charset']			= 'iso-8859-1';
					$config['crlf']					= "\r\n";
					$config['newline']			= "\r\n";
					$config['wordwrap']			= TRUE;
					$config['mailtype']			= 'html';
					$config['protocol']			= "mail";

					//$config['useragent']		= 'CodeIgniter';
					//$config['protocol']			= "smtp";
					//$config['smtp_host']		= "smtp.webmanager.com.br";
					//$config['smtp_port']		= "465";
					//$config['smtp_user']		= "no-reply@webmanager.com.br"; 
					//$config['smtp_pass']		= "julho2017";
					//$config['smtp_timeout'] = "10";
					//$config['validate']			= TRUE;

					/*
						$mailer->Host = 'servidor_de_saida'; //Onde em 'servidor_de_saida' deve ser alterado por um dos hosts abaixo:
						//Para cPanel: 'localhost';
						//Para Plesk 11 / 11.5: 'smtp.dominio.com.br';
						 
						//Descomente a linha abaixo caso revenda seja 'Plesk 11.5 Linux'
						//$mailer->SMTPSecure = 'tls';
					*/

					$ci->email->clear();
					$ci->email->clear(TRUE);
					$ci->email->initialize($config);
					$ci->email->from( $strFromEmail, $strFromName );
					if( !empty($strReplyTo) ) { $ci->email->reply_to($strReplyTo, $strFromName); }
					//$ci->email->to( $strTo );
					$ci->email->to( $userEmail );
					if(!empty($strToCC)): $ci->email->cc($strToCC); endif;
					if(!empty($strToBCC)): $ci->email->bcc($strToBCC); endif;
					$ci->email->subject( $strSubject);
					$ci->email->message( $strMessage );	
					
					if( is_array($strAttach) ):
						foreach ($strAttach as $Attach) :
							$ci->email->attach($Attach);	
						endforeach;
					endif;

					//print $userEmail ."<hr>";
					if( $ci->email->send() )
					{
						//print 'true';
					}
					else
					{
						//print 'false';
					}
					//echo $ci->email->print_debugger();
				endforeach;

			}
		}
	}
}


/**
 * fct_gera_html_email :
 */
if ( ! function_exists('fct_gera_html_email'))
{
	function fct_gera_html_email( $args=array() )
	{
		$ci =& get_instance();

		$enviar_para = array();
		$tpl_email = "";

		//$enviar_para = isset($args["enviar_para"]) ? $args["enviar_para"] : array();
		$data_parser = isset($args["data_parser"]) ? $args["data_parser"] : array();
		$cfg_chave = isset($args["cfg_chave"]) ? $args["cfg_chave"] : '';
		//$enviar_para = array(
			////'cleliapuc@gmail.com',
			////'ab@ipge.com.br',
			//'listasguardiao@gmail.com',
		//);

		/**
		 * -------------------------------------------------
		 * recuperamos o html configurado no admin
		 * -------------------------------------------------
		**/
		$ci->db->select(" * ")
			->from('tbl_configuracoes')
			->where(array(
					'cfg_area'		=> 'template-emails',
					'cfg_chave'		=> $cfg_chave
				)	
			)
			->limit(1);
		$query = $ci->db->get();
		if( (int)$query->num_rows() >=1 )
		{
			$rs_email = $query->row();
			$tpl_email = $rs_email->cfg_valor;
			$cfg_json = json_decode($rs_email->cfg_json, true);
		
			$enviar_para = preg_split('/[,;]+/', $cfg_json, -1, PREG_SPLIT_NO_EMPTY);
			$enviar_para = array_map('trim',$enviar_para);
		}
		$enviar_para = array_unique($enviar_para);
		//fct_print_debug( $enviar_para );


		/**
		 * -------------------------------------------------
		 * parsemos os dados com o template
		 * -------------------------------------------------
		**/
		//$dataMail = array(
			//'cnt_nome'			=> $cnt_nome,
			//'cnt_email'			=> $cnt_email,
			//'cnt_mensagem'	=> nl2br($cnt_mensagem) ." enviando para: ". json_encode($enviar_para),
		//);

		$tpl_email_parse = $ci->parser->parse_string($tpl_email, $data_parser, TRUE);
		//$tpl_email_parse = $this->parser->parse('email/contato', $dataMail, TRUE);

		$dataMail = array(
			'titulo_pagina'			=> 'Canal Ouvidoria',
			'conteudo_template'	=> $tpl_email_parse,
		);
		$body = $ci->parser->parse('email/template-padrao', $dataMail, TRUE);

		$arr_return = array(
			"enviar_para" => $enviar_para,
			"body" => $body,
		);
		return $arr_return;
	}
}


/* End of file funcoes_helper.php */
/* Location: ./system/helpers/funcoes_helper.php */