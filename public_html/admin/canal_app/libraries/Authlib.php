<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

Class AuthLib {

	public $CI;
	public $var_session;
	public $cfg;

	public function __construct(){
		$this->CI =& get_instance();

		$this->CI->config->load('cfg_variaveis');
		$this->cfg = $this->CI->config->item('cfg_constantes');
		//$session = $ci->session->userdata( $ci->cfg["SESSION"] );
		$this->var_session = $this->cfg["SESSION_CLIE"];
	}


	public function logged_in()
	{
		$array_session = $this->CI->session->userdata( $this->var_session );
		//fct_print_debug( $array_session );

		if( count($array_session) == 0)
		{
			redirect( site_url('login') );
			exit();
		}

		//self::get_session();
	}


	public function get_session()
	{
		$array_session = $this->CI->session->userdata( $this->var_session );

		if( $array_session["logado"] == "logado" )
		{

			$this->CI->load->vars(array('session_userid' => $array_session["_id"]));
			$this->CI->load->vars(array('session_username' => $array_session["_name"]));
			$this->CI->load->vars(array('session_level' => $array_session["_level"]));

		}

		//fct_print_debug( $array_session["logado"] );

		//exit();
		//$this->load->vars(array('rs_submenu_expertise' => $rs));


		//if( $array_session["logado"] == "logado" and !empty($array_session["nome"]) ):
			//return $array_session;
		//else:
			//return "";
		//endif;
	}


	public function get_session_by_value()
	{
		$array_session = $this->CI->session->userdata( $this->var_session );

		if( $array_session["logado"] == "logado" )
		{
			//fct_print_debug( $array_session );
			return $array_session;
		}
	}



}// end MrsAuth

/* End of file AuthLib.php */
/* Location: ./application/libraries/AuthLib.php */