<?php
require_once APPPATH . 'third_party/PHPMailer/PHPMailerAutoload.php';

class PhpmailerLib {

	public function __construct(){
		$this->PHPMail = new PHPMailer();
	}

	//public function __construct(array $options = array()){
		//$options['charset']		= "ISO-8859-1";
		////$options['extension']	= ".php";
		//$this->mm = new Mustache_Engine($options);
	//}

	//public function render($template, $context = array()){
		//return $this->mm->render($template, $context);
	//}

	public function PHPMail(){
		return $this->PHPMail;
	}

	public function sendmail_OLD(){
		//$SMTPhost		= "";
		//$username		= "";
		//$password		= "";

		$config['smtp_host'] = "";
		$config['smtp_port'] = 587;
		$config['smtp_user'] = ""; // @merzbiolab.com.br
		$config['smtp_pass'] = "";

		//Create a new PHPMailer instance
		//$mail = new PHPMailer;
		$mail = $this->PHPMail;

		$mail->isSMTP();

		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug = 2;
		$mail->Debugoutput = 'html';

		$mail->Host = $config['smtp_host'];
		$mail->Port = $config['smtp_port'];
		//$mail->SMTPSecure = 'tls';
		$mail->SMTPAuth = true;

		$mail->Username = $config['smtp_user'];
		$mail->Password = $config['smtp_pass'];

		$mail->setFrom('no-reply@merzbrasil.com.br', 'First Last');
		$mail->addReplyTo('no-reply@merzbrasil.com.br', 'First Last');
		$mail->addAddress('listasguardiao@gmail.com', 'John Doe');


		//Set the subject line
		$mail->Subject = 'PHPMailer GMail SMTP test';

		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
		$mail->Body = 'This is a plain-text message body';

		//Replace the plain text body with one created manually
		$mail->AltBody = 'This is a plain-text message body';

		//Attach an image file
		//$mail->addAttachment('images/phpmailer_mini.png');

		$mail->isHTML = true;

		//send the message, check for errors
		if (!$mail->send()) {
				echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
				echo "Message sent!";
		}
	}

	public function create_mail($args = array())
	{
		$ci =& get_instance();

		/*
		 * CONFIG PHPMAILER
		**/
		$arrEmailTo = array(
			'listasguardiao@gmail.com',
		);

		$dataTpl			= isset($args["data_tpl"]) ? $args["data_tpl"] : '';
		$file_parser	= isset($args["file_parser"]) ? $args["file_parser"] : '';
		$subject			= isset($args["subject"]) ? $args["subject"] : '';
		$attach				= isset($args["attach"]) ? $args["attach"] : '';
		$moreEmailTo	= isset($args["mail_to"]) ? $args["mail_to"] : array();

		if( is_array($moreEmailTo) and count($moreEmailTo)>0 ):
			$arrEmailTo = array_merge($arrEmailTo, $moreEmailTo);
		endif;
		$arrEmailTo = array_map('trim',$arrEmailTo);
		$arrEmailTo = array_unique($arrEmailTo);

		if( empty($file_parser)): return; endif;

		$ci->load->library( array('email', 'parser') );
		$body = $ci->parser->parse($file_parser, $dataTpl, TRUE);
		$arrInfos = array(
			'to'				=> $arrEmailTo,
			'to_cc'			=> '',
			'to_bcc'		=> '',
			//'subject'		=> utf8_encode('['. $nome_project .'] '). ' : '. $subject,
			'subject'		=> $subject,
			'message'		=> $body,
			'attach'		=> $attach			
		);
		//self::sendmail($arrInfos);

		//fct_print_debug( $arrInfos );
		//self::sendmail_teste();
		self::sendmail($arrInfos);
	}

	public function sendmail_teste(){
		//if( empty($args) ) return;

		$strnome = "marcio";
		$stremail = "listasguardiao@gmail.com";
		$corpo = "teste";

		$args = array(
			"strnome"	=> $strnome,
			"stremail"	=> $stremail,
		);

		$subject = "[eagletranslator] : Contato";	// Assunto do Email

		$arrEmails = array();
		if( !in_array($stremail, $arrEmails) ): array_push($arrEmails, $stremail); endif;
		$arr_anexos		= array();

		$args = array(
			"email_destinos" => $arrEmails,
			"subject" => $subject,
			"body" => $corpo,
			"anexos" => $arr_anexos,
		);


		/*
		 * config do email
		**/
		$username		= "no-reply@eagletranslator.com";
		//$password		= "";
		//$SMTPhost		= 'smtp.'.substr(strstr($username, '@'), 1);

		$name_from		= "eagletranslator";
		$remetente		= "contato@eagletranslator.com"; // E-Mail do Remetente

		/*
		 * parametros para envio
		**/
		$email_destinos = isset( $args["email_destinos"] ) ? $args["email_destinos"] : "";
		$subject				= isset( $args["subject"] ) ? $args["subject"] : "";
		$body						= isset( $args["body"] ) ? $args["body"] : "";
		$anexos					= isset( $args["anexos"] ) ? $args["anexos"] : array();

		$mail = $this->PHPMail;
		foreach($email_destinos as $keyValue){
			$mail->IsHTML(true);
			$mail->isMail();
			//$mail->isSMTP();
			//Enable SMTP debugging
			// 0 = off (for production use)
			// 1 = client messages
			// 2 = client and server messages
			//$mail->SMTPDebug = 0;
			//Ask for HTML-friendly debug output
			//$mail->Debugoutput = 'html';

			//$mail->Charset		= 'utf8_decode()';
			$mail->CharSet			= "UTF-8";
			//$mail->Port			= 587;
			////$mail->SMTPSecure	= "tls";
			//$mail->SMTPAuth		= true;
			//$mail->Host			= $SMTPhost;
			//$mail->Username		= $username;
			//$mail->Password		= $password;

			// Definir que o email de onde a mensagem vai ser enviada
			$mail->setFrom($username, $name_from);

			// Definir que o email alternativo para resposta
			$mail->addReplyTo($remetente, $name_from);

			//Set who the message is to be sent to
			$mail->addAddress( utf8_decode($keyValue), utf8_decode($keyValue));

			$mail->Subject = $subject;
			$mail->Body = $body;
			//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
			$mail->AltBody = $mail->Body;

			foreach($anexos as $valAnexo){
				$mail->addAttachment($valAnexo);
			}

			//$mail->send();
			if (!$mail->send()) {
				//echo "Mailer Error: " . $mail->ErrorInfo;
			} else {
				//echo "Message sent!";
			}					
			$mail->ClearAddresses();

			fct_print_debug( $mail );
		}
		unset($mail);
	}

	public function sendmail($args = array())
	{
		if(!is_array($args)) return;
		if($_SERVER['SERVER_NAME'] == "localhost") return;

		$ci =& get_instance();

		$strTo				= (isset($args["to"]) ? $args["to"] : "");
		$strToCC			= (isset($args["to_cc"]) ? $args["to_cc"] : "");
		$strToBCC			= (isset($args["to_bcc"]) ? $args["to_bcc"] : "");
		$strSubject		= (isset($args["subject"]) ? $args["subject"] : "");
		$strMessage		= (isset($args["message"]) ? $args["message"] : "");
		$strAttach		= (isset($args["attach"]) ? $args["attach"] : "");

		// ---------------------------------------------------------

		/*
		 * CONFIG PHPMAILER
		**/
		$ci->config->load('cfg_settings');
		$cfgVar = $ci->config->item('cfg_settings');
		$cfgMail = (object) $cfgVar["CFG_INFOS_SENDMAIL"];
		
		//fct_print_debug( $cfgVar );
		//fct_print_debug( $cfgMail );
		//fct_print_debug( $config );
		//fct_print_debug( $strTo );
		//exit();

		// ---------------------------------------------------------

		$mail = $this->PHPMail;
		foreach ($strTo as $userEmail)
		{
			//fct_print_debug( "email:". $userEmail );
			$mail->clearAllRecipients();
			$mail->ClearAddresses();
			$mail->ClearReplyTos();
			$mail->ClearCCs();
			$mail->ClearBCCs();

			$mail->IsHTML(true);

			if( $cfgMail->smtp_auth == true ){
				$mail->isSMTP();
				$mail->Port			= $cfgMail->smtp_port;
				//$mail->SMTPSecure	= "tls";
				$mail->SMTPAuth		= true;
				$mail->Host				= $cfgMail->smtp_host;
				$mail->Username		= $cfgMail->smtp_user;
				$mail->Password		= $cfgMail->smtp_pass;

				if( $cfgMail->mail_debug == true )
				{
					// Enable SMTP debugging
					//	0 = off (for production use)
					//	1 = client messages
					//	2 = client and server messages
					$mail->SMTPDebug = 0;
					
					// HTML debug output
					$mail->Debugoutput = 'html';
				}

			}else{
				$mail->isMail();
				//fct_print_debug( "isMail" );
			}


			//$mail->Charset		= 'utf8_decode()';
			$mail->CharSet			= "UTF-8";

			// Definir que o email de onde a mensagem vai ser enviada
			$mail->setFrom( $cfgMail->sender_mail, $cfgMail->sender_name );

			// Definir que o email alternativo para resposta
			$mail->addReplyTo($userEmail, $userEmail);

			//Set who the message is to be sent to
			//$mail->addAddress( utf8_decode($keyValue), utf8_decode($keyValue));
			$mail->addAddress( utf8_decode($userEmail), utf8_decode($userEmail));

			$mail->Subject = '['. $cfgMail->sender_name .'] : '. $strSubject;
			
			$mail->Body = $strMessage;
			//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
			$mail->AltBody = $mail->Body;
			
			if(is_array($strAttach) && count($strAttach)>0){
				foreach($strAttach as $vAnexo){
					$mail->addAttachment($vAnexo);
				}
			}

			if ( !$mail->send() )
			{
				//echo "Mailer Error: " . $mail->ErrorInfo;
			} else {
				//echo "Message sent! ". utf8_decode($userEmail);
			}

			//if( $cfgMail->mail_debug == true )
			//{
				////echo "<hr>Debug: " . $mail->ErrorInfo;
			//}

			//var_dump( $mail );
			//fct_print_debug( $mail );


			//$mail->ClearAddresses();
			//exit();
		}
		unset($mail);
		// ---------------------------------------------------------
	}

}
