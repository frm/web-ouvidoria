<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracoes extends MY_Controller {

	
	private $var_url;


	function __construct()
	{
		parent::__construct();

		/**
		 * AUTH
		**/
		$this->authlib->logged_in();


		$this->var_url = array(
			"url_form" => site_url('configuracoes/form'),
			"url_list" => site_url('configuracoes')
		);
		$this->load->vars(array('url' => $this->var_url));
	}


	public function index()
	{
		//self::listar();
	}


	public function listar($action="")
	{
		$data = array();

		/**
		 * --------------------------------------------------------
		 * excluir registros
		 * --------------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		if($baseAcao == "SEND-FORMULARIO")
		{
			$chk_delete = $this->input->post('chk_delete');
			if( is_array($chk_delete) && count($chk_delete)>0 )
			{
				foreach ($chk_delete as $key=>$item)
				{
					//$this->db->where('emp_id', $item);
					//$this->db->delete('tbl_rel_empresas_x_naturezas');

					//$this->db->where('emp_id', $item);
					//$this->db->delete('tbl_emp_contratos');

					$this->db->where('nat_id', $item );
					$this->db->delete('tbl_configuracoes');
				}
			}
		}


		$this->db->select(" * ")
			->from('tbl_configuracoes')
			->order_by( 'nat_id', 'DESC' );
		$query = $this->db->get();
		$data['rs_list'] = $query->result();
		//fct_print_debug( $data['rs_list'] );

		$this->load->view('naturezas', $data);
	}


	public function form( $nat_id="" )
	{
		$data = array();


		self::gravar_dados( $nat_id );


		if( $nat_id > 0 )
		{
			$this->db->select(" * ")
				->from('tbl_configuracoes')
				->where(array(
						'nat_id' => $nat_id
					)	
				)
				->limit(1);
			$query = $this->db->get();
			$data['rs_bd'] = $query->row();
			//fct_print_debug( $data['rs_bd'] );
		}


		$this->load->view('naturezas-form', $data);
	}


	public function gravar_dados($cfg_id="")
	{
		$var_erro = 0;
		$acao = "INSERT";

		// -------------------------------------------------
		// verifica se dados são válidos
		// -------------------------------------------------
		$baseAcao = $this->input->post('baseAcao');
		$acao = 'INSERT';
		if($baseAcao == "SEND-FORMULARIO")
		{
			$cfg_id = (int)$cfg_id;
			if( $cfg_id > 0 )
			{
				$this->db->select(" * ")
					->from('tbl_configuracoes')
					->where(array(
							'cfg_id' => $cfg_id
						)	
					)
					->limit(1);
				$query = $this->db->get();
				//$rows = (int)$query->num_rows();
				$acao = ( (int)$query->num_rows() >=1 ) ? "UPDATE" : $acao;
			}


			/**
			 * --------------------------------------------------------
			 * request fields
			 * --------------------------------------------------------
			**/
			$cfg_area				= $this->input->post('cfg_area');
			$cfg_chave			= $this->input->post('cfg_chave');
			$cfg_valor			= $this->input->post('cfg_valor');
			$cfg_json				= $this->input->post('cfg_json');
			$cfg_ativo			= (int)$this->input->post('cfg_ativo');

			$var_erro = empty($cfg_area) ? 1 : $var_erro;
			$var_erro = empty($cfg_chave) ? 1 : $var_erro;
			$var_erro = empty($cfg_valor) ? 1 : $var_erro;

			if( $var_erro == 0 )
			{
				$data_bd = array(
					'cfg_area'					=> $cfg_area,
					'cfg_chave'					=> $cfg_chave,
					'cfg_valor'					=> $cfg_valor,
					'cfg_json'					=> json_encode($cfg_json),
					'cfg_dte_cadastro'	=> date("Y-m-d H:i:s"),
					'cfg_dte_alteracao'	=> date("Y-m-d H:i:s"),
					'cfg_ativo'					=> $cfg_ativo,
				);
		
				if($acao=="UPDATE"){
					unset($data_bd["cfg_dte_cadastro"]);
					$where_bd = array('cfg_id' => $cfg_id);

					$this->db->where($where_bd);
					$this->db->update('tbl_configuracoes', $data_bd);
					//return $this->db->affected_rows();

				}else{
					
					$this->db->insert('tbl_configuracoes', $data_bd); 
					$cfg_id = (int)$this->db->insert_id();

				}

				$this->session->set_flashdata('message_validate', 'As informações foram salvas com sucesso!');
				redirect( $this->var_url["url_form"] .'/'. $cfg_id );
			}
			else
			{
				$message = 'Preencha corretamente todos os campos';
			}
		};
		// -------------------------------------------------
	}


	public function emails()
	{
		$data = array();


		$this->var_url = array(
			"url_form" => site_url('configuracoes/emails_form'),
			"url_list" => site_url('configuracoes/emails')
		);
		$this->load->vars(array('url' => $this->var_url));


		/**
		 * --------------------------------------------------------
		 * excluir registros
		 * --------------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		if($baseAcao == "SEND-FORMULARIO")
		{
			$chk_delete = $this->input->post('chk_delete');
			if( is_array($chk_delete) && count($chk_delete)>0 )
			{
				foreach ($chk_delete as $key=>$item)
				{
					$this->db->where('cfg_id', $item );
					$this->db->delete('tbl_configuracoes');
				}
			}
		}


		$this->db->select(" * ")
			->from('tbl_configuracoes')
			->where(array(
					'cfg_area' => 'template-emails'
				)	
			)
			->order_by( 'cfg_id', 'DESC' );
		$query = $this->db->get();
		$data['rs_list'] = $query->result();
		//fct_print_debug( $data['rs_list'] );

		$this->load->view('configuracoes-emails', $data);
	}


	public function emails_form( $cfg_id="" )
	{
		$data = array();


		$this->var_url = array(
			"url_form" => site_url('configuracoes/emails_form'),
			"url_list" => site_url('configuracoes/emails')
		);
		$this->load->vars(array('url' => $this->var_url));


		self::gravar_dados( $cfg_id );


		if( $cfg_id > 0 )
		{
			$this->db->select(" * ")
				->from('tbl_configuracoes')
				->where(array(
						'cfg_area' => 'template-emails',
						'cfg_id' => $cfg_id
					)	
				)
				->limit(1);
			$query = $this->db->get();
			$data['rs_bd'] = $query->row();
			//fct_print_debug( $data['rs_bd'] );
		}

		$this->load->view('configuracoes-emails-form', $data);		
	}


	public function backup()
	{
		$data = array();


		$this->var_url = array(
			"url_form" => site_url('configuracoes/backup_form'),
			"url_list" => site_url('configuracoes/backup'),
			"url_ajax" => site_url('configuracoes')
		);
		$this->load->vars(array('url' => $this->var_url));


		$this->db->select(" * ")
			->from('tbl_config_backup')
			->where(array(
					'cfg_area' => 'backup-base-de-dados'
				)	
			)
			->order_by( 'cfg_id', 'DESC' );
		$query = $this->db->get();
		$data['rs_list'] = $query->result();
		//fct_print_debug( $data['rs_list'] );


		$this->load->view('configuracoes-backup', $data);
	}


	public function json($action="")
	{
		$data = array();

		$error_num = "1";
		$error_msg = "Ocorreu um erro!";


		switch ($action) {			
		case "EXCLUIR-NATUREZA" :
			
			$nat_id = (int)$this->input->post('post_id');

			if( $nat_id > 0)
			{
				$this->db->select(" * ")
					->from('tbl_configuracoes')
					->where(array(
							'nat_id' => $nat_id
						)	
					)
					->limit(1);
				$query = $this->db->get();
				$rs_bd = $query->row();
				//fct_print_debug( $data['rs_bd'] );
				if( $query->num_rows() == 1 )
				{
					$pathFile = $this->config->item('folder_images') .'/naturezas/'. $rs_bd->nat_arquivo;
					fct_delete_arquivo($pathFile);

					$this->db->where('nat_id', $nat_id );
					$this->db->delete('tbl_configuracoes');
				
					$error_num = "0";
					$error_msg = "Registro excluído com sucesso!";
				}
			}
			else
			{
				$error_num = "0";
				$error_msg = "Código inválido!";		
			}

			$json["report"]["id"] = $nat_id;
			$json["report"]["error"] = $error_num;
			$json["report"]["mensagem"] = $error_msg;
			exit( json_encode($json) );
		
		break;
		case "BACKUP-BASE-DE-DADOS-EXCLUIR" :
			
			$post_id = (int)$this->input->post('post_id');


			$error_num = "0";
			$error_msg = "Ocorreu um erro!";


			$json["report"]["id"] = $post_id;
			$json["report"]["error"] = $error_num;
			$json["report"]["mensagem"] = $error_msg;
			exit( json_encode($json) );
		
		break;
		case "BACKUP-BASE-DE-DADOS-GERAR" :
			
			$var_erro = 0;
			$acao = "INSERT";

			$post_id = (int)$this->input->post('post_id');


			// ----------------------------------------------------------------------
			$folder		= "__xx-CI_DATA_BACKUP-xx_";
			//$folder		= "dados";

			$folder = (_CONST_SERVER_NAME == "localhost" ) ? "__xx-CI_DATA_BACKUP-xx_" : "dados";
			$dir_root	= dirname( $_SERVER['DOCUMENT_ROOT'] ) . DIRECTORY_SEPARATOR . $folder;
			if( !is_dir($dir_root) ) mkdir($dir_root, 0777, TRUE);
			
			$dte_file_name_arq	= date("d-m-Y__H-i-s");
			$dte_file_name_db		= date("Y-m-d H:i:s");
			$file_name	= $this->db->database .'_'. $dte_file_name_arq .'.gz';
			$file_path	= $dir_root .'/'. $file_name;

			$this->load->dbutil();

			// Backup your entire database and assign it to a variable
			$backup = $this->dbutil->backup();

			// Load the file helper and write the file to your server
			$this->load->helper('file');
			write_file( $file_path, $backup);
			// ----------------------------------------------------------------------



			/*
			 * enviar notificação por e-mail
			*/
			//$body = '
				//Backup Gerado: <strong>'. $file_path .'</strong><br /><br />
				//data: '. date("d/m/Y H:i:s") .'<br />
				//IP: '. $_SERVER["REMOTE_ADDR"].'
			//';
			//$arrInfos = array(
				//'from_nome'		=> 'Intac',
				//'from_email'	=> 'no-reply@intac.com.br',
				//'to_cc'			=> '',
				//'to_bcc'		=> '',
				//'subject'		=> utf8_encode('[Backup]'),
				//'message'		=> $body
			//);
			//fct_sendmail($arrInfos, true, false);
			// ----------------------------------------------------------------------


			/**
			 * --------------------------------------------------------
			 * request fields
			 * --------------------------------------------------------
			**/
			$cfg_area				= 'backup-base-de-dados';
			$cfg_chave			= $file_name;
			$cfg_valor			= 'nenhum valor';
			$cfg_json				= '';
			$cfg_ativo			= 1;

			$var_erro = empty($cfg_area) ? 1 : $var_erro;
			$var_erro = empty($cfg_chave) ? 1 : $var_erro;
			//$var_erro = empty($cfg_valor) ? 1 : $var_erro;

			if( $var_erro == 0 )
			{
				$filesize = fct_filesize_convert(filesize($file_path));
				$cfg_json = array(
					'filesize' => $filesize  		
				);

				$data_bd = array(
					'cfg_area'					=> $cfg_area,
					'cfg_chave'					=> $cfg_chave,
					'cfg_valor'					=> $cfg_valor,
					'cfg_json'					=> json_encode($cfg_json),
					'cfg_dte_cadastro'	=> $dte_file_name_db,
					'cfg_dte_alteracao'	=> $dte_file_name_db,
					'cfg_ativo'					=> $cfg_ativo,
				);
		
				$this->db->insert('tbl_config_backup', $data_bd); 
				$post_id = (int)$this->db->insert_id();


				$error_num = "0";
				$error_msg = "";
			}

			$json["report"]["id"] = $post_id;
			$json["report"]["error"] = $error_num;
			$json["report"]["mensagem"] = $error_msg;
			$json["report"]["dir_root"] = $dir_root;
			
			exit( json_encode($json) );
		
		break;
		}

	}


}
