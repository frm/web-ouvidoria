<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesquisas extends MY_Controller {


	private $var_url;


	function __construct()
	{
		parent::__construct();


		/**
		 * AUTH
		**/
		$this->authlib->logged_in();


		$this->var_url = array(
			"url_form" => site_url('empresas/form'),
			"url_list" => site_url('empresas')
		);
		$this->load->vars(array('url' => $this->var_url));
	}


	public function index()
	{
		$busca = false;
		$this->session->set_userdata( "##BUSCA#REGISTROS##", '');
		self::listar($pgnum="", $busca);
	}


	public function pg($pgnum=""){
		$busca = false;
		$session_busca = $this->session->userdata( "##BUSCA#REGISTROS##" );
		if( !empty($session_busca) )
		{
			$busca = true;
		}
		self::listar($pgnum, $busca);
	}


	public function listar($pgnum="", $busca="")
	{
		$data = array();


		$config_paginacao = array(
			"pagina_atual"	=> 1,
			"page_size"			=> 100,
			"page_limit"		=> 15,
		);
		$page_atual = $config_paginacao["pagina_atual"];
		$page_size	= $config_paginacao["page_size"];
		$page_limit	= $config_paginacao["page_limit"];


		/**
		 * --------------------------------------------------------
		 * excluir registros
		 * --------------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		$acao = 'INSERT';
		if($baseAcao == "SEND-FORMULARIO")
		{
			$chk_delete = $this->input->post('chk_delete');
			if( is_array($chk_delete) && count($chk_delete)>0 )
			{
				foreach ($chk_delete as $key=>$item)
				{
					// CONTATOS relacionados com analistas
					//$this->db->join('tbl_analistas_contatos', 'tbl_analistas_contatos.ana_id = tbl_analistas.ana_id', 'INNER');
					//$this->db->where('tbl_analistas.emp_id',$item);
					//$this->db->delete('tbl_analistas');
					$query = "
					DELETE CNT.* FROM tbl_analistas_contatos CNT 
						INNER JOIN tbl_analistas ANA ON ANA.ana_id = CNT.ana_id
					WHERE ANA.emp_id = '". $item ."'
					";
					$query = $this->db->query($query);
					//fct_print_debug( $this->db->last_query() );

					//// NATUREZAS relacionados com analistas
					//$this->db->join('tbl_rel_analistas_x_naturezas', 'tbl_rel_analistas_x_naturezas.ana_id = tbl_analistas.ana_id', 'INNER');
					//$this->db->where('tbl_analistas.emp_id',$item);
					//$this->db->delete('tbl_analistas');
					$query = "
					DELETE RELNAT.* FROM tbl_rel_analistas_x_naturezas RELNAT 
						INNER JOIN tbl_analistas ANA ON ANA.ana_id = RELNAT.ana_id
					WHERE ANA.emp_id = '". $item ."'
					";
					$query = $this->db->query($query);
					//fct_print_debug( $this->db->last_query() );

					// ANALISTAS
					$this->db->where('emp_id', $item);
					$this->db->delete('tbl_analistas');		

					// NATUREZAS
					$this->db->where('emp_id', $item);
					$this->db->delete('tbl_rel_empresas_x_naturezas');

					// CONTRATOS
					$this->db->where('emp_id', $item);
					$this->db->delete('tbl_emp_contratos');

					// EMPRESA
					$this->db->where('emp_id', $item );
					$this->db->delete('tbl_empresas');
				}
			}
		}


		//$this->db->select(" * ")
			//->from('tbl_empresas')
			//->order_by( 'emp_id', 'DESC' );
		//$query = $this->db->get();


		$query = "SELECT emp_id, emp_parent_id, emp_razao_social, emp_codigo, emp_email, emp_dte_cadastro, emp_ativo,
				CONCAT_WS(' / ', nivel2, nivel1) as emp_path,
				count_parent
			FROM 
				(SELECT 
					E1.emp_id as emp_id ,
					E1.emp_parent_id as emp_parent_id ,
					E1.emp_razao_social as emp_razao_social, 
					E1.emp_codigo as emp_codigo,
					E1.emp_email as emp_email,
					E1.emp_dte_cadastro as emp_dte_cadastro,
					E1.emp_ativo as emp_ativo,

					E2.emp_razao_social AS nivel2, 
					E1.emp_razao_social AS nivel1,

					E2.emp_id AS emp_id2, 
					E1.emp_id AS emp_id1,

					(SELECT 
					count(emp_id) as count_parent
					FROM tbl_empresas			
					WHERE emp_parent_id = E1.emp_id) AS count_parent

				FROM tbl_empresas  as E1
					LEFT JOIN tbl_empresas as E2 ON E2.emp_id = E1.emp_parent_id
				) as depth_table
			ORDER BY emp_path
		";
		$query = $this->db->query($query);
		$data['rs_list'] = $query->result();
		//fct_print_debug( $data['rs_list'] );


		$this->load->view('empresas', $data);
	}


	public function form($emp_id="")
	{
		$data = array();
		
		$emp_id = (int)$emp_id;

		//print $action .'<br />'; 
		//print $this->get("id") .'<br />';

		//$this->load->library('encryption');
		//$this->encryption->initialize(array('driver' => 'mcrypt'));
		//$key = bin2hex($this->encryption->create_key(32));
		//print "<!-- ++++ ". $key ." -->";


		self::gravar_dados( $emp_id );

		
		if( $emp_id > 0 )
		{
			$this->db->select(" * ")
				->from('tbl_empresas')
				->where(array(
						'emp_id' => $emp_id
					)	
				)
				->limit(1);
			$query = $this->db->get();
			$data['rs_bd'] = $query->row();
			//fct_print_debug( $data['rs_bd'] );


			/**
			 * --------------------------------------------------------
			 * contratos relacionados
			 * --------------------------------------------------------
			**/
			$this->db->select(" * ")
				->from('tbl_emp_contratos')
				->where(array(
						'emp_id' => $emp_id
					)	
				)
				->order_by( 'cntr_id', 'ASC' );
			$query = $this->db->get();
			$data['rs_list_cntr'] = $query->result();
			//$data['rs_list_cntr'] = $query->result_array();


			/**
			 * --------------------------------------------------------
			 * naturezas relacionadas
			 * --------------------------------------------------------
			**/
			$this->db->select(" * ")
				->from('tbl_rel_empresas_x_naturezas')
				->where(array(
						'emp_id' => $emp_id
					)	
				)
				->order_by( 'nat_id', 'DESC' );
			$query = $this->db->get();
			//$data['rs_list_nat_rel'] = $query->result();
			$data['rs_list_nat_rel'] = $query->result_array();
			//fct_print_debug( $data['rs_list_nat_rel'] );

			$data['rs_list_nat_rel_base']		= array_column($data['rs_list_nat_rel'], 'natemp_prazo', 'nat_id');
			$data['rs_list_nat_rel_image']	= array_column($data['rs_list_nat_rel'], 'natemp_arquivo', 'nat_id');
			$data['rs_list_nat_rel_ordem']	= array_column($data['rs_list_nat_rel'], 'natemp_ordem', 'nat_id');
			//$last_names = array_column($records, 'last_name', 'id');
			//print_r($last_names);

			//$data['rs_list_nat_rel_base'] = array_column($data['rs_list_nat_rel'], 'nat_id');
			//fct_print_debug( $data['rs_list_nat_rel_base'] );
		}


		/**
		 * naturezas
		**/
		$this->db->select(" * ")
			->from('tbl_naturezas')
			->order_by( 'nat_titulo', 'ASC' )
			->order_by( 'nat_id', 'DESC' );
		$query = $this->db->get();
		$data['rs_list_nat'] = $query->result();
		//fct_print_debug( $data['rs_list_nat'] );


		/**
		 * empresa matriz
		**/
		$this->db->select(" * ")
			->from('tbl_empresas')
			->where(array(
					'emp_parent_id' => 0
				)	
			)
			->order_by( 'emp_id', 'DESC' );
		$query = $this->db->get();
		$data['rs_emp_parent'] = $query->result();
		//fct_print_debug( $data['rs_emp_parent'] );



		$this->load->view('empresas-form', $data);
	}


	public function gravar_dados($emp_id="")
	{
		$var_erro = 0;
		$acao = "INSERT";

		/**
		 * --------------------------------------------------------
		 * verifica se dados são válidos
		 * --------------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		if($baseAcao == "SEND-FORMULARIO")
		{
			$emp_id = (int)$emp_id;
			if( $emp_id > 0 )
			{
				$this->db->select(" * ")
					->from('tbl_empresas')
					->where(array(
							'emp_id' => $emp_id
						)	
					)
					->limit(1);
				$query = $this->db->get();
				//$rows = (int)$query->num_rows();
				$acao = ( (int)$query->num_rows() >=1 ) ? "UPDATE" : $acao;
			}

			$emp_hashkey				= md5(date("Y-m-d H:i:s"));
			$emp_parent_id			= (int)$this->input->post('emp_parent_id');
			$emp_codigo					= $this->input->post('emp_codigo');
			$emp_razao_social		= $this->input->post('emp_razao_social');
			$emp_nome_fantasia	= $this->input->post('emp_nome_fantasia');
			$emp_cnpj						= $this->input->post('emp_cnpj');
			$emp_responsavel		= $this->input->post('emp_responsavel');
			$emp_telefone				= $this->input->post('emp_telefone');
			$emp_descricao			= $this->input->post('emp_descricao');

			$emp_end_cep				= $this->input->post('emp_end_cep');
			$emp_endereco				= $this->input->post('emp_endereco');
			$emp_end_numero			= $this->input->post('emp_end_numero');
			$emp_end_complemento = $this->input->post('emp_end_complemento');
			$emp_end_bairro			= $this->input->post('emp_end_bairro');
			$emp_end_cidade			= $this->input->post('emp_end_cidade');
			$emp_end_estado			= $this->input->post('emp_end_estado');
			$emp_email					= $this->input->post('emp_email');
			$emp_senha					= $this->input->post('emp_senha');
			$emp_ativo					= (int)$this->input->post('emp_ativo');

			$var_erro = empty($emp_codigo) ? 1 : $var_erro;
			$var_erro = empty($emp_razao_social) ? 1 : $var_erro;
			$var_erro = empty($emp_nome_fantasia) ? 1 : $var_erro;
			$var_erro = empty($emp_cnpj) ? 1 : $var_erro;
			$var_erro = empty($emp_email) ? 1 : $var_erro;
			if( $acao=="INSERT" ){
				$var_erro = empty($emp_senha) ? 1 : $var_erro;
			}

			if( $var_erro == 0 )
			{
				$data_bd = array(
					'emp_parent_id'				=> $emp_parent_id,
					'emp_hashkey'					=> $emp_hashkey,
					'emp_codigo'					=> $emp_codigo,	
					'emp_razao_social'		=> $emp_razao_social,
					'emp_nome_fantasia'		=> $emp_nome_fantasia,
					'emp_cnpj'						=> $emp_cnpj,
					'emp_responsavel'			=> $emp_responsavel,
					'emp_telefone'				=> $emp_telefone,
					'emp_descricao'				=> $emp_descricao,
					'emp_email'						=> $emp_email,
					'emp_senha'						=> fct_encripta($emp_senha),

					'emp_end_cep'					=> $emp_end_cep,
					'emp_endereco'				=> $emp_endereco,
					'emp_end_numero'			=> $emp_end_numero,
					'emp_end_complemento'	=> $emp_end_complemento,
					'emp_end_bairro'			=> $emp_end_bairro,
					'emp_end_cidade'			=> $emp_end_cidade,
					'emp_end_estado'			=> $emp_end_estado,

					//'post_urlpage'			=> url_title( convert_accented_characters($this->input->post('post_titulo')), '-', TRUE ),
					'emp_dte_cadastro'	=> date("Y-m-d H:i:s"),
					'emp_dte_alteracao'	=> date("Y-m-d H:i:s"),
					'emp_ativo'					=> $emp_ativo,
				);

				//fct_print_debug( $data_bd );
				//fct_print_debug( $acao );

				if( $acao=="UPDATE" ){
					if( empty($emp_senha) ){
						unset($data_bd["emp_senha"]);
					}
					unset($data_bd["emp_dte_cadastro"]);
					$where_bd = array('emp_id' => $emp_id);

					$this->db->where($where_bd);
					$this->db->update('tbl_empresas', $data_bd);
					//return $this->db->affected_rows();
		
				}else{
					
					$this->db->insert('tbl_empresas', $data_bd); 
					$emp_id = (int)$this->db->insert_id();
				
				}

				/**
				 * --------------------------------------------------------
				 * enviar email para alterar senha
				 * --------------------------------------------------------
				**/
					$moreEmailTo = array($emp_email);

					$data_mail = array(
						"usr_nome"		=> $emp_razao_social,
						"usr_email"		=> $emp_email,	
						"usr_link"		=> site_url('alterar-senha/'. $emp_hashkey) 	
					);
					$args_email = array(
						"data_parser"	=> $data_mail,
						"cfg_chave"		=> 'alterar-senha',
					);
					$arr_retorno = fct_gera_html_email($args_email);
					$enviar_para = $arr_retorno["enviar_para"];

					if( is_array($moreEmailTo) and count($moreEmailTo)>0 ):
						$enviar_para = array_merge($enviar_para, $moreEmailTo);
					endif;
					$enviar_para = array_map('trim',$enviar_para);
					$enviar_para = array_unique($enviar_para);

					$arrInfos = array(
						'from_nome'		=> 'Canal Ouvidoria',
						'from_email'	=> 'no-reply@webmanager.com.br',
						'replyto'			=> 'atendimento@webmanager.com.br',
						'to'					=> $enviar_para,
						'to_cc'				=> '',
						//'to_bcc'			=> array('listasguardiao@gmail.com'),
						'to_bcc'			=> '',
						'subject'			=> utf8_decode('[Canal Ouvidoria] : Alterar Senha'),
						'message'			=> $arr_retorno["body"] .' ENVIADO PARA: '. json_encode($enviar_para) 
					);
					fct_sendmailexec($arrInfos);


				/**
				 * --------------------------------------------------------
				 * contratos relacionados
				 * --------------------------------------------------------
				**/
					$cntr_num_id						= $this->input->post('cntr_id');
					$cntr_num_contrato			= $this->input->post('cntr_num_contrato');
					$cntr_dte_ini_contrato	= $this->input->post('cntr_dte_ini_contrato');
					$cntr_dte_fim_contrato	= $this->input->post('cntr_dte_fim_contrato');
					$cntr_ativo							= 1;

					if( is_array($cntr_num_contrato) && count($cntr_num_contrato)>0 )
					{
						foreach ($cntr_num_contrato as $key=>$item) {
							$cntr_id = (int)$cntr_num_id[$key];

							$data_cntr_bd = array(
								'emp_id'									=> (int)$emp_id,
								'cntr_num_contrato'				=> $item,
								'cntr_dte_ini_contrato'		=> fct_date2bd($cntr_dte_ini_contrato[$key]),
								'cntr_dte_fim_contrato'		=> fct_date2bd($cntr_dte_fim_contrato[$key]),
								'cntr_dte_cadastro'				=> date("Y-m-d H:i:s"),
								'cntr_dte_alteracao'			=> date("Y-m-d H:i:s"),
								'cntr_ativo'							=> $cntr_ativo,
							);

							$this->db->select(" * ")
								->from('tbl_emp_contratos')
								->where(array(
										'emp_id' => (int)$emp_id,
										'cntr_id' => (int)$cntr_id
									)	
								)
								->limit(1);
							$qry_cntr = $this->db->get();
							if( (int)$qry_cntr->num_rows() >= 1 )
							{
								unset($data_cntr_bd["cntr_dte_ini_contrato"]);
								$where_cntr_bd = array(
									'emp_id' => (int)$emp_id,
									'cntr_id' => (int)$cntr_id
								);
								$this->db->where($where_cntr_bd);
								$this->db->update('tbl_emp_contratos', $data_cntr_bd);
						
							}else{
								$this->db->insert('tbl_emp_contratos', $data_cntr_bd); 
							}
							
						}
					}

				/**
				 * --------------------------------------------------------
				 * natureza relacionadas
				 * --------------------------------------------------------
				**/
					// upload do arquivo
					$config_upl['upload_path']		= $this->config->item('folder_images') .'/naturezas/';
					$config_upl['allowed_types']	= 'gif|jpg|jpeg|png';
					$config_upl['max_size']				= '102048';
					$config_upl['max_width']			= '5000';
					$config_upl['max_height']			= '5000';
					$natemp_arquivo								= '';

					$chk_natureza = $this->input->post('chk_natureza');
					//$arr_natemp_prazo = $this->input->post('natemp_prazo');
					//fct_print_debug( $chk_natureza );

					//fct_print_debug( $arr_natemp_prazo );
					//$fileUplNaturezaEmp = $this->input->post('fileUplNaturezaEmp');
					if( is_array($chk_natureza) && count($chk_natureza)>0 )
					{
						//$imov_arquivo = '';
						//if($this->input->post('strArqFileUpl')) : 
							//$imov_arquivo = $this->input->post('strArqFileUpl');
							//$pathFile = $config_upl['upload_path'] . $imov_arquivo;
							//if( is_file($pathFile) And file_exists($pathFile) ) :
								//if($this->input->post('delArqFileUpl') == "1") : 
									//unlink($pathFile);
									//$imov_arquivo = '';
								//endif;
							//else :
								//$imov_arquivo = '';
							//endif;
						//endif;

						$this->db->where('emp_id', (int)$emp_id);
						$this->db->delete('tbl_rel_empresas_x_naturezas');
												
						foreach ($chk_natureza as $key => $item) {
							$natemp_arquivo = '';
							$uplFIELD = 'uplArqFileUpl_'. $item;
							$strFIELD = 'strArqFileUpl_'. $item;
							
							$natemp_prazo = $this->input->post('natemp_prazo_'. $item);
							$natemp_ordem = $this->input->post('natemp_ordem_'. $item);
							//fct_print_debug( 'natemp_prazo_'. $key ." | ". $natemp_prazo );

							$imov_arquivo = '';
							if($this->input->post($strFIELD))
							{
								$natemp_arquivo = $this->input->post($strFIELD);
								$pathFile = $config_upl['upload_path'] . $natemp_arquivo;
								//if( is_file($pathFile) And file_exists($pathFile) ) :
									//if($this->input->post('delArqFileUpl') == "1") : 
										//unlink($pathFile);
										//$imov_arquivo = '';
									//endif;
								//else :
									//$imov_arquivo = '';
								//endif;
							}
							
							if( isset($_FILES[$uplFIELD]["name"]) )
							{
								$images = fct_upload_files_unico($uplFIELD, $config_upl);
								if(is_array($images))
								{
									//$pathFile = $config_upl['upload_path'] . $natemp_arquivo;			
									//if( is_file($pathFile) And file_exists($pathFile) ) : unlink($pathFile); endif;
									$natemp_arquivo = $images["orig_name"]; 
								} // is_array images
							};

							//$natemp_prazo = 0;
							$data_nat_bd = array(
								'emp_id'					=> (int)$emp_id,
								'nat_id'					=> (int)$item,
								'natemp_arquivo'	=> $natemp_arquivo,
								'natemp_prazo'		=> $natemp_prazo,
								'natemp_ordem'		=> $natemp_ordem,
							);
							$this->db->insert('tbl_rel_empresas_x_naturezas', $data_nat_bd); 
						}
					}


					$this->session->set_flashdata('message_validate', 'As informações foram salvas com sucesso!');
					redirect( $this->var_url["url_form"] .'/'. $emp_id );

			}
			else
			{
				$message = 'Preencha corretamente todos os campos';
			}
		};
		// -------------------------------------------------
	}


}
