<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Denuncias extends MY_Controller {

	function __construct()
	{
		parent::__construct();


		/**
		 * AUTH
		**/
		$this->authlib->logged_in();
		$this->authSession = $this->authlib->get_session_by_value();


		$this->var_url = array(
			"url_form" => site_url('denuncias/form'),
			"url_list" => site_url('denuncias')
		);
		$this->load->vars(array('url' => $this->var_url));
	}


	public function index()
	{
		$busca = false;
		$this->session->set_userdata( "##BUSCA#REGISTROS##", '');
		self::listar($pgnum="", $busca);
	}


	public function pg($pgnum=""){
		$busca = false;
		$session_busca = $this->session->userdata( "##BUSCA#REGISTROS##" );
		if( !empty($session_busca) )
		{
			$busca = true;
		}
		self::listar($pgnum, $busca);
	}


	public function listar($pgnum="", $busca)
	{
		$data = array();


		$config_paginacao = array(
			"pagina_atual"	=> 1,
			"page_size"			=> 40,
			"page_limit"		=> 15,
		);
		$page_atual = $config_paginacao["pagina_atual"];
		$page_size	= $config_paginacao["page_size"];
		$page_limit	= $config_paginacao["page_limit"];


		//(int)$this->authSession["emp_id"],
		//$this->authSession;
		//fct_print_debug( $this->authSession );
		/*
			Array
			(
					[logado] => logado
					[_id] => 1
					[_name] => Márcio
					[_level] => analista
			)
		*/


		/**
		 * --------------------------------------------------------
		 * excluir registros
		 * --------------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		$acao = 'INSERT';
		/*
		if($baseAcao == "SEND-FORMULARIO")
		{
			$chk_delete = $this->input->post('chk_delete');
			if( is_array($chk_delete) && count($chk_delete)>0 )
			{
				foreach ($chk_delete as $key=>$item)
				{
					//$this->db->where('emp_id', $item);
					//$this->db->delete('tbl_rel_empresas_x_naturezas');

					//$this->db->where('emp_id', $item);
					//$this->db->delete('tbl_emp_contratos');

					//$this->db->where('den_id', $item );
					//$this->db->delete('tbl_denuncias');
				}
			}
		}
		*/


		/**
		 * --------------------------------------------------------
		 * filtro de pesquisa
		 * --------------------------------------------------------
		**/
		$bsc_search = "";
		if($baseAcao == "FILTRO-PESQUISA")
		{
			$bsc_search = $this->input->post('bsc_search');
			if(!empty($bsc_search)) 
			{
					$this->session->set_userdata( "##BUSCA#REGISTROS##", $bsc_search);
			}
		}
		if($busca == true) { $bsc_search = $this->session->userdata( "##BUSCA#REGISTROS##" ); }

		if(!empty($bsc_search)) 
		{
				$data['bsc_search']	= $bsc_search;
				$this->db->or_group_start();
					$this->db->or_like('DEN.den_num_protocolo', $bsc_search);
					$this->db->or_like('EMP.emp_razao_social', $bsc_search);
				$this->db->group_end();
		}


		/**
		 * --------------------------------------------------------
		 * monta query
		 * --------------------------------------------------------
		**/
		/*
			SELECT 
				`ANA`.`ana_id`,
				`NAT`.`nat_id`,
				`DEN`.`den_id`, 
				`DEN`.`den_descricao`, 
				`DEN`.`den_status`, 
				`DEN`.`den_dte_resposta`, `DEN`.`den_num_protocolo`, `DEN`.`den_dte_cadastro`, 
				`DEN`.`den_dte_alteracao`, 
				`EMP`.*, 
				`NAT`.`nat_titulo`
			FROM `tbl_denuncias` `DEN`
				INNER JOIN `tbl_empresas` `EMP` ON `EMP`.`emp_id` = `DEN`.`emp_id`
				INNER JOIN `tbl_naturezas` `NAT` ON `NAT`.`nat_id` = `DEN`.`nat_id`
				LEFT JOIN `tbl_autores` `DNT` ON `DNT`.`dnt_id` = `DEN`.`dnt_id`
				
				INNER JOIN `tbl_rel_analistas_x_naturezas` `REL_AN` ON `REL_AN`.`nat_id` = `NAT`.`nat_id`
				INNER JOIN `tbl_analistas` `ANA` ON `ANA`.`ana_id` = `REL_AN`.`ana_id`

			WHERE `ANA`.`ana_id` = 1
				
			ORDER BY `den_id` DESC
			LIMIT 5
		*/

		$this->db->select(" DEN.den_id, 
				DEN.den_parent_id,
				DEN.den_descricao,
				DEN.den_status, 
				DEN.den_status_admin,
				DEN.den_dte_resposta, 
				DEN.den_num_protocolo, 
				DEN.den_num_protocolo_relac,
				DEN.den_dte_cadastro, 
				DEN.den_dte_alteracao ")
			->select(" EMP.* ")
			->select("NAT.nat_id, NAT.nat_titulo ")
			->from('tbl_denuncias DEN')
			->join('tbl_empresas EMP', 'EMP.emp_id = DEN.emp_id', 'INNER')
			->join('tbl_naturezas NAT', 'NAT.nat_id = DEN.nat_id', 'INNER')
			->join('tbl_autores DNT', 'DNT.dnt_id = DEN.dnt_id', 'LEFT')
			->order_by( 'den_id', 'DESC' );
		//$query = $this->db->get();

		if( $this->authSession["_level"] == "analista" )
		{
			$this->db->select(" ANA.ana_id ");
			$this->db->join('tbl_rel_analistas_x_naturezas REL_AN', 'REL_AN.nat_id = NAT.nat_id', 'INNER');
			$this->db->join('tbl_analistas ANA', 'ANA.ana_id = REL_AN.ana_id AND ANA.emp_id = EMP.emp_id', 'INNER');
			$this->db->where( 'ANA.ana_id', (int)$this->authSession["_id"] );
		}

		if( $this->authSession["_level"] == "empresa" )
		{
			$this->db->where( 'DEN.emp_id', (int)$this->authSession["_id"] );
		}


		/**
		 * --------------------------------------------------------
		 * query para paginacao
		 * --------------------------------------------------------
		**/
		$subQuery = $this->db->get_compiled_select('', FALSE);	
		$pag_num_rows = $this->db->query($subQuery)->num_rows();

		$pgnum = (int)$pgnum;
		(int)$page_atual = 1;
		if( $pgnum > 0){ $page_atual = $pgnum; }

		$inicio = 0;
		if(!empty($page_atual)){ $inicio = ($page_atual - 1) * $page_size; }
		$page_count = ceil($pag_num_rows / $page_size); 
		$this->db->limit($page_size, $inicio);

		$query = $this->db->get();
		//fct_print_debug( $this->db->last_query() );

		$data['paginacao']	= "";
		if( $page_count>1 )
		{
			$data['paginacao'] = fct_paginacao_admin(
				$page_atual, 
				$page_count, 
				$config_paginacao["page_limit"], 
				$this->var_url["url_list"] , 
				"", 
				$pag_num_rows, 
				$pForm="#frmFiltroPesquisa"
			);
		}
		//fct_print_debug( $data['paginacao'] );


		/**
		 * --------------------------------------------------------
		 * gerar array com listagem
		 * --------------------------------------------------------
		**/
		$data['rs_list'] = $query->result();
		//fct_print_debug( $this->db->last_query() );
		//fct_print_debug( $data['rs_list'] );


		$this->load->view('denuncias', $data);
	}


	public function form($den_id="")
	{
		$data = array();

		$den_id = (int)$den_id;
	
		self::gravar_dados($den_id);


		if( $den_id > 0 )
		{
			//$this->db->select(" DEN.* ")
				//->select(" EMP.emp_razao_social ")
				//->select(" NAT.nat_titulo ")
				//->from('tbl_denuncias DEN')
				//->join('tbl_empresas EMP', 'EMP.emp_id = DEN.emp_id', 'INNER JOIN')
				//->join('tbl_naturezas NAT', 'NAT.nat_id = NAT.nat_id', 'INNER JOIN')
				//->where(array(
						//'DEN.den_id' => $den_id
					//)	
				//)
				//->limit(1);

			$this->db->select(" DEN.den_id, 
					DEN.den_parent_id,
					DEN.den_descricao,
					DEN.den_status,
					DEN.den_status_admin,
					DEN.dnt_json,
					DEN.den_dte_resposta, 
					DEN.den_num_protocolo,
					DEN.den_num_protocolo_relac,
					DEN.den_dte_cadastro, 
					DEN.den_dte_alteracao ")
				->select(" EMP.* ")
				->select(" NAT.nat_titulo ")
				->from('tbl_denuncias DEN')
				->join('tbl_empresas EMP', 'EMP.emp_id = DEN.emp_id', 'INNER')
				->join('tbl_naturezas NAT', 'NAT.nat_id = DEN.nat_id', 'INNER')
				->join('tbl_autores DNT', 'DNT.dnt_id = DEN.dnt_id', 'LEFT')
				->where(array(
						'DEN.den_id' => $den_id
					)	
				)
				->limit(1);

			if( $this->authSession["_level"] == "analista" )
			{
				$this->db->select(" ANA.ana_id, NAT.nat_id ");
				$this->db->join('tbl_rel_analistas_x_naturezas REL_AN', 'REL_AN.nat_id = NAT.nat_id', 'INNER');
				$this->db->join('tbl_analistas ANA', 'ANA.ana_id = REL_AN.ana_id', 'INNER');
				$this->db->where( 'ANA.ana_id', (int)$this->authSession["_id"] );
			}
			$query = $this->db->get();
			$data['rs_bd'] = $query->row();
			//fct_print_debug( $data['rs_bd'] );
			//fct_print_debug( $this->db->last_query() );

			/**
			 * --------------------------------------------------------
			 * anexos
			 * --------------------------------------------------------
			**/
			$this->db->select(" * ")
				->from('tbl_denunc_anexos')
				->where(array(
						'den_id' => $den_id
					)	
				)
				->order_by( 'danx_id', 'DESC' );
			$query = $this->db->get();
			$data['rs_den_anexo'] = $query->result();
			//fct_print_debug( $data['rs_den_anexo'] );


			/**
			 * --------------------------------------------------------
			 * respostas
			 * --------------------------------------------------------
			**/
			$this->db->select(" * ")
				->from('tbl_denunc_respostas')
				->where(array(
						'den_id' => $den_id
					)	
				)
				->order_by( 'dresp_id', 'DESC' );
			$query = $this->db->get();
			$data['rs_den_resp'] = $query->result();

		}


		$this->load->view('denuncias-form', $data);
	}

	public function gravar_dados($den_id="")
	{
		$var_erro = 0;


		/**
		 * --------------------------------------------------------
		 * upload do arquivo
		 * --------------------------------------------------------
		**/
		$config_upl['upload_path']		= $this->config->item('folder_images') .'/denuncias/';
		$config_upl['allowed_types']	= 'gif|jpg|jpeg|png|xls|xlsx|ppt|pptx|mp4|m4v|mov|avi|flv|mpg|wmv|3gp';
		$config_upl['max_size']				= (15*1024); // 15mb				=> '102048'; // 2mb = 2048
		$config_upl['max_width']			= '0';
		$config_upl['max_height']			= '0';


		/**
		 * --------------------------------------------------------
		 * verifica se dados são válidos
		 * --------------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		$acao = 'INSERT';
		if($baseAcao == "SEND-FORMULARIO")
		{
			$den_id = (int)$den_id;
			if( $den_id > 0 )
			{
				//$this->db->select(" * ")
					//->from('tbl_denuncias')
					//->where(array(
							//'den_id' => $den_id
						//)	
					//)
					//->limit(1);
				//$query = $this->db->get();
				////$rows = (int)$query->num_rows();
				//$acao = ( (int)$query->num_rows() >=1 ) ? "UPDATE" : $acao;
			}

			$den_descricao	= $this->input->post('den_descricao_resp');
			$dresp_aprovada	= (int)$this->input->post('dresp_aprovada');

			if( $this->authSession["_level"] == "analista" )
			{
				$var_erro = empty($den_descricao) ? 1 : $var_erro;
			}

			if( $var_erro == 0 )
			{
				/*
				precisamos saber quem está respondendo para então incluir o status correto
				*/
				//fct_print_debug( $this->cfg_status_definidos );
				//exit();

				$den_status = $this->cfg_status_definidos["default"];
				switch ($this->authSession["_level"]){
					case 'analista' :
						$den_status = $this->cfg_status_definidos["analista"];

					break;
					case 'empresa' :
					case 'administrador' :
					case 'admin' :
						$den_status = $this->cfg_status_definidos["empresa"];
				
					break;
					default : 
						$den_status = $this->cfg_status_definidos["default"];
				}

				$data_bd = array(
					'den_id'							=> (int)$den_id,
					'dresp_descricao'			=> $den_descricao,
					'dresp_status'				=> $den_status,
					'dresp_autor'					=> $this->authSession["_level"],
					'dresp_aprovada'			=> $dresp_aprovada,
					'dresp_dte_cadastro'	=> date("Y-m-d H:i:s"),
					'dresp_dte_alteracao'	=> date("Y-m-d H:i:s")
				);
				$this->db->insert('tbl_denunc_respostas', $data_bd); 
				$dresp_id = (int)$this->db->insert_id();


				/**
				 * -------------------------------------------------
				 * ini : upload dos anexos das respostas
				 * -------------------------------------------------
				**/
				if( isset($_FILES["uplArqFileUpl"]["name"]) )
				{
					$images = fct_upload_files_unico("uplArqFileUpl", $config_upl);
					if(is_array($images))
					{
						if( !empty($images["orig_name"]) )
						{
							$data_bd = array(
								'dresp_id'						=> $dresp_id,
								'dranx_arquivo'				=> $images["orig_name"],
								'dranx_dte_cadastro'	=> date("Y-m-d H:i:s"),
								'dranx_dte_alteracao'	=> date("Y-m-d H:i:s"),
							);		
							$this->db->insert('tbl_denunc_resp_anexos', $data_bd); 
						}

					} // is_array images
				};


				// atualizamos o status da denuncia
				$data_bd = array(
					'den_status_admin'	=> $den_status,
					'den_dte_alteracao'	=> date("Y-m-d H:i:s")
				);
				if($dresp_aprovada == 1){
					$data_bd_temp = array(
						'den_status' => $den_status,
					);
					$data_bd = array_merge($data_bd, $data_bd_temp);
				}
				$where_den = array(
					'den_id' => (int)$den_id,
				);
				$this->db->where($where_den);
				$this->db->update('tbl_denuncias', $data_bd);

				/**
				 * --------------------------------------------------------
				 * enviando email para os responsáveis
				 * --------------------------------------------------------
				**/
				switch ($this->authSession["_level"]){
					case 'analista' :
						//$den_status = $this->cfg_status_definidos["analista"];

					break;
					case 'empresa' :
					case 'administrador' :
					case 'admin' :
						//$den_status = $this->cfg_status_definidos["empresa"];
				
					break;
					default : 
						//$den_status = $this->cfg_status_definidos["default"];
				}

				$enviar_para = array(
					//'cleliapuc@gmail.com',
					//'ab@ipge.com.br',
					'listasguardiao@gmail.com',
				);

				/**
				 * --------------------------------------------------------
				 * recuperamos o html configurado no admin
				 * --------------------------------------------------------
				**/
				$tpl_email = '';
				$this->db->select(" * ")
					->from('tbl_configuracoes')
					->where(array(
							'cfg_area'	=> 'template-emails',
							'cfg_chave' => 'admin-denuncia-analista'
						)	
					)
					->limit(1);
				$query = $this->db->get();
				if( (int)$query->num_rows() >=1 )
				{
					$rs_email = $query->row();
					//fct_print_debug( $rs_email );
					$tpl_email = $rs_email->cfg_valor;	
					$cfg_json = json_decode($rs_email->cfg_json, true);
				
					if( is_array($cfg_json) && count($cfg_json) )
					{
						$enviar_para = preg_split('/[,;]+/', $cfg_json, -1, PREG_SPLIT_NO_EMPTY);
						$enviar_para = array_map('trim',$enviar_para);
					}
				}
				$enviar_para = array_unique($enviar_para);
				//fct_print_debug( $enviar_para );

				/**
				 * --------------------------------------------------------
				 * parsemos os dados com o template
				 * --------------------------------------------------------
				**/
				$cnt_mensagem = 'TESTE';
				$dataMail = array(
					//'cnt_nome'			=> $cnt_nome,
					//'cnt_email'			=> $cnt_email,
					'cnt_mensagem'	=> nl2br($cnt_mensagem) ." enviando para: ". json_encode($enviar_para),
				);
				$tpl_email_parse = $this->parser->parse_string($tpl_email, $dataMail, TRUE);
				//$tpl_email_parse = $this->parser->parse('email/contato', $dataMail, TRUE);

				$dataMail = array(
					'titulo_pagina'			=> 'Canal Ouvidoria',
					'conteudo_template'	=> $tpl_email_parse,
				);
				$body = $this->parser->parse('email/template-padrao', $dataMail, TRUE);
				
				/**
				 * --------------------------------------------------------
				 * configuracao do email
				 * --------------------------------------------------------
				**/
				$arrInfos = array(
					'from_nome'		=> 'Canal Ouvidoria',
					'from_email'	=> 'no-reply@webmanager.com.br',
					'replyto'			=> 'atendimento@webmanager.com.br',
					'to'					=> $enviar_para,
					'to_cc'				=> '',
					//'to_bcc'			=> array('listasguardiao@gmail.com'),
					'to_bcc'			=> '',
					'subject'			=> utf8_decode('[Canal Ouvidoria] : Denúncia : Em Aprovação'),
					'message'			=> $body
				);
				fct_sendmailexec($arrInfos);

			}
			else
			{
				$message = 'Preencha corretamente todos os campos';
			}
		};
		// -------------------------------------------------
	}	

}
