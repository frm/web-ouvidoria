<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends MY_Controller {


	private $var_url;


	function __construct()
	{
		parent::__construct();


		/**
		 * AUTH
		**/
		$this->authlib->logged_in();


		$this->var_url = array(
			"url_form" => site_url('usuarios/form'),
			"url_list" => site_url('usuarios')
		);
		$this->load->vars(array('url' => $this->var_url));
	}


	public function index()
	{
		$busca = false;
		$this->session->set_userdata( "##BUSCA#REGISTROS##", '');
		self::listar($pgnum="", $busca);
	}


	public function pg($pgnum=""){
		$busca = false;
		$session_busca = $this->session->userdata( "##BUSCA#REGISTROS##" );
		if( !empty($session_busca) )
		{
			$busca = true;
		}
		self::listar($pgnum, $busca);
	}


	public function listar($pgnum="", $busca="")
	{
		$data = array();


		$config_paginacao = array(
			"pagina_atual"	=> 1,
			"page_size"			=> 100,
			"page_limit"		=> 15,
		);
		$page_atual = $config_paginacao["pagina_atual"];
		$page_size	= $config_paginacao["page_size"];
		$page_limit	= $config_paginacao["page_limit"];


		/**
		 * --------------------------------------------------------
		 * excluir registros
		 * --------------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		$acao = 'INSERT';
		if($baseAcao == "SEND-FORMULARIO")
		{
			$chk_delete = $this->input->post('chk_delete');
			if( is_array($chk_delete) && count($chk_delete)>0 )
			{
				foreach ($chk_delete as $key=>$item)
				{
					$this->db->where('usr_id', $item );
					$this->db->delete('tbl_usuarios');
				}
			}
		}


		$this->db->select(" * ")
			->from('tbl_usuarios')
			->order_by( 'usr_id', 'DESC' );
		$query = $this->db->get();

		$data['rs_list'] = $query->result();
		//fct_print_debug( $data['rs_list'] );


		$this->load->view('usuarios', $data);
	}


	public function form($usr_id="")
	{
		$data = array();
		
		$usr_id = (int)$usr_id;

		//print $action .'<br />'; 
		//print $this->get("id") .'<br />';

		//$this->load->library('encryption');
		//$this->encryption->initialize(array('driver' => 'mcrypt'));
		//$key = bin2hex($this->encryption->create_key(32));
		//print "<!-- ++++ ". $key ." -->";


		self::gravar_dados( $usr_id );

		
		if( $usr_id > 0 )
		{
			$this->db->select(" * ")
				->from('tbl_usuarios')
				->where(array(
						'usr_id' => $usr_id
					)	
				)
				->limit(1);
			$query = $this->db->get();
			$data['rs_bd'] = $query->row();
			//fct_print_debug( $data['rs_bd'] );
		}


		$this->load->view('usuarios-form', $data);
	}


	public function gravar_dados($usr_id="")
	{
		$var_erro = 0;
		$acao = "INSERT";

		/**
		 * --------------------------------------------------------
		 * verifica se dados são válidos
		 * --------------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		if($baseAcao == "SEND-FORMULARIO")
		{
			$usr_id = (int)$usr_id;
			if( $usr_id > 0 )
			{
				$this->db->select(" * ")
					->from('tbl_usuarios')
					->where(array(
							'usr_id' => $usr_id
						)	
					)
					->limit(1);
				$query = $this->db->get();
				//$rows = (int)$query->num_rows();
				$acao = ( (int)$query->num_rows() >=1 ) ? "UPDATE" : $acao;
			}

			$usr_nome						= $this->input->post('usr_nome');
			$usr_email					= $this->input->post('usr_email');
			$usr_senha					= $this->input->post('usr_senha');
			$usr_ativo					= (int)$this->input->post('usr_ativo');

			$var_erro = empty($usr_nome) ? 1 : $var_erro;
			$var_erro = empty($usr_email) ? 1 : $var_erro;
			if( $acao=="INSERT" ){
				$var_erro = empty($usr_senha) ? 1 : $var_erro;
			}

			if( $var_erro == 0 )
			{
				$data_bd = array(
					'usr_nome	'					=> $usr_nome,
					'usr_email'					=> $usr_email,
					'usr_senha'					=> fct_encripta($usr_senha),

					//'post_urlpage'			=> url_title( convert_accented_characters($this->input->post('post_titulo')), '-', TRUE ),
					'usr_dte_cadastro'	=> date("Y-m-d H:i:s"),
					'usr_dte_alteracao'	=> date("Y-m-d H:i:s"),
					'usr_ativo'					=> $usr_ativo,
				);

				//fct_print_debug( $data_bd );
				//fct_print_debug( $acao );

				if( $acao=="UPDATE" ){
					if( empty($usr_senha) ){
						unset($data_bd["usr_senha"]);
					}
					unset($data_bd["usr_dte_cadastro"]);
					$where_bd = array('usr_id' => $usr_id);

					$this->db->where($where_bd);
					$this->db->update('tbl_usuarios', $data_bd);
					//return $this->db->affected_rows();
		
				}else{
					
					$this->db->insert('tbl_usuarios', $data_bd); 
					$usr_id = (int)$this->db->insert_id();
				
				}

			}
			else
			{
				$message = 'Preencha corretamente todos os campos';
				print $message;
			}
		};
		// -------------------------------------------------
	}


}
