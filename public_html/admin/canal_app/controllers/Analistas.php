<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analistas extends MY_Controller {


	private $var_url;
	private $authSession;


	function __construct()
	{
		parent::__construct();


		/**
		 * AUTH
		**/
		$this->authlib->logged_in();
		$this->authSession = $this->authlib->get_session_by_value();


		$this->var_url = array(
			"url_form" => site_url('analistas/form'),
			"url_list" => site_url('analistas'),
			"url_grid" => site_url('analistas/grid'),
			"url_json" => site_url('analistas/json'),
		);
		$this->load->vars(array('url' => $this->var_url));
	}


	public function index()
	{
		$busca = false;
		$this->session->set_userdata( "##BUSCA#REGISTROS##", '');
		self::listar($pgnum="", $busca);
	}


	public function pg($pgnum=""){
		$busca = false;
		$session_busca = $this->session->userdata( "##BUSCA#REGISTROS##" );
		if( !empty($session_busca) )
		{
			$busca = true;
		}
		self::listar($pgnum, $busca);
	}


	public function listar($pgnum="", $busca="")
	{
		$data = array();


		$config_paginacao = array(
			"pagina_atual"	=> 1,
			"page_size"			=> 100,
			"page_limit"		=> 15,
		);
		$page_atual = $config_paginacao["pagina_atual"];
		$page_size	= $config_paginacao["page_size"];
		$page_limit	= $config_paginacao["page_limit"];


		/**
		 * --------------------------------------------------------
		 * excluir registros
		 * --------------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		$acao = 'INSERT';
		if($baseAcao == "SEND-FORMULARIO")
		{
			$chk_delete = $this->input->post('chk_delete');
			if( is_array($chk_delete) && count($chk_delete)>0 )
			{
				foreach ($chk_delete as $key=>$item)
				{

					if( $this->authSession["_level"] == "empresa" )
					{
						$this->db->where('ana_id', $item);
						$this->db->delete('tbl_rel_analistas_x_naturezas');

						$this->db->where('ana_id', $item);
						$this->db->delete('tbl_analistas_contatos');

						$this->db->where('emp_id', (int)$this->authSession["_id"] );
						$this->db->where('ana_id', $item);
						$this->db->delete('tbl_analistas');
					}

					if( $this->authSession["_level"] == "administrador" )
					{
						$this->db->where('ana_id', $item);
						$this->db->delete('tbl_rel_analistas_x_naturezas');

						$this->db->where('ana_id', $item);
						$this->db->delete('tbl_analistas_contatos');

						$this->db->where('ana_id', $item);
						$this->db->delete('tbl_analistas');
					}

				}
			}
		}


		/**
		 * --------------------------------------------------------
		 * filtro de pesquisa
		 * --------------------------------------------------------
		**/
		$bsc_search = "";
		if($baseAcao == "FILTRO-PESQUISA")
		{
			$bsc_search = $this->input->post('bsc_search');
			if(!empty($bsc_search)) 
			{
					$this->session->set_userdata( "##BUSCA#REGISTROS##", $bsc_search);
			}
		}
		if($busca == true) { $bsc_search = $this->session->userdata( "##BUSCA#REGISTROS##" ); }

		if(!empty($bsc_search)) 
		{
				$data['bsc_search']	= $bsc_search;
				$this->db->or_group_start();
					$this->db->or_like('ana_nome', $bsc_search);
					$this->db->or_like('ana_email', $bsc_search);
				$this->db->group_end();
		}


		/**
		 * --------------------------------------------------------
		 * monta query
		 * --------------------------------------------------------
		**/
		$this->db->select(" * ")
			->from('tbl_analistas')
			->order_by( 'ana_id', 'DESC' );
		//$query = $this->db->get();

		if( $this->authSession["_level"] == "empresa" )
		{
			$this->db->where( 'emp_id', (int)$this->authSession["_id"] );
		}


		/**
		 * --------------------------------------------------------
		 * query para paginacao
		 * --------------------------------------------------------
		**/
		$subQuery = $this->db->get_compiled_select('', FALSE);	
		$pag_num_rows = $this->db->query($subQuery)->num_rows();

		$pgnum = (int)$pgnum;
		(int)$page_atual = 1;
		if( $pgnum > 0){ $page_atual = $pgnum; }

		$inicio = 0;
		if(!empty($page_atual)){ $inicio = ($page_atual - 1) * $page_size; }
		$page_count = ceil($pag_num_rows / $page_size); 
		$this->db->limit($page_size, $inicio);

		$query = $this->db->get();
		//fct_print_debug( $this->db->last_query() );

		$data['paginacao']	= "";
		if( $page_count>1 )
		{
			$data['paginacao'] = fct_paginacao_admin(
				$page_atual, 
				$page_count, 
				$config_paginacao["page_limit"], 
				$this->var_url["url_list"] , 
				"", 
				$pag_num_rows, 
				$pForm="#frmFiltroPesquisa"
			);
		}
		//fct_print_debug( $data['paginacao'] );


		/**
		 * --------------------------------------------------------
		 * gerar array com listagem
		 * --------------------------------------------------------
		**/
		$data['rs_list'] = $query->result();
		//fct_print_debug( $data['rs_list'] );


		$this->load->view('analistas', $data);
	}


	public function form($ana_id="")
	{
		$data = array();

		$ana_id = (int)$ana_id;


		self::gravar_dados( $ana_id );


		if( $ana_id > 0 )
		{
			$this->db->select(" * ")
				->from('tbl_analistas')
				->where(array(
						'ana_id' => $ana_id
					)	
				)
				->limit(1);

			if( $this->authSession["_level"] == "empresa" )
			{
				$this->db->where( 'emp_id', (int)$this->authSession["_id"] );
			}

			$query = $this->db->get();
			$data['rs_bd'] = $query->row();
			//fct_print_debug( $data['rs_bd'] );


			/**
			 * --------------------------------------------------------
			 * contatos relacionados
			 * --------------------------------------------------------
			**/
			$this->db->select(" * ")
				->from('tbl_analistas_contatos')
				->where(array(
						'ana_id' => $ana_id
					)	
				)
				->order_by( 'acnt_id', 'ASC' );
			$query = $this->db->get();
			$data['rs_list_acnt'] = $query->result();
			//$data['rs_list_acnt'] = $query->result_array();


			/**
			 * --------------------------------------------------------
			 * naturezas relacionadas
			 * --------------------------------------------------------
			**/
			$this->db->select(" * ")
				->from('tbl_rel_analistas_x_naturezas')
				->where(array(
						'ana_id' => $ana_id
					)	
				)
				->order_by( 'nat_id', 'DESC' );
			$query = $this->db->get();
			//$data['rs_list_nat_rel'] = $query->result();
			$data['rs_list_nat_rel'] = $query->result_array();
			//fct_print_debug( $data['rs_list_nat_rel'] );

			$data['rs_list_nat_rel_base'] = array_column($data['rs_list_nat_rel'], 'nat_id');
			//fct_print_debug( $data['rs_list_nat_rel_base'] );
		}


		$this->db->select(" * ")
			->from('tbl_empresas')
			->order_by( 'emp_id', 'DESC' );
		$query = $this->db->get();
		$data['rs_list_emp'] = $query->result();
		//fct_print_debug( $data['rs_list_emp'] );


		//$this->db->select(" * ")
			//->from('tbl_naturezas')
			//->order_by( 'nat_titulo', 'ASC' )
			//->order_by( 'nat_id', 'DESC' );
		
		if( isset($data['rs_bd']) )
		{
			$this->db->select(" NAT.* ")
				->from('tbl_naturezas AS NAT')
				->join('tbl_rel_empresas_x_naturezas REL', 'REL.nat_id = NAT.nat_id', 'INNER')
				->where(array(
						'REL.emp_id' => (int)$data['rs_bd']->emp_id 
					)	
				)
				->order_by( 'NAT.nat_titulo', 'ASC' )
				->order_by( 'NAT.nat_id', 'DESC' );
			$query = $this->db->get();
			$data['rs_list_nat'] = $query->result();
			//fct_print_debug( $this->db->last_query() );
			//fct_print_debug( $data['rs_list_nat'] );
		}




		$this->load->view('analistas-form', $data);
	}


	public function gravar_dados( $ana_id="" )
	{
		$var_erro = 0;
		$acao = "INSERT";

		// -------------------------------------------------
		// verifica se dados são válidos
		// -------------------------------------------------
		$baseAcao = $this->input->post('baseAcao');
		$acao = 'INSERT';
		if($baseAcao == "SEND-FORMULARIO")
		{
			$ana_id = (int)$ana_id;
			if( $ana_id > 0 )
			{
				$this->db->select(" * ")
					->from('tbl_analistas')
					->where(array(
							'ana_id' => $ana_id
						)	
					)
					->limit(1);

				if( $this->authSession["_level"] == "empresa" )
				{
					$this->db->where( 'emp_id', (int)$this->authSession["_id"] );
				}

				$query = $this->db->get();
				//$rows = (int)$query->num_rows();
				$acao = ( (int)$query->num_rows() >=1 ) ? "UPDATE" : $acao;
			}

			$emp_id = (int)$this->input->post('emp_id');
			if( $this->authSession["_level"] == "empresa" )
			{
				$emp_id = (int)$this->authSession["_id"];
			}
			
			$ana_hashkey		= md5(date("Y-m-d H:i:s"));
			$ana_nome				= $this->input->post('ana_nome');
			$ana_cpf				= $this->input->post('ana_cpf');
			$ana_telefone		= $this->input->post('ana_telefone');
			$ana_email			= $this->input->post('ana_email');
			$ana_senha			= $this->input->post('ana_senha');
			$ana_ativo			= (int)$this->input->post('ana_ativo');
			$ana_sendmail		= (int)$this->input->post('ana_sendmail');

			$var_erro = empty($ana_nome) ? 1 : $var_erro;
			$var_erro = empty($ana_cpf) ? 1 : $var_erro;
			$var_erro = empty($ana_email) ? 1 : $var_erro;
			if( $acao=="INSERT" ){
				$var_erro = empty($ana_senha) ? 1 : $var_erro;
			}			

			if( $var_erro == 0 )
			{
				$data_bd = array(
					'emp_id'							=> (int)$emp_id,
					'ana_hashkey'					=> $ana_hashkey,
					'ana_nome'						=> $ana_nome,
					'ana_cpf'							=> $ana_cpf,
					'ana_telefone'				=> $ana_telefone,
					'ana_email'						=> $ana_email,
					'ana_senha'						=> fct_encripta($ana_senha),
					//'post_urlpage'			=> url_title( convert_accented_characters($this->input->post('post_titulo')), '-', TRUE ),
					'ana_dte_cadastro'	=> date("Y-m-d H:i:s"),
					'ana_dte_alteracao'	=> date("Y-m-d H:i:s"),
					'ana_ativo'					=> $ana_ativo,
				);

				if($acao=="UPDATE"){
					if( empty($ana_senha) ){
						unset($data_bd["ana_senha"]);
					}
					unset($data_bd["ana_dte_cadastro"]);
					$where_bd = array('ana_id' => $ana_id);

					$this->db->where($where_bd);
					$this->db->update('tbl_analistas', $data_bd);

				}else{
					
					$this->db->insert('tbl_analistas', $data_bd); 
					$ana_id = (int)$this->db->insert_id();

				}


				/**
				 * --------------------------------------------------------
				 * enviar email para alterar senha
				 * --------------------------------------------------------
				**/
					if( $ana_sendmail == 1 )
					{
						$moreEmailTo = array($ana_email);

						$data_mail = array(
							"usr_nome"		=> $ana_nome,
							"usr_email"		=> $ana_email,	
							"usr_link"		=> site_url('alterar-senha/'. $ana_hashkey) 	
						);
						$args_email = array(
							"data_parser"	=> $data_mail,
							"cfg_chave"		=> 'alterar-senha',
						);
						$arr_retorno = fct_gera_html_email($args_email);
						$enviar_para = $arr_retorno["enviar_para"];

						if( is_array($moreEmailTo) and count($moreEmailTo)>0 ):
							$enviar_para = array_merge($enviar_para, $moreEmailTo);
						endif;
						$enviar_para = array_map('trim',$enviar_para);
						$enviar_para = array_unique($enviar_para);

						$arrInfos = array(
							'from_nome'		=> 'Canal Ouvidoria',
							'from_email'	=> 'no-reply@webmanager.com.br',
							'replyto'			=> 'atendimento@webmanager.com.br',
							'to'					=> $moreEmailTo,
							'to_cc'				=> '',
							//'to_bcc'			=> array('listasguardiao@gmail.com'),
							'to_bcc'			=> '',
							'subject'			=> utf8_decode('[Canal Ouvidoria] : Alterar Senha'),
							'message'			=> $arr_retorno["body"] .' ENVIADO PARA: '. json_encode($enviar_para) 
						);
						fct_sendmailexec($arrInfos);
					}


				/**
				 * --------------------------------------------------------
				 * contatos relacionados
				 * --------------------------------------------------------
				**/
				$acnt_num_id		= $this->input->post('acnt_id');
				$acnt_email			= $this->input->post('acnt_email');
				$acnt_telefone	= $this->input->post('acnt_telefone');
				$acnt_ativo			= 1;

				if( is_array($acnt_email) && count($acnt_email)>0 )
				{
					foreach ($acnt_email as $key=>$item) {
						$acnt_id = (int)$acnt_num_id[$key];
						if( !empty($item) ){
							$data_acnt_bd = array(
								'ana_id'							=> (int)$ana_id,
								'acnt_email'					=> $item,
								'acnt_telefone'				=> $acnt_telefone[$key],
								'acnt_dte_cadastro'		=> date("Y-m-d H:i:s"),
								'acnt_dte_alteracao'	=> date("Y-m-d H:i:s"),
								'acnt_ativo'					=> $acnt_ativo,
							);

							$this->db->select(" * ")
								->from('tbl_analistas_contatos')
								->where(array(
										'ana_id' => (int)$ana_id,
										'acnt_id' => (int)$acnt_id
									)	
								)
								->limit(1);
							$qry_acnt = $this->db->get();
							if( (int)$qry_acnt->num_rows() >= 1 )
							{
								unset($data_acnt_bd["acnt_dte_cadastro"]);
								$where_acnt_bd = array(
									'ana_id' => (int)$ana_id,
									'acnt_id' => (int)$acnt_id
								);
								$this->db->where($where_acnt_bd);
								$this->db->update('tbl_analistas_contatos', $data_acnt_bd);
						
							}else{
								$this->db->insert('tbl_analistas_contatos', $data_acnt_bd); 
							}
						}
					}
				}

				/**
				 * --------------------------------------------------------
				 * natureza relacionadas
				 * --------------------------------------------------------
				**/
				$chk_natureza = $this->input->post('chk_natureza');
				if( is_array($chk_natureza) && count($chk_natureza)>0 )
				{
					$this->db->where('ana_id', (int)$ana_id);
					$this->db->delete('tbl_rel_analistas_x_naturezas');
					
					foreach ($chk_natureza as $key=>$item) {
						$natemp_prazo = 0;
						$data_nat_bd = array(
							'ana_id'				=> (int)$ana_id,
							'nat_id'				=> (int)$item,
							'ananat_ativo'	=> '1',
						);
						$this->db->insert('tbl_rel_analistas_x_naturezas', $data_nat_bd); 
					}
				}

				$this->session->set_flashdata('message_validate', 'As informações foram salvas com sucesso!');
				redirect( $this->var_url["url_form"] .'/'. $ana_id );
			}
			else
			{
				$message = 'Preencha corretamente todos os campos';
			}
		};
		// -------------------------------------------------
	}


	public function grid($ana_id="")
	{
		$data = array();

		//$this->db->select(" * ")
			//->from('tbl_naturezas')
			//->order_by( 'nat_titulo', 'ASC' )
			//->order_by( 'nat_id', 'DESC' );


		$empresa_ident = 0;
		if( $this->authSession["_level"] == "empresa" )
		{
			$empresa_ident = (int)$this->authSession["_id"];
		}

		// -------------------------------------------------
		// verifica se dados são válidos
		// -------------------------------------------------
		$baseAcao = $this->input->post('baseAcao');
		if($baseAcao == "FILTRO-PESQUISA")
		{
			$empresa_ident = (int)$this->input->post('empresa_ident');
			$this->load->vars( array(
				'empresa_ident' => $empresa_ident
			));
		}

		



		if( $this->authSession["_level"] == "administrador" )
		{
			$this->db->select(" * ")
				->from('tbl_empresas')
				->order_by( 'emp_id', 'DESC' );
			$query = $this->db->get();
			$data['rs_list_emp'] = $query->result();
			//fct_print_debug( $data['rs_list_emp'] );
				
		}


		if( $empresa_ident >0 )
		{
			/**
			 * --------------------------------------------------------
			 * analistas
			 * --------------------------------------------------------
			**/
			$this->db->select(" * ")
				->from('tbl_analistas')
				->order_by( 'ana_id', 'DESC' );

				if( $empresa_ident >0 )
				{
					$this->db->where( 'emp_id', $empresa_ident );
				}

			$query = $this->db->get();
			//$data['rs_list_ana'] = $query->result();
			$data['rs_list_ana'] = $query->result_array();
			//fct_print_debug( $data['rs_list_nat_rel'] );

			$data['rs_list_ana_base'] = array_column($data['rs_list_ana'], 'ana_id');
			//fct_print_debug( $data['rs_list_ana_base'] );


			/**
			 * --------------------------------------------------------
			 * relacao analistas x naturezas
			 * --------------------------------------------------------
			**/
					$this->db->select(" NAT.* ")
						->select(" ANA.* ")
						->select(" RELANA.ananat_ativo, RELANA.ananat_dte_alteracao ")
						->from('tbl_naturezas NAT')
						->join('tbl_rel_empresas_x_naturezas RELNAT', 'RELNAT.nat_id = NAT.nat_id', 'INNER')
						->join('tbl_rel_analistas_x_naturezas RELANA', 'RELANA.nat_id = RELNAT.nat_id', 'LEFT')
						->join('tbl_analistas ANA', 'ANA.ana_id = RELANA.ana_id', 'INNER')
						->order_by( 'NAT.nat_titulo', 'ASC' );

					$this->db->where( 'RELNAT.emp_id', $empresa_ident );

					$query = $this->db->get();
					//$this->db->reset_query();
					$rs_list = $query->result();
					//fct_print_debug( $rs_list );

					$arr_nat_rel = array();
					foreach ($rs_list AS $key=>$item)
					{
						$arr_respons = array();
						$arra_analista = array(
							"ana_id"				=> $item->ana_id,
							"ana_nome"			=> $item->ana_nome,
							"ananat_ativo"	=> $item->ananat_ativo,
							"ananat_ativo"	=> $item->ananat_ativo,
							"ananat_data"		=> $item->ananat_dte_alteracao,
						);
						if ( !array_key_exists( (int)$item->nat_id , $arr_nat_rel) ) { 
							$arr_respons[] = $arra_analista;
							$arr_nat_rel[$item->nat_id] = array( 
								"nat_titulo"		=> $item->nat_titulo,
								"nat_id"				=> $item->nat_id,
								"responsaveis"	=> $arr_respons
							);
							//fct_print_debug( 'respons' );
							//fct_print_debug( $arr_respons );
							//fct_print_debug( 'respons relat' );
							//fct_print_debug( $arr_nat_rel );
						}else{
							$arr_old = $arr_nat_rel[$item->nat_id]["responsaveis"];
							//fct_print_debug( 'old' );
							//fct_print_debug( $arr_old );

							$arr_respons = $arra_analista;
							//array_push(array('responsaveis'=>$arr_old), $arr_respons);
							array_push($arr_old, $arr_respons);

							$arr_nat_rel[$item->nat_id] = array( 
								"nat_titulo"		=> $item->nat_titulo,
								"nat_id"				=> $item->nat_id,
								"responsaveis"	=> $arr_old
							);
						}
						//$data['rs_list_nat_t1'][$item->nat_titulo] = array( 
							//"ana_id" => $item->ana_id
						//);
					}
					$data['rs_list_nat_rel'] = $arr_nat_rel;
					//fct_print_debug( $data['rs_list_nat_rel'] );
			
			/*
			$this->db->select(" NAT.nat_id, NAT.nat_titulo ")
				->select(" ANA.ana_id, ANA.ananat_ativo, ANA.ananat_dte_alteracao")
				->select(" REL.emp_id")
				->from('tbl_naturezas AS NAT')
				->join('tbl_rel_empresas_x_naturezas AS REL', 'REL.nat_id = NAT.nat_id', 'INNER')
				->join('tbl_rel_analistas_x_naturezas AS ANA', 'ANA.nat_id = NAT.nat_id', 'LEFT')
				->order_by( 'NAT.nat_titulo', 'ASC' )
				->order_by( 'NAT.nat_id', 'DESC' );

				$this->db->where( 'emp_id', $empresa_ident );


			$query = $this->db->get();
			//fct_print_debug( $this->db->last_query() );
			$data['rs_list_nat'] = $query->result();
			//$data['rs_list_nat'] = $query->result();
			//fct_print_debug( $data['rs_list_nat'] );

			$arr_nat_rel = array(); 
			foreach ($data['rs_list_nat'] AS $key=>$item)
			{

				$ana_item = $item->ana_id .'|'. $item->ananat_ativo; 
				if ( !array_key_exists( (int)$item->nat_id , $arr_nat_rel) ) { 
					$arr_nat_rel[$item->nat_id] = array( 
						"nat_titulo"	=> $item->nat_titulo,
						"nat_id"			=> $item->nat_id,
						"ana_id"			=> array($item->ana_id => $ana_item),
					);
					//fct_print_debug( array($item->ana_id => $ana_item) );
				}else{
					$arr_old = $arr_nat_rel[$item->nat_id]["ana_id"];
					//$cesta = array("laranja", "morango");
					//array_push($arr_old, array($item->ana_id => $ana_item));
					$arr_old = array_merge($arr_old, array($item->ana_id => $ana_item));

					$arr_nat_rel[$item->nat_id] = array( 
						"nat_titulo"	=> $item->nat_titulo,
						"nat_id"			=> $item->nat_id,
						"ana_id"			=> $arr_old
					);
				}
				//$data['rs_list_nat_t1'][$item->nat_titulo] = array( 
					//"ana_id" => $item->ana_id
				//);
			}
			$data['rs_list_nat_rel'] = $arr_nat_rel;
			//fct_print_debug( $data['rs_list_nat_rel'] );

			*/
		}


		//$data['rs_list_nat_t2'] = array(
			//"furto"					=> array('x', '-', 'x'),
			//"roubo"					=> array('x', 'x', 'x'),
			//"corrupcao"			=> array('-', 'x', '-'),
			//"assedio"				=> array('-', 'x', '-'),
		//);
		//fct_print_debug( $data['rs_list_nat_t2'] );
		




		//fct_print_debug( 'Session' );
		//fct_print_debug( $this->authSession );
		//fct_print_debug( $this->authSession["_name"] .' | '. $this->authSession["_level"] );
		
		$this->load->view('analistas-grid', $data);	
	}


	public function json($route="")
	{
		switch ($route){
			case "relacionar-natureza":

				//if( $this->authSession["_level"] == "empresa" )
				//{
					$ana_id = (int)$this->input->post('ana_id');
					$nat_id = (int)$this->input->post('nat_id');
					$checked = (int)$this->input->post('checked');
				
					if( $ana_id > 0 && $nat_id > 0 )
					{
						$where = array(
							'ana_id' => (int)$ana_id,
							'nat_id' => (int)$nat_id
						);

						$this->db->select(" * ")
							->from('tbl_rel_analistas_x_naturezas')
							->where($where)
							->limit(1);
						$query = $this->db->get();
						if( (int)$query->num_rows() >= 1 )
						{
							$data_nat_bd = array(
								'ananat_ativo'					=> $checked,
								'ananat_dte_alteracao'	=> date("Y-m-d H:i:s"),
							);
							$this->db->where($where);
							$this->db->update('tbl_rel_analistas_x_naturezas', $data_nat_bd);
						}else{
							$data_nat_bd = array(
								'ana_id'								=> (int)$ana_id,
								'nat_id'								=> (int)$nat_id,
								'ananat_ativo'					=> $checked,
								'ananat_dte_alteracao'	=> date("Y-m-d H:i:s"),
							);
							$this->db->insert('tbl_rel_analistas_x_naturezas', $data_nat_bd); 
						}
				
				}

				$obj['num_rows']	= (int)$query->num_rows();
				$obj['dados']			= $ana_id .' | '. $nat_id .' | '. $checked;
				$obj['error']			= 'error';
				$obj['message']		= 'message';
				$obj['active']		= 'active';
				echo json_encode($obj);

			break;
		}		
	}

}
