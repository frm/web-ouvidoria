<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	function __construct()
	{
		parent::__construct();

		/**
		 * AUTH
		**/
		$this->authlib->logged_in();
	}

	public function index()
	{
		$data = array();
		//$array_session = $this->session->userdata( $this->var_session );
		//fct_print_debug( $array_session );

		$this->load->view('dashboard', $data);
	}


	public function sendmail()
	{
		$data = array();

		$strFromName	= (isset($arrInfos["from_nome"]) ? $arrInfos["from_nome"] : "");
		$strFromEmail	= (isset($arrInfos["from_email"]) ? $arrInfos["from_email"] : "");
		$strTo				= (isset($arrInfos["to"]) ? $arrInfos["to"] : "");
		$strToCC			= (isset($arrInfos["to_cc"]) ? $arrInfos["to_cc"] : "");
		$strToBCC			= (isset($arrInfos["to_bcc"]) ? $arrInfos["to_bcc"] : "");
		$strSubject		= (isset($arrInfos["subject"]) ? $arrInfos["subject"] : "");
		$strMessage		= (isset($arrInfos["message"]) ? $arrInfos["message"] : "");
		$strAttach		= (isset($arrInfos["attach"]) ? $arrInfos["attach"] : "");

		$arrInfos = array(
			"from_nome"		=> "CanalOuvidoria",
			"from_email"	=> "no-reply@webmanager.com.br",
			"to"					=> array("listasguardiao@gmail.com"),
			"to_cc"				=> "",
			"to_bcc"			=> "",
			"subject"			=> "TESTE CanalOuvidoria",
			"message"			=> "TESTE",
			"attach"			=> "",
		);
		fct_sendmail($arrInfos, true, true);

	}

}
