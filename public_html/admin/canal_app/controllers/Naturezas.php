<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Naturezas extends MY_Controller {

	
	private $var_url;


	function __construct()
	{
		parent::__construct();

		/**
		 * AUTH
		**/
		$this->authlib->logged_in();


		$this->var_url = array(
			"url_form" => site_url('naturezas/form'),
			"url_list" => site_url('naturezas')
		);
		$this->load->vars(array('url' => $this->var_url));
	}


	public function index()
	{
		self::listar();
	}


	public function listar($action="")
	{
		$data = array();

		/**
		 * --------------------------------------------------------
		 * excluir registros
		 * --------------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		if($baseAcao == "SEND-FORMULARIO")
		{
			$chk_delete = $this->input->post('chk_delete');
			if( is_array($chk_delete) && count($chk_delete)>0 )
			{
				foreach ($chk_delete as $key=>$item)
				{
					//$this->db->where('emp_id', $item);
					//$this->db->delete('tbl_rel_empresas_x_naturezas');

					//$this->db->where('emp_id', $item);
					//$this->db->delete('tbl_emp_contratos');

					$this->db->where('nat_id', $item );
					$this->db->delete('tbl_naturezas');
				}
			}
		}


		$this->db->select(" * ")
			->from('tbl_naturezas')
			->order_by( 'nat_id', 'DESC' );
		$query = $this->db->get();
		$data['rs_list'] = $query->result();
		//fct_print_debug( $data['rs_list'] );

		$this->load->view('naturezas', $data);
	}


	public function form( $nat_id="" )
	{
		$data = array();


		self::gravar_dados( $nat_id );


		if( $nat_id > 0 )
		{
			$this->db->select(" * ")
				->from('tbl_naturezas')
				->where(array(
						'nat_id' => $nat_id
					)	
				)
				->limit(1);
			$query = $this->db->get();
			$data['rs_bd'] = $query->row();
			//fct_print_debug( $data['rs_bd'] );
		}


		$this->load->view('naturezas-form', $data);
	}


	public function gravar_dados($nat_id="")
	{
		$var_erro = 0;
		$acao = "INSERT";

		// -------------------------------------------------
		// verifica se dados são válidos
		// -------------------------------------------------
		$baseAcao = $this->input->post('baseAcao');
		$acao = 'INSERT';
		if($baseAcao == "SEND-FORMULARIO")
		{
			$nat_id = (int)$nat_id;
			if( $nat_id > 0 )
			{
				$this->db->select(" * ")
					->from('tbl_naturezas')
					->where(array(
							'nat_id' => $nat_id
						)	
					)
					->limit(1);
				$query = $this->db->get();
				//$rows = (int)$query->num_rows();
				$acao = ( (int)$query->num_rows() >=1 ) ? "UPDATE" : $acao;
			}


			/**
			 * --------------------------------------------------------
			 * upload do arquivo
			 * --------------------------------------------------------
			**/
			$config_upl['upload_path']		= $this->config->item('folder_images') .'/naturezas/';
			$config_upl['allowed_types']	= 'gif|jpg|jpeg|png';
			$config_upl['max_size']				= '102048';
			$config_upl['max_width']			= '5000';
			$config_upl['max_height']			= '5000';



			$nat_arquivo = ($this->input->post('strArqFileUpl') ? $this->input->post('strArqFileUpl') : '');

			$pathFile = $config_upl['upload_path'] . $nat_arquivo;
			if( is_file($pathFile) And file_exists($pathFile) ) 
			{
				if($this->input->post('delArqFileUpl') == "1")
				{
					fct_delete_arquivo($pathFile);
					$nat_arquivo = '';
				}
			}
			else
			{
				$nat_arquivo = '';
			}


			if( isset($_FILES["uplArqFileUpl"]["name"]) )
			{
				$images = fct_upload_files_unico("uplArqFileUpl", $config_upl);
				if(is_array($images))
				{
					$pathFile = $config_upl['upload_path'] . $nat_arquivo;			
					if( is_file($pathFile) And file_exists($pathFile) ) : unlink($pathFile); endif;
					$nat_arquivo = $images["orig_name"]; 
				} // is_array images
			};



			$nat_titulo			= $this->input->post('nat_titulo');
			$nat_prazo			= $this->input->post('nat_prazo');
			$nat_descricao	= $this->input->post('nat_descricao');
			$nat_padrao			= (int)$this->input->post('nat_padrao');
			$nat_ativo			= (int)$this->input->post('nat_ativo');

			$var_erro = empty($nat_titulo) ? 1 : $var_erro;
			$var_erro = empty($nat_prazo) ? 1 : $var_erro;

			if( $var_erro == 0 )
			{
				$data_bd = array(
					'nat_titulo'				=> $nat_titulo,
					'nat_prazo'					=> $nat_prazo,
					'nat_descricao'			=> $nat_descricao,
					'nat_arquivo'				=> $nat_arquivo,
					'nat_padrao'				=> $nat_padrao,
					//'post_urlpage'			=> url_title( convert_accented_characters($this->input->post('post_titulo')), '-', TRUE ),
					'nat_dte_cadastro'	=> date("Y-m-d H:i:s"),
					'nat_dte_alteracao'	=> date("Y-m-d H:i:s"),
					'nat_ativo'					=> $nat_ativo,
				);
		
				if($acao=="UPDATE"){
					unset($data_bd["ana_dte_cadastro"]);
					$where_bd = array('nat_id' => $nat_id);

					$this->db->where($where_bd);
					$this->db->update('tbl_naturezas', $data_bd);
					//return $this->db->affected_rows();
		
				}else{
					
					$this->db->insert('tbl_naturezas', $data_bd); 
					$nat_id = (int)$this->db->insert_id();
				
				}

				$this->session->set_flashdata('message_validate', 'As informações foram salvas com sucesso!');
				redirect( $this->var_url["url_form"] .'/'. $nat_id );
			}
			else
			{
				$message = 'Preencha corretamente todos os campos';
			}
		};
		// -------------------------------------------------
	}


	public function json($action="")
	{
		$data = array();

		$error_num = "1";
		$error_msg = "Ocorreu um erro!";

		//$data['segmento1'] = $this->uri->segment(1);
		//$data['segmento2'] = $this->uri->segment(2);
		//$data['segmento3'] = $this->uri->segment(3);
		//$data['menu_selected'] = "";

		switch ($action) {			
		case "EXCLUIR-NATUREZA" :
			
			$nat_id = (int)$this->input->post('post_id');

			if( $nat_id > 0)
			{
				$this->db->select(" * ")
					->from('tbl_naturezas')
					->where(array(
							'nat_id' => $nat_id
						)	
					)
					->limit(1);
				$query = $this->db->get();
				$rs_bd = $query->row();
				//fct_print_debug( $data['rs_bd'] );
				if( $query->num_rows() == 1 )
				{
					$pathFile = $this->config->item('folder_images') .'/naturezas/'. $rs_bd->nat_arquivo;
					fct_delete_arquivo($pathFile);

					$this->db->where('nat_id', $nat_id );
					$this->db->delete('tbl_naturezas');
				
					$error_num = "0";
					$error_msg = "Registro excluído com sucesso!";
				}
			}
			else
			{
				$error_num = "0";
				$error_msg = "Código inválido!";		
			}

			$json["report"]["id"] = $nat_id;
			$json["report"]["error"] = $error_num;
			$json["report"]["mensagem"] = $error_msg;
			exit( json_encode($json) );
		
		break;
		}

	}

}
