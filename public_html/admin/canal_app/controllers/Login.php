<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		// RENAME TABLE `tbl__denunciantes` TO `tbl_autores`;
		// RENAME TABLE `tbl_denunciantes` TO `tbl_autores`;
	}

	public function index()
	{
		$arr_session_data = array();
		//$this->config->load('cfg_variaveis');
		//$this->cfg = $this->config->item('cfg_constantes');
		//$this->var_session = $this->cfg["SESSION_CLIE"];
		//$this->session->unset_userdata( $this->var_session );

		$message = '';
		//fct_print_debug( $this->var_session );
		
		/**
		 * verifica se dados são válidos
		**/
		$baseAcao = $this->input->post('baseAcao');
		if($baseAcao == "SEND-FORMULARIO")
		{
			$stremail = ($this->input->post('str_email'));
			$strsenha = ($this->input->post('str_senha'));

			if( !empty($stremail) And !empty($strsenha) )
			{
				/**
				 * query referente a empresa
				**/
				$this->db->select(" 
					emp_id As idcode,
					emp_razao_social As name,
					emp_email As email,
					emp_senha As password,
					'empresa' As level
					")
					->from('tbl_empresas')
					->where(array( 
						'emp_email' => $stremail,
						'emp_senha' => fct_encripta($strsenha),
					));
				//$query = $this->db->get();
				$query_emp = $this->db->get_compiled_select();
				//fct_print_debug( $query_emp );
				$this->db->reset_query();

				/**
				 * query referente a analistas
				**/
				$this->db->select(" 
						ana_id As idcode,
						ana_nome As name, 
						ana_email As email,
						ana_senha As password,
						'analista' As level
					")
					->from('tbl_analistas')
					->where(array( 
						'ana_email' => $stremail,
						'ana_senha' => fct_encripta($strsenha),
					));
				//$query = $this->db->get();
				$query_ana = $this->db->get_compiled_select();
				//fct_print_debug( $query_ana );
				$this->db->reset_query();


				/**
				 * query referente a administrador
				**/
				$this->db->select(" 
						usr_id As idcode,
						usr_nome As name, 
						usr_email As email,
						usr_senha As password,
						'administrador' As level 
					")
					->from('tbl_usuarios')
					->where(array( 
						'usr_email' => $stremail,
						'usr_senha' => fct_encripta($strsenha),
					));
				//$query = $this->db->get();
				$query_adm = $this->db->get_compiled_select();
				//fct_print_debug( $query_adm );
				$this->db->reset_query();

				/**
				 * query union
				**/
				$query_union = $query_emp ." 
					UNION ALL (". $query_ana .")
					UNION ALL (". $query_adm .")
				ORDER BY `idcode` desc
				LIMIT 1";
				$query = $this->db->limit(1)->query($query_union);
				//$query_last = $this->db->last_query();
				//fct_print_debug( $query_last );

				if( $query )
				{
					//$rows = $query->num_rows();
					if( (int)$query->num_rows() > 0 )
					{
						$rs_login = $query->row();				
						//$rsLogin = $query->row_array();
						//$rsLogin = $query->result();

						$arr_session_data = array(
							"logado"		=> 'logado',
							"_id"				=> (int)$rs_login->idcode,
							"_name"			=> $rs_login->name,
							"_level"		=> $rs_login->level,
						);
						$this->session->set_userdata( $this->var_session, $arr_session_data);

						//fct_print_debug( $arr_session_data );
						//fct_print_debug( 'session' );
						//fct_print_debug( $this->session->userdata( $this->var_session ) );

						redirect( site_url('dashboard') );
						exit('exit');
					}else{
						$message = 'E-mail ou senha inválidos!';	
					}
				}else{
					$message = 'E-mail ou senha inválidos!';
				};			
			}
			else
			{
				$message = 'Preencha corretamente o e-mail e a senha!';
			}
		};
		// -------------------------------------------------

		$data['message_error'] = $message;
		$this->load->view('login', $data);
	}


	public function alterar_senha($hashkey="")
	{
		$arr_session_data = array();
		$var_erro = 0;
		$arr_error = array();
		$message = '';


		/**
		 * -------------------------------------------------
		 * query referente a empresa
		 * -------------------------------------------------
		**/
		$this->db->select(" 
			emp_id As idcode,
			emp_hashkey As hashkey,
			'empresa' As level
			")
			->from('tbl_empresas')
			->where(array( 'emp_hashkey' => $hashkey ));
		//$query = $this->db->get();
		$query_emp = $this->db->get_compiled_select();
		//fct_print_debug( $query_emp );
		$this->db->reset_query();


		/**
		 * -------------------------------------------------
		 * query referente a analistas
		 * -------------------------------------------------
		**/
		$this->db->select(" 
				ana_id As idcode,
				ana_hashkey As hashkey,
				'analista' As level
			")
			->from('tbl_analistas')
			->where(array( 'ana_hashkey' => $hashkey ));
		//$query = $this->db->get();
		$query_ana = $this->db->get_compiled_select();
		//fct_print_debug( $query_ana );
		$this->db->reset_query();


		/**
		 * -------------------------------------------------
		 * query union
		 * -------------------------------------------------
		**/
		$query_union = $query_emp ." 
			UNION ALL (". $query_ana .")
		ORDER BY `idcode` desc
		LIMIT 1";
		$query = $this->db->limit(1)->query($query_union);
		//$query_last = $this->db->last_query();
		//fct_print_debug( $this->db->last_query() );



		
		/**
		 * -------------------------------------------------
		 * verifica se dados são válidos
		 * -------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		if($baseAcao == "SEND-FORMULARIO")
		{
			$str_senha = ($this->input->post('str_senha'));
			$str_senha_confirm = ($this->input->post('str_senha_confirm'));

			$var_erro = empty($str_senha) ? $arr_error[] = '<li>Preencha corretamente o campo nova senha!</li>' : $var_erro;
			$var_erro = strlen(trim($str_senha))<6 ? $arr_error[] = '<li>A nova senha deve ter no mínimo 6 caracteres!</li>' : $var_erro;
			$var_erro = empty($str_senha_confirm) ? $arr_error[] = '<li>Preencha corretamente o campo repita a senha!</li>'  : $var_erro;
			//$var_erro = strlen(trim($str_senha_confirm))<8 ? $arr_error[] = '<li>A nova senha deve ter no mínimo 8 caracteres!</li>' : $var_erro;
			$var_erro = ($str_senha != $str_senha_confirm) ? $arr_error[] = '<li>Os dois campos de senha devem ser idênticos!</li>' : $var_erro;

			if( count($arr_error) == 0 )
			{
					if( $query )
					{
						if( (int)$query->num_rows() > 0 )
						{
							$rs_login = $query->row();
							//fct_print_debug( $rs_login );

							switch ( $rs_login->level ){
								case 'analista' :
									$data_bd = array(
										'ana_senha'					=> fct_encripta($str_senha),
										'ana_dte_alteracao'	=> date("Y-m-d H:i:s"),
									);
									$where_bd = array(
										'ana_id'				=> $rs_login->idcode,
										'ana_hashkey'		=> $hashkey
									);
									$this->db->where($where_bd);
									$this->db->update('tbl_analistas', $data_bd);

								break;
								case 'empresa' :
									$data_bd = array(
										'emp_senha'					=> fct_encripta($str_senha),
										'emp_dte_alteracao'	=> date("Y-m-d H:i:s"),
									);
									$where_bd = array(
										'emp_id'				=> $rs_login->idcode,
										'emp_hashkey'		=> $hashkey
									);
									$this->db->where($where_bd);
									$this->db->update('tbl_empresas', $data_bd);

								break;
							}

							$message = '<li>Senha alterada com sucesso!</li>';

						}else{
							$message = '<li>Não encontramos o cadastro em nosso sistema!</li>';	
						}
					}else{
						$message = '<li>Não encontramos o cadastro em nosso sistema!</li>';
					};

			}else{

				$message .= '<ul style="margin:0; margin-left:10px; padding:0;">';
				foreach ($arr_error as $key=>$val) {
					$message .= $val;
				}
				$message .= '</ul>';

			}
		};
		// -------------------------------------------------

		$data['message_error'] = $message;
		$this->load->view('alterar-senha', $data);
	}


	public function logout()
	{
		$this->config->load('cfg_variaveis');
		$this->cfg = $this->config->item('cfg_constantes');
		$this->var_session = $this->cfg["SESSION_CLIE"];
		$this->session->unset_userdata( $this->var_session );

		$this->session->sess_destroy();
		redirect( site_url('login') );
	}
}
