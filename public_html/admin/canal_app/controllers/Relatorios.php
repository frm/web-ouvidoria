<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorios extends MY_Controller {


	private $var_url;


	function __construct()
	{
		parent::__construct();


		/**
		 * AUTH
		**/
		$this->authlib->logged_in();
		$this->authSession = $this->authlib->get_session_by_value();


		$this->var_url = array(
			"url_form" => site_url('relatorios/form'),
			"url_list" => site_url('relatorios')
		);
		$this->load->vars(array('url' => $this->var_url));
	}


	public function index()
	{
		$busca = false;
		$this->session->set_userdata( "##BUSCA#REGISTROS##", '');
		self::listar($pgnum="", $busca);
	}


	public function pg($pgnum=""){
		$busca = false;
		$session_busca = $this->session->userdata( "##BUSCA#REGISTROS##" );
		if( !empty($session_busca) )
		{
			$busca = true;
		}
		self::listar($pgnum, $busca);
	}


	public function listar($pgnum="", $busca="")
	{
		$data = array();


		$config_paginacao = array(
			"pagina_atual"	=> 1,
			"page_size"			=> 100,
			"page_limit"		=> 15,
		);
		$page_atual = $config_paginacao["pagina_atual"];
		$page_size	= $config_paginacao["page_size"];
		$page_limit	= $config_paginacao["page_limit"];


		/**
		 * --------------------------------------------------------
		 * excluir registros
		 * --------------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		$acao = 'INSERT';
		if($baseAcao == "FILTRO-PESQUISA")
		{
			$data_fields = array(
				'bsc_empresa'				=> (int)$this->input->post('bsc_empresa'),
				'bsc_analista'			=> (int)$this->input->post('bsc_analista'),
				'bsc_natureza'			=> (int)$this->input->post('bsc_natureza'),
				'bsc_protocolo'			=> $this->input->post('bsc_protocolo'),
				'bsc_tipo_relat'		=> $this->input->post('bsc_tipo_relat'),

				'bsc_dte_ini_dia'		=> $this->input->post('bsc_dte_ini_dia'),
				'bsc_dte_ini_mes'		=> $this->input->post('bsc_dte_ini_mes'),
				'bsc_dte_ini_ano'		=> $this->input->post('bsc_dte_ini_ano'),

				'bsc_dte_fim_dia'		=> $this->input->post('bsc_dte_fim_dia'),
				'bsc_dte_fim_mes'		=> $this->input->post('bsc_dte_fim_mes'),
				'bsc_dte_fim_ano'		=> $this->input->post('bsc_dte_fim_ano'),
			);
			$this->session->set_userdata( "SESSION#RELATORIOS", $data_fields);
			$data = array_merge($data_fields, $data);

			$bsc_dte_ini_dia	= str_pad((int)$data_fields['bsc_dte_ini_dia'],2,"0", STR_PAD_LEFT);
			$bsc_dte_ini_mes	= str_pad((int)$data_fields['bsc_dte_ini_mes'],2,"0", STR_PAD_LEFT);
			$bsc_dte_ini_ano	= str_pad((int)$data_fields['bsc_dte_ini_ano'],4,"0", STR_PAD_LEFT);

			$bsc_dte_fim_dia	= str_pad((int)$data_fields['bsc_dte_fim_dia'],2,"0", STR_PAD_LEFT);
			$bsc_dte_fim_mes	= str_pad((int)$data_fields['bsc_dte_fim_mes'],2,"0", STR_PAD_LEFT);
			$bsc_dte_fim_ano	= str_pad((int)$data_fields['bsc_dte_fim_ano'],4,"0", STR_PAD_LEFT);

			$_dte_ini = "";		$_dte_ini_mes = "";
			if( $bsc_dte_ini_dia>0 && $bsc_dte_ini_mes>0 && $bsc_dte_ini_ano>0 ){
				$_dte_ini = $bsc_dte_ini_ano ."-". $bsc_dte_ini_mes ."-". $bsc_dte_ini_dia;
			}else{
				if( $bsc_dte_ini_mes>0 && $bsc_dte_ini_ano>0 ){ $_dte_ini_mes = $bsc_dte_ini_ano ."-". $bsc_dte_ini_mes; }
			}

			$_dte_fim = "";		$_dte_fim_mes = "";
			if( $bsc_dte_fim_dia>0 && $bsc_dte_fim_mes>0 && $bsc_dte_fim_ano>0 ){
				$_dte_fim = $bsc_dte_fim_ano ."-". $bsc_dte_fim_mes ."-". $bsc_dte_fim_dia;
			}else{
				if( $bsc_dte_fim_mes>0 && $bsc_dte_fim_ano>0 ){ $_dte_fim_mes = $bsc_dte_fim_ano ."-". $bsc_dte_fim_mes; }
			}



	

			
			switch ($data['bsc_tipo_relat']){
				case "quantitativo":
					// -------------------------------------------------------------------------
					// DENUNCIA QUANTITATIVA
					// -------------------------------------------------------------------------

					if(!empty($_dte_ini) && !empty($_dte_fim)){
						$where_date = " DATE_FORMAT(DEN.den_dte_cadastro, '%Y-%m-%d') BETWEEN '". $_dte_ini ."' AND '". $_dte_fim ."' ";
						$this->db->where($where_date);
					}
					if(!empty($_dte_ini_mes) && !empty($_dte_fim_mes)){
						$where_date = " DATE_FORMAT(DEN.den_dte_cadastro, '%Y-%m') BETWEEN '". $_dte_ini_mes ."' AND '". $_dte_fim_mes ."' ";
						$this->db->where($where_date);
					}

					if( $data['bsc_empresa'] > 0 ){
						$this->db->where('emp_id', $data['bsc_empresa']);
					}
					if( $data['bsc_analista'] > 0 ){
						//$this->db->where('ana_id', $data['bsc_analista']);
					}
					if( $data['bsc_natureza'] > 0 ){
						$this->db->where('DEN.nat_id', $data['bsc_natureza']);
					}


					$this->db->select(" NAT.nat_titulo");
					$this->db->join('tbl_naturezas NAT', 'NAT.nat_id = DEN.nat_id', 'INNER');
					
					$this->db->select(" DEN.* ")
						->from('tbl_denuncias DEN')
						->order_by( 'NAT.nat_titulo', 'ASC' )
						->order_by( 'DEN.den_id', 'DESC' );

					//$queryCache = $this->db->get_compiled_select('', FALSE);	
					//$pag_num_rows = $this->db->query($subQuery)->num_rows();
					//fct_print_debug( $queryCache );
					//$arr_query = array(
						//"select" => 
					//);

					$this->db->group_by(array("DEN.nat_id"));
					$this->db->select(" count(DEN.nat_id) As quant ");
				
					$query = $this->db->get();
					//$this->db->reset_query();
					//$this->db->get_where()
					$data['rs_list'] = $query->result();
							

				break;
				case "quantitativo-status":
					// -------------------------------------------------------------------------
					// DENUNCIA POR STATUS
					// -------------------------------------------------------------------------

					if(!empty($_dte_ini) && !empty($_dte_fim)){
						$where_date = " DATE_FORMAT(DEN.den_dte_cadastro, '%Y-%m-%d') BETWEEN '". $_dte_ini ."' AND '". $_dte_fim ."' ";
						$this->db->where($where_date);
					}
					if(!empty($_dte_ini_mes) && !empty($_dte_fim_mes)){
						$where_date = " DATE_FORMAT(DEN.den_dte_cadastro, '%Y-%m') BETWEEN '". $_dte_ini_mes ."' AND '". $_dte_fim_mes ."' ";
						$this->db->where($where_date);
					}

					if( $data['bsc_empresa'] > 0 ){
						$this->db->where('emp_id', $data['bsc_empresa']);
					}
					if( $data['bsc_analista'] > 0 ){
						//$this->db->where('ana_id', $data['bsc_analista']);
					}
					if( $data['bsc_natureza'] > 0 ){
						$this->db->where('DEN.nat_id', $data['bsc_natureza']);
					}

					$this->db->select(" NAT.nat_titulo");
					$this->db->join('tbl_naturezas NAT', 'NAT.nat_id = DEN.nat_id', 'INNER');

					$this->db->select(" DEN.* ")
						->from('tbl_denuncias DEN')
						->order_by( 'NAT.nat_titulo', 'ASC' )
						->order_by( 'DEN.den_id', 'DESC' );

					//$queryCache = $this->db->get_compiled_select('', FALSE);	
					//$pag_num_rows = $this->db->query($subQuery)->num_rows();
					//fct_print_debug( $queryCache );
					//$arr_query = array(
						//"select" => 
					//);

					$this->db->select(" count(DEN.den_status) As quant ");
					$this->db->group_by(array("DEN.nat_id", "DEN.den_status"));	
				
					$query = $this->db->get();
					//$this->db->reset_query();
					$data['rs_list'] = $query->result();

				break;
				case "analistas":
				// -------------------------------------------------------------------------
				// ANALISTAS
				// -------------------------------------------------------------------------
					/*
						SELECT NAT.*, ANA.*, RELANA.ananat_ativo, RELANA.ananat_dte_alteracao FROM tbl_naturezas NAT
							INNER JOIN tbl_rel_empresas_x_naturezas RELNAT ON RELNAT.nat_id = NAT.nat_id
							LEFT JOIN tbl_rel_analistas_x_naturezas RELANA ON RELANA.nat_id = RELNAT.nat_id
							INNER JOIN tbl_analistas ANA ON ANA.ana_id = RELANA.ana_id
						WHERE RELNAT.emp_id = 1
						ORDER BY NAT.nat_titulo ASC;
					*/

					if( $data['bsc_empresa'] > 0 ){
						$this->db->where('RELNAT.emp_id', $data['bsc_empresa']);
					}
					if( $data['bsc_analista'] > 0 ){
						$this->db->where('ANA.ana_id', $data['bsc_analista']);
					}
					if( $data['bsc_natureza'] > 0 ){
						$this->db->where('NAT.nat_id', $data['bsc_natureza']);
					}

					$this->db->select(" NAT.* ")
						->select(" ANA.* ")
						->select(" RELANA.ananat_ativo, RELANA.ananat_dte_alteracao ")
						->from('tbl_naturezas NAT')
						->join('tbl_rel_empresas_x_naturezas RELNAT', 'RELNAT.nat_id = NAT.nat_id', 'INNER')
						->join('tbl_rel_analistas_x_naturezas RELANA', 'RELANA.nat_id = RELNAT.nat_id', 'LEFT')
						->join('tbl_analistas ANA', 'ANA.ana_id = RELANA.ana_id', 'INNER')
						->order_by( 'NAT.nat_titulo', 'ASC' );

					$query = $this->db->get();
					//$this->db->reset_query();
					$rs_list = $query->result();
					//fct_print_debug( $rs_list );

					$arr_nat_rel = array();
					foreach ($rs_list AS $key=>$item)
					{
						$arr_respons = array();
						$arra_analista = array(
							"ana_id" => $item->ana_id,
							"ana_nome" => $item->ana_nome,
							"ananat_ativo" => $item->ananat_ativo,
							"ananat_ativo" => $item->ananat_ativo,
							"ananat_data" => $item->ananat_dte_alteracao,
						);
						if ( !array_key_exists( (int)$item->nat_id , $arr_nat_rel) ) { 
							$arr_respons[] = $arra_analista;
							$arr_nat_rel[$item->nat_id] = array( 
								"nat_titulo" => $item->nat_titulo,
								"nat_id" => $item->nat_id,
								"responsaveis" => $arr_respons
							);
							//fct_print_debug( 'respons' );
							//fct_print_debug( $arr_respons );
							//fct_print_debug( 'respons relat' );
							//fct_print_debug( $arr_nat_rel );
						}else{
							$arr_old = $arr_nat_rel[$item->nat_id]["responsaveis"];
							//fct_print_debug( 'old' );
							//fct_print_debug( $arr_old );

							$arr_respons = $arra_analista;
							//array_push(array('responsaveis'=>$arr_old), $arr_respons);
							array_push($arr_old, $arr_respons);

							$arr_nat_rel[$item->nat_id] = array( 
								"nat_titulo" => $item->nat_titulo,
								"nat_id" => $item->nat_id,
								"responsaveis" => $arr_old
							);
						}
						//$data['rs_list_nat_t1'][$item->nat_titulo] = array( 
							//"ana_id" => $item->ana_id
						//);
					}
					$data['rs_list'] = $arr_nat_rel;
					//fct_print_debug( $data['rs_list'] );

				break;				
			}

			//fct_print_debug( $this->db->last_query() );
			//fct_print_debug( $data['rs_list'] );
			//fct_print_debug( $data );
		}





		if( $this->authSession["_level"] == "administrador" )
		{
			$this->db->select(" * ")
				->from('tbl_empresas')
				->order_by( 'emp_razao_social', 'ASC' );
			$query = $this->db->get();
			$data['rs_list_emp'] = $query->result();
			//fct_print_debug( $data['rs_list_emp'] );
		}

		if( $this->authSession["_level"] == "administrador" )
		{
			$this->db->select(" * ")
				->from('tbl_analistas')
				->order_by( 'ana_nome', 'ASC' );
			$query = $this->db->get();
			$data['rs_list_ana'] = $query->result();
			//fct_print_debug( $data['rs_list_ana'] );
		}

		if( $this->authSession["_level"] == "administrador" )
		{
			$this->db->select(" * ")
				->from('tbl_naturezas')
				->order_by( 'nat_titulo', 'ASC' );
			$query = $this->db->get();
			$data['rs_list_nat'] = $query->result();
			//fct_print_debug( $data['rs_list_nat'] );
		}


		$this->load->view('relatorios', $data);
	}

	public function json()
	{
		$data = array();


		//fct_print_debug( $rowsTEMP );
		//fct_print_debug( $this->input->get() );
		///fct_print_debug( $this->input->get('nat_id', TRUE) );

		$data_fields = $this->session->userdata( "SESSION#RELATORIOS" );
		//$this->db->select($queryCacheTEMP, FALSE);

		$nat_id = (int)$this->input->get('nat_id', TRUE);
		if( $nat_id > 0 )
		{
			$this->db->where('DEN.nat_id', $nat_id);
		}

		//fct_print_debug( $queryCacheTEMP );
		//$rowsTEMP = $this->db->query($queryCacheTEMP);
		//fct_print_debug( $rowsTEMP );

		//$queryCacheNEW = $this->db->get_compiled_select('', FALSE);	
		//fct_print_debug( $queryCacheNEW );
		

		



		/**
		 * --------------------------------------------------------
		 * excluir registros
		 * --------------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		$acao = 'INSERT';
		//if($baseAcao == "FILTRO-PESQUISA")
		//{
			//$data_fields = array(
				//'bsc_empresa'				=> (int)$this->input->post('bsc_empresa'),
				//'bsc_analista'			=> (int)$this->input->post('bsc_analista'),
				//'bsc_natureza'			=> (int)$this->input->post('bsc_natureza'),
				//'bsc_protocolo'			=> $this->input->post('bsc_protocolo'),
				//'bsc_tipo_relat'		=> $this->input->post('bsc_tipo_relat'),

				//'bsc_dte_ini_dia'		=> $this->input->post('bsc_dte_ini_dia'),
				//'bsc_dte_ini_mes'		=> $this->input->post('bsc_dte_ini_mes'),
				//'bsc_dte_ini_ano'		=> $this->input->post('bsc_dte_ini_ano'),

				//'bsc_dte_fim_dia'		=> $this->input->post('bsc_dte_fim_dia'),
				//'bsc_dte_fim_mes'		=> $this->input->post('bsc_dte_fim_mes'),
				//'bsc_dte_fim_ano'		=> $this->input->post('bsc_dte_fim_ano'),
			//);

			//fct_print_debug( $data_fields );
			$data = array_merge($data_fields, $data);

			$bsc_dte_ini_dia	= str_pad((int)$data_fields['bsc_dte_ini_dia'],2,"0", STR_PAD_LEFT);
			$bsc_dte_ini_mes	= str_pad((int)$data_fields['bsc_dte_ini_mes'],2,"0", STR_PAD_LEFT);
			$bsc_dte_ini_ano	= str_pad((int)$data_fields['bsc_dte_ini_ano'],4,"0", STR_PAD_LEFT);

			$bsc_dte_fim_dia	= str_pad((int)$data_fields['bsc_dte_fim_dia'],2,"0", STR_PAD_LEFT);
			$bsc_dte_fim_mes	= str_pad((int)$data_fields['bsc_dte_fim_mes'],2,"0", STR_PAD_LEFT);
			$bsc_dte_fim_ano	= str_pad((int)$data_fields['bsc_dte_fim_ano'],4,"0", STR_PAD_LEFT);

			$_dte_ini = "";		$_dte_ini_mes = "";
			if( $bsc_dte_ini_dia>0 && $bsc_dte_ini_mes>0 && $bsc_dte_ini_ano>0 ){
				$_dte_ini = $bsc_dte_ini_ano ."-". $bsc_dte_ini_mes ."-". $bsc_dte_ini_dia;
			}else{
				if( $bsc_dte_ini_mes>0 && $bsc_dte_ini_ano>0 ){ $_dte_ini_mes = $bsc_dte_ini_ano ."-". $bsc_dte_ini_mes; }
			}

			$_dte_fim = "";		$_dte_fim_mes = "";
			if( $bsc_dte_fim_dia>0 && $bsc_dte_fim_mes>0 && $bsc_dte_fim_ano>0 ){
				$_dte_fim = $bsc_dte_fim_ano ."-". $bsc_dte_fim_mes ."-". $bsc_dte_fim_dia;
			}else{
				if( $bsc_dte_fim_mes>0 && $bsc_dte_fim_ano>0 ){ $_dte_fim_mes = $bsc_dte_fim_ano ."-". $bsc_dte_fim_mes; }
			}

			if(!empty($_dte_ini) && !empty($_dte_fim)){
				$where_date = " DATE_FORMAT(DEN.den_dte_cadastro, '%Y-%m-%d') BETWEEN '". $_dte_ini ."' AND '". $_dte_fim ."' ";
				$this->db->where($where_date);
			}
			if(!empty($_dte_ini_mes) && !empty($_dte_fim_mes)){
				$where_date = " DATE_FORMAT(DEN.den_dte_cadastro, '%Y-%m') BETWEEN '". $_dte_ini_mes ."' AND '". $_dte_fim_mes ."' ";
				$this->db->where($where_date);
			}

			if( $data['bsc_empresa'] > 0 ){
				$this->db->where('emp_id', $data['bsc_empresa']);
			}
			if( $data['bsc_analista'] > 0 ){
				//$this->db->where('ana_id', $data['bsc_analista']);
			}
			if( $data['bsc_natureza'] > 0 ){
				$this->db->where('DEN.nat_id', $data['bsc_natureza']);
			}




			$this->db->select("NAT.nat_id,  NAT.nat_titulo");
			$this->db->join('tbl_naturezas NAT', 'NAT.nat_id = DEN.nat_id', 'INNER');
			

			$data['bsc_tipo_relat'] = "quantitativo";
			if( $data['bsc_tipo_relat'] == "quantitativo" ){
				//$this->db->group_by(array("DEN.nat_id"));
				//$this->db->select(" count(DEN.nat_id) As quant ");
			}

			if( $data['bsc_tipo_relat'] == "quantitativo-status" ){
				$this->db->select(" count(DEN.den_status) As quant ");
				$this->db->group_by(array("DEN.nat_id", "DEN.den_status"));	
			}

			$this->db->select(" DEN.* ")				
				->from('tbl_denuncias DEN')
				->order_by( 'NAT.nat_titulo', 'ASC' )
				->order_by( 'DEN.den_id', 'DESC' );
			$query = $this->db->get();
			$data['rs_list'] = $query->result();
			//fct_print_debug( $this->db->last_query() );
			//fct_print_debug( $data['rs_list'] );
			//fct_print_debug( $data );



		//$nat_id = (int)$this->input->get('nat_id', TRUE);
		if( $nat_id > 0 )
		{
			$this->db->select(" * ")
				->from('tbl_naturezas')
				->where('nat_id', $nat_id)
				->order_by( 'nat_titulo', 'ASC' )
				->limit(1);
			$query = $this->db->get();
			$data['rs_nat'] = $query->row();
			//fct_print_debug( $this->db->last_query() );
			//fct_print_debug( $data['rs_nat'] );
		}


		$this->load->view('relatorios-modal', $data);
	}

	public function denuncia()
	{
		$data = array();
		$data_fields = array();


		//fct_print_debug( $rowsTEMP );
		//fct_print_debug( $this->input->get() );
		///fct_print_debug( $this->input->get('nat_id', TRUE) );

		$data_fields = $this->session->userdata( "SESSION#RELATORIOS" );
		//fct_print_debug( $data_fields );

		$den_id = (int)$this->input->get('den_id', TRUE);
		if( $den_id > 0 )
		{
			$this->db->where('DEN.den_id', $den_id);
		}
	



		/**
		 * --------------------------------------------------------
		 * detalhes da denuncia
		 * --------------------------------------------------------
		**/
			$this->db->select(" DEN.* ")				
				->from('tbl_denuncias DEN')
				->order_by( 'DEN.den_id', 'DESC' )
				->limit(1);
			$query = $this->db->get();
			//$data['rs_den'] = $query->result();
			$data['rs_den'] = $query->row();
			//fct_print_debug( $this->db->last_query() );
			//fct_print_debug( $data['rs_list'] );
			//fct_print_debug( $data );

			//fct_print_debug( $data['rs_den'] );


		/**
		 * --------------------------------------------------------
		 * histórico geral da denuncia
		 * --------------------------------------------------------
		**/
			if( $query->num_rows() >= 1 )
			{
				$this->db->select(" DRESP.* ")				
					->from('tbl_denunc_respostas DRESP')
					->where('DRESP.den_id', $data['rs_den']->den_id)
					->order_by( 'DRESP.dresp_id', 'DESC' );
				$query = $this->db->get();
				$data['rs_resp'] = $query->result();
				//$data['rs_resp'] = $query->row();
				//fct_print_debug( $this->db->last_query() );


				//fct_print_debug( $data['rs_resp'] );
			}






		$this->load->view('relatorios/denuncia-detalhe', $data);
	}


}
