<?php
class MY_Controller extends CI_Controller {

	function  __construct() {
		parent::__construct();

		/*
		 * MODELS
		**/
		//$this->load->model('frontend_md', 'frontMD', true);


		/*
		 * LIBRARIES
		**/
		$library = array();
		//$this->load->library( $library );


		/*
		 * HELPERS
		**/
		$helper = array();
		//$this->load->helper( $helper );


		/*
		 * CONFIG
		**/
		$this->config->load('cfg_variaveis');
		$this->cfg = $this->config->item('cfg_constantes');
		$this->var_session = $this->cfg["SESSION_CLIE"];
		$this->cfg_den_status = $this->config->item('cfg_den_status');
		$this->cfg_status_definidos = $this->config->item('cfg_status_definidos');


		//fct_print_debug( $this->cfg_den_status );
		//fct_print_debug( $this->cfg_status_definidos );
		//exit();


		//$this->form_validation->set_error_delimiters('<li class="lbl_error">', '</li>');
		
		$this->authlib->get_session();
	}

   
}