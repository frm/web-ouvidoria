<?php
class MY_Controller extends CI_Controller {

	function  __construct() {
		parent::__construct();

		/*
		 * MODELS
		**/
		//$this->load->model('frontend_md', 'frontMD', true);


		/*
		 * LIBRARIES
		**/
		$library = array();
		//$this->load->library( $library );


		/*
		 * HELPERS
		**/
		$helper = array();
		//$this->load->helper( $helper );


		/*
		 * CONFIG
		**/
		$this->config->load('cfg_variaveis');
		$this->cfg = $this->config->item('cfg_constantes');
		//$session = $ci->session->userdata( $ci->cfg["SESSION"] );
		$this->var_session = $this->cfg["SESSION_CLIE"];


		//$this->form_validation->set_error_delimiters('<li class="lbl_error">', '</li>');
		
		$this->authlib->get_session();

		/*
		 * CONFIG
		**/
		//$this->seo_title		= 'MerzBrasil';
		//$this->seo_description	= utf8_encode('');
		//$this->seo_keywords		= utf8_encode('');
		//$this->seo_image		= '';	//base_url("assets/default/images/logo-seo-guguta.jpg");

	}

   
}