jQuery(document).ready(function($){	
	
	fnPlaceHolder($);

	// Jquery : Mask : Igor Escobar - config
	// -----------------------------------------------------------
	$('.mask-ishour').mask('00:00', {placeholder: "00:00", clearIfNotMatch: true});
	$('.mask-isdate').mask('00/00/0000', {placeholder: "dd/mm/yyyy", clearIfNotMatch: true});
	$('.mask-iscpf').mask('000.000.000-00', {placeholder: "000.000.000-00", reverse: true, clearIfNotMatch: true});
	$('.mask-iscnpj').mask('00.000.000/0000-00', {placeholder: "00.000.000/0000-00", reverse: true, clearIfNotMatch: true});
	$('.mask-iscep').mask('00000-000', {placeholder: "00000-000", clearIfNotMatch: true});
	$('.mask-isphone-no-ddd').mask('0000-0000', {placeholder: "0000-0000", clearIfNotMatch: true});
	$('.mask-ismoney').mask('000.000.000.000.000,00', {placeholder: "00,00", reverse: true});
	$('.mask-ispercent').mask('00,00', {placeholder: "00,00", reverse: true});
	//$($box +'.ismoney').each(function(i)		{ $("#"+ $(this).attr('id')).mask("000.000.000.000.000,00", {reverse: true}); });

	var SPMaskBehavior = function (val) {
	  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
	  onKeyPress: function(val, e, field, options) {
		  field.mask(SPMaskBehavior.apply({}, arguments), options);
		}
	};
	$('.mask-isphone-ddd').mask(SPMaskBehavior, {placeholder: "(00) 00000-0000", clearIfNotMatch: true});
	$('.mask-isphone').mask(SPMaskBehavior, {placeholder: "(00) 00000-0000", clearIfNotMatch: true});
	// -----------------------------------------------------------


	$(".click-dropdown-menu").on('click', function(event) {
		 event.stopPropagation();
		$(this).closest('.checkgroup').find('[data-toggle=dropdown]').dropdown('toggle');
	});
	$("ul.dropdown-menu input[type=checkbox]").on('ifUnchecked', function(event) {
		var line = "";
		$("ul.dropdown-menu input[type=checkbox]").each(function() {
			if($(this).is(":checked")) { line += $(this).closest('a').find('span').html() + "; "; }
		});
		$(this).closest('.checkgroup').find("input.lblselected").val(line);
	});
	$("ul.dropdown-menu input[type=checkbox]").on('ifChecked', function(event) {
		var line = "";
		//console.log( "val: "+ $(this).closest('a').find('span').html() );
		$("ul.dropdown-menu input[type=checkbox]").each(function() {
			if($(this).is(":checked")) { line += $(this).closest('a').find('span').html() + "; "; }
		});
		$(this).closest('.checkgroup').find("input.lblselected").val(line);
	});
	$(".dropdown-menu").find('a').on('click', function(event) {
		event.stopPropagation();
		//console.log( "val: "+ $(this).html() );
		var $checked = $(this).find("input[type='checkbox']").is(":checked");
		if($checked){
			$(this).find("input[type='checkbox']").iCheck("uncheck");
		}else{
			$(this).find("input[type='checkbox']").iCheck("check");		
		}
	});


	$(document).on('click', '.clsSearchPages', function(event){
		var $frmID = $(this).data('form');
		var $form = $($frmID);
		var $url = $(this).data('url');	//$(this).attr('rel');
		console.log( $url );
		$form.attr("action", $url);
		$form.submit();
		return false;
	});

	$(document).on('click', '.cmdBtnBuscaFiltro', function(event){
		var $frmID = $(this).data('form');
		var $form = $($frmID);
		var $url = $(this).data('url');	//$(this).attr('rel');
		$form.attr("action", $url);
		$form.submit();
		return false;
	});


	// click new register
	$('.mrBtnAddNovo').on("click", function(event){
		document.location.href = $(this).attr('rel');
		//var $form = $("form#formBusca");
		//var $urlAtual = $(this).attr('rel');
		//$form.attr("action", $urlAtual);
		//$form.submit();
		return false;
	});

	// click list all registers
	$('.mrBtnListReg').on("click", function(event){
		document.location.href = $(this).attr('rel');
		return false;
	});

	// click delete registers selected
	$('.mrBtnDelReg').on("click", function(event){
		var itens   = new String('');
		//$("input[@name=chkItens]").each(function(){
		$(".itemdelete").each(function(){
			if(this.checked){
				if( itens.length > 0) itens += ',';
				itens += $(this).val();
			}
		});
		if(itens.length < 1) {
			alert('Selecione no mínimo um (1) registro para excluir!');
			return false;
		}
		if (!confirm("Este processo não pode ser revertido, deseja continuar?")){
			return false;
		}
		var $form = $("form#frmFormList");
		$form.submit();

		return false;
	});

	// click save register
	$('.mrBtnSaveReg').on("click", function(event){
		var $form = $("form#frmFormulario");
		$form.submit();
		return false;
	});

	// click save register
	$('.mrBtnSaveArq').on("click", function(event){
		var $form = $("form#frmFormArquivo");
		$form.submit();
		return false;
	});

	// click checked all registers
	$('#chkAllDelete').on("change", function(event){
		alert('123');
		//var $form = $("form#frmFormulario");
		//$form.submit();
		return false;
	});


	//$('.openBtnIframe').click(function(){
	$('.openBtnIframe').on("click", function(event){
		var $href = $(this).attr('rel');
		var $modal = $('#compose-modal-iframe');

		$modal.find('iframe').attr("src", $href);

		//alert( $href );
		//alert( $(this).attr('href') );
		/*
		$('#myModal').on('show', function () {
			$('iframe').attr("src", $href);		  
		});
		*/
		//$('#compose-modal-iframe').modal({show:true});
		$modal.modal({show:true});
		return false;
	});


	// click list all registers
	$('.mrBtnArqEdit').on("click", function(event){
		var $href = $(this).attr('rel');
		var $modal = $('#compose-modal-iframe');
		$modal.find('iframe').attr("src", $href);
		$modal.modal({show:true});		
		return false;
	});
	//$(".modal-wide").on("show.bs.modal", function() {
	//	var height = $(window).height() - 200;
	//	$(this).find(".modal-body").css("max-height", height);
	//});
	$('.mrBtnArqDel').on("click", function(event){
		var $modal = $('#modal-confirm-file');

		var $form = $('form#frmFormArqDel');
		var data = $form.serialize();
		var $idarquivo = $form.find('#idarquivo').val();
		//alert( $idarquivo ); return false;
		$.post($form.attr('action'), data, function(json){
			if(json.report.success == "true"){
				$modal.find('.callout').find('#msgsucesso').html(json.report.mensagem);
				$modal.find('.callout').show();
				$("#box-arquivo-"+ $idarquivo).remove();
				
				$modal.find('.modal-footer').find('.btn_xx_fechar').show();
				$modal.find('.modal-footer').find('.btn_xx_abrir').hide();
			}
			return false;
		},"json"
		);

		/*
		var $href = $(this).attr('rel');
		var $modal = $('#compose-modal-iframe');
		$modal.find('iframe').attr("src", $href);
		$modal.modal({show:true});
		*/
		return false;
	});
	$('.mrBtnArqDelConfirm').on("click", function(event){
		var $el = $(this);
		var $file_codigo	= $el.find('.mrBoxArqInfos').find('.file_codigo').html();
		var $file_name		= $el.find('.mrBoxArqInfos').find('.file_name').html();
		var $file_legenda	= $el.find('.mrBoxArqInfos').find('.file_legenda').html();
		//alert( $file_name );
		var $modal = $('#modal-confirm-file');
		$modal.find('.callout').find('#msgsucesso').html('');
		$modal.find('.callout').hide();

		$modal.find('.modal-footer').find('.btn_xx_fechar').hide();
		$modal.find('.modal-footer').find('.btn_xx_abrir').show();


		$modal.find('.mrBoxArqInfos').find('.file_codigo').val($file_codigo);
		$modal.find('.mrBoxArqInfos').find('.file_name').html($file_name);
		$modal.find('.mrBoxArqInfos').find('.file_legenda').html($file_legenda);
		$modal.modal({show:true});	
		/*
		var $href = $(this).attr('rel');
		var $modal = $('#compose-modal-iframe');
		$modal.find('iframe').attr("src", $href);
		$modal.modal({show:true});
		*/
		return false;
	});


	// --------------------------------------------------------
	// Adicionar mais fotos
	// --------------------------------------------------------
	var $ixItem = $(".tplArqUpl").size();
	$('.mrBtnAddMaisArq').on("click", function(event){		
		$ixItem++;
		var arrItens = [{ item: $ixItem }];
		if($ixItem > 5){
			alert('Limite-se a 5 o número de upload de arquivos simultâneos.');
			return false;
		}
		$( "#divItemTemplate" ).tmpl( arrItens ).appendTo( "#tmpRowsArq" );	
		return false;
	});
	// --------------------------------------------------------

	/*
	 * -------------------------------------------------------
	 * ini : arquivos de produtos
	 * -------------------------------------------------------
	**/
	var $ixItem = $(".tplArqUplProd").size();
	$('.mrBtnAddMaisArqProd').on("click", function(event){		
		$ixItem++;
		var arrItens = [{ item: $ixItem }];
		if($ixItem > 5){
			alert('Limite-se a 5 o número de upload de arquivos simultâneos.');
			return false;
		}
		$( "#divItemTplProd" ).tmpl( arrItens ).appendTo( "#tmpRowsArqProd" );	
		return false;
	});

	$('.mrBtnArqEditProd').on("click", function(event){
		var $href = $(this).attr('rel');
		var $modal = $('#compose-modal-iframe');
		$modal.find('iframe').attr("src", $href);
		$modal.modal({show:true});		
		return false;
	});

	$('.mrBtnArqDelProd').on("click", function(event){
		var $modal = $('#modal-confirm-file-produto');

		var $form = $('form#frmFormArqDelProd');
		var data = $form.serialize();
		//console.log(data);
		var $idarquivo = $form.find('.file_codigo').val();
		$.post($form.attr('action'), data, function(json){
			//console.log(json);
			if(json.report.success == "true"){
				$modal.find('.callout').find('#msgsucesso').html(json.report.mensagem);
				$modal.find('.callout').show();
				$("#box-arquivo-"+ $idarquivo).remove();
				
				$modal.find('.modal-footer').find('.btn_xx_fechar').show();
				$modal.find('.modal-footer').find('.btn_xx_abrir').hide();
			}
			return false;
		},"json");
		return false;
	});

	$('.mrBtnArqDelConfirmProd').on("click", function(event){
		var $el = $(this);
		var $file_codigo	= $el.find('.mrBoxArqInfos').find('.file_codigo').html();
		var $file_name		= $el.find('.mrBoxArqInfos').find('.file_name').html();
		var $file_legenda	= $el.find('.mrBoxArqInfos').find('.file_legenda').html();

		var $modal = $('#modal-confirm-file-produto');
		$modal.find('.callout').find('#msgsucesso').html('');
		$modal.find('.callout').hide();

		$modal.find('.modal-footer').find('.btn_xx_fechar').hide();
		$modal.find('.modal-footer').find('.btn_xx_abrir').show();


		$modal.find('.mrBoxArqInfos').find('.file_codigo').val($file_codigo);
		$modal.find('.mrBoxArqInfos').find('.file_name').html($file_name);
		$modal.find('.mrBoxArqInfos').find('.file_legenda').html($file_legenda);
		$modal.modal({show:true});	
		return false;
	});
	/*
	 * -------------------------------------------------------
	 * end : arquivos de produtos
	 * -------------------------------------------------------
	**/

	// --------------------------------------------------------
	// Adicionar mais opções
	// --------------------------------------------------------
	var $ixItemEnqOption = $(".tplEnqOption").size();
	$('.mrBtnAddMaisOption').on("click", function(event){		
		$ixItemEnqOption++;
		var arrItens = [{ item: $ixItemEnqOption }];
		$( "#divTplEnqueteOption" ).tmpl( arrItens ).appendTo( "#tmpRowsEnqOption" );	
		return false;
	});
	// --------------------------------------------------------



	// --------------------------------------------------------
	// Delete Tags
	// --------------------------------------------------------
	// click delete registers selected
	$('.mrBtnDelTagArea').on("click", function(event){
		var $el = $(this).closest( ".label-tags" );
		var $tag_codigo	= $el.find('.mrBoxTagInfos').find('.tag_codigo').html();
		var $tag_name	= $el.find('.mrBoxTagInfos').find('.tag_name').html();
		//alert( $tag_codigo +' | '+ $tag_name );
		//var $file_legenda	= $el.find('.mrBoxArqInfos').find('.file_legenda').html();
		//alert( $file_name );
		var $modal = $('#modal-confirm-tag-area');
		//$modal.find('.callout').find('#msgsucesso').html('');
		//$modal.find('.callout').hide();

		$modal.find('.modal-footer').find('.btn_xx_fechar').hide();
		$modal.find('.modal-footer').find('.btn_xx_abrir').show();

		$modal.find('.mrBoxTagInfos').find('.tag_codigo').val($tag_codigo);
		$modal.find('.mrBoxTagInfos').find('.tag_name').html($tag_name);
		//$modal.find('.mrBoxTagInfos').find('.file_legenda').html($file_legenda);
		$modal.modal({show:true});	
		/*
		var $href = $(this).attr('rel');
		var $modal = $('#compose-modal-iframe');
		$modal.find('iframe').attr("src", $href);
		$modal.modal({show:true});
		*/
		return false;
	});
	$('.mrBtnDelTagAreaOK').on("click", function(event){
		var $modal = $('#modal-confirm-tag-area');

		var $tag_codigo	= $modal.find('.mrBoxTagInfos').find('.tag_codigo').val();
		var $tag_name	= $modal.find('.mrBoxTagInfos').find('.tag_name').html();

		var $el = $("#areaTags_"+ $tag_codigo);
		var $elTag = $el.closest( ".label-tags" );
		$elTag.remove();

		var $fechar = $modal.find('.modal-footer').find('.btn_xx_fechar');
		$fechar.trigger( "click" );
		return false;
	});




	/*
	//iCheck for checkbox and radio inputs
	$('input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		radioClass: 'iradio_minimal-blue'
	});
	*/
	//When unchecking the checkbox
	$("#check-all-del").on('ifUnchecked', function(event) {
		//Uncheck all checkboxes
		$("input[type='checkbox']", ".table-list-register > tbody").iCheck("uncheck");
	});
	//When checking the checkbox
	$("#check-all-del").on('ifChecked', function(event) {
		//Check all checkboxes
		$("input[type='checkbox']", ".table-list-register > tbody").iCheck("check");
	});

	/*
	//When checking the checkbox
	$("#check-all-delete").on('ifChecked', function(event) {
		//Check all checkboxes
		alert('123');
		$("input[type='checkbox']", ".table-list-register > tbody").iCheck("check");
	});
	$("#chkAllDelete").change(function() {
		$check = this.checked; 
		//$("input[@name=chkItens]").each(function(){this.checked = $check;});
		$(".itemdelete").each(function(){this.checked = $check;});
	});
	$('#chkAllDelete').on('ifChanged', function(event){
		alert('123');
		var $check = $(this);
		var $strcheck = false;
		if( $check.is(':checked') ){
			$strcheck = true;
		}		
		$(".itemdelete").each(function(){
			var $item = $(this);
			if( $strcheck ){
				$item.iCheck('check');
			}else{
				$item.iCheck('uncheck');
			}
		});
	});
	*/



	// click list all registers
	$('.mrBtnModalUpload').on("click", function(event){
		var $href = $(this).attr('rel');
		var $modal = $('#compose-modal-iframe-upload');
		$modal.find('iframe').attr("src", $href);
		$modal.modal({show:true});		
		return false;
	});

	$(document).on('click', '.mrBtnArqDelTemp', function(event){
		var $el = $(this).closest( ".box-file-temp" );
		$el.remove();
		return false;
	});

	$('.mrBtnModalUploadList').on("click", function(event){
		var $url = $(this).attr('data-url');
		//alert( $url );
		
		var $href = $(this).attr('rel');
		$.jqFiles.FileList({ 
			pagejson: $url,//'<?php echo(site_url()); ?>/json/LISTAR_TODOS_ARQUIVOS',
			pagethumb: $url, //'thumbs/',
			pagina: 1 
		});

		//load-file-temp

		
		return false;
		var $modal = $('#modal-confirm-file');

		var $form = $('form#frmFormArqDel');
		var data = $form.serialize();
		var $idarquivo = $form.find('#idarquivo').val();
		//alert( $idarquivo ); return false;
		$.post($form.attr('action'), data, function(json){
			if(json.report.success == "true"){
				$modal.find('.callout').find('#msgsucesso').html(json.report.mensagem);
				$modal.find('.callout').show();
				$("#box-arquivo-"+ $idarquivo).remove();
				
				$modal.find('.modal-footer').find('.btn_xx_fechar').show();
				$modal.find('.modal-footer').find('.btn_xx_abrir').hide();
			}
			return false;
		},"json"
		);
		return false;
	});



	$(".icon-select").on('click', function(event) {
		var $itemLI = $(this).closest("li");
		var $item = $itemLI.find('.icon-input');
		console.log( 'item: '+ $item.val() );
		console.log( 'id: '+ $item.attr('id') );
		//$("#"+ $item.attr('id')).prop("checked", true);

		//$("input[type='radio']", $(this).closest("li")).iCheck("check");
		//$("#"+ $item.attr('id')).iCheck("check");
		//$("#"+ $item.attr('id')).addClass('checked');

		//$('#icon_grupo_guia_az_0').iCheck({
			//handle: 'radio'
			//labelHover: false,
			//cursor: true
		//});

		//$('#icon_grupo_guia_az_0').iCheck('check', function(){
		//	alert('Well done, Sir :: '+ $item.attr('id'));
		//});

		$('.icon-website > li').removeClass('checked');
		$("#"+ $item.attr('id')).iCheck('check', function(){
			//alert('Well done, Sir :: '+ $item.attr('id'));
			$itemLI.addClass('checked');
		});

		//$('input').on('ifChecked', function(event){
			//alert(event.type + ' callback');
		//});

		//event.stopPropagation();
		//event.preventDefault();
		return false;

		/*
		//console.log( "val: "+ $(this).html() );
		var $checked = $(this).find("input[type='checkbox']").is(":checked");
		if($checked){
			$(this).find("input[type='checkbox']").iCheck("uncheck");
		}else{
			$(this).find("input[type='checkbox']").iCheck("check");
		}
		*/
	});


	$(document).on('click', '.clickGuiaBox', function(event){
	//$(".clickGuiaBox").on('click', function(event) {
		var $this = $(this);
		var $box = $this.data( "box" );
		if( $this.hasClass('active') ){
			return;
		}
		$('.clickGuiaBox').removeClass('active bg-aqua');
		$this.addClass('active bg-aqua');
		$('.boxDivGuia').hide();

		if( $box == "#guia_user_new"){
			//$("#ajax_anunciante").html('');
		}
		if( $box == "#guia_user_exist"){
			$("#guia_user_nome").val('');
			$("#guia_user_email").val('');
			$("#guia_user_login").val('');
			$("#guia_user_senha").val('');
		}

		//$box.slideDown( "slow", function() { /* Animation complete.*/ });
		$( $box ).fadeIn( "slow", function() { /* Animation complete.*/ });
		// https://www.youtube.com/watch?v=eSs9vhTP7wU


		//$el.remove();
		return false;
	});

//	Sortable.create(el, {
//		handle: ".my-handle"
//	});

	// List with handle
	if ( $( "#sortable_box" ).length ) {
		//var el = document.getElementById('sortable_box');
		Sortable.create(sortable_box, {
			handle: '.my-handle',
			ghostClass: 'ghost',
			//chosenClass: 'chosen',
			animation: 150
		});
		$(".box-header").css("cursor","move");
	}

	/*
	$(".sortable_box").sortable({
		handle: ".box-header",
		placeholder: "sort-highlight",
		connectWith: ".sortable_item"
	}).disableSelection();
	$(".box-header").css("cursor","move");
	*/


//    $(".connectedSortable").sortable({
//        placeholder: "sort-highlight",
//        //connectWith: ".connectedSortable",
//        handle: ".box-header, .nav-tabs",
//        forcePlaceholderSize: true,
//        zIndex: 999999
//    }).disableSelection();
//
//    //jQuery UI sortable for the todo list
//    $(".todo-list").sortable({
//        placeholder: "sort-highlight",
//        handle: ".handle",
//        forcePlaceholderSize: true,
//        zIndex: 999999
//    }).disableSelection();;

	/*
	//$(":checkbox").labelauty({ class: "chkLabelAuty" });
	//$(".to-labelauty").labelauty({ minimum_width: "155px" });
	$(".to-labelauty").labelauty({
		image: false,
		class: "labelauty sm",
	});
	*/


});