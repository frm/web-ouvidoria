jQuery(document).ready(function($){	
	$.ajaxSetup({cache: false});

	// click save register
	$(document).on('click', '.mrBtnSaveReg', function (event) {
		//$('.mrBtnDelReg').on("click", function(event){
		var $form = $("form#frmFormulario");
		$form.submit();
		return false;
	});


	$('.mrBtnAddNovo').on("click", function(event){
		document.location.href = $(this).attr('rel');
		return false;
	});

	$('.mrBtnListReg').on("click", function(event){
		document.location.href = $(this).attr('rel');
		return false;
	});

	$(document).on('click', '.mrBtnDelReg', function (event) {
		//$('.mrBtnDelReg').on("click", function(event){

		var itens   = new String('');
		$(".itemdelete").each(function(){
			if(this.checked){
				if( itens.length > 0) itens += ',';
				itens += $(this).val();
			}
		});
		if(itens.length < 1) {
			$.alert({
				title: 'Atenção',
				confirmButtonClass: 'btn-info',
				cancelButtonClass: 'btn-danger',
				confirmButton: 'OK',
				//cancelButton: 'NO never !',
				content: 'Selecione no mínimo um (1) registro para excluir!',
				confirm: function () {
					//$.alert('Confirmed!');
				}
			});
			return false;
		}

		$.confirm({
			title: 'Atenção',
			confirmButtonClass: 'btn-info',
			cancelButtonClass: 'btn-danger',
			confirmButton: 'OK',
			//cancelButton: 'NO never !',
			content: 'Este processo não pode ser revertido, deseja continuar?',
			confirm: function () {
				var $form = $("form#frmFormList");
				$form.submit();
			}
		});


		//if (!confirm("Este processo não pode ser revertido, deseja continuar?")){
			//return false;
		//}


		return false;
	});


	$('.mrAddNovoContratosss').on("click", function(event){
		//document.location.href = $(this).attr('rel');
		return false;
	});


	// --------------------------------------------------------
	// Adicionar novo contrato
	// --------------------------------------------------------
	var $ixItemCTR = $(".tplRowContrato").size();
	$('.mrAddNovoContrato').on("click", function(event){
		$ixItemCTR++;
		//console.log( 'item: '+ $ixItemCTR );
		var arrItens = [{ item: $ixItemCTR }];
		$( "#divItemContrato" ).tmpl( arrItens ).appendTo( "#divResultContrato" );

		var $el = $("#tplRowContrato-"+ $ixItemCTR);
		$el.find('.field-date').each(function(){
			var $id = $(this).attr('id');
			var $idpos = $(this).closest('.input-group').find('.icon-date-picker').attr('id');

			var obj = {};
			obj[$id] = "%d/%m/%Y";

			var opts = { 
				formElements:obj, 
				positioned: $idpos,
				clickActivated: true
			};
			datePickerController.createDatePicker(opts);
		});

		return false;
	});
	// --------------------------------------------------------


	// --------------------------------------------------------
	// Adicionar novo contato
	// --------------------------------------------------------
	var $ixItemCT = $(".tplRowContato").size();
	$('.mrAddNovoContato').on("click", function(event){
		$ixItemCT++;
		//console.log( 'item: '+ $ixItemCT );
		var arrItens = [{ item: $ixItemCT }];
		$( "#divItemContato" ).tmpl( arrItens ).appendTo( "#divResultContato" );

		var $selector = $("#tplRowContato-"+ $ixItemCT).find('.inputmask');
		$selector.inputmask("(99) 99999-9999", {"placeholder": "(__) _____-____"});

		return false;
	});
	// --------------------------------------------------------


	// -------------------------------------------------------------------
	// ini : click busca cep
	// -------------------------------------------------------------------
	$(document).on('click', '.cmdbuscacep', function (event) {
	//$('.cmdbuscacep').on('click', function() {
		var $cep = $("#emp_end_cep").val();
		var $cep = $cep.replace('-', '');
		$.getJSON("https://viacep.com.br/ws/"+ $cep +"/json/?callback=?", function(dados){
			if(!("erro" in dados)){
				//console.log( dados );
				
				$('#emp_endereco').val(dados.logradouro);
				$('#emp_end_bairro').val(dados.bairro);
				$('#emp_end_cidade').val(unescape(dados.localidade));
				$('#emp_end_estado').val(unescape(dados.uf));
				$('#emp_end_numero').val('');
				$('#emp_end_complemento').val('');
				//$('#emp_endereco').val(unescape(dados.localidade));
				//$('select[name="country_id"]').find('option[value="'+ $country_id +'"]').attr('selected', true);

			}else{
				$('#emp_endereco').val('');
				$('#emp_end_bairro').val('');
				$('#emp_end_cidade').val('');
				$('#emp_end_estado').val('');
				$('#emp_end_numero').val('');
				$('#emp_end_complemento').val('');
				alert( "CEP não encontrado." );
			}
		});
	});


	// -------------------------------------------------------------------
	// ini : 
	// -------------------------------------------------------------------
	$(document).on('click', '.treegrid-expander', function (event) {
		//console.log('click');
		//console.log( $(this).data('code') );

		var $el = $('.'+ $(this).data('code'));
		//$('.'+ $el).show();
		if( $el.is(":visible") == true ) { 
			$el.hide();
			$(this).find('span').removeClass('fa-minus-square-o');
			$(this).find('span').addClass('fa-plus-square-o');
		}
		else {
			$(this).find('span').removeClass('fa-plus-square-o');
			$(this).find('span').addClass('fa-minus-square-o');
			$el.show();
		}


		//var $cep = $("#emp_end_cep").val();
		//var $cep = $cep.replace('-', '');
		//$.getJSON("https://viacep.com.br/ws/"+ $cep +"/json/?callback=?", function(dados){
			//if(!("erro" in dados)){
				//console.log( dados );
				
				//$('#emp_endereco').val(dados.logradouro);
				//$('#emp_end_bairro').val(dados.bairro);
				//$('#emp_end_cidade').val(unescape(dados.localidade));
				//$('#emp_end_estado').val(unescape(dados.uf));
				//$('#emp_end_numero').val('');
				//$('#emp_end_complemento').val('');
				////$('#emp_endereco').val(unescape(dados.localidade));
				////$('select[name="country_id"]').find('option[value="'+ $country_id +'"]').attr('selected', true);

			//}else{
				//$('#emp_endereco').val('');
				//$('#emp_end_bairro').val('');
				//$('#emp_end_cidade').val('');
				//$('#emp_end_estado').val('');
				//$('#emp_end_numero').val('');
				//$('#emp_end_complemento').val('');
				//alert( "CEP não encontrado." );
			//}
		//});
	});


	/*
	$(document).on('change', ':file', function() {
		var input = $(this),
			numFiles = input.get(0).files ? input.get(0).files.length : 1,
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

			//console.log( numFiles );
			//console.log( label );
		input.trigger('fileselect', [numFiles, label]);
	});

	$(':file').on('fileselect', function(event, numFiles, label) {
		var input = $(this).parents('.input-group').find(':text'),
			log = numFiles > 1 ? numFiles + ' files selected' : label;

		if( input.length ) {
			input.val(log);
		} else {
			if( log ) alert(log);
		}
	});
	*/


	// ----------------------------------------------------------------
	// ini : excluir registro com ajax
	// ----------------------------------------------------------------
	$(document).on('click', '.btnDeleteAjax', function (e) {
		var $this = $(this);
		var $url = $this.data('url');
		var $ID = $this.data('id');


		$.alert({
			title: 'Atenção',
			content: 'Botão sem ação efetiva. Aguarde!',
			confirmButtonClass: 'btn-info',
			confirmButton: 'OK',
		});


		return false;


		if( $ID.length == 0 ) { 
			$.alert({
				title: 'Atenção',
				content: 'Código Inválido!',
				confirmButtonClass: 'btn-info',
				confirmButton: 'OK',
			});
			return false;
		}

		var $formData = { post_id: $ID };

		$.confirm({
			title: 'Atenção',
			confirmButtonClass: 'btn-info',
			cancelButtonClass: 'btn-danger',
			confirmButton: 'OK',
			//cancelButton: 'NO never !',
			content: 'Este processo não pode ser revertido, deseja continuar?',
			confirm: function () {

				// ini : fct_ajax
				// ----------------------------------------------
				fct_ajax({
						url: $url,
						formdata: $formData
					},
					function(error) {

						console.log('ERROR --------------------');
						console.log( error );



						if (!error) {
							$('#tr-row-'+ $ID).remove();
						} else {
							console.log('error num');
							$.alert({
								title: 'Atenção',
								content: 'Não foi possível processar sua solicitação!',
								confirmButtonClass: 'btn-info',
								confirmButton: 'OK',
							});
						}


					}
				);
				// end : fct_ajax 
				// ----------------------------------------------

			}
		});
		return false;
	});
	// ----------------------------------------------------------------
	// end : excluir registro com ajax
	// ----------------------------------------------------------------


	// ----------------------------------------------------------------
	// ini : processa dados via ajax
	// ----------------------------------------------------------------
	$(document).on('click', '.btnAjaxCommon', function (e) {
		var $this = $(this);
		var $url = $this.data('url');
		var $ID = $this.data('id');

		console.log( $url );
		console.log( $ID );

		//return false;


		if( $ID.length == 0 ) { 
			alert("Código Inválido");
			return false;
		}

		var $formData = { post_id: $ID };

		$("#imgLoading").show();


		// ini : enviando os dados
		$.ajax({
			url: $url,
			type: 'POST',
			data: $formData, 
			//processData: false,
			//contentType: false,
			//data: JSON.stringify($formData),
			//contentType: "application/json; charset=utf-8",
			//dataType: "json",
			beforeSend: function(data)	{ },
			complete: function(data)	{ },
			success: function(data){
				console.log( data );
				var json = JSON.parse(data);

				if( json.report.error == "0" ){
					//console.log( json.report.error );
					window.setTimeout(function() {
						$("#imgLoading").hide();
						window.location.reload();
						//clearInterval(intervalo);
					}, 3000);

				}
				return false;
			}
		});
		// ----------------------------------------------------------------

		return false;
	});
	// ----------------------------------------------------------------
	// end : processa dados via ajax
	// ----------------------------------------------------------------


	//$('input.chkRelAnaNat').on('ifChecked', function(event){
		////alert(event.type + ' callback2');
		//console.log(  'callback' );
	//});
	$('input.chkRelAnaNat').on('ifChanged', function(event) {
		//console.log(  'callback' );
		//console.log(  event.target.checked );
		//alert('checked = ' + event.target.checked);
		//alert('value = ' + event.target.value);

		var $this			= $(this); 
		var $ana_id		= $this.data('ana_id');
		var $nat_id		= $this.data('nat_id');
		var $checked	= (event.target.checked) ? 1 : 0;
		var $route		= 'relacionar-natureza';
		var $url			= urlAnalistaJson +'/'+ $route;

		var $formData = { 
			acao: $route,
			ana_id: $ana_id,
			nat_id: $nat_id,
			checked: $checked
		};

		//console.log( $formData );
		//console.log( urlAnalistaJson );
		//return false;

		// ----------------------------------------------------------------
		// ini : enviando os dados via ajax
		// ----------------------------------------------------------------
		$.ajax({
			url: $url,
			type: 'POST',
			data: $formData, 
			//processData: false,
			//contentType: false,
			//data: JSON.stringify($formData),
			//contentType: "application/json; charset=utf-8",
			//dataType: "json",
			beforeSend: function(data)	{ },
			complete: function(data)	{ },
			success: function(data){

				console.log( data );
				//var json = JSON.parse(data);
				//if( json.report.error == "0" ){ 
					////$stremail.val('');
					//$('#tr-row-'+ $ID).remove();
				//}
				//alert( json.report.mensagem );
				//var json = JSON.parse(data);
				//if(json.report.error == "false"){ 
					//$stremail.val('');
				//}
				//alert( json.report.mensagem );
				return false;
			}
		});
		// ----------------------------------------------------------------

	});




	$(document).on('click', '.chkRelAnaNat', function (event) {
		//console.log(  $(this).html() );
		//console.log(  $(this).val() );
		//document.location.href = $(this).attr('rel');
		//return false;
	});

});



var fct_ajax = function(p, callback){
	var $url = p.url;
	var $formData = p.formdata;

	var $error = false;

	// ----------------------------------------------------------------
	// ini : enviando os dados via ajax
	// ----------------------------------------------------------------
	$.ajax({
		url: $url,
		type: 'POST',
		data: $formData, 
		//processData: false,
		//contentType: false,
		//data: JSON.stringify($formData),
		//contentType: "application/json; charset=utf-8",
		//dataType: "json",
		beforeSend: function(data)	{ },
		complete: function(data)	{ },
		success: function(data){
			console.log( data );

			var json = JSON.parse(data);
			if( json.report.error != "0" ){ 
				//$stremail.val('');
				//$('#tr-row-'+ $ID).remove();
				$error = true;
			}
			callback( $error );

			//alert( json.report.mensagem );
			//var json = JSON.parse(data);
			//if(json.report.error == "false"){ 
				//$stremail.val('');
			//}
			//alert( json.report.mensagem );
			//return false;
		}
	});
	// ----------------------------------------------------------------
	
	//callback ($error);
}


var speakOrders = function (orders, callback) {
	console.log(orders.name);
	var error = false;
	callback (error);
}





