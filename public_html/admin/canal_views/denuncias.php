<?php $this->load->view( "includes/doctype" ); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


	<?php $this->load->view( "includes/header" ); ?>

  <?php $this->load->view( "includes/menu" ); ?>



  <div class="content-wrapper">

    <section class="content-header">
      <h1>
        Gerenciar Denúncias
        <small>Listagem</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="javascript:;"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="javascript:;">Denúncias</a></li>
        <li class="active">Listagem</li>
      </ol>
    </section>


		<section class="contents" style="min-height: auto !important; padding:15px 15px 15px 15px !important;">
				<!-- botoes de acoes -->
				<div class="row" style="margin-bottom:0px;">

					<div class="col-xs-6" >
						<!--## FILTRO PARA PESQUISA -->
						<FORM action="<?php echo( $url["url_list"] ); ?>" method="POST" name="frmFiltroPesquisa" id="frmFiltroPesquisa">
							<input type="hidden" name="baseAcao" id="baseAcao" value="FILTRO-PESQUISA">

							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label for="exampleInputEmail1">Palavra Chave</label>
										<input type="text" name="bsc_search" id="bsc_search" class="form-control" value="<?php echo(isset($bsc_search)? $bsc_search : ""); ?>">
									</div>
								</div>
								<div class="col-xs-2 hide">
									<div class="form-group" style="margin-bottom:0px !important;">
										<label>Categoria</label>
										<select name="bsc_categoria" id="bsc_categoria" class="form-control">
											<option value="">- selecione -</option>
											<?php
											//$cbo = '';
											//if ( isset($rsCateg) ){
												//foreach ($rsCateg AS $item){
													//$idcategoria	= $item->idcategoria;
													//$strtitulo		= $item->strtitulo;
													//$selected		= ($idcategoria == $bsc_categoria)?"selected":"";
													//$cbo .= '<option value="'. $idcategoria .'" '. $selected .'>'. $strtitulo .'</option>';
												//}
												//print $cbo;
											//}// isset : rsCateg
											?>
										</select>
									</div>
								</div>
								<div class="col-xs-2 no-padding">
									<div class="form-group" style="margin-bottom:0px !important;">
										<label>&nbsp;</label><br />
										<button class="btn btn-primary clsSearchPages" data-url="<?php echo(current_url()); ?>" data-form="#frmFiltroPesquisa" >Buscar</button>
									</div>
								</div>
							</div>
						</FORM>
						<!-- +++++++++++++++++++++++++++++++++ FILTRO PARA PESQUISA -->
					</div><!-- /.col -->


					<?php if( $session_level == "admin" ){ ?>
					<div class="col-xs-6" >

						<div class="form-group pull-right">
							<label for="exampleInputEmail1">&nbsp;</label><br />
							<div class="pull-right">
								<button class="btn btn-info mrBtnAddNovo" rel="<?php echo( $url["url_form"] ); ?>"><i class="fa fa-edit"></i> Criar Novo Registro</button>
								<button class="btn btn-danger mrBtnDelReg"><i class="fa fa-trash-o"></i> Excluir</button>
							</div>
						</div>
						<div class="clear"></div>
					
					</div><!-- /.col -->
					<?php } //if session_level == administrador?>


				</div><!-- /.row // botoes de acoes-->

				<div class="clear"></div>
				<div style="border-bottom: 1px solid #D8D8D8;"></div>
		</section>


		<!--## INI : FORMULARIO -->
		<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmFormList" id="frmFormList" enctype="multipart/form-data">
			<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORMULARIO">

			<section class="content">

				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Lista de denúncias</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<?php echo( (isset($paginacao)? $paginacao : "") ); ?>

								<table id="tableList" class="table table-bordered table-hover">
									<thead>
									<tr>
										<th style="width: 70px;">ID</th>
										<th style="width: 200px;">Protocolo</th>
										<th>Empresa / Natureza</th>
										<th class="center" style="width: 140px;">Data</th>
										<th class="center" style="width: 170px;">Status</th>
										<th class="center" style="width: 90px;">Ação</th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ( isset($rs_list) ){
										foreach ($rs_list AS $item){
											$den_id						= (int)$item->den_id;
											$emp_razao_social	= $item->emp_razao_social;
											$nat_titulo				= $item->nat_titulo;
											$den_status				= $item->den_status;
											$den_status_admin	= $item->den_status_admin;
											$den_num_protocolo = $item->den_num_protocolo;
											$den_dte_cadastro	= fct_formatdate($item->den_dte_cadastro, 'd/m/Y H:i');
											//$icon_ativo				= ($item->ana_ativo==1)?"sim":"não";
											//$icon_ativo				= 'Em Aprovação';
											$redirect					= $url["url_form"] ."/". $den_id;


											
											$den_status				= (!empty($den_status_admin) ? $den_status_admin : $item->den_status);
											$icon_ativo				= (isset($this->cfg_den_status[$den_status]["titulo"]) ? $this->cfg_den_status[$den_status]["titulo"] : $den_status );
											
											$color_status			= (isset($this->cfg_den_status[$den_status]["color"] ) ? $this->cfg_den_status[$den_status]["color"] : "label-default" );
									?>
									<tr>
										<td><?php echo($den_id); ?></td>
										<td><?php echo($den_num_protocolo); ?></td>
										<td>
											<div><?php echo($emp_razao_social); ?></div>
											<h6 class="text-light-blue mg-empcode"><?php echo($nat_titulo); ?></h6>
										</td>
										<td class="center"><?php echo($den_dte_cadastro); ?></td>
										<td class="center">
											<div class="<?php echo($color_status); ?>"><?php echo($icon_ativo); ?></div>
										</td>
										<td class="center">
											<div style="padding:0 10px; display:inline-block">
												<a href="<?php echo($redirect); ?>"><i class="fa fa-search-plus fa-ft15px"></i></a>
											</div>
										</td>
									</tr>
									<?php
										};
									};// isset : rs_list
									?>
									</tbody>
									<!--
									<tfoot>
									<tr>
										<th>ID</th>
										<th>Razão Social</th>
										<th>E-mail</th>
										<th>Data</th>
										<th>Ativo</th>
									</tr>
									</tfoot>
									-->
								</table>

								<?php echo( (isset($paginacao)? $paginacao : "") ); ?>
							</div>
							<!-- /.box-body -->
						</div><!-- /.box -->
					</div>
					<!-- /.col -->
				</div>

			</section><!-- /.content -->
		</FORM>
		<!--## END : FORMULARIO -->
    
  </div>



	<?php $this->load->view( "includes/footer" ); ?>

	<?php $this->load->view( "includes/scripts" ); ?>
