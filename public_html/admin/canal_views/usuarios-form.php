<?php $this->load->view( "includes/doctype" ); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


	<?php $this->load->view( "includes/header" ); ?>

  <?php $this->load->view( "includes/menu" ); ?>


  <div class="content-wrapper">

    <section class="content-header">
      <h1>
        Gerenciar Usuários
        <small>Formulário</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="javascript:;"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="javascript:;">Usuários</a></li>
        <li class="active">Formulário</li>
      </ol>
    </section>


		<!--## INI : FORMULARIO -->
		<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmFormulario" id="frmFormulario" enctype="multipart/form-data">
			<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORMULARIO">
			<input type="hidden" name="emp_id" ID="emp_id" value="<?php echo(isset($rs_bd->emp_id)? $rs_bd->emp_id : ""); ?>">

			<section class="content">

				<!-- botoes de acoes -->
				<div class="row" style="margin-bottom:15px;">
					<div class="col-xs-12" >
						<p style="float:right;">
							<button class="btn btn-info mrBtnAddNovo" rel="<?php echo( $url["url_form"] ); ?>"><i class="fa fa-edit"></i> Criar Novo Registro</button>
							<button class="btn btn-warning mrBtnListReg" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-list-alt"></i> Lista de Registros</button>
							<button class="btn btn-success mrBtnSaveReg" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-check-square-o"></i> Salvar</button>
						</p>

						<div class="clear"></div>
						<div style="border-bottom: 1px solid #D8D8D8;"></div>
					</div><!-- /.col -->
				</div><!-- /.row // botoes de acoes-->


				<div class="row">
					<div class='col-xs-12'>
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active ">
									<a href="#tab-mod_formulario" data-toggle="tab">Detalhes Principais</a>
								</li>
							</ul>

							<div class="tab-content">
								
								<!-- tab : tab-mod_formulario -->
								<div class="tab-pane active " id="tab-mod_formulario">
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
									<div class="row">

										<div class="col-md-8">

											<!-- Dados principais --> 
											<div class="box slim box-primary">
												<div class="box-header with-border">
													<h3 class="box-title">Dados principais</h3>
												</div><!-- /.box-header -->

												<div class="box-body">

													<div class="row" >
														<div class="col-xs-12">
															<div class="form-group">
																<label for="usr_nome">Nome do usuário</label>
																<input type="text" name="usr_nome" id="usr_nome" class="form-control" placeholder="" value="<?php echo( (isset($rs_bd->usr_nome)? $rs_bd->usr_nome : '') ); ?>" />
															</div>
														</div>
													</div><!-- /.row -->

												</div><!-- /.box-body -->

												<div class="box-footer hide">
													<button type="submit" class="btn btn-primary">Salvar</button>
												</div><!-- /.box-footer -->
											</div><!-- /.box -->


											<!-- Dados de acesso --> 
											<div class="box slim box-primary">
												<div class="box-header with-border">
													<h3 class="box-title">Dados de acesso</h3>
												</div><!-- /.box-header -->

												<div class="box-body">

													<div class="row" >
														<div class="col-xs-6">
															<div class="form-group">
																<label for="usr_email">E-mail</label>
																<input type="text" name="usr_email" id="usr_email" class="form-control" placeholder="" value="<?php echo( (isset($rs_bd->usr_email)? $rs_bd->usr_email : '') ); ?>" >
															</div>
														</div>

														<div class="col-xs-6">
															<div class="form-group">
																<label for="usr_senha">Senha</label>
																<input type="text" name="usr_senha" id="usr_senha" class="form-control" placeholder="">
															</div>
														</div>
													</div><!-- /.row -->

												</div><!-- /.box-body -->

												<div class="box-footer hide"></div><!-- /.box-footer -->
											</div><!-- /.box -->

										</div>

										<div class="col-md-4">
											<div class="box slim box-primary">

												<div class="box-header with-border">
													<h3 class="box-title">Status</h3>
												</div><!-- /.box-header -->

												<div class="box-body">
													<div class="form-group smXT">
														<?php
														$usr_ativo = isset($rs_bd->usr_ativo)?$rs_bd->usr_ativo:1;
														?>
														<label for="usr_ativo" class="lblsm">Ativo</label>
														<select name='usr_ativo' id='usr_ativo' class="form-control">
															<option value="1" <?php echo($usr_ativo=="1"? "selected":""); ?>>Sim</option>
															<option value="0" <?php echo($usr_ativo=="0"? "selected":""); ?>>Não</option>
														</select>
													</div>
												</div><!-- /.box-body -->

												<div class="box-footer hide">
													<button type="submit" class="btn btn-primary">Submit</button>
												</div><!-- /.box-footer -->

											</div>
										</div>

									</div>
									<div class="clear"></div>
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
								</div>

							</div>

						</div>
					</div>
				</div><!-- /.row -->

			</section><!-- /.content -->
		</FORM>
		<!--## END : FORMULARIO -->
    
  </div>


	<?php $this->load->view( "includes/footer" ); ?>

	<?php $this->load->view( "includes/scripts" ); ?>


	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
	<!-- ini : TEMPLATE ITEM OCULTO -->
	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
	<div id="itemTplOculto" style="display:none;">
		<script id="divItemContrato" type="text/x-jquery-tmpl">
			<div class="row tplRowContrato" id="tplRowContrato-${item}" >
				<input type="hidden" name="cntr_id[]" id="cntr_id_${item}" value="0" />

				<div class="col-xs-6">
					<div class="form-group">
						<label for="cntr_num_contrato_${item}">Número do contrato</label>
						<input type="text" name="cntr_num_contrato[]" id="cntr_num_contrato_${item}" class="form-control" placeholder="">
					</div>
				</div>

				<div class="col-xs-3">
					<div class="form-group">
						<label for="cntr_dte_ini_contrato_${item}" class="lblsm">Data de início</label>
						<div class="input-group">
							<div class="input-group-addon"><span class="icon-date-picker" id="imgdte_dte_ini_contrato_${item}"></span></div>
							<input type="text" name="cntr_dte_ini_contrato[]" id="cntr_dte_ini_contrato_${item}" class="form-control field-date " placeholder="" />
						</div><!-- /.input group -->
					</div>
				</div>

				<div class="col-xs-3">
					<div class="form-group">
						<label for="cntr_dte_fim_contrato_${item}" class="lblsm">Data de término</label>
						<div class="input-group">
							<div class="input-group-addon"><span class="icon-date-picker" id="imgdte_dte_fim_contrato_${item}"></span></div>
							<input type="text" name="cntr_dte_fim_contrato[]" id="cntr_dte_fim_contrato_${item}" class="form-control field-date " placeholder="" />
						</div><!-- /.input group -->
					</div>
				</div>
			</div><!-- /.row -->
		</script>
	</div>
	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
	<!-- end : TEMPLATE ITEM OCULTO -->
	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
