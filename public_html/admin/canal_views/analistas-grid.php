<?php $this->load->view( "includes/doctype" ); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


	<?php $this->load->view( "includes/header" ); ?>

  <?php $this->load->view( "includes/menu" ); ?>



  <div class="content-wrapper">


    <section class="content-header">
      <h1>
        Gerenciar Analistas
        <small>Grid</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="javascript:;"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="javascript:;">Analistas</a></li>
        <li class="active">Grid</li>
      </ol>
    </section>


		<section class="contents" style="min-height: auto !important; padding:15px 15px 15px 15px !important;">
				<!-- botoes de acoes -->
				<div class="row" style="margin-bottom:0px;">

					<div class="col-xs-6" >
						<!--## FILTRO PARA PESQUISA -->
						<FORM action="<?php echo( $url["url_grid"] ); ?>" method="POST" name="frmFiltroPesquisa" id="frmFiltroPesquisa">
							<input type="hidden" name="baseAcao" id="baseAcao" value="FILTRO-PESQUISA">

							<div class="row">
								<?php if( $session_level == "administrador" ){ ?>
								<div class="col-xs-6">

									<div class="form-group" style="margin-bottom:10px !important;">
										<?php
										$emp_id = 0;
										$emp_id = isset($rs_bd->emp_id)?$rs_bd->emp_id:0;
										?>
										<label for="empresa_ident">Selecione a empresa</label>
										<select name='empresa_ident' id='empresa_ident' class="form-control">
											<option value="" >- selecione -</option>
											<?php
											if ( isset($rs_list_emp) ){
												foreach ($rs_list_emp AS $item){
													$selected = ($empresa_ident == (int)$item->emp_id) ? ' selected ' : ''; 
											?>
											<option value="<?php echo((int)$item->emp_id); ?>" <?php echo($selected); ?> ><?php echo($item->emp_razao_social); ?></option>
											<?php
												}// foreach
											}// if
											?>
										</select>
									</div>

								</div>
								<?php } //if session_level == administrador ?>

								<div class="col-xs-2">
									<div class="form-group" style="margin-bottom:0px !important;">
										<label>&nbsp;</label><br />
										<button class="btn btn-primary clsSearchPages" data-url="<?php echo(current_url()); ?>" data-form="#frmFiltroPesquisa" >Filtrar</button>
									</div>
								</div>
							</div>
						</FORM>
						<!-- +++++++++++++++++++++++++++++++++ FILTRO PARA PESQUISA -->
					</div><!-- /.col -->

					<div class="col-xs-6 hide" >

						<div class="form-group pull-right" style="margin-bottom:0px !important;">
							<label for="exampleInputEmail1">&nbsp;</label><br />
							<div class="pull-right">
								<button class="btn btn-info mrBtnAddNovo" rel="<?php echo( $url["url_form"] ); ?>"><i class="fa fa-edit"></i> Criar Novo Registro</button>
								<button class="btn btn-danger mrBtnDelReg"><i class="fa fa-trash-o"></i> Excluir</button>
							</p>
						</div>
						<div class="clear"></div>
					
					</div><!-- /.col -->
				</div><!-- /.row // botoes de acoes-->

				<div class="clear"></div>
				<div style="border-bottom: 1px solid #D8D8D8;"></div>
		</section>



		<!--## INÍCIO DO FORMULÁRIO -->
		<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmFormulario" id="frmFormulario" enctype="multipart/form-data">
			<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORMULARIO">

			<section class="content">

				<!-- botoes de acoes -->
				<div class="row hide" style="margin-bottom:15px;">
					<div class="col-xs-12" >
						<p style="float:right;">
							<button class="btn btn-info mrBtnAddNovo hide" rel="<?php echo( $url["url_form"] ); ?>"><i class="fa fa-edit"></i> Criar Novo Registro</button>
							<button class="btn btn-warning mrBtnListReg hide" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-list-alt"></i> Lista de Registros</button>
							<button class="btn btn-success mrBtnSaveReg" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-check-square-o"></i> Salvar</button>
						</p>

						<div class="clear"></div>
						<div style="border-bottom: 1px solid #D8D8D8;"></div>
					</div><!-- /.col -->
				</div><!-- /.row // botoes de acoes-->


				<div class="row">
					<div class='col-xs-12'>
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active ">
									<a href="#tab-mod_formulario" data-toggle="tab">Detalhes Principais</a>
								</li>
							</ul>

							<div class="tab-content">
								
								<!-- tab : tab-mod_formulario -->
								<div class="tab-pane active " id="tab-mod_formulario">
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

									<div class="row">

										<div class="col-md-12">
											<?php
												//print count($rs_list_ana);
												if ( isset($rs_list_ana) && count($rs_list_ana)>0 ){
											?>

											<?php
											 $count_list_ana = ( isset($rs_list_ana) ? count($rs_list_ana) : 0 );
											?>
											<table id="tableList" class="table table-bordered table-hover">
												<thead>
													<tr>
														<th class="center" rowspan="2" style="width: 200px; vertical-align: middle;" valign="">Naurezas</th>
														<th class="center" colspan="<?php echo($count_list_ana); ?>">Analistas</th>
													</tr>
													<tr>
														<?php
														if ( isset($rs_list_ana) ){
															$rs_list_ana_base = array_column($rs_list_ana, 'ana_id');
															foreach ($rs_list_ana AS $key=>$item){
														?>
															<th class="center"><?php echo($item["ana_nome"]); ?></th>
														<?php
															}// foreach
														}// if
														?>
													</tr>
												</thead>
												<tbody>
													<?php
													//fct_print_debug( $rs_list_nat_rel );
													if ( isset($rs_list_nat_rel) ){
														foreach ($rs_list_nat_rel AS $key=>$item){
															$nat_titulo		= $item["nat_titulo"];
															$nat_id				= $item["nat_id"];
															$arr_select		= $item["responsaveis"];
													?>
														<tr>
															<td class="center"><?php echo($nat_titulo); ?></td>
																<?php
																if ( isset($rs_list_ana) ){
																	//fct_print_debug( $rs_list_ana );
																	foreach ($rs_list_ana AS $keyA=>$itemA){
																		$item_arr_id = "chk_rel_nat[". $key ."][". $itemA["ana_id"] ."]";
																		$item_chk = "chk_rel_nat_". $key ."_". $keyA;
																		$checked = '';

																		$key = array_search((int)$itemA["ana_id"], array_column($arr_select, 'ana_id'));
																		if($key !== NULL ){
																			$checked = ($arr_select[$key]['ananat_ativo'] == '1' ? ' checked ' : '');			
																		}
																?>
																	<td class="center lineChk">
																		<input type="checkbox" name="<?php echo($item_arr_id); ?>" ID="<?php echo($item_chk); ?>" value="<?php echo($keyA); ?>" data-ana_id="<?php echo($itemA["ana_id"]); ?>" data-nat_id="<?php echo($nat_id); ?>" class="minimal chkRelAnaNat" <?php echo($checked); ?>  />
																	</td>
																<?php
																	}// foreach
																}// if
																?>
														</tr>
													<?php
														}// foreach
													}// if
													?>


												</tbody>
											</table>
											<?php
												}
											?>

										</div><!-- /.row -->

									</div>
									<div class="clear"></div>
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
								</div>

							</div>

						</div>
					</div>
				</div><!-- /.row -->

			</section><!-- /.content -->
		</FORM>
    
  </div>


	<?php $this->load->view( "includes/footer" ); ?>

	<?php $this->load->view( "includes/scripts" ); ?>


	<script>
		var urlAnalistaJson = '<?php echo( $url["url_json"] ); ?>';
		//console.log( urlAnalistaJson );
	</script>


	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
	<!-- ini : TEMPLATE ITEM OCULTO -->
	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
	<div id="itemTplOculto" style="display:none;">
		<script id="divItemContato" type="text/x-jquery-tmpl">
			<div class="row tplRowContato" id="tplRowContato-${item}" >
				<input type="hidden" name="acnt_id[]" id="acnt_id_${item}" value="0" />

				<div class="col-xs-6">
					<div class="form-group">
						<label for="acnt_email_${item}">Email</label>
						<input type="text" name="acnt_email[]" id="acnt_email_${item}" class="form-control" placeholder="">
					</div>
				</div>

				<div class="col-xs-6">
					<div class="form-group">
						<label for="acnt_telefone_${item}">Telefone</label>
						<input type="text" name="acnt_telefone[]" id="acnt_telefone_${item}" class="form-control inputmask" data-inputmask='"mask": "(99) 9-9999-9999"' data-mask placeholder="">
					</div>
				</div>
			</div><!-- /.row -->
		</script>
	</div>
	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
	<!-- end : TEMPLATE ITEM OCULTO -->
	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
