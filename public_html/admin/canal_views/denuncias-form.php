<?php $this->load->view( "includes/doctype" ); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


	<?php $this->load->view( "includes/header" ); ?>

  <?php $this->load->view( "includes/menu" ); ?>



  <div class="content-wrapper">


    <section class="content-header">
      <h1>
        Gerenciar Denúncias
        <small>Formulário</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="javascript:;"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="javascript:;">Denúncias</a></li>
        <li class="active">Formulário</li>
      </ol>
    </section>


		<section class="contents" style="min-height: auto !important; padding:15px 15px 15px 15px !important;">
				<!-- botoes de acoes -->
				<div class="row" style="margin-bottom:0px;">
					<div class="col-xs-12" >
						<p class="pull-right">
							<button class="btn btn-info mrBtnAddNovo hide" rel="<?php echo( $url["url_form"] ); ?>"><i class="fa fa-edit"></i> Criar Novo Registro</button>
							<button class="btn btn-warning mrBtnListReg" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-list-alt"></i> Lista de Registros</button>
							<button class="btn btn-success mrBtnSaveReg" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-check-square-o"></i> Salvar</button>
						</p>
						<div class="clear"></div>
					</div><!-- /.col -->
				</div><!-- /.row // botoes de acoes-->
				<div style="border-bottom: 1px solid #D8D8D8;"></div>
		</section>


		<!--## INÍCIO DO FORMULÁRIO -->
		<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmFormulario" id="frmFormulario" enctype="multipart/form-data">
			<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORMULARIO">
			<input type="hidden" name="den_id" ID="den_id" value="<?php echo(isset($rs_bd->den_id)? $rs_bd->den_id : ""); ?>">

			<section class="content">

				<div class="row">
					<div class='col-xs-12'>
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#tab-mod_historico" data-toggle="tab">Histórico</a>
								</li>
							</ul>

							<div class="tab-content">

								<!-- tab : tab-content -->
								<div class="tab-pane active" id="tab-mod_historico">


									<div class="row">

										<div class="col-md-5">
											<div class="box box-widget widget-user-2 custom">
												<div class="widget-user-header bg-yellow-custom">
													<div class="widget-user-image">
														<img class="img-circle" src="assets/img/avatar-user-01.png" alt="User Avatar">
													</div>
													<?php
														$dnt_json = isset($rs_bd->dnt_json)?$rs_bd->dnt_json:'';
														$json = json_decode($dnt_json, true);
														//fct_print_debug( $json );
														if( is_array($json) && count($json)>0 ){
													?>
													<h3 class="widget-user-username"><?php echo( ($json["dnt_nome"]) ); ?></h3>
													<h5 class="widget-user-desc">
														<div><?php echo($json["dnt_email"]) ?></div>
														<div><?php echo($json["dnt_cargo"]) ?></div>
														<div><?php echo($json["dnt_telefone"]) ?></div>
													</h5>
													<?php 
														}else{
													?>
													<h3 class="widget-user-username">Autor não identificado</h3>
													<h5 class="widget-user-desc"></h5>
													<?php 
														}// $json
													?>
												</div>
											</div>
										</div>



										<div class="col-md-7">
											<div class="row">

												<div class="col-md-6">
													<div class="box box-widget widget-user-2 custom3">
														<div class="widget-user-header bg-yellow-custom" style="min-height: 86px !important;">
															<?php
																$den_num_protocolo = isset($rs_bd->den_num_protocolo)?$rs_bd->den_num_protocolo:'';
															?>
															<h5 class="widget-user-desc">Protocolo</h5>
															<h3 class="widget-user-username bold"><?php echo($den_num_protocolo); ?></h3>
														</div>
														<?php
															$den_status = isset($rs_bd->den_status) ? $rs_bd->den_status : '';
															$status_titulo = isset($rs_bd->den_status) ? $this->cfg_den_status[$den_status]["titulo"] : '';
															$status_color = isset($rs_bd->den_status) ? $this->cfg_den_status[$den_status]["color"] : '';
															//$color_status = $this->cfg_den_status[$den_status]["color"];
														?>
														<div class="box-footer no-padding <?php echo($status_color); ?>" style="/*background-color: #DFFFA4 !important;*/">
															<ul class="nav nav-stacked" >
																<li>
																	<h3 class="widget-user-username bold" style="text-align:center;"><?php print $status_titulo; ?></h3>
																</li>
															</ul>
														</div>
													</div>
												</div>


												<?php
												$redirect = '';
												$den_parent_id = (int)isset($rs_bd->den_parent_id)?$rs_bd->den_parent_id:'';
												if( $den_parent_id>0 )
												{
													$redirect = $url["url_form"] ."/". $den_parent_id;
													$den_num_protocolo_relac = isset($rs_bd->den_num_protocolo_relac)?$rs_bd->den_num_protocolo_relac:'';
												?>
												<div class="col-md-6">
													<div class="box box-widget widget-user-2 custom4">
														<div class="widget-user-header bg-yellow-custom">
															<h5 class="widget-user-desc">Denúncia Relacionada</h5>
															<h3 class="widget-user-username bold"><?php echo($den_num_protocolo_relac); ?></h3>
														</div>
													</div>
												</div>
												<?php } ?>

											</div>
										</div>

									</div>


									<ul class="timeline timeline-inverse">

										<!-- timeline time label -->
										<li class="time-label">

											<span class="bg-red">
												<i class="fa fa-clock-o"></i> &nbsp;&nbsp;
												<?php
													//$den_num_protocolo = isset($rs_bd->den_num_protocolo)?$rs_bd->den_num_protocolo:'';
													//print $den_num_protocolo;
													//print '&nbsp;&nbsp; : &nbsp;&nbsp;';

													$den_dte_cadastro	= fct_formatdate($rs_bd->den_dte_cadastro, 'd.m.Y xx H:i');
													print str_replace("x", "&nbsp;", $den_dte_cadastro);
												?>
											</span>
										</li>
										<!-- /.timeline-label -->

										<!-- timeline item -->
										<li>
											<i class="fa fa-hourglass-2 bg-blue hide"></i>
											<div class="timeline-item">
												<?php
													//<span class="time"><i class="fa fa-clock-o"></i>
													//$den_dte_cadastro	= fct_formatdate($rs_bd->den_dte_cadastro, 'd.m.Y H:i');
													//print $den_dte_cadastro;
												?>
												</span>
												<a class="btn btn-warning btn-flat btn-xs hide" style="
												float: right;
												padding: 9px;
												"><?php
												$den_status = isset($rs_bd->den_status)?$rs_bd->den_status:'';
												$den_status_admin = isset($rs_bd->den_status_admin)?$rs_bd->den_status_admin:'';
												print $this->cfg_den_status[$den_status]["titulo"];
												?></a>
												

												<h3 class="timeline-header">
												<?php
													//$den_num_protocolo = isset($rs_bd->den_num_protocolo)?$rs_bd->den_num_protocolo:'';
													//print 'protocolo número: <strong>'. $den_num_protocolo .'</strong>';
													//print '&nbsp;&nbsp; : &nbsp;&nbsp;';
													print isset($rs_bd->nat_titulo)?$rs_bd->nat_titulo:'';
												?>
												</h3>
												<div class="timeline-body">
													<?php echo(isset($rs_bd->den_descricao)? $rs_bd->den_descricao : ""); ?>
												</div>
												<div class="timeline-footer">
													<?php
													/**
													 * --------------------------------------------------------
													 * respostas
													 * --------------------------------------------------------
													**/
													if ( isset($rs_den_anexo) ){
														if( count($rs_den_anexo)>=1){
															print '<h4>Anexos:</h4>';
															foreach ($rs_den_anexo AS $itemAnexo){
																$danx_id				= (int)$itemAnexo->danx_id;
																$danx_arquivo		= $itemAnexo->danx_arquivo;

																$image_path = $this->config->item('folder_images') .'denuncias/'. $danx_arquivo;
																if( file_exists($image_path) and is_file($image_path) )
																{
																	$load_image = base_url($image_path);
																	//$nat_arquivo_tag = '<img src="'. $load_image .'" alt="" class="img-responsive" style="height: 200px;" />';
																	print '<a href="'. $load_image .'" target="_blank" class="btn btn-primary">'. $danx_arquivo .'</a>';
																};
															}
														}
													}
													?>													
													<!--
													<a class="btn btn-primary btn-xs">Read more</a>
													<a class="btn btn-danger btn-xs">Delete</a>
													-->
												</div>
											</div>
										</li>
										<!-- END timeline item -->


										<!-- timeline time label -->
										<li class="time-label timeline-respostas">
											<span class="bg-red">
												<?php print 'Histórico de Respostas'; ?>
											</span>
										</li>
										<!-- /.timeline-label -->
									<?php
									/**
									 * --------------------------------------------------------
									 * respostas
									 * --------------------------------------------------------
									**/
									$formEdicao = true;
									$xx = 0;
									if ( isset($rs_den_resp) ){
										foreach ($rs_den_resp AS $itemResp){
											$xx++;
											$dresp_id						= (int)$itemResp->dresp_id;
											$dresp_descricao		= $itemResp->dresp_descricao;
											$dresp_status				= $itemResp->dresp_status;
											$dresp_autor				= $itemResp->dresp_autor;
											$dresp_aprovada			= (int)$itemResp->dresp_aprovada;
											$dresp_dte_cadastro	= fct_formatdate($itemResp->dresp_dte_cadastro, 'd.m.Y H:i');

											if($xx == 1){
												$formEdicao = ($dresp_aprovada == 1) ? false : true;
											}

											$css_status					= ($dresp_autor == 'analista' ? '' : 'status-analista' ); 
											$icon_status				= ($dresp_autor == 'analista' ? 'fa-check-square-o' : 'fa-hourglass-2' );
											$align_item					= ($dresp_autor == 'analista' ? '' : 'right' );
											$analista_title			= ($dresp_autor == 'analista' ? 'Analista' : 'Autor' );
											$icon_avatar				= ($dresp_autor == 'analista' ? 'avatar-canal-01.png' : 'avatar-user-01.png' );
											$direct_chat_admin	= ($dresp_autor != 'empresa' ? '' : '' );

											$niveladmin = ($dresp_autor == 'empresa' || $dresp_autor == 'administrador' ) ? true : false;
											$css_status					= ($niveladmin == false? $css_status : '' ); 
											$icon_status				= ($niveladmin == false ? $icon_status : 'fa-check-square-o' );
											$align_item					= ($niveladmin == false ? $align_item : '' );
											$analista_title			= ($niveladmin == false ? $analista_title : 'Admin' );
											$icon_avatar				= ($niveladmin == false ? $icon_avatar : 'avata-company.png' );
											$direct_chat_admin	= ($niveladmin == false ? $direct_chat_admin : 'direct_chat_admin' );
											$bg_item_aprov			= ($dresp_aprovada == 1 ? '' : 'naoprov' );

											/**
											 * --------------------------------------------------------
											 * anexos
											 * --------------------------------------------------------
											**/
											$this->db->select(" * ")
												->from('tbl_denunc_resp_anexos')
												->where( array('dresp_id' => $dresp_id) )
												->order_by( 'dranx_id', 'DESC' );
											$query = $this->db->get();
											$rs_resp_anexo = $query->result();
											//fct_print_debug( $data['rs_den_anexo'] );
									?>
										<!-- timeline time label -->
										<li class="time-label <?php print $css_status; ?> hide">
											<span class="bg-green">
												<?php print $dresp_dte_cadastro; ?>
											</span>
										</li>
										<!-- /.timeline-label -->

										<!-- timeline item -->
										<li class=" <?php print $css_status; ?> ">
											<i class="fa <?php print $icon_status; ?> bg-green hide"></i>
											<div class="timeline-item hide">
												<?php
													//<span class="time"><i class="fa fa-clock-o"></i>
													//$den_dte_cadastro	= fct_formatdate('2017-07-03 09:03:00', 'd.m.Y H:i');
													//print $den_dte_cadastro;</span>
												?>
												<a class="btn btn-success btn-flat btn-xs" style="
												float: right;
												padding: 9px;
												"><?php
												print $this->cfg_den_status[$dresp_status]["titulo"];
												?></a>
												
												<h3 class="timeline-header">
												<?php
													print 'resposta enviada em: <strong>'. $dresp_dte_cadastro .'</strong>';
												?>
												</h3>
												<div class="timeline-body">
													<?php print $dresp_descricao; ?>
												</div>
												<div class="timeline-footer">
													<a class="btn btn-primary btn-xs">Read more</a>
													<a class="btn btn-danger btn-xs">Delete</a>
												</div>
											</div>

											<div class="direct-chat-messages direct-chat-warning <?php echo($direct_chat_admin); ?>" style="height: auto; margin-left: 60px;">
												<div class="direct-chat-msg <?php echo($align_item); ?>">
													<div class="direct-chat-info clearfix" style="font-size: 16px;">
														<span class="direct-chat-name pull-left"><?php echo($analista_title); ?></span>
														<span class="direct-chat-timestamp pull-right">
															<?php
																print 'resposta enviada em: <strong>'. $dresp_dte_cadastro .'</strong>';
															?>
														</span>
													</div>
													<img class="direct-chat-img" src="assets/img/<?php echo($icon_avatar); ?>" alt="message user image">
													<div class="direct-chat-text bgcustom">
														<?php print ( empty($dresp_descricao) ? '-- não há comentário --' : $dresp_descricao); ?>
													</div>

													<?php if( $dresp_autor == "empresa" OR $dresp_autor == "administrador" ){ ?>
													<div class="direct-chat-aprovada <?php print $bg_item_aprov; ?>">
														<div class="pull-left"><h3><?php echo( ($dresp_aprovada == 1 ? "SIM" : "NÃO") ); ?></h3></div>
														<div class="pull-left texto">- <?php echo( ($dresp_aprovada == 0 ? "NÃO" : "") ); ?> APROVADA A RESPOSTA DA EMPRESA QUE SERÁ ENVIADA AO AUTOR DA DENÚNCIA</div>
														<div class="clearfix"></div>
													</div>
													<?php } ?>


													<?php 
													/**
													 * --------------------------------------------------------
													 * anexos
													 * --------------------------------------------------------
													**/
													if ( isset($rs_resp_anexo)  && count($rs_resp_anexo)>=1 ){
														$list_anexos = '';
														foreach ($rs_resp_anexo AS $itemRespAnexo){
																$dranx_id				= (int)$itemRespAnexo->dranx_id;
																$dranx_arquivo		= $itemRespAnexo->dranx_arquivo;
																
																$image_path = $this->config->item('folder_images') .'denuncias/'. $dranx_arquivo;
																if( file_exists($image_path) and is_file($image_path) )
																{
																	$load_image = base_url($image_path);
																	$list_anexos .= '<a href="'. $load_image .'" target="_blank" class="btn btn-primary">'. $dranx_arquivo .'</a>';
																};
														}
													?>
													<div class="direct-chat-anexos">
														<div class="pull-left"><h3>Anexos: </h3></div>
														<div class="pull-left texto"><?php echo($list_anexos); ?></div>
														<div class="clearfix"></div>
													</div>
													<?php } ?>

												</div>
											</div>

										</li>
										<!-- END timeline item -->
										<?php
											}; // foreach
										};// isset : rs_list
										?>



										<?php if( $formEdicao == true ){ ?>

										<?php
											$mg_top_item = "";
											if( $session_level == "administrador" OR $session_level == "empresa" ){
												$mg_top_item = "margin-top:10px !important;";
										?>
										<!-- timeline item -->
										<li class="timeline-formulario">
											<div class="timeline-item" style="background: none;">
												<h3 class="timeline-header">
													<input type="checkbox" name="dresp_aprovada" ID="dresp_aprovada" value="1"  class="minimal"  /> 
													&nbsp; APROVAR A RESPOSTA DA EMPRESA QUE SERÁ ENVIADA AO AUTOR DA DENÚNCIA?
												</h3>
											</div>
										</li>
										<?php
											} // if session_level
										?>

										<li class="timeline-formulario" style="<?php echo($mg_top_item); ?>">
											<i class="fa fa-edit bg-blue"></i>
											<div class="timeline-item">
												<h3 class="timeline-header">RESPONDER</h3>
												<div class="timeline-body">
													<textarea name="den_descricao_resp" id="den_descricao_resp" class="fld_ckeditor" rows="10" cols="80" style="width:100%;resize:none;"></textarea>	
												</div>
												<div class="timeline-footer hide">
													<a class="btn btn-primary btn-xs">Read more</a>
													<a class="btn btn-danger btn-xs">Delete</a>
												</div>
											</div>
										</li>
										<!-- END timeline item -->

										<!-- timeline item : anexos -->
										<li class="timeline-formularios">
											<div class="timeline-item" style="background: none; padding: 8px;">
												<div class='col-xs-6' style="padding-left: 0px;">
													<div class="form-group" style="margin:0;">
														<input type="file" name="uplArqFileUpl" id="uplArqFileUpl" class="fld_file" />
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="clearfix"></div>
											</div>
										</li>
										<!-- END timeline item -->

										<?php } ?>






										<!-- timeline item -->
										<li class="" style="margin-top:30px;">
											<i class="fa fa-genderless bg-gray"></i>
										</li>
										<!-- END timeline item -->

									</ul>

									<br />
									<br />
									<br />


									<div class="direct-chat-messages hide" style="height: auto;">
										<div class="direct-chat-msg">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left">Alexander Pierce</span>
                        <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                      </div>
                      <!-- /.direct-chat-info -->
                      <img class="direct-chat-img" src="assets/img/user2-160x160.jpg" alt="message user image"><!-- /.direct-chat-img -->
                      <div class="direct-chat-text">
                        Is this template really for free? That's unbelievable!
                      </div>
                      <!-- /.direct-chat-text -->
                    </div>
									</div>



								</div>

							</div>

						</div>
					</div>
				</div><!-- /.row -->

			</section><!-- /.content -->
		</FORM>
    
  </div>



	<?php $this->load->view( "includes/footer" ); ?>

	<?php $this->load->view( "includes/scripts" ); ?>
