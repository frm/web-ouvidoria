<?php $this->load->view( "includes/doctype" ); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


	<?php $this->load->view( "includes/header" ); ?>

  <?php $this->load->view( "includes/menu" ); ?>



  <div class="content-wrapper">

    <section class="content-header">
      <h1>
        Gerenciar Analistas
        <small>Listagem</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="javascript:;"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="javascript:;">Analistas</a></li>
        <li class="active">Listagem</li>
      </ol>
    </section>


		<section class="contents" style="min-height: auto !important; padding:15px 15px 15px 15px !important;">
				<!-- botoes de acoes -->
				<div class="row" style="margin-bottom:0px;">

					<div class="col-xs-6" >
						<!--## FILTRO PARA PESQUISA -->
						<FORM action="<?php echo( $url["url_list"] ); ?>" method="POST" name="frmFiltroPesquisa" id="frmFiltroPesquisa">
							<input type="hidden" name="baseAcao" id="baseAcao" value="FILTRO-PESQUISA">

							<div class="row">
								<div class="col-xs-6">
									<div class="form-group" style="margin-bottom:0px !important;">
										<label for="exampleInputEmail1">Palavra Chave</label>
										<input type="text" name="bsc_search" id="bsc_search" class="form-control" value="<?php echo(isset($bsc_search)? $bsc_search : ""); ?>">
									</div>
								</div>
								<div class="col-xs-2 hide">
									<div class="form-group" style="margin-bottom:0px !important;">
										<label>Categoria</label>
										<select name="bsc_categoria" id="bsc_categoria" class="form-control">
											<option value="">- selecione -</option>
											<?php
											//$cbo = '';
											//if ( isset($rsCateg) ){
												//foreach ($rsCateg AS $item){
													//$idcategoria	= $item->idcategoria;
													//$strtitulo		= $item->strtitulo;
													//$selected		= ($idcategoria == $bsc_categoria)?"selected":"";
													//$cbo .= '<option value="'. $idcategoria .'" '. $selected .'>'. $strtitulo .'</option>';
												//}
												//print $cbo;
											//}// isset : rsCateg
											?>
										</select>
									</div>
								</div>
								<div class="col-xs-2 no-padding">
									<div class="form-group" style="margin-bottom:0px !important;">
										<label>&nbsp;</label><br />
										<button class="btn btn-primary clsSearchPages" data-url="<?php echo(current_url()); ?>" data-form="#frmFiltroPesquisa" >Buscar</button>
									</div>
								</div>
							</div>
						</FORM>
						<!-- +++++++++++++++++++++++++++++++++ FILTRO PARA PESQUISA -->
					</div><!-- /.col -->

					<div class="col-xs-6" >

						<div class="form-group pull-right" style="margin-bottom:0px !important;">
							<label for="exampleInputEmail1">&nbsp;</label><br />
							<div class="pull-right">
								<button class="btn btn-info mrBtnAddNovo" rel="<?php echo( $url["url_form"] ); ?>"><i class="fa fa-edit"></i> Criar Novo Registro</button>
								<button class="btn btn-danger mrBtnDelReg"><i class="fa fa-trash-o"></i> Excluir</button>
							</p>
						</div>
						<div class="clear"></div>
					
					</div><!-- /.col -->
				</div><!-- /.row // botoes de acoes-->

				<div class="clear"></div>
				<div style="border-bottom: 1px solid #D8D8D8;"></div>
		</section>


		<!--## INI : FORMULARIO -->
		<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmFormList" id="frmFormList" enctype="multipart/form-data">
			<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORMULARIO">

			<section class="content">

				<div class="row">
					<div class="col-xs-12">

						<div class="box">
							<div class="box-header">
								<div class="row">
									<div class="col-md-9">
										<h3 class="box-title">Lista de analistas</h3>
									</div>
									<div class="col-md-3 ">
										<div class="input-group input-group-sm hide">
											<input type="text" name="table_search" class="form-control pull-right" placeholder="Pesquisar">
											<div class="input-group-btn">
												<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<?php echo( (isset($paginacao)? $paginacao : "") ); ?>

								<table id="tableList" class="table table-bordered table-hover">
									<thead>
									<tr>
										<th class="center" style="width: 50px;">#</th>
										<th style="width: 70px;">ID</th>
										<th style="width: 250px;">Nome do analista</th>
										<th>E-mail</th>
										<th class="center" style="width: 140px;">Data</th>
										<th class="center" style="width: 80px;">Ativo</th>
										<th class="center" style="width: 90px;">Editar</th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ( isset($rs_list) ){
										foreach ($rs_list AS $item){
											$ana_id						= (int)$item->ana_id;
											$ana_nome					= $item->ana_nome;
											$ana_email				= $item->ana_email;
											$ana_dte_cadastro	= fct_formatdate($item->ana_dte_cadastro, 'd/m/Y H:i');
											$icon_ativo				= ($item->ana_ativo==1)?"sim":"não";
											$redirect					= $url["url_form"] ."/". $ana_id;
									?>
									<tr>
										<td class="center">
											<input type="checkbox" name="chk_delete[]" ID="chk_delete_<?php echo($ana_id); ?>" value="<?php echo($ana_id); ?>" class="minimal itemdelete">
										</td>
										<td><?php echo($ana_id); ?></td>
										<td><?php echo($ana_nome); ?></td>
										<td><?php echo($ana_email); ?></td>
										<td class="center"><?php echo($ana_dte_cadastro); ?></td>
										<td class="center"><?php echo($icon_ativo); ?></td>
										<td class="center">
											<div style="padding:0 10px; display:inline-block">
												<a href="<?php echo($redirect); ?>"><i class="fa fa-edit fa-ft15px"></i></a>
											</div>
											<!--<div style="padding:0 10px; display:inline-block">
												<a href="<?php echo($redirect); ?>"><i class="fa fa-trash-o fa-ft15px"></i></a>
											</div>-->
										</td>
									</tr>
									<?php
										};
									};// isset : rs_list
									?>
									</tbody>
									<!--
									<tfoot>
									<tr>
										<th>ID</th>
										<th>Razão Social</th>
										<th>E-mail</th>
										<th>Data</th>
										<th>Ativo</th>
									</tr>
									</tfoot>
									-->
								</table>

								<?php echo( (isset($paginacao)? $paginacao : "") ); ?>
							</div>
							<!-- /.box-body -->
						</div><!-- /.box -->
					</div>
					<!-- /.col -->
				</div>

			</section><!-- /.content -->
		</FORM>
		<!--## END : FORMULARIO -->
    
  </div>



	<?php $this->load->view( "includes/footer" ); ?>

	<?php $this->load->view( "includes/scripts" ); ?>
