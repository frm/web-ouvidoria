<?php $this->load->view( "includes/doctype" ); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


	<?php $this->load->view( "includes/header" ); ?>

  <?php $this->load->view( "includes/menu" ); ?>



  <div class="content-wrapper">


    <section class="content-header">
      <h1>
        Gerenciar Denúncias
        <small>Formulário</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="javascript:;"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="javascript:;">Denúncias</a></li>
        <li class="active">Formulário</li>
      </ol>
    </section>


		<section class="contents" style="min-height: auto !important; padding:15px 15px 15px 15px !important;">
				<!-- botoes de acoes -->
				<div class="row" style="margin-bottom:0px;">
					<div class="col-xs-12" >
						<p class="pull-right">
							<button class="btn btn-info mrBtnAddNovo hide" rel="<?php echo( $url["url_form"] ); ?>"><i class="fa fa-edit"></i> Criar Novo Registro</button>
							<button class="btn btn-warning mrBtnListReg" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-list-alt"></i> Lista de Registros</button>
							<button class="btn btn-success mrBtnSaveReg" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-check-square-o"></i> Salvar</button>
						</p>
						<div class="clear"></div>
					</div><!-- /.col -->
				</div><!-- /.row // botoes de acoes-->
				<div style="border-bottom: 1px solid #D8D8D8;"></div>
		</section>


		<!--## INÍCIO DO FORMULÁRIO -->
		<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmFormulario" id="frmFormulario" enctype="multipart/form-data">
			<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORMULARIO">
			<input type="hidden" name="den_id" ID="den_id" value="<?php echo(isset($rs_bd->den_id)? $rs_bd->den_id : ""); ?>">

			<section class="content">

				<div class="row">
					<div class='col-xs-12'>
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#tab-mod_historico" data-toggle="tab">Histórico</a>
								</li>
							</ul>

							<div class="tab-content">

								<!-- tab : tab-content -->
								<div class="tab-pane active" id="tab-mod_historico">


									<div class="row">

										<div class="col-md-5">
											<div class="box box-widget widget-user-2 custom">
												<div class="widget-user-header bg-yellow-custom">
													<div class="widget-user-image">
														<img class="img-circle" src="assets/img/avatar-user-01.png" alt="User Avatar">
													</div>
													<?php
														$dnt_json = isset($rs_bd->dnt_json)?$rs_bd->dnt_json:'';
														$json = json_decode($dnt_json, true);
														//fct_print_debug( $json );
														if( is_array($json) && count($json)>0 ){
													?>
													<h3 class="widget-user-username"><?php echo( ($json["dnt_nome"]) ); ?></h3>
													<h5 class="widget-user-desc">
														<div><?php echo($json["dnt_email"]) ?></div>
														<div><?php echo($json["dnt_cargo"]) ?></div>
														<div><?php echo($json["dnt_telefone"]) ?></div>
													</h5>
													<?php 
														}else{
													?>
													<h3 class="widget-user-username">Autor não identificado</h3>
													<h5 class="widget-user-desc"></h5>
													<?php 
														}// $json
													?>
												</div>
											</div>
										</div>



										<div class="col-md-7">
											<div class="row">

												<div class="col-md-6">
													<div class="box box-widget widget-user-2 custom3">
														<div class="widget-user-header bg-yellow-custom" style="min-height: 86px !important;">
															<?php
																$den_num_protocolo = isset($rs_bd->den_num_protocolo)?$rs_bd->den_num_protocolo:'';
															?>
															<h5 class="widget-user-desc">Protocolo</h5>
															<h3 class="widget-user-username bold"><?php echo($den_num_protocolo); ?></h3>
														</div>
														<?php
															$den_status = isset($rs_bd->den_status) ? $rs_bd->den_status : '';
															$status_titulo = isset($rs_bd->den_status) ? $this->cfg_den_status[$den_status]["titulo"] : '';
															$status_color = isset($rs_bd->den_status) ? $this->cfg_den_status[$den_status]["color"] : '';
															//$color_status = $this->cfg_den_status[$den_status]["color"];
														?>
														<div class="box-footer no-padding <?php echo($status_color); ?>" style="/*background-color: #DFFFA4 !important;*/">
															<ul class="nav nav-stacked" >
																<li>
																	<h3 class="widget-user-username bold" style="text-align:center;"><?php print $status_titulo; ?></h3>
																</li>
															</ul>
														</div>
													</div>
												</div>


												<?php
												$redirect = '';
												$den_parent_id = (int)isset($rs_bd->den_parent_id)?$rs_bd->den_parent_id:'';
												if( $den_parent_id>0 )
												{
													$redirect = $url["url_form"] ."/". $den_parent_id;
													$den_num_protocolo_relac = isset($rs_bd->den_num_protocolo_relac)?$rs_bd->den_num_protocolo_relac:'';
												?>
												<div class="col-md-6">
													<div class="box box-widget widget-user-2 custom4">
														<div class="widget-user-header bg-yellow-custom">
															<h5 class="widget-user-desc">Denúncia Relacionada</h5>
															<h3 class="widget-user-username bold"><?php echo($den_num_protocolo_relac); ?></h3>
														</div>
													</div>
												</div>
												<?php } ?>

											</div>
										</div>

									</div>


									<ul class="timeline timeline-inverse">

										<!-- timeline time label -->
										<li class="time-label">

											<span class="bg-red">
												<i class="fa fa-clock-o"></i> &nbsp;&nbsp;
												<?php
													//$den_num_protocolo = isset($rs_bd->den_num_protocolo)?$rs_bd->den_num_protocolo:'';
													//print $den_num_protocolo;
													//print '&nbsp;&nbsp; : &nbsp;&nbsp;';

													$den_dte_cadastro	= fct_formatdate($rs_bd->den_dte_cadastro, 'd.m.Y xx H:i');
													print str_replace("x", "&nbsp;", $den_dte_cadastro);
												?>
											</span>
										</li>
										<!-- /.timeline-label -->

										<!-- timeline item -->
										<li>
											<i class="fa fa-hourglass-2 bg-blue hide"></i>
											<div class="timeline-item">
												<?php
													//<span class="time"><i class="fa fa-clock-o"></i>
													//$den_dte_cadastro	= fct_formatdate($rs_bd->den_dte_cadastro, 'd.m.Y H:i');
													//print $den_dte_cadastro;
												?>
												</span>
												<a class="btn btn-warning btn-flat btn-xs hide" style="
												float: right;
												padding: 9px;
												"><?php
												$den_status = isset($rs_bd->den_status)?$rs_bd->den_status:'';
												print $this->cfg_den_status[$den_status]["titulo"];
												?></a>
												

												<h3 class="timeline-header">
												<?php
													//$den_num_protocolo = isset($rs_bd->den_num_protocolo)?$rs_bd->den_num_protocolo:'';
													//print 'protocolo número: <strong>'. $den_num_protocolo .'</strong>';
													//print '&nbsp;&nbsp; : &nbsp;&nbsp;';
													print isset($rs_bd->nat_titulo)?$rs_bd->nat_titulo:'';
												?>
												</h3>
												<div class="timeline-body">
													<?php echo(isset($rs_bd->den_descricao)? $rs_bd->den_descricao : ""); ?>
												</div>
												<div class="timeline-footer hide">
													<a class="btn btn-primary btn-xs">Read more</a>
													<a class="btn btn-danger btn-xs">Delete</a>
												</div>
											</div>
										</li>
										<!-- END timeline item -->


										<!-- timeline time label -->
										<li class="time-label timeline-respostas">
											<span class="bg-red">
												<?php print 'Histórico de Respostas'; ?>
											</span>
										</li>
										<!-- /.timeline-label -->
									<?php
									/**
									 * --------------------------------------------------------
									 * respostas
									 * --------------------------------------------------------
									**/
									if ( isset($rs_den_resp) ){
										foreach ($rs_den_resp AS $itemResp){
											$dresp_id						= (int)$itemResp->dresp_id;
											$dresp_descricao		= $itemResp->dresp_descricao;
											$dresp_status				= $itemResp->dresp_status;
											$dresp_autor				= $itemResp->dresp_autor;											
											$dresp_dte_cadastro	= fct_formatdate($itemResp->dresp_dte_cadastro, 'd.m.Y H:i');

											$css_status					= ($dresp_autor == 'analista' ? '' : 'status-analista' ); 
											$icon_status				= ($dresp_autor == 'analista' ? 'fa-check-square-o' : 'fa-hourglass-2' );
											$align_item					= ($dresp_autor == 'analista' ? '' : 'right' );
											$analista_title			= ($dresp_autor == 'analista' ? 'Analista' : 'Denunciante' );
											$icon_avatar				= ($dresp_autor == 'analista' ? 'avatar-canal-01.png' : 'avatar-user-01.png' );
									?>
										<!-- timeline time label -->
										<li class="time-label <?php print $css_status; ?> hide">
											<span class="bg-green">
												<?php print $dresp_dte_cadastro; ?>
											</span>
										</li>
										<!-- /.timeline-label -->

										<!-- timeline item -->
										<li class=" <?php print $css_status; ?> ">
											<i class="fa <?php print $icon_status; ?> bg-green hide"></i>
											<div class="timeline-item hide">
												<?php
													//<span class="time"><i class="fa fa-clock-o"></i>
													//$den_dte_cadastro	= fct_formatdate('2017-07-03 09:03:00', 'd.m.Y H:i');
													//print $den_dte_cadastro;
												?>
												</span>
												<a class="btn btn-success btn-flat btn-xs" style="
												float: right;
												padding: 9px;
												"><?php
												print $this->cfg_den_status[$dresp_status]["titulo"];
												?></a>
												
												<h3 class="timeline-header">
												<?php
													print 'resposta enviada em: <strong>'. $dresp_dte_cadastro .'</strong>';
												?>
												</h3>
												<div class="timeline-body">
													<?php print $dresp_descricao; ?>
												</div>
												<div class="timeline-footer hide">
													<a class="btn btn-primary btn-xs">Read more</a>
													<a class="btn btn-danger btn-xs">Delete</a>
												</div>
											</div>

											<div class="direct-chat-messages direct-chat-warning" style="height: auto; margin-left: 60px;">
												<div class="direct-chat-msg <?php echo($align_item); ?>">
													<div class="direct-chat-info clearfix" style="font-size: 16px;">
														<span class="direct-chat-name pull-left"><?php echo($analista_title); ?></span>
														<span class="direct-chat-timestamp pull-right">
															<?php
																print 'resposta enviada em: <strong>'. $dresp_dte_cadastro .'</strong>';
															?>
														</span>
													</div>
													<img class="direct-chat-img" src="assets/img/<?php echo($icon_avatar); ?>" alt="message user image">
													<div class="direct-chat-text bgcustom">
														<?php print $dresp_descricao; ?>
													</div>
												</div>
											</div>

										</li>
										<!-- END timeline item -->
										<?php
											}; // foreach
										};// isset : rs_list
										?>





										<!-- timeline item -->
										<li class="timeline-formulario">
											<i class="fa fa-edit bg-blue"></i>
											<div class="timeline-item">
												<h3 class="timeline-header">RESPONDER</h3>
												<div class="timeline-body">
													<textarea name="den_descricao_resp" id="den_descricao_resp" class="fld_ckeditor" rows="10" cols="80" style="width:100%;"></textarea>	
												</div>
												<div class="timeline-footer hide">
													<a class="btn btn-primary btn-xs">Read more</a>
													<a class="btn btn-danger btn-xs">Delete</a>
												</div>
											</div>
										</li>
										<!-- END timeline item -->







										<!-- timeline item -->
										<li class="hide">
											<i class="fa fa-user bg-aqua"></i>

											<div class="timeline-item">
												<span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>

												<h3 class="timeline-header no-border"><a href="javascript:;">Sarah Young</a> accepted your friend request
												</h3>
											</div>
										</li>
										<!-- END timeline item -->

										<!-- timeline item -->
										<li class="hide">
											<i class="fa fa-comments bg-yellow"></i>

											<div class="timeline-item">
												<span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

												<h3 class="timeline-header"><a href="javascript:;">Jay White</a> commented on your post</h3>

												<div class="timeline-body">
													Take me to your leader!
													Switzerland is small and neutral!
													We are more like Germany, ambitious and misunderstood!
												</div>
												<div class="timeline-footer">
													<a class="btn btn-warning btn-flat btn-xs">View comment</a>
												</div>
											</div>
										</li>
										<!-- END timeline item -->
										<!-- timeline time label -->
										<li class="time-label hide">
													<span class="bg-green">
														3 Jan. 2014
													</span>
										</li>
										<!-- /.timeline-label -->
										<!-- timeline item -->
										<li class="hide">
											<i class="fa fa-camera bg-purple"></i>

											<div class="timeline-item">
												<span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>

												<h3 class="timeline-header"><a href="javascript:;">Mina Lee</a> uploaded new photos</h3>

												<div class="timeline-body">
													<img src="http://placehold.it/150x100" alt="..." class="margin">
													<img src="http://placehold.it/150x100" alt="..." class="margin">
													<img src="http://placehold.it/150x100" alt="..." class="margin">
													<img src="http://placehold.it/150x100" alt="..." class="margin">
												</div>
											</div>
										</li>
										<!-- END timeline item -->
										<li class="">
											<i class="fa fa-genderless bg-gray"></i>
										</li>
									</ul>

									<br />
									<br />
									<br />


									<div class="direct-chat-messages hide" style="height: auto;">
										<div class="direct-chat-msg">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left">Alexander Pierce</span>
                        <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                      </div>
                      <!-- /.direct-chat-info -->
                      <img class="direct-chat-img" src="assets/img/user2-160x160.jpg" alt="message user image"><!-- /.direct-chat-img -->
                      <div class="direct-chat-text">
                        Is this template really for free? That's unbelievable!
                      </div>
                      <!-- /.direct-chat-text -->
                    </div>
									</div>



								</div>

							</div>

						</div>
					</div>
				</div><!-- /.row -->

			</section><!-- /.content -->
		</FORM>
    
  </div>



	<?php $this->load->view( "includes/footer" ); ?>

	<?php $this->load->view( "includes/scripts" ); ?>
