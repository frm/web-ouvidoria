<?php $this->load->view( "includes/doctype" ); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


	<?php $this->load->view( "includes/header" ); ?>

  <?php $this->load->view( "includes/menu" ); ?>


  <div class="content-wrapper">

    <section class="content-header">
      <h1>
        Relatórios
      </h1>
      <ol class="breadcrumb">
        <li><a href="javascript:;"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Relatórios</li>
      </ol>
    </section>



		<section class="contents" style="min-height: auto !important; padding:15px 15px 15px 15px !important;">
				<!-- botoes de acoes -->
				<div class="row" style="">

					<div class="col-xs-12" >
						<!--## FILTRO PARA PESQUISA -->
						<FORM action="<?php echo( $url["url_list"] ); ?>" method="POST" name="frmFiltroPesquisa" id="frmFiltroPesquisa">
							<input type="hidden" name="baseAcao" id="baseAcao" value="FILTRO-PESQUISA">

							<div class="row" style="margin-top:10px;">
								<div class="col-xs-3">
									<div class="form-group" style="margin-bottom:0px !important;">
										<label for="bsc_empresa">Empresa</label>
										<?php
										$bsc_empresa = isset($bsc_empresa)?$bsc_empresa:0;
										?>
										<select name='bsc_empresa' id='bsc_empresa' class="form-control">
											<option value="" >- selecione -</option>
											<?php
											if ( isset($rs_list_emp) ){
												foreach ($rs_list_emp AS $item){
													$selected = ($bsc_empresa == (int)$item->emp_id) ? ' selected ' : ''; 
													//$selected = ''; 
											?>
											<option value="<?php echo((int)$item->emp_id); ?>" <?php echo($selected); ?> ><?php echo($item->emp_razao_social); ?></option>
											<?php
												}// foreach
											}// if
											?>
										</select>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group" style="margin-bottom:0px !important;">
										<label for="exampleInputEmail1">Analista</label>
										<?php
										$bsc_analista = isset($bsc_analista)?$bsc_analista:0;
										?>
										<select name='bsc_analista' id='bsc_analista' class="form-control">
											<option value="" >- selecione -</option>
											<?php
											if ( isset($rs_list_ana) ){
												foreach ($rs_list_ana AS $item){
													$selected = ($bsc_analista == (int)$item->ana_id) ? ' selected ' : ''; 
											?>
											<option value="<?php echo((int)$item->ana_id); ?>" <?php echo($selected); ?> ><?php echo($item->ana_nome); ?></option>
											<?php
												}// foreach
											}// if
											?>
										</select>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group" style="margin-bottom:0px !important;">
										<label for="exampleInputEmail1">Natureza</label>
										<?php
										$bsc_natureza = isset($bsc_natureza)?$bsc_natureza:0;
										?>
										<select name='bsc_natureza' id='bsc_natureza' class="form-control">
											<option value="" >- selecione -</option>
											<?php
											if ( isset($rs_list_nat) ){
												foreach ($rs_list_nat AS $item){
													$selected = ($bsc_natureza == (int)$item->nat_id) ? ' selected ' : ''; 
											?>
											<option value="<?php echo((int)$item->nat_id); ?>" <?php echo($selected); ?> ><?php echo($item->nat_titulo); ?></option>
											<?php
												}// foreach
											}// if
											?>
										</select>
									</div>
								</div>
							</div>

							<div class="row" style="margin-top:10px;">
								<div class="col-xs-3">
									<div class="form-group" style="margin-bottom:0px !important;">
										<label for="bsc_protocolo">Protocolo</label>
										<input type="text" name="bsc_protocolo" id="bsc_protocolo" class="form-control" value="<?php echo(isset($bsc_search)? $bsc_search : ""); ?>">
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group" style="margin-bottom:0px !important;">
										<label for="bsc_dte_ini_dia">Data Inicial</label>

										<div class="row">
											<div class="col-xs-12">
												<div class="col-xs-4 no-padding">
													<input type="text" name="bsc_dte_ini_dia" id="bsc_dte_ini_dia" class="form-control autotab" value="<?php echo(isset($bsc_dte_ini_dia)? $bsc_dte_ini_dia : ""); ?>" maxlength="2" placeholder="dia" />
												</div>
												<div class="col-xs-4 no-padding">
													<input type="text" name="bsc_dte_ini_mes" id="bsc_dte_ini_mes" class="form-control autotab" value="<?php echo(isset($bsc_dte_ini_mes)? $bsc_dte_ini_mes : ""); ?>" maxlength="2" placeholder="mês" />
												</div>
												<div class="col-xs-4 no-padding">
													<input type="text" name="bsc_dte_ini_ano" id="bsc_dte_ini_ano" class="form-control autotab" value="<?php echo(isset($bsc_dte_ini_ano)? $bsc_dte_ini_ano : ""); ?>" maxlength="4" placeholder="ano" />
												</div>
											</div>
										</div>

									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group" style="margin-bottom:0px !important;">
										<label for="bsc_dte_fim_dia">Data Final</label>

										<div class="row">
											<div class="col-xs-12">
												<div class="col-xs-4 no-padding">
													<input type="text" name="bsc_dte_fim_dia" id="bsc_dte_fim_dia" class="form-control autotab" value="<?php echo(isset($bsc_dte_fim_dia)? $bsc_dte_fim_dia : ""); ?>" maxlength="2" placeholder="dia" />
												</div>
												<div class="col-xs-4 no-padding">
													<input type="text" name="bsc_dte_fim_mes" id="bsc_dte_fim_mes" class="form-control autotab" value="<?php echo(isset($bsc_dte_fim_mes)? $bsc_dte_fim_mes : ""); ?>" maxlength="2" placeholder="mês" />
												</div>
												<div class="col-xs-4 no-padding">
													<input type="text" name="bsc_dte_fim_ano" id="bsc_dte_fim_ano" class="form-control autotab" value="<?php echo(isset($bsc_dte_fim_ano)? $bsc_dte_fim_ano : ""); ?>" maxlength="4" placeholder="ano" />
												</div>
											</div>
										</div>

									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group" style="margin-bottom:0px !important;">
										<label for="exampleInputEmail1">Tipo de Relatório</label>
										<?php
										$bsc_tipo_relat = isset($bsc_tipo_relat) ? $bsc_tipo_relat : "";
										$bsc_tipo_relat = !empty($bsc_tipo_relat) ? $bsc_tipo_relat : "quantitativo";
										?>
										<select name='bsc_tipo_relat' id='bsc_tipo_relat' class="form-control">
											<option value="" >- selecione -</option>
											<option value="quantitativo" <?php echo($bsc_tipo_relat=="quantitativo"? "selected":""); ?>>Quantitativo</option>
											<option value="quantitativo-status" <?php echo($bsc_tipo_relat=="quantitativo-status"? "selected":""); ?>>Quantitativo por status</option>
											<option value="analistas" <?php echo($bsc_tipo_relat=="analistas"? "selected":""); ?>>Analistas</option>
										</select>
									</div>
								</div>
							</div>

							<div class="row" style="margin-top:10px;">
								<div class="col-xs-12">
									<div class="form-group" style="margin: 0 auto;">
										<button class="btn btn-primary clsSearchPages" data-url="<?php echo(current_url()); ?>" data-form="#frmFiltroPesquisa" >Buscar</button>
									</div>
								</div>
							</div>
						</FORM>
						<!-- +++++++++++++++++++++++++++++++++ FILTRO PARA PESQUISA -->
					</div><!-- /.col -->

				</div><!-- /.row // botoes de acoes-->

				<div class="clear"></div>
				<div style="margin-top:15px; border-bottom: 1px solid #D8D8D8;"></div>
		</section>


	<?php $this->load->view( "relatorios/". $bsc_tipo_relat ); ?>

