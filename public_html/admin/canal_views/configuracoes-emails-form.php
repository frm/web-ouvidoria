<?php $this->load->view( "includes/doctype" ); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


	<?php $this->load->view( "includes/header" ); ?>

  <?php $this->load->view( "includes/menu" ); ?>



  <div class="content-wrapper">


    <section class="content-header">
      <h1>
        Gerenciar Configurações / Emails
        <small>Formulário</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="javascript:;"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="javascript:;">Emails</a></li>
        <li class="active">Formulário</li>
      </ol>
    </section>


		<!--## INÍCIO DO FORMULÁRIO -->
		<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmFormulario" id="frmFormulario" enctype="multipart/form-data">
			<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORMULARIO">
			<input type="hidden" name="nat_id" ID="nat_id" value="<?php echo(isset($rs_bd->nat_id)? $rs_bd->nat_id : ""); ?>" />

			<section class="content">

				<!-- botoes de acoes -->
				<div class="row" style="margin-bottom:15px;">
					<div class="col-xs-12" >
						<p style="float:right;">
							<button class="btn btn-info mrBtnAddNovo" rel="<?php echo( $url["url_form"] ); ?>"><i class="fa fa-edit"></i> Criar Novo Registro</button>
							<button class="btn btn-warning mrBtnListReg" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-list-alt"></i> Lista de Registros</button>
							<button class="btn btn-success mrBtnSaveReg2" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-check-square-o"></i> Salvar</button>
						</p>

						<div class="clear"></div>
						<div style="border-bottom: 1px solid #D8D8D8;"></div>
					</div><!-- /.col -->
				</div><!-- /.row // botoes de acoes-->


				<?php if( $this->session->flashdata('message_validate') ) { ?>
					<div class="row">
						<div class="col-lg-12">
							<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<h4><i class="icon fa fa-check"></i></i> Atenção!</h4>
								<?php echo( $this->session->flashdata('message_validate') ); ?>
							</div>
						</div>
					</div>
				<?php } ?>


				<div class="row">
					<div class='col-xs-12'>
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active ">
									<a href="#tab-mod_formulario" data-toggle="tab">Detalhes Principais</a>
								</li>
							</ul>

							<div class="tab-content">
								
								<!-- tab : tab-mod_formulario -->
								<div class="tab-pane active " id="tab-mod_formulario">
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
									<div class="row">

										<div class="col-md-8">
											<!-- box principal --> 
											<div class="box slim box-primary">
												<div class="box-header with-border">
													<h3 class="box-title">Informações</h3>
												</div><!-- /.box-header -->

												<div class="box-body">

													<div class="row" >
														<div class="col-xs-6">
															<div class="form-group">
																<label for="cfg_area">Área</label>
																<input type="text" name="cfg_area" id="cfg_area" class="form-control" placeholder="" value="<?php echo(isset($rs_bd->cfg_area)? $rs_bd->cfg_area : ""); ?>" />
															</div>
														</div>

														<div class="col-xs-6">
															<div class="form-group">
																<label for="cfg_chave">Chave</label>
																<input type="text" name="cfg_chave" id="cfg_chave" class="form-control" value="<?php echo(isset($rs_bd->cfg_chave)? $rs_bd->cfg_chave : ""); ?>" />
															</div>
														</div>
													</div><!-- /.row -->

													<?php 
														$cfg_json = isset($rs_bd->cfg_json)? $rs_bd->cfg_json : "";
														$cfg_json = json_decode($cfg_json);
													?>
													<div class="row" >
														<div class="col-xs-12">
															<div class="form-group">
																<label for="cfg_json">Destinatários (separados com ponto e vírgula ";")</label>
																<textarea name="cfg_json" id="cfg_json" class="form-control" rows="3" cols="80" style="width:100%;resize:none;"><?php echo($cfg_json); ?></textarea> 
															</div>
														</div>
													</div><!-- /.row -->


													<div class="row" >
														<div class="col-xs-12">

															<div class="callout callout-warning">
																<h4>Tags Especiais</h4>

																<div class="col-xs-6">
																	<strong>formulario de contato</strong> <br />
																	Nome : {cnt_nome}<br />
																	E-mail : {cnt_email}<br />
																	Mensagem : {cnt_mensagem}
																</div>

																<div class="col-xs-6">
																	<strong>outras tags</strong> <br />
																	Nome do analista : {ana_nome} <br />
																	Natureza título: {nat_titulo} <br />
																	Natureza prazo: {nat_prazo} <br />
																</div>

																<div class="clear"></div>
															</div>

														</div>
													</div><!-- /.row -->

													<div class="row" >
														<div class="col-xs-12">
															<div class="form-group">
																<label for="cfg_valor">Descrição</label>
																<textarea name="cfg_valor" id="cfg_valor" class="fld_ckeditor" rows="10" cols="80" style="width:100%;"><?php echo(isset($rs_bd->cfg_valor)? $rs_bd->cfg_valor : ""); ?></textarea> 
															</div>
														</div>
													</div><!-- /.row -->

												</div><!-- /.box-body -->

												<div class="box-footer hide">
													<button type="submit" class="btn btn-primary">Salvar</button>
												</div><!-- /.box-footer -->

											</div><!-- /.box -->
										</div>

										<div class="col-md-4">
											<div class="box slim box-primary">

												<div class="box-header with-border">
													<h3 class="box-title">Status</h3>
												</div><!-- /.box-header -->

												<div class="box-body">
													<div class="form-group smXT">
														<?php
														$cfg_ativo = 1;
														$cfg_ativo = isset($rs_bd->cfg_ativo)?$rs_bd->cfg_ativo:1;
														?>
														<label for="cfg_ativo" class="lblsm">Ativo</label>
														<select name='cfg_ativo' id='cfg_ativo' class="form-control">
															<option value="1" <?php echo($cfg_ativo=="1"? "selected":""); ?>>Sim</option>
															<option value="0" <?php echo($cfg_ativo=="0"? "selected":""); ?>>Não</option>
														</select>
													</div>
												</div><!-- /.box-body -->

												<div class="box-footer hide">
													<button type="submit" class="btn btn-primary">Submit</button>
												</div><!-- /.box-footer -->

											</div>

										</div>

									</div>
									<div class="clear"></div>
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
								</div>

							</div>

						</div>
					</div>
				</div><!-- /.row -->

			</section><!-- /.content -->
		</FORM>
    
  </div>



	<?php $this->load->view( "includes/footer" ); ?>

	<?php $this->load->view( "includes/scripts" ); ?>


	<script>
		jQuery(document).ready(function ($) {
			$(document).on('click', '.mrBtnSaveReg2', function (e) {
				//e.preventDefault();
				var $form	= $('form#frmFormulario');
				var $msg	= '';

				var $emp_codigo = $form.find("#emp_codigo");
				var $emp_razao_social = $form.find("#emp_razao_social");
				var $emp_nome_fantasia = $form.find("#emp_nome_fantasia");
				var $emp_cnpj = $form.find("#emp_cnpj");
				var $emp_email = $form.find("#emp_email");

				if( $emp_codigo.val().length == 0 )					{ $msg += "<p>- Preencha corretamente a código.</p>"; }
				if( $emp_razao_social.val().length == 0 )		{ $msg += "<p>- Preencha corretamente a razão social.</p>"; }
				if( $emp_nome_fantasia.val().length == 0 )	{ $msg += "<p>- Preencha corretamente o nome fantasia.</p>"; }
				if( $emp_cnpj.val().length == 0 )						{ $msg += "<p>- Preencha corretamente o cnpj.</p>"; }
				if( $emp_email.val().length == 0 )					{ $msg += "<p>- Preencha corretamente o e-mail.</p>"; }

				if( $msg.length > 0)
				{
					$.alert({
						title: 'Atenção',
						confirmButtonClass: 'btn-info',
						cancelButtonClass: 'btn-danger',
						confirmButton: 'OK',
						//cancelButton: 'NO never !',
						content: $msg,
						confirm: function () {
							//$.alert('Confirmed!');
						}
					});
					return false;
				}else{
					$form.submit();
				}
			});
		});
	</script>
