<?php $this->load->view( "includes/doctype" ); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


	<?php $this->load->view( "includes/header" ); ?>

  <?php $this->load->view( "includes/menu" ); ?>



  <div class="content-wrapper">

    <section class="content-header">
      <h1>
        Gerenciar Usuários
        <small>Listagem</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="javascript:;"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="javascript:;">Usuários</a></li>
        <li class="active">Listagem</li>
      </ol>
    </section>


		<!--## INI : FORMULARIO -->
		<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmFormList" id="frmFormList" enctype="multipart/form-data">
			<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORMULARIO">

			<section class="content">

				<!-- botoes de acoes -->
				<div class="row" style="margin-bottom:15px;">
					<div class="col-xs-12" >
						<p style="float:right;">
							<button class="btn btn-info mrBtnAddNovo" rel="<?php echo( $url["url_form"] ); ?>"><i class="fa fa-edit"></i> Criar Novo Registro</button>
							<button class="btn btn-danger mrBtnDelReg"><i class="fa fa-trash-o"></i> Excluir</button>
						</p>

						<div class="clear"></div>
						<div style="border-bottom: 1px solid #D8D8D8;"></div>
					</div><!-- /.col -->
				</div><!-- /.row // botoes de acoes-->

				
				<style>
				.line_parent{ display:none; }
				</style>				
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Lista de registros</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<table id="tableList" class="table table-bordered table-hover">
									<thead>
									<tr>
										<th class="center" style="width: 50px;">#</th>
										<th>ID</th>
										<th style="width: 250px;">Nome</th>
										<th>E-mail</th>
										<th class="center" style="width: 140px;">Data</th>
										<th class="center" style="width: 80px;">Ativo</th>
										<th class="center" style="width: 90px;">Ação</th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ( isset($rs_list) ){
										foreach ($rs_list AS $item){
											$usr_id						= (int)$item->usr_id;
											$usr_nome					= $item->usr_nome;
											$usr_email				= $item->usr_email;
											$usr_dte_cadastro	= fct_formatdate($item->usr_dte_cadastro, 'd/m/Y H:i');
											$icon_ativo				= ($item->usr_ativo==1)?"sim":"não";
											$redirect					= $url["url_form"] ."/". $usr_id;
									?>
									<tr id="tr-row-<?php echo($usr_id); ?>">
										<td class="center">
											<input type="checkbox" name="chk_delete[]" ID="chk_delete_<?php echo($usr_id); ?>" value="<?php echo($usr_id); ?>" class="minimal itemdelete">
										</td>
										<td><?php echo($usr_id); ?></td>
										<td>
											<div><?php echo($usr_nome); ?></div>
										</td>
										<td><?php echo($usr_email); ?></td>
										<td class="center"><?php echo($usr_dte_cadastro); ?></td>
										<td class="center"><?php echo($icon_ativo); ?></td>
										<td class="center">
											<div style="padding:0 10px; display:inline-block">
												<a href="<?php echo($redirect); ?>"><i class="fa fa-edit fa-ft15px"></i></a>
											</div>
											<div style="padding:0 10px; display:inline-block">
												<a href="<?php echo($redirect); ?>"><i class="fa fa-trash-o fa-ft15px"></i></a>
											</div>
										</td>
									</tr>
									<?php
										};
									};// isset : rs_list
									?>
									</tbody>
									<!--
									<tfoot>
									<tr>
										<th>ID</th>
										<th>Razão Social</th>
										<th>E-mail</th>
										<th>Data</th>
										<th>Ativo</th>
									</tr>
									</tfoot>
									-->
								</table>
							</div>
							<!-- /.box-body -->
						</div><!-- /.box -->
					</div>
					<!-- /.col -->
				</div>

			</section><!-- /.content -->
		</FORM>
		<!--## END : FORMULARIO -->
    
  </div>



	<?php $this->load->view( "includes/footer" ); ?>

	<?php $this->load->view( "includes/scripts" ); ?>
