<?php $this->load->view( "includes/doctype" ); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


	<?php $this->load->view( "includes/header" ); ?>

  <?php $this->load->view( "includes/menu" ); ?>



  <div class="content-wrapper">


    <section class="content-header">
      <h1>
        Gerenciar Analistas
        <small>Formulário</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="javascript:;"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="javascript:;">Analistas</a></li>
        <li class="active">Formulário</li>
      </ol>
    </section>


		<!--## INÍCIO DO FORMULÁRIO -->
		<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmFormulario" id="frmFormulario" enctype="multipart/form-data">
			<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORMULARIO">
			<input type="hidden" name="ana_id" ID="ana_id" value="<?php echo(isset($rs_bd->ana_id)? $rs_bd->ana_id : ""); ?>">

			<section class="content">

				<!-- botoes de acoes -->
				<div class="row" style="margin-bottom:15px;">
					<div class="col-xs-12" >
						<p style="float:right;">
							<button class="btn btn-info mrBtnAddNovo" rel="<?php echo( $url["url_form"] ); ?>"><i class="fa fa-edit"></i> Criar Novo Registro</button>
							<button class="btn btn-warning mrBtnListReg" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-list-alt"></i> Lista de Registros</button>
							<button class="btn btn-success mrBtnSaveReg2" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-check-square-o"></i> Salvar</button>
						</p>

						<div class="clear"></div>
						<div style="border-bottom: 1px solid #D8D8D8;"></div>
					</div><!-- /.col -->
				</div><!-- /.row // botoes de acoes-->


				<?php if( $this->session->flashdata('message_validate') ) { ?>
					<div class="row">
						<div class="col-lg-12">
							<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<h4><i class="icon fa fa-check"></i></i> Atenção!</h4>
								<?php echo( $this->session->flashdata('message_validate') ); ?>
							</div>
						</div>
					</div>
				<?php } ?>


				<div class="row">
					<div class='col-xs-12'>
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active ">
									<a href="#tab-mod_formulario" data-toggle="tab">Detalhes Principais</a>
								</li>
								<li>
									<a href="#tab-mod_naturezas" data-toggle="tab">Naturezas Relacionadas</a>
								</li>
								<li class="hide">
									<a href="#tab-mod_galeria" data-toggle="tab">Galeria de Fotos</a>
								</li>
							</ul>

							<div class="tab-content">
								
								<!-- tab : tab-mod_formulario -->
								<div class="tab-pane active " id="tab-mod_formulario">
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
									<div class="row">

										<div class="col-md-8">

											<!-- Dados principais --> 
											<div class="box slim box-primary">
												<div class="box-header with-border">
													<h3 class="box-title">Dados principais</h3>
												</div><!-- /.box-header -->

												<div class="box-body">


													<?php if( $session_level == "administrador" ){ ?>
													<div class="row" >
														<div class="col-xs-6">
															<div class="form-group">
																<?php
																$emp_id = 0;
																$emp_id = isset($rs_bd->emp_id)?$rs_bd->emp_id:0;
																?>
																<label for="emp_id">Empresa</label>
																<select name='emp_id' id='emp_id' class="form-control">
																	<option value="" >- selecione -</option>
																	<?php
																	if ( isset($rs_list_emp) ){
																		foreach ($rs_list_emp AS $item){
																			$selected = ($emp_id == (int)$item->emp_id) ? ' selected ' : ''; 
																	?>
																	<option value="<?php echo((int)$item->emp_id); ?>" <?php echo($selected); ?> ><?php echo($item->emp_razao_social); ?></option>
																	<?php
																		}// foreach
																	}// if
																	?>
																</select>
															</div>
														</div>
													</div><!-- /.row -->
													<?php } //if session_level == empresa ?>


													<div class="row" >
														<div class="col-xs-6">
															<div class="form-group">
																<label for="ana_nome">Nome</label>
																<input type="text" name="ana_nome" id="ana_nome" class="form-control" placeholder="" value="<?php echo( (isset($rs_bd->ana_nome)? $rs_bd->ana_nome : '') ); ?>" />
															</div>
														</div>

														<div class="col-xs-6">
															<div class="form-group">
																<label for="ana_cpf">CPF</label>
																<input type="text" name="ana_cpf" id="ana_cpf" class="form-control" data-inputmask="'mask': ['999.999.999-99']" data-mask value="<?php echo( (isset($rs_bd->ana_cpf)? $rs_bd->ana_cpf : '') ); ?>" />
															</div>
														</div>
													</div><!-- /.row -->

													<div class="row" >
														<div class="col-xs-6">
															<div class="form-group">
																<label for="ana_telefone">Telefone</label>
																<input type="text" name="ana_telefone" id="ana_telefone" class="form-control" data-inputmask='"mask": "(99) 99999-9999"' data-mask placeholder="" value="<?php echo( (isset($rs_bd->ana_telefone)? $rs_bd->ana_telefone : '') ); ?>" />
															</div>
														</div>
													</div><!-- /.row -->

												</div><!-- /.box-body -->

												<div class="box-footer hide">
													<button type="submit" class="btn btn-primary">Salvar</button>
												</div><!-- /.box-footer -->

											</div><!-- /.box -->


											<!-- Dados de acesso --> 
											<div class="box slim box-primary">
												<div class="box-header with-border">
													<h3 class="box-title">Dados de acesso</h3>
												</div><!-- /.box-header -->

												<div class="box-body">

													<div class="row" >
														<div class="col-xs-6">
															<div class="form-group">
																<label for="ana_email">E-mail</label>
																<input type="text" name="ana_email" id="ana_email" class="form-control" placeholder="" value="<?php echo( (isset($rs_bd->ana_email)? $rs_bd->ana_email : '') ); ?>" />
															</div>
														</div>

														<div class="col-xs-6">
															<div class="form-group">
																<label for="ana_senha">Senha</label>
																<input type="text" name="ana_senha" id="ana_senha" class="form-control" placeholder="">
															</div>
														</div>
													</div><!-- /.row -->

													<div class="row" >
														<div class="col-xs-12">
															<div class="form-group">
																<div class="checkbox" style="margin-top: 0px;">
																	<label style="padding-left: 0px;">
																		<input type="checkbox" name="ana_sendmail" id="ana_sendmail" class="minimal" value="1"> Enviar e-mail para alterar senha de acesso
																	</label>
																</div>
															</div>
														</div>
													</div><!-- /.row -->

												</div><!-- /.box-body -->

												<div class="box-footer hide"></div><!-- /.box-footer -->
											</div><!-- /.box -->

											
											<!-- Outras formas de contato --> 
											<div class="box slim box-primary">
												<div class="box-header with-border">
													<h3 class="box-title">Outras formas de contato</h3>
												</div><!-- /.box-header -->

												<div class="box-body">
													<?php
													$x_acnt = 0;
													if ( isset($rs_list_acnt) && count($rs_list_acnt) >= 1 ){
														foreach ($rs_list_acnt AS $item){
															$x_acnt++;
															$acnt_id				= (int)$item->acnt_id;
															$acnt_email			= $item->acnt_email;
															$acnt_telefone	= $item->acnt_telefone;
													?>
													<div class="row tplRowContrato" id="tplRowContrato-<?php echo($x_acnt); ?>" >
														<input type="hidden" name="acnt_id[]" id="acnt_id_<?php echo($x_acnt); ?>" value="<?php echo($acnt_id); ?>" />

														<div class="col-xs-6">
															<div class="form-group">
																<label for="acnt_email">Email</label>
																<input type="text" name="acnt_email[]" id="acnt_email_<?php echo($x_acnt); ?>" class="form-control" placeholder="" value="<?php echo($acnt_email); ?>" />
															</div>
														</div>

														<div class="col-xs-3">
															<div class="form-group">
																<label for="acnt_telefone">Telefone</label>
																<input type="text" name="acnt_telefone[]" id="acnt_telefone_<?php echo($x_acnt); ?>" class="form-control inputmask" data-inputmask='"mask": "(99) 99999-9999"' data-mask placeholder="" value="<?php echo($acnt_telefone); ?>" />
															</div>
														</div>

														<div class="col-xs-3">
															<div class="form-group">
																<label>&nbsp;</label><div style="clear:both;"></div>
																<a class="btn btn-danger mrExcluirContato" rel=""><i class="fa fa-check-square-o"></i> Excluir</a>
															</div>															
														</div>
													</div><!-- /.row -->
													<?php
														};
													}else{ ;// isset : rs_list_acnt
													?>
													<div class="row tplRowContato" id="tplRowContato-1" >
														<input type="hidden" name="acnt_id[]" id="acnt_id_1" value="0" />

														<div class="col-xs-6">
															<div class="form-group">
																<label for="acnt_email">Email</label>
																<input type="text" name="acnt_email[]" id="acnt_email_1" class="form-control" placeholder="">
															</div>
														</div>

														<div class="col-xs-6">
															<div class="form-group">
																<label for="acnt_telefone">Telefone</label>
																<input type="text" name="acnt_telefone[]" id="acnt_telefone_1" class="form-control inputmask" data-inputmask='"mask": "(99) 99999-9999"' data-mask placeholder="">
															</div>
														</div>
													</div><!-- /.row -->
													<?php
													}// endif : rs_list_acnt
													?>

													<div id="divResultContato"></div>

												</div><!-- /.box-body -->

												<div class="box-footer">
													<a class="btn btn-success mrAddNovoContato" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-check-square-o"></i> Adicionar novo contato</a>
												</div><!-- /.box-footer -->
											</div><!-- /.box -->

										</div>

										<div class="col-md-4">
											<div class="box slim box-primary">

												<div class="box-header with-border">
													<h3 class="box-title">Status</h3>
												</div><!-- /.box-header -->

												<div class="box-body">
													<div class="form-group smXT">
														<?php
														$ana_ativo = 1;
														$ana_ativo = isset($rs_bd->ana_ativo)?$rs_bd->ana_ativo:1;
														?>
														<label for="ana_ativo" class="lblsm">Ativo</label>
														<select name='ana_ativo' id='ana_ativo' class="form-control">
															<option value="1" <?php echo($ana_ativo=="1"? "selected":""); ?>>Sim</option>
															<option value="0" <?php echo($ana_ativo=="0"? "selected":""); ?>>Não</option>
														</select>
													</div>
												</div><!-- /.box-body -->

												<div class="box-footer hide">
													<button type="submit" class="btn btn-primary">Submit</button>
												</div><!-- /.box-footer -->

											</div>
										</div>

									</div>
									<div class="clear"></div>
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
								</div>

								<!-- tab : tab-mod_naturezas -->
								<div class="tab-pane " id="tab-mod_naturezas">
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
									<div class="row">
										<div class="col-md-8">

											<!-- Naturezas relacionadas --> 
											<div class="box slim box-primary">
												<div class="box-header with-border">
													<h3 class="box-title">Naturezas relacionadas</h3>
												</div><!-- /.box-header -->

												<div class="box-body">

													<div class="row" >
														<div class="col-xs-6">
																<?php
																$arr_exists_nat = array();
																if ( isset($rs_list_nat_rel_base) ){ $arr_exists_nat = $rs_list_nat_rel_base; }
																?>
																<?php
																if ( isset($rs_list_nat) ){
																	foreach ($rs_list_nat AS $item){
																		$checked = ''; 
																		if (in_array( (int)$item->nat_id , $arr_exists_nat)) { $checked = ' checked '; }
																?>
																<div class="form-group">	
																	<div class="checkbox">
																		<label>
																			<input type="checkbox" name="chk_natureza[]" ID="chk_natureza_<?php echo((int)$item->nat_id); ?>" value="<?php echo((int)$item->nat_id); ?>" class="minimal" <?php echo($checked); ?> >
																			&nbsp; <?php echo($item->nat_titulo); ?>
																		</label>
																	</div>
																</div>
																<?php
																	}// foreach
																}// if
																?>
														</div>
													</div><!-- /.row -->

												</div><!-- /.box-body -->

												<div class="box-footer hide">
													<button type="submit" class="btn btn-primary">Salvar</button>
												</div><!-- /.box-footer -->
											</div><!-- /.box -->

										</div>
										<div class="col-md-4">
											&nbsp;
										</div>
									</div>
									<div class="clear"></div>
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
								</div>

								<!-- tab : tab-mod_galeria -->
								<div class="tab-pane " id="tab-mod_galeria">
									tab-mod_galeria
								</div>

							</div>

						</div>
					</div>
				</div><!-- /.row -->

			</section><!-- /.content -->
		</FORM>
    
  </div>


	<?php $this->load->view( "includes/footer" ); ?>

	<?php $this->load->view( "includes/scripts" ); ?>


	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
	<!-- ini : TEMPLATE ITEM OCULTO -->
	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
	<div id="itemTplOculto" style="display:none;">
		<script id="divItemContato" type="text/x-jquery-tmpl">
			<div class="row tplRowContato" id="tplRowContato-${item}" >
				<input type="hidden" name="acnt_id[]" id="acnt_id_${item}" value="0" />

				<div class="col-xs-6">
					<div class="form-group">
						<label for="acnt_email_${item}">Email</label>
						<input type="text" name="acnt_email[]" id="acnt_email_${item}" class="form-control" placeholder="">
					</div>
				</div>

				<div class="col-xs-6">
					<div class="form-group">
						<label for="acnt_telefone_${item}">Telefone</label>
						<input type="text" name="acnt_telefone[]" id="acnt_telefone_${item}" class="form-control inputmask" data-inputmask='"mask": "(99) 9-9999-9999"' data-mask placeholder="">
					</div>
				</div>
			</div><!-- /.row -->
		</script>
	</div>
	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
	<!-- end : TEMPLATE ITEM OCULTO -->
	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

	<script>
		jQuery(document).ready(function ($) {
			$(document).on('click', '.mrBtnSaveReg2', function (e) {
				//e.preventDefault();
				var $form	= $('form#frmFormulario');
				var $msg	= '';

				var $ana_nome = $form.find("#ana_nome");
				var $ana_cpf = $form.find("#ana_cpf");
				var $ana_email = $form.find("#ana_email");

				if( $ana_nome.val().length == 0 )			{ $msg += "<p>- Preencha corretamente o nome.</p>"; }
				if( $ana_cpf.val().length == 0 )			{ $msg += "<p>- Preencha corretamente o cpf.</p>"; }
				if( $ana_email.val().length == 0 )		{ $msg += "<p>- Preencha corretamente o e-mail.</p>"; }

				if( $msg.length > 0)
				{
					$.alert({
						title: 'Atenção',
						confirmButtonClass: 'btn-info',
						cancelButtonClass: 'btn-danger',
						confirmButton: 'OK',
						//cancelButton: 'NO never !',
						content: $msg,
						confirm: function () {
							//$.alert('Confirmed!');
						}
					});
					return false;
				}else{
					$form.submit();
				}
			});
		});
	</script>
