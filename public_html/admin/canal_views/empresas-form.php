<?php $this->load->view( "includes/doctype" ); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


	<?php $this->load->view( "includes/header" ); ?>

  <?php $this->load->view( "includes/menu" ); ?>


  <div class="content-wrapper">

    <section class="content-header">
      <h1>
        Gerenciar Empresas
        <small>Formulário</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="javascript:;"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="javascript:;">Empresas</a></li>
        <li class="active">Formulário</li>
      </ol>
    </section>


		<!--## INI : FORMULARIO -->
		<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmFormulario" id="frmFormulario" enctype="multipart/form-data">
			<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORMULARIO">
			<input type="hidden" name="emp_id" ID="emp_id" value="<?php echo(isset($rs_bd->emp_id)? $rs_bd->emp_id : ""); ?>">

			<section class="content">

				<!-- botoes de acoes -->
				<div class="row" style="margin-bottom:15px;">
					<div class="col-xs-12" >
						<p style="float:right;">
							<button class="btn btn-info mrBtnAddNovo" rel="<?php echo( $url["url_form"] ); ?>"><i class="fa fa-edit"></i> Criar Novo Registro</button>
							<button class="btn btn-warning mrBtnListReg" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-list-alt"></i> Lista de Registros</button>
							<button class="btn btn-success mrBtnSaveReg2" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-check-square-o"></i> Salvar</button>
						</p>

						<div class="clear"></div>
						<div style="border-bottom: 1px solid #D8D8D8;"></div>
					</div><!-- /.col -->
				</div><!-- /.row // botoes de acoes-->


				<?php if( $this->session->flashdata('message_validate') ) { ?>
					<div class="row">
						<div class="col-lg-12">
							<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<h4><i class="icon fa fa-check"></i></i> Atenção!</h4>
								<?php echo( $this->session->flashdata('message_validate') ); ?>
							</div>
						</div>
					</div>
				<?php } ?>


				<div class="row">
					<div class='col-xs-12'>
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active ">
									<a href="#tab-mod_formulario" data-toggle="tab">Detalhes Principais</a>
								</li>
								<li class="">
									<a href="#tab-mod_contrato" data-toggle="tab">Contratos</a>
								</li>
								<li>
									<a href="#tab-mod_naturezas" data-toggle="tab">Naturezas Relacionadas</a>
								</li>
								<li class="hide">
									<a href="#tab-mod_galeria" data-toggle="tab">Galeria de Fotos</a>
								</li>
							</ul>

							<div class="tab-content">
								
								<!-- tab : tab-mod_formulario -->
								<div class="tab-pane active " id="tab-mod_formulario">
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
									<div class="row">

										<div class="col-md-8">

											<!-- Dados principais --> 
											<div class="box slim box-primary">
												<div class="box-header with-border">
													<h3 class="box-title">Dados principais</h3>
												</div><!-- /.box-header -->

												<div class="box-body">

													<div class="row" >
														<div class="col-xs-6">
															<div class="form-group">
																<?php
																$emp_parent_id = 0;
																$emp_parent_id = isset($rs_bd->emp_parent_id)?$rs_bd->emp_parent_id:0;
																?>
																<label for="emp_parent_id">Empresa Matriz</label>
																<select name='emp_parent_id' id='emp_parent_id' class="form-control">
																	<option value="" >- selecione -</option>
																	<?php
																	if ( isset($rs_emp_parent) ){
																		foreach ($rs_emp_parent AS $item){
																			$selected = ($emp_parent_id == (int)$item->emp_id) ? ' selected ' : ''; 
																	?>
																	<option value="<?php echo((int)$item->emp_id); ?>" <?php echo($selected); ?> ><?php echo($item->emp_razao_social); ?></option>
																	<?php
																		}// foreach
																	}// if
																	?>
																</select>
															</div>
														</div>

														<div class="col-xs-6">
															<div class="form-group">
																<label for="emp_codigo">Código da Empresa</label>
																<input type="text" name="emp_codigo" id="emp_codigo" class="form-control" placeholder="" value="<?php echo( (isset($rs_bd->emp_codigo)? $rs_bd->emp_codigo : '') ); ?>" />
															</div>
														</div>
													</div>

													<div class="row" >
														<div class="col-xs-6">
															<div class="form-group">
																<label for="emp_razao_social">Nome da Empresa</label>
																<input type="text" name="emp_razao_social" id="emp_razao_social" class="form-control" placeholder="" value="<?php echo( (isset($rs_bd->emp_razao_social)? $rs_bd->emp_razao_social : '') ); ?>" />
															</div>
														</div>

														<div class="col-xs-6">
															<div class="form-group">
																<label for="emp_nome_fantasia">Nome Fantasia</label>
																<input type="text" name="emp_nome_fantasia" id="emp_nome_fantasia" class="form-control" placeholder="" value="<?php echo( (isset($rs_bd->emp_nome_fantasia)? $rs_bd->emp_nome_fantasia : '') ); ?>" >
															</div>
														</div>
													</div><!-- /.row -->

													<div class="row" >
														<div class="col-xs-6">
															<div class="form-group">
																<label for="emp_cnpj">CNPJ</label>
																<input type="text" name="emp_cnpj" id="emp_cnpj" class="form-control" data-mask data-inputmask='"mask": "999.999.999/9999-99"' placeholder="" value="<?php echo( (isset($rs_bd->emp_cnpj)? $rs_bd->emp_cnpj : '') ); ?>" >
															</div>
														</div>

														<div class="col-xs-6">
															<div class="form-group">
																<label for="emp_responsavel">Responsável</label>
																<input type="text" name="emp_responsavel" id="emp_responsavel" class="form-control" placeholder="" value="<?php echo( (isset($rs_bd->emp_responsavel)? $rs_bd->emp_responsavel : '') ); ?>" >
															</div>
														</div>
													</div><!-- /.row -->

													<div class="row" >
														<div class="col-xs-6">
															<div class="form-group">
																<label for="emp_telefone">Telefone</label>
																<input type="text" name="emp_telefone" id="emp_telefone" class="form-control" data-mask data-inputmask='"mask": "(99) 99999-9999"' placeholder="" value="<?php echo( (isset($rs_bd->emp_telefone)? $rs_bd->emp_telefone : '') ); ?>" >
															</div>
														</div>
													</div><!-- /.row -->

												</div><!-- /.box-body -->

												<div class="box-footer hide">
													<button type="submit" class="btn btn-primary">Salvar</button>
												</div><!-- /.box-footer -->
											</div><!-- /.box -->


											<!-- Dados de acesso --> 
											<div class="box slim box-primary">
												<div class="box-header with-border">
													<h3 class="box-title">Dados de acesso</h3>
												</div><!-- /.box-header -->

												<div class="box-body">

													<div class="row" >
														<div class="col-xs-6">
															<div class="form-group">
																<label for="emp_email">E-mail</label>
																<input type="text" name="emp_email" id="emp_email" class="form-control" placeholder="" value="<?php echo( (isset($rs_bd->emp_email)? $rs_bd->emp_email : '') ); ?>" >
															</div>
														</div>

														<div class="col-xs-6">
															<div class="form-group">
																<label for="emp_senha">Senha</label>
																<input type="text" name="emp_senha" id="emp_senha" class="form-control" placeholder="">
															</div>
														</div>
													</div><!-- /.row -->

													<div class="row" >
														<div class="col-xs-12">
															<div class="form-group">
																<div class="checkbox" style="margin-top: 0px;">
																	<label style="padding-left: 0px;">
																		<input type="checkbox" name="emp_sendmail" id="emp_sendmail" class="minimal" value="1"> Enviar e-mail para alterar senha de acesso
																	</label>
																</div>
															</div>
														</div>
													</div><!-- /.row -->

												</div><!-- /.box-body -->

												<div class="box-footer hide"></div><!-- /.box-footer -->
											</div><!-- /.box -->


											<!-- Endereço --> 
											<div class="box slim box-primary">
												<div class="box-header with-border">
													<h3 class="box-title">Endereço</h3>
												</div><!-- /.box-header -->

												<div class="box-body">

													<div class="row" >
														<div class="col-xs-4">

															<div class="form-group">
																<label for="emp_end_cep">CEP</label>
																<div class="input-group input-group-sm">
																	<input type="text" name="emp_end_cep" id="emp_end_cep" class="form-control" data-mask data-inputmask='"mask": "99999-999"' placeholder="" value="<?php echo( (isset($rs_bd->emp_end_cep)? $rs_bd->emp_end_cep : '') ); ?>" >
																	<span class="input-group-btn">
																		<button type="button" class="btn btn-info btn-flat cmdbuscacep">Buscar</button>
																	</span>
																</div>																
															</div>
														</div>

														<div class="col-xs-8">
															<div class="form-group">
																<label for="emp_endereco">Logradouro</label>
																<input type="text" name="emp_endereco" id="emp_endereco" class="form-control" placeholder="" value="<?php echo( (isset($rs_bd->emp_endereco)? $rs_bd->emp_endereco : '') ); ?>" >
															</div>
														</div>
													</div><!-- /.row -->

													<div class="row" >
														<div class="col-xs-2">
															<div class="form-group">
																<label for="emp_end_numero">Número</label>
																<input type="text" name="emp_end_numero" id="emp_end_numero" class="form-control" placeholder="" value="<?php echo( (isset($rs_bd->emp_end_numero)? $rs_bd->emp_end_numero : '') ); ?>" >
															</div>
														</div>

														<div class="col-xs-5">
															<div class="form-group">
																<label for="emp_end_complemento">Complemento</label>
																<input type="text" name="emp_end_complemento" id="emp_end_complemento" class="form-control" placeholder="" value="<?php echo( (isset($rs_bd->emp_end_complemento)? $rs_bd->emp_end_complemento : '') ); ?>" >
															</div>
														</div>

														<div class="col-xs-5">
															<div class="form-group">
																<label for="emp_end_bairro">Bairro</label>
																<input type="text" name="emp_end_bairro" id="emp_end_bairro" class="form-control" placeholder="" value="<?php echo( (isset($rs_bd->emp_end_bairro)? $rs_bd->emp_end_bairro : '') ); ?>" >
															</div>
														</div>
													</div><!-- /.row -->

													<div class="row" >
														<div class="col-xs-9">
															<div class="form-group">
																<label for="emp_end_cidade">Cidade</label>
																<input type="text" name="emp_end_cidade" id="emp_end_cidade" class="form-control" placeholder="" value="<?php echo( (isset($rs_bd->emp_end_cidade)? $rs_bd->emp_end_cidade : '') ); ?>" >
															</div>
														</div>
														<div class="col-xs-3">
															<div class="form-group">
																<label for="emp_end_estado">Estado</label>
																<input type="text" name="emp_end_estado" id="emp_end_estado" class="form-control" placeholder="" value="<?php echo( (isset($rs_bd->emp_end_estado)? $rs_bd->emp_end_estado : '') ); ?>" >
															</div>
														</div>
													</div><!-- /.row -->
												</div><!-- /.box-body -->

												<div class="box-footer hide"></div><!-- /.box-footer -->
											</div><!-- /.box -->


											<!-- Descrição --> 
											<div class="box slim box-primary">
												<div class="box-header with-border">
													<h3 class="box-title">Descrição</h3>
												</div><!-- /.box-header -->

												<div class="box-body">

													<div class="row" >
														<div class="col-xs-12">
															<div class="form-group">
																<textarea name="emp_descricao" id="emp_descricao" class="fld_ckeditor" rows="10" cols="80" style="width:100%;"><?php echo(isset($rs_bd->emp_descricao)? $rs_bd->emp_descricao : ""); ?></textarea> 
															</div>
														</div>
													</div><!-- /.row -->
												</div><!-- /.box-body -->

												<div class="box-footer hide"></div><!-- /.box-footer -->
											</div><!-- /.box -->


										</div>

										<div class="col-md-4">
											<div class="box slim box-primary">

												<div class="box-header with-border">
													<h3 class="box-title">Status</h3>
												</div><!-- /.box-header -->

												<div class="box-body">
													<div class="form-group smXT">
														<?php
														$emp_ativo = isset($rs_bd->emp_ativo)?$rs_bd->emp_ativo:1;
														?>
														<label for="emp_ativo" class="lblsm">Ativo</label>
														<select name='emp_ativo' id='emp_ativo' class="form-control">
															<option value="1" <?php echo($emp_ativo=="1"? "selected":""); ?>>Sim</option>
															<option value="0" <?php echo($emp_ativo=="0"? "selected":""); ?>>Não</option>
														</select>
													</div>
												</div><!-- /.box-body -->

												<div class="box-footer hide">
													<button type="submit" class="btn btn-primary">Submit</button>
												</div><!-- /.box-footer -->

											</div>
										</div>

									</div>
									<div class="clear"></div>
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
								</div>

								<!-- tab : tab-mod_contrato -->
								<div class="tab-pane " id="tab-mod_contrato">
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
									<div class="row">

										<div class="col-md-12">
											<!-- Dados dos contratos --> 
											<div class="box slim box-primary">
												<div class="box-header with-border">
													<h3 class="box-title">Dados dos contratos</h3>
												</div><!-- /.box-header -->

												<div class="box-body">
													<?php
													$x_cntr = 0;
													//fct_print_debug( $rs_list_cntr );
													if ( isset($rs_list_cntr) && count($rs_list_cntr)>0  ){
														foreach ($rs_list_cntr AS $item){
															$x_cntr++;
															$cntr_id								= (int)$item->cntr_id;
															$cntr_num_contrato			= $item->cntr_num_contrato;
															$cntr_dte_ini_contrato	= fct_formatdate($item->cntr_dte_ini_contrato, 'd/m/Y');
															$cntr_dte_fim_contrato	= fct_formatdate($item->cntr_dte_fim_contrato, 'd/m/Y');
													?>
													<div class="row tplRowContrato" id="tplRowContrato-<?php echo($x_cntr); ?>" >
														<input type="hidden" name="cntr_id[]" id="cntr_id_<?php echo($x_cntr); ?>" value="<?php echo($cntr_id); ?>" />

														<div class="col-xs-4">
															<div class="form-group">
																<label for="cntr_num_contrato_1">Número do contrato</label>
																<input type="text" name="cntr_num_contrato[]" id="cntr_num_contrato_<?php echo($x_cntr); ?>" class="form-control" placeholder="" value="<?php echo($cntr_num_contrato); ?>" />
															</div>
														</div>

														<div class="col-xs-2">
															<div class="form-group">
																<label for="cntr_dte_ini_contrato_<?php echo($x_cntr); ?>" class="lblsm">Data de início</label>
																<div class="input-group">
																	<div class="input-group-addon"><span class="icon-date-picker" id="imgdte_dte_ini_contrato<?php echo($x_cntr); ?>"></span></div>
																	<input type="text" name="cntr_dte_ini_contrato[]" id="cntr_dte_ini_contrato_<?php echo($x_cntr); ?>" class="form-control field-date " placeholder="" value="<?php echo($cntr_dte_ini_contrato); ?>" />
																</div><!-- /.input group -->
															</div>
														</div>

														<div class="col-xs-2">
															<div class="form-group">
																<label for="cntr_dte_fim_contrato_<?php echo($x_cntr); ?>" class="lblsm">Data de término</label>
																<div class="input-group">
																	<div class="input-group-addon"><span class="icon-date-picker" id="imgdte_dte_fim_contrato_<?php echo($x_cntr); ?>"></span></div>
																	<input type="text" name="cntr_dte_fim_contrato[]" id="cntr_dte_fim_contrato_<?php echo($x_cntr); ?>" class="form-control field-date " placeholder="" value="<?php echo($cntr_dte_fim_contrato); ?>" />
																</div><!-- /.input group -->
															</div>
														</div>

														<div class="col-xs-2">
															<div class="form-group">
																<label>&nbsp;</label><div style="clear:both;"></div>
																<a class="btn btn-danger mrExcluirContrato" rel=""><i class="fa fa-check-square-o"></i> Excluir</a>
															</div>															
														</div>

														<div class="row hide">
															<div class="col-xs-12 form-group">	
																<div class="checkbox">
																	<label>
																		<input type="checkbox" name="cntr_id_excluir[]" ID="cntr_id_excluir_<?php echo($x_cntr); ?>" value="<?php echo($cntr_id); ?>" class="minimal" />
																		&nbsp; <span>Excluir este registro</span>
																	</label>
																</div>
															</div>
														</div><!-- /.row -->

													</div><!-- /.row -->
													<?php
														};
													}else{ ;// isset : rs_list_cntr
													?>
													<div class="row tplRowContrato" id="tplRowContrato-1" >
														<input type="hidden" name="cntr_id[]" id="cntr_id_1" value="0" />

														<div class="col-xs-6">
															<div class="form-group">
																<label for="cntr_num_contrato_1">Número do contrato</label>
																<input type="text" name="cntr_num_contrato[]" id="cntr_num_contrato_1" class="form-control" placeholder="">
															</div>
														</div>

														<div class="col-xs-3">
															<div class="form-group">
																<label for="cntr_dte_ini_contrato_1" class="lblsm">Data de início</label>
																<div class="input-group">
																	<div class="input-group-addon"><span class="icon-date-picker" id="imgdte_dte_ini_contrato_1"></span></div>
																	<?php
																	//$post_dtecadastro = isset($edit_rows->post_dtecadastro) ? $edit_rows->post_dtecadastro : ""; 
																	//$post_dtecadastro = empty($post_dtecadastro)? date("d/m/Y") : fct_formatdate($post_dtecadastro, 'd/m/Y');
																	?>
																	<input type="text" name="cntr_dte_ini_contrato[]" id="cntr_dte_ini_contrato_1" class="form-control field-date " placeholder="" />
																</div><!-- /.input group -->
															</div>
														</div>

														<div class="col-xs-3">
															<div class="form-group">
																<label for="cntr_dte_fim_contrato_1" class="lblsm">Data de término</label>
																<div class="input-group">
																	<div class="input-group-addon"><span class="icon-date-picker" id="imgdte_dte_fim_contrato_1"></span></div>
																	<?php
																	//$post_dtecadastro = isset($edit_rows->post_dtecadastro) ? $edit_rows->post_dtecadastro : ""; 
																	//$post_dtecadastro = empty($post_dtecadastro)? date("d/m/Y") : fct_formatdate($post_dtecadastro, 'd/m/Y');
																	?>
																	<input type="text" name="cntr_dte_fim_contrato[]" id="cntr_dte_fim_contrato_1" class="form-control field-date " placeholder="" />
																</div><!-- /.input group -->
															</div>
														</div>
													</div><!-- /.row -->
													<?php
													}// endif : rs_list_cntr
													?>

													<div id="divResultContrato"></div>
												</div><!-- /.box-body -->

												<div class="box-footer">
													<a class="btn btn-success mrAddNovoContrato" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-check-square-o"></i> Adicionar novo contrato</a>
												</div><!-- /.box-footer -->

											</div><!-- /.box -->
										</div>

									</div>
									<div class="clear"></div>
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
								</div>

								<!-- tab : tab-mod_naturezas -->
								<div class="tab-pane " id="tab-mod_naturezas">
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
									<div class="row">
										<div class="col-md-12">

											<!-- Naturezas relacionadas --> 
											<div class="box slim box-primary">
												<div class="box-header with-border">
													<h3 class="box-title">Naturezas relacionadas</h3>
												</div><!-- /.box-header -->

												<div class="box-body">
													<?php
													$arr_exists_nat = array();
													$arr_exists_ordem = array();
													$arr_exists_image = array();
													if ( isset($rs_list_nat_rel_base) ){ $arr_exists_nat = $rs_list_nat_rel_base; }
													if ( isset($rs_list_nat_rel_image) ){ $arr_exists_image = $rs_list_nat_rel_image; }
													if ( isset($rs_list_nat_rel_ordem) ){ $arr_exists_ordem = $rs_list_nat_rel_ordem; }
													?>
													<?php
													if ( isset($rs_list_nat) ){
														foreach ($rs_list_nat as $key => $item){
															$checked = ''; 
															$natemp_prazo = '';
															$natemp_ordem = '';
															$natemp_arquivo = '';
															$item_nat_id = (int)$item->nat_id;

															//if (in_array( $item_nat_id , $arr_exists_nat)) { 
																//$checked = ' checked ';
																//$natemp_prazo = isset($rs_list_nat_rel[$key]['natemp_prazo']) ? $rs_list_nat_rel[$key]['natemp_prazo'] : "";

																///// $key = array_search(5, $array) // if ($key !== false) {
															//}

															if (array_key_exists($item_nat_id, $arr_exists_nat)) {
																//echo "O elemento 'primeiro' está no array!";
																$checked = ' checked ';
																$natemp_prazo = isset($arr_exists_nat[$item_nat_id]) ? $arr_exists_nat[$item_nat_id]: "";
																$natemp_ordem = isset($arr_exists_ordem[$item_nat_id]) ? $arr_exists_ordem[$item_nat_id]: "";
																$natemp_arquivo = isset($arr_exists_image[$item_nat_id]) ? $arr_exists_image[$item_nat_id]: "";
															}
															//if (in_array( $item_nat_id , $arr_exists_nat)) { 
																//$checked = ' checked ';
																//$natemp_prazo = isset($rs_list_nat_rel[$key]['natemp_prazo']) ? $rs_list_nat_rel[$key]['natemp_prazo'] : "";

																///// $key = array_search(5, $array) // if ($key !== false) {
															//}

															$css_box = ($key == 0 OR $key == 4) ? 'box-success' : '';
															$css_btn = ($key == 0 OR $key == 4) ? 'btn-success' : '';
															$css_txt = ($key == 0 OR $key == 4) ? 'SELECIONADO' : 'SELECIONAR';

															$css_box = '';
															$css_btn = '';
															$css_txt = 'SELECIONAR';
													?>
													<div class="col-xs-4">


														<div class="box box-default <?php echo($css_box); ?> box-solid" style="height:400px;">
															<div class="box-header with-border">
																<h3 class="box-title"><?php echo($item->nat_titulo); ?></h3>
															</div>
															<div class="box-body">

																	<div class="col-xs-12">
																		<div class="form-group smXT">
																			<div style="margin:0 auto; border:0px dotted red; text-align:center;">
																				<?php
																					$natemp_arquivo_tag = '<img data-src="holder.js" class="rounded" alt="Sem Imagem" style="margin:0 auto; width: 70%; height: 180px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15cc09e90d7%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15cc09e90d7%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2275.5%22%20y%3D%22104.5%22%3EImagem%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">';

																					$image_path = $this->config->item('folder_images') .'/naturezas/'. $natemp_arquivo;
																					if( file_exists($image_path) and is_file($image_path) )
																					{
																						$load_image = base_url($image_path);
																						$natemp_arquivo_tag = '<img src="'. $load_image .'" alt="" class="img-responsive" style="height: 180px;" />';
																					};
																					print $natemp_arquivo_tag;
																				?>
																			</div>
																		</div>

																		<div class="form-group smXT">
																			<input type="hidden" name="strArqFileUpl_<?php echo($item_nat_id); ?>" id="strArqFileUpl_<?php echo($item_nat_id); ?>" value="<?php echo($natemp_arquivo); ?>" />
																			<input type="file" name="uplArqFileUpl_<?php echo($item_nat_id); ?>" id="uplArqFileUpl_<?php echo($item_nat_id); ?>" class="fld_file" />
																		</div>
																	</div>

																	<div class="col-xs-6" style="padding-right: 0px;">
																		<div class="input-group">
																			<div class="input-group-btn">
																				<button type="button" class="btn btn-default">Prazo</button>
																			</div>
																			<input type="text" name="natemp_prazo_<?php echo($item_nat_id); ?>" id="natemp_prazo_<?php echo($item_nat_id); ?>" class="form-control" placeholder="" value="<?php echo($natemp_prazo); ?>" />
																		</div>
																	</div>
																	<div class="col-xs-6">
																		<div class="input-group">
																			<div class="input-group-btn">
																				<button type="button" class="btn btn-default">Ordem</button>
																			</div>
																			<input type="text" name="natemp_ordem_<?php echo($item_nat_id); ?>" id="natemp_ordem_<?php echo($item_nat_id); ?>" class="form-control" placeholder="" value="<?php echo($natemp_ordem); ?>" />
																		</div>
																	</div>

																	<div class="clear"></div>

																	<div class="col-xs-12">
																		<div class="form-group">	
																			<div class="checkbox">
																				<label style="padding-left: 0px !important;">
																					<input type="checkbox" name="chk_natureza[]" ID="chk_natureza_<?php echo((int)$item->nat_id); ?>" value="<?php echo((int)$item->nat_id); ?>" class="minimal" <?php echo($checked); ?> >
																					&nbsp; SELECIONAR
																				</label>
																			</div>
																		</div>
																	</div>

																	<!--
																	<?php //if( !empty($css_box) ) { ?>
																	<div class="btn-block" style="margin-top:15px !important;">
																		<div class="col-md-12">
																			<button class="btn btn-default <?php echo($css_btn); ?> btn-block btnNatEmp" rel=""><?php echo($css_txt); ?></button>
																		</div>
																	</div>
																	<?php //} ?>
																	-->

															</div>
														</div>
													</div><!-- /.row -->
													<?php
														}// foreach
													}// if
													?>
												</div><!-- /.box-body -->

												<div class="box-footer hide">
													<button type="submit" class="btn btn-primary">Salvar</button>
												</div><!-- /.box-footer -->
											</div><!-- /.box -->

										</div>
									</div>
									<div class="clear"></div>
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
								</div>

							</div>

						</div>
					</div>
				</div><!-- /.row -->

			</section><!-- /.content -->
		</FORM>
		<!--## END : FORMULARIO -->
    
  </div>


	<?php $this->load->view( "includes/footer" ); ?>

	<?php $this->load->view( "includes/scripts" ); ?>


	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
	<!-- ini : TEMPLATE ITEM OCULTO -->
	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
	<div id="itemTplOculto" style="display:none;">
		<script id="divItemContrato" type="text/x-jquery-tmpl">
			<div class="row tplRowContrato" id="tplRowContrato-${item}" >
				<input type="hidden" name="cntr_id[]" id="cntr_id_${item}" value="0" />

				<div class="col-xs-6">
					<div class="form-group">
						<label for="cntr_num_contrato_${item}">Número do contrato</label>
						<input type="text" name="cntr_num_contrato[]" id="cntr_num_contrato_${item}" class="form-control" placeholder="">
					</div>
				</div>

				<div class="col-xs-3">
					<div class="form-group">
						<label for="cntr_dte_ini_contrato_${item}" class="lblsm">Data de início</label>
						<div class="input-group">
							<div class="input-group-addon"><span class="icon-date-picker" id="imgdte_dte_ini_contrato_${item}"></span></div>
							<input type="text" name="cntr_dte_ini_contrato[]" id="cntr_dte_ini_contrato_${item}" class="form-control field-date " placeholder="" />
						</div><!-- /.input group -->
					</div>
				</div>

				<div class="col-xs-3">
					<div class="form-group">
						<label for="cntr_dte_fim_contrato_${item}" class="lblsm">Data de término</label>
						<div class="input-group">
							<div class="input-group-addon"><span class="icon-date-picker" id="imgdte_dte_fim_contrato_${item}"></span></div>
							<input type="text" name="cntr_dte_fim_contrato[]" id="cntr_dte_fim_contrato_${item}" class="form-control field-date " placeholder="" />
						</div><!-- /.input group -->
					</div>
				</div>
			</div><!-- /.row -->
		</script>
	</div>
	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
	<!-- end : TEMPLATE ITEM OCULTO -->
	<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


	<script>
		jQuery(document).ready(function ($) {
			//$('form#frmREGISTRO').submit( function(e){
			$(document).on('click', '.mrBtnSaveReg2', function (e) {
				//e.preventDefault();
				var $form	= $('form#frmFormulario');
				var $msg	= '';

				var $emp_codigo = $form.find("#emp_codigo");
				var $emp_razao_social = $form.find("#emp_razao_social");
				var $emp_nome_fantasia = $form.find("#emp_nome_fantasia");
				var $emp_cnpj = $form.find("#emp_cnpj");
				var $emp_email = $form.find("#emp_email");

				if( $emp_codigo.val().length == 0 )					{ $msg += "<p>- Preencha corretamente a código.</p>"; }
				if( $emp_razao_social.val().length == 0 )		{ $msg += "<p>- Preencha corretamente a razão social.</p>"; }
				if( $emp_nome_fantasia.val().length == 0 )	{ $msg += "<p>- Preencha corretamente o nome fantasia.</p>"; }
				if( $emp_cnpj.val().length == 0 )						{ $msg += "<p>- Preencha corretamente o cnpj.</p>"; }
				if( $emp_email.val().length == 0 )					{ $msg += "<p>- Preencha corretamente o e-mail.</p>"; }

				if( $msg.length > 0)
				{
					$.alert({
						title: 'Atenção',
						confirmButtonClass: 'btn-info',
						cancelButtonClass: 'btn-danger',
						confirmButton: 'OK',
						//cancelButton: 'NO never !',
						content: $msg,
						confirm: function () {
							//$.alert('Confirmed!');
						}
					});
					return false;
				}else{
					$form.submit();
				}
			});
		});
	</script>

