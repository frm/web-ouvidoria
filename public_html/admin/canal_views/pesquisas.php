<?php $this->load->view( "includes/doctype" ); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


	<?php $this->load->view( "includes/header" ); ?>

  <?php $this->load->view( "includes/menu" ); ?>


  <div class="content-wrapper">

    <section class="content-header">
      <h1>
        Gerenciar Pesquisas
        <small>Listagem</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="javascript:;"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="javascript:;">Empresas</a></li>
        <li class="active">Listagem</li>
      </ol>
    </section>


		<!--## INI : FORMULARIO -->
		<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmFormList" id="frmFormList" enctype="multipart/form-data">
			<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORMULARIO">

			<section class="content">

				<!-- botoes de acoes -->
				<div class="row" style="margin-bottom:15px;">
					<div class="col-xs-12" >
						<p style="float:right;">
							<button class="btn btn-info mrBtnAddNovo" rel="<?php echo( $url["url_form"] ); ?>"><i class="fa fa-edit"></i> Criar Novo Registro</button>
							<button class="btn btn-danger mrBtnDelReg"><i class="fa fa-trash-o"></i> Excluir</button>
						</p>

						<div class="clear"></div>
						<div style="border-bottom: 1px solid #D8D8D8;"></div>
					</div><!-- /.col -->
				</div><!-- /.row // botoes de acoes-->

				
				<style>
				.line_parent{ display:none; }
				</style>				
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Lista de empresas</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<table id="tableList" class="table table-bordered table-hover">
									<thead>
									<tr>
										<th class="center" style="width: 15px;">&nbsp;</th>
										<th class="center" style="width: 50px;">#</th>
										<th>ID</th>
										<th style="width: 250px;">Razão Social</th>
										<th>E-mail</th>
										<th class="center" style="width: 140px;">Data</th>
										<th class="center" style="width: 80px;">Ativo</th>
										<th class="center" style="width: 90px;">Ação</th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ( isset($rs_list) ){
										foreach ($rs_list AS $item){
											$emp_id						= (int)$item->emp_id;
											$emp_parent_id		= (int)$item->emp_parent_id;
											$emp_razao_social	= $item->emp_razao_social;
											$emp_codigo				= $item->emp_codigo;
											$emp_email				= $item->emp_email;
											$emp_dte_cadastro	= fct_formatdate($item->emp_dte_cadastro, 'd/m/Y H:i');
											$icon_ativo				= ($item->emp_ativo==1)?"sim":"não";
											$redirect					= $url["url_form"] ."/". $emp_id;
											$count_parent			= (int)$item->count_parent;
											$show_parent			= ($emp_parent_id>0) ? 'line_parent' : '';
									?>
									<tr class="<?php echo( $show_parent ); ?> parent_<?php echo( $emp_parent_id ); ?>" >
										<td class="center">
											<?php if( $count_parent>0 ) { ?>
												<a href="javascript:void(0);" class="treegrid-expander" data-code="parent_<?php echo($emp_id); ?>"><span class="fa  fa-plus-square-o"></span></a>
											<?php } ?>
										</td>										
										<td class="center">
											<input type="checkbox" name="chk_delete[]" ID="chk_delete_<?php echo($emp_id); ?>" value="<?php echo($emp_id); ?>" class="minimal itemdelete">
										</td>
										<td><?php echo($emp_id); ?></td>
										<td>
											<div><?php echo($emp_razao_social); ?></div>
											<h6 class="text-light-blue mg-empcode"><?php echo($emp_codigo); ?></h6>
										</td>
										<td><?php echo($emp_email); ?></td>
										<td class="center"><?php echo($emp_dte_cadastro); ?></td>
										<td class="center"><?php echo($icon_ativo); ?></td>
										<td class="center">
											<div style="padding:0 10px; display:inline-block">
												<a href="<?php echo($redirect); ?>"><i class="fa fa-edit fa-ft15px"></i></a>
											</div>
											<div style="padding:0 10px; display:inline-block">
												<a href="<?php echo($redirect); ?>"><i class="fa fa-trash-o fa-ft15px"></i></a>
											</div>
										</td>
									</tr>
									<?php
										};
									};// isset : rs_list
									?>
									</tbody>
									<!--
									<tfoot>
									<tr>
										<th>ID</th>
										<th>Razão Social</th>
										<th>E-mail</th>
										<th>Data</th>
										<th>Ativo</th>
									</tr>
									</tfoot>
									-->
								</table>
							</div>
							<!-- /.box-body -->
						</div><!-- /.box -->
					</div>
					<!-- /.col -->
				</div>

			</section><!-- /.content -->
		</FORM>
		<!--## END : FORMULARIO -->
    
  </div>



	<?php $this->load->view( "includes/footer" ); ?>

	<?php $this->load->view( "includes/scripts" ); ?>
