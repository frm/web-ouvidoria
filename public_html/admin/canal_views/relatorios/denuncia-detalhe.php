	
	<div id="boxRelDenunciaDetalhe" class="content-wrapper-modal boxprinter">
	
		<link rel="stylesheet" href="<?php echo(base_url()); ?>assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo(base_url()); ?>assets/css/AdminLTE.min.css">
		<link rel="stylesheet" href="<?php echo(base_url()); ?>assets/css/custom.css">


				<div class="row" style="margin-bottom:25px;">
					<div class="col-xs-12">
						<table border="0" cellpadding="2" cellspacing="2" width="100%">
							<tr>
								<td class="hidden-print" valign="top">
									<h2 style="padding:10px 0 !important; margin:5px 0 !important;">HISTÓRICO DA DENÚNCIA</h2>
								</td>
								<td class="show-print" valign="top">
									<div class="pull-right ">
										<a href="javascript:;" class="linkClickPrinter" data-target="boxRelDenunciaDetalhe">imprimir</a>
									</div>
								</td>
							</tr>
						</table>
					</div>
					<div style="clear:both;"></div>
				</div>


				<?php if( isset($rs_den) && count($rs_den) > 0) { ?>
				<div class="row" style="margin-bottom:25px;">
					<div class="col-xs-12">
						<table border="0" cellpadding="2" cellspacing="2" width="100%">
							<tr>
								<td width="33%" valign="top">
									<label>protocolo:</label><br />
									<h3 style="padding:0 !important; margin:5px 0 !important;"><?php echo($rs_den->den_num_protocolo); ?></h3>
								</td>
								<td width="33%" valign="top">
									<label>data:</label><br />
									<?php echo($rs_den->den_dte_cadastro); ?>
								</td>
								<td style="width: 33%;" valign="top">
									<label>protocolo relacionado:</label><br />
									<?php echo($rs_den->den_num_protocolo_relac); ?>
								</td>
							</tr>
							<tr>
								<td width="100%" colspan="3" valign="middle">
									<?php echo( strip_tags($rs_den->den_descricao) ); ?>
								</td>
							</tr>
						</table>
					</div>
					<div style="clear:both;"></div>
				</div>
				<?php } ?>


				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<!-- /.box-header -->
							<div class="box-body">
									<?php
									/**
									 * --------------------------------------------------------
									 * respostas
									 * --------------------------------------------------------
									**/
									$xx = 0;
									if ( isset($rs_resp) ){
										foreach ($rs_resp AS $itemResp){
											$xx++;
											$dresp_id						= (int)$itemResp->dresp_id;
											$dresp_descricao		= $itemResp->dresp_descricao;
											$dresp_status				= $itemResp->dresp_status;
											$dresp_autor				= $itemResp->dresp_autor;
											$dresp_aprovada			= (int)$itemResp->dresp_aprovada;
											$dresp_dte_cadastro	= fct_formatdate($itemResp->dresp_dte_cadastro, 'd.m.Y H:i');

											$css_status					= ($dresp_autor == 'analista' ? '' : 'status-analista' ); 
											$icon_status				= ($dresp_autor == 'analista' ? 'fa-check-square-o' : 'fa-hourglass-2' );
											$align_item					= ($dresp_autor == 'analista' ? '' : 'right' );
											$analista_title			= ($dresp_autor == 'analista' ? 'Analista' : 'Autor' );
											$icon_avatar				= ($dresp_autor == 'analista' ? 'avatar-canal-01.png' : 'avatar-user-01.png' );
											$direct_chat_admin	= ($dresp_autor != 'empresa' ? '' : '' );

											$niveladmin = ($dresp_autor == 'empresa' || $dresp_autor == 'administrador' ) ? true : false;
											$css_status					= ($niveladmin == false? $css_status : '' ); 
											$icon_status				= ($niveladmin == false ? $icon_status : 'fa-check-square-o' );
											$align_item					= ($niveladmin == false ? $align_item : '' );
											$analista_title			= ($niveladmin == false ? $analista_title : 'Admin' );
											$icon_avatar				= ($niveladmin == false ? $icon_avatar : 'avata-company.png' );
											$direct_chat_admin	= ($niveladmin == false ? $direct_chat_admin : 'direct_chat_admin' );
											$bg_item_aprov			= ($dresp_aprovada == 1 ? '' : 'naoprov' );

											/**
											 * --------------------------------------------------------
											 * anexos
											 * --------------------------------------------------------
											**/
											$this->db->select(" * ")
												->from('tbl_denunc_resp_anexos')
												->where( array('dresp_id' => $dresp_id) )
												->order_by( 'dranx_id', 'DESC' );
											$query = $this->db->get();
											$rs_resp_anexo = $query->result();
											//fct_print_debug( $data['rs_den_anexo'] );
									?>
											<div class="row" style="padding-bottom:0px;">
												<div class="col-md-12">

													<table border="0" cellpadding="2" cellspacing="2" width="100%">
														<tr>
															<td width="30%" valign="top">

																<div class="form-group-sm">
																	<strong><?php echo($analista_title); ?></strong>
																</div>
																<div class="form-group-sm">
																	<label>status:</label> <?php echo($dresp_status); ?>
																</div>
																<div class="form-group-sm">
																	<label>data:</label> <?php echo($dresp_dte_cadastro); ?>
																</div>

															</td>
															<td width="70%" valign="top">

																	<div class="form-group-sm">
																		<?php $dresp_descricao = strip_tags(trim($dresp_descricao)); ?>
																		<?php print ( empty($dresp_descricao) ? '-- não há comentário --' : $dresp_descricao); ?>
																	</div>

															</td>
														</tr>
													</table>

												</div>
												<div class="col-md-12">
													<div style="margin:10px 0; border-bottom:1px solid #CCC;"></div>													
												</div>
												<div style="clear:both;"></div>												
											</div>
									<?php
										}; // foreach
									};// isset : rs_list
									?>
							</div>
							<!-- /.box-body -->
						</div><!-- /.box -->

					</div>
					<!-- /.col -->
				</div>

  </div><!-- //content-wrapper -->



	<script>
	jQuery(document).ready(function($){			
		$('a[data-action="printee"]').on('click', function(e) {
			//console.log('print');

			$(".boxprinter").printThis({
				debug: true,									// show the iframe for debugging
				//importCSS: true,						// import page CSS
				//printContainer: true,					// grab outer container as well as the contents of the selector
				//loadCSS: "path/to/my.css",		//* path to additional css file
				//pageTitle: "",							// add title to print page
				//removeInline: false					//remove all inline styles from print elements
			});

			//$(".boxprinter").printElement({printMode:'popup'});

			return false;
		});
	});
	</script>



	<script>
	jQuery(document).ready(function($){
		$('.linkClickPrinter').on('click', function(e) {
				var $target = $(this).data('target');
				
				var contents = $("#"+ $target).html();
				var frame1 = $('<iframe />');
				frame1[0].name = "frame1";
				frame1.css({ "position": "absolute", "top": "-1000000px" });
				$("body").append(frame1);
				var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
				frameDoc.document.open();
				//Create a new HTML document.
				frameDoc.document.write('<html><head><title>DIV Contents</title>');
				frameDoc.document.write('</head><body>');

				//Append the external CSS file.
				frameDoc.document.write('<link href="<?php echo(base_url()); ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />');
				frameDoc.document.write('<link href="<?php echo(base_url()); ?>assets/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />');
				frameDoc.document.write('<link href="<?php echo(base_url()); ?>assets/css/custom.css" rel="stylesheet" type="text/css" />');

				//Append the DIV contents.
				frameDoc.document.write(contents);
				frameDoc.document.write('</body></html>');
				frameDoc.document.close();
				setTimeout(function () {
						window.frames["frame1"].focus();
						window.frames["frame1"].print();
						frame1.remove();
				}, 500);
		});
	});
	</script>



	<style>
		.hidden-print { display: none; }
		.show-print { display: block; }

		@media print {
			.hidden-print { display: block !important; }
			.show-print { display: none !important; }
		}
	</style>