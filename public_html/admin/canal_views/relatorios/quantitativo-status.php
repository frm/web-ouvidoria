

		<?php
		$listJson = '';
		if ( isset($rs_list) && count($rs_list)>0 ){
		?>
		<section class="content">

				<!-- google chart : pie -->
				<div class="border">
					<div id="chart_div" class="chart"></div>
					<div style="clear:both"></div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div class="box">

							<!-- /.box-header -->
							<div class="box-body">
								<table id="tableList" class="table table-bordered table-hover">
									<thead>
									<tr>
										<th>ID</th>
										<th>Natureza</th>
										<th>Status</th>
										<th>Quant</th>
									</tr>
									</thead>
									<tbody>
									<?php
										foreach ($rs_list AS $item){
											$nat_id						= (int)$item->nat_id;
											//$emp_parent_id		= (int)$item->emp_parent_id;
											$nat_titulo				= $item->nat_titulo;
											$den_status				= $item->den_status;
											$quant						= $item->quant;

											//$emp_dte_cadastro	= fct_formatdate($item->emp_dte_cadastro, 'd/m/Y H:i');
											//$icon_ativo				= ($item->emp_ativo==1)?"sim":"não";
											//$redirect					= $url["url_form"] ."/". $emp_id;
											//$count_parent			= (int)$item->count_parent;
											//$show_parent			= ($emp_parent_id>0) ? 'line_parent' : '';

											//$listJson .= !($listJson) ? "," : "";  
											$listJson .= '{"c":[{"v": "'. $nat_titulo .' - '. $den_status .'"}, {"v": '. $quant .'}]},';
									?>
									<tr>
										<td><?php echo($nat_id); ?></td>
										<td><?php echo($nat_titulo); ?></td>
										<td><?php echo($den_status); ?></td>
										<td><?php echo($quant); ?></td>
									</tr>
									<?php
										};
									?>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div><!-- /.box -->
					</div>
					<!-- /.col -->
				</div>

		</section><!-- /.content -->
		<?php
		}
		?> 
		
  </div><!-- //content-wrapper -->




	<?php $this->load->view( "includes/footer" ); ?>

	<?php $this->load->view( "includes/scripts" ); ?>


	<?php if( !empty($listJson) ){ ?>
		<!-- Google Chart -->
		<script src="assets/plugins/googlechart/jsapi.js" type="text/javascript"></script>
		<script src="assets/plugins/googlechart/uds_api_contents.js" type="text/javascript"></script>

		<?php
		//$listPieChartjson = '{
			//"cols":[
				//{"id": "lbl", "label": "label", "type": "string"},
				//{"id": "val", "label": "values", "type": "number"}
			//],
			//"rows": [
				//{"c":[{"v": "jan"}, {"v": 15}]},
				//{"c":[{"v": "fev"}, {"v": 1}]},
				//{"c":[{"v": "ago"}, {"v": 5}]},
				//{"c":[{"v": "set"}, {"v": 8}]},
				//{"c":[{"v": "out"}, {"v": 31}]},
				//{"c":[{"v": "nov"}, {"v": 43}]},
				//{"c":[{"v": "dez"}, {"v": 18}]}
			//]
		//}';
		//print $listJson;
		$listPieChartjson = '{
			"cols":[
				{"id": "lbl", "label": "label", "type": "string"},
				{"id": "val", "label": "values", "type": "number"}
			],
			"rows": [
				'. $listJson .'
			]
		}';
		//echo($json);
		?>
		<script>
		jQuery(document).ready(function($){
			// ----------------------------------------------------
			// pie
			// ----------------------------------------------------
			function pieChart() {

				//// GET DATA JSON
				//var jsonData = $.ajax({
					//url: "json.php",
					//dataType:"html",
					//async: false
				//}).responseText;

				//console.log(jsonData);
				//var data = new google.visualization.DataTable(jsonData);
				var data = new google.visualization.DataTable(<?php echo($listPieChartjson); ?>);

				// Set chart options
				var options = {
					'title':'',
					pieSliceText: 'percentage', // value
					is3D: true
				};
				// Instantiate and draw our chart, passing in some options.
				var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
				chart.draw(data, options);
			}
			google.setOnLoadCallback(pieChart);
		});
		</script>

		<style>
			.center{
				display:table;
				width:100%;
				margin:0 auto;
				text-align:center;
				border:0px solid #ff0000; 
			}
			.border {
				background-color: #FFFFFF;
				border:3px solid #DDDDDD; 
				border-radius: 3px;
				width:90%;
				margin:10px auto;
				text-align:center;
			}
			.chart {
				width:95%;
				min-height:380px;	
			}
		</style>
	<?php }// !empty : listJson   ?>
