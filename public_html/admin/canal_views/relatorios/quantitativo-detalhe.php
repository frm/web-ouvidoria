
	<div id="boxRelNaturezaList" class="content-wrapper-modal boxprinter">
		<?php
		$listJson = '';
		if ( isset($rs_list) && count($rs_list)>0 ){
		?>
				<div class="row" style="margin-bottom:25px;">
					<div class="col-xs-12">
						<table border="0" cellpadding="2" cellspacing="2" width="100%">
							<tr>
								<td class="hidden-print" valign="top">
									<h2 style="padding:10px 0 !important; margin:5px 0 !important;">LISTAGEM DE DENÚNCIA POR NATUREZA</h2>
								</td>
								<td class="show-print" valign="top">
									<div class="pull-right ">
										<a href="javascript:;" class="linkClickPrinter" data-target="boxRelNaturezaList">imprimir</a>
									</div>
								</td>
							</tr>
						</table>
					</div>
					<div style="clear:both;"></div>
				</div>

				<div class="row">
					<div class="col-xs-6">
						<h3 style="padding:0 !important; margin:0 !important;"><?php echo($rs_nat->nat_titulo); ?></h3>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div class="box">

							<!-- /.box-header -->
							<div class="box-body">
								<table id="tableList" class="table table-bordered table-hover">
									<thead>
									<tr>
										<th style="width: 170px;">Data</th>
										<th style="width: 165px;">Protocolo</th>
										<th style="width: 135px;">Status</th>
										<th style="width: 260px;">Autor</th>
										<th >Denúncia</th>
									</tr>
									</thead>
									<tbody>
									<?php
										foreach ($rs_list AS $item){
											$den_id							= $item->den_id;
											$den_dte_cadastro		= $item->den_dte_cadastro;
											$den_num_protocolo	= $item->den_num_protocolo;
											$den_status					= $item->den_status;
											$den_descricao			= strip_tags($item->den_descricao);
											$dnt_json						= json_decode($item->dnt_json);
											$quant							= '';

											$dnt_nome						= isset($dnt_json->dnt_nome) ? $dnt_json->dnt_nome : 'anônimo';
									?>
									<tr>
										<td><?php echo($den_dte_cadastro); ?></td>
										<td><?php echo($den_num_protocolo); ?></td>
										<td><?php echo($den_status); ?></td>
										<td><?php echo($dnt_nome); ?></td>
										<td>
											<a href="javascript:;" data-href="<?php echo( $url["url_list"] .'/denuncia' ); ?>" data-remote="false" data-toggle="modal-den" data-target="#myModal2" data-code="<?php echo($den_id); ?>"><?php echo($den_descricao); ?></a>
										</td>
									</tr>
									<?php
										};
									?>
									</tbody>
								</table>





							</div>
							<!-- /.box-body -->
						</div><!-- /.box -->
					</div>
					<!-- /.col -->
				</div>

		<?php
		}
		?> 
  </div><!-- //content-wrapper -->


	<script>
	jQuery(document).ready(function($){			





		$('a[data-toggle="modal-den"]').on('click', function(e) {
			var target_modal = $(e.currentTarget).data('target');
			var $href = $(e.currentTarget).data('href');
			var remote_content = $href +"/?den_id="+ $(e.currentTarget).data("code");

			console.log( target_modal );

			// Find the target modal in the DOM
			var modal = $(target_modal);
			var modalBody = $(target_modal + ' .modal-body');

			//modal.on('show.bs.modal', function () {
					//modalBody.load(remote_content);
			//}).modal();

			$(target_modal).modal({
				show: true,
				keyboard: false,
				backdrop: 'static',
				remote: modalBody.load(remote_content)
			});


			// Now return a false (negating the link action) to prevent Bootstrap's JS 3.1.1
			// from throwing a 'preventDefault' error due to us overriding the anchor usage.
			return false;
		});





	});
	</script>



	<script>
	jQuery(document).ready(function($){
		$('.linkClickPrinter').on('click', function(e) {
				var $target = $(this).data('target');
				
				var contents = $("#"+ $target).html();
				var frame1 = $('<iframe />');
				frame1[0].name = "frame1";
				frame1.css({ "position": "absolute", "top": "-1000000px" });
				$("body").append(frame1);
				var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
				frameDoc.document.open();
				//Create a new HTML document.
				frameDoc.document.write('<html><head><title>DIV Contents</title>');
				frameDoc.document.write('</head><body>');

				//Append the external CSS file.
				frameDoc.document.write('<link href="<?php echo(base_url()); ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />');
				frameDoc.document.write('<link href="<?php echo(base_url()); ?>assets/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />');
				frameDoc.document.write('<link href="<?php echo(base_url()); ?>assets/css/custom.css" rel="stylesheet" type="text/css" />');

				//Append the DIV contents.
				frameDoc.document.write(contents);
				frameDoc.document.write('</body></html>');
				frameDoc.document.close();
				setTimeout(function () {
						window.frames["frame1"].focus();
						window.frames["frame1"].print();
						frame1.remove();
				}, 500);
		});
	});
	</script>



	<style>
		.hidden-print { display: none; }
		.show-print { display: block; }

		@media print {
			.hidden-print { display: block !important; }
			.show-print { display: none !important; }
		}
	</style>