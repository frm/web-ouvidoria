

		<?php
		$listJson = '';
		if ( isset($rs_list) && count($rs_list)>0 ){
		?>
		<section id="boxRelAnalistasGroup" class="content ">


				<div class="row" style="margin-bottom:25px;">
					<div class="col-xs-12">
						<table border="0" cellpadding="2" cellspacing="2" width="100%">
							<tr>
								<td valign="top">
									<h2 style="padding:10px 0 !important; margin:5px 0 !important;">LISTAGEM DE NATUREZAS E RESPONSÁVEIS</h2>
								</td>
								<td class="show-print" valign="top">
									<div class="pull-right ">
										<a href="javascript:;" class="btn btn-success linkClickPrinter" data-target="boxRelAnalistasGroup">imprimir</a>
									</div>
								</td>
							</tr>
						</table>
					</div>
					<div style="clear:both;"></div>
				</div>


				<!-- google chart : pie -->
				<div class="border" style="display:none;">
					<div id="chart_div" class="chart"></div>
					<div style="clear:both"></div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div class="box">

							<!-- /.box-header -->
							<div class="box-body">
								<table id="tableList" class="table table-bordered table-hover">
									<thead>
									<tr>
										<th style="width: 75px;">ID</th>
										<th style="width: 250px;">Natureza</th>
										<th>Responsável</th>
									</tr>
									</thead>
									<tbody>
									<?php
										$html = '';
										$nat_atual = '';
										foreach ($rs_list AS $key=>$item){
											$nat_titulo		= $item["nat_titulo"];
											$nat_id				= (int)$item["nat_id"];
											//$ana_id			= $item["ana_id"];
											$arr_respo		= $item["responsaveis"];

											$html_resp = '';
											foreach ($arr_respo AS $keyResp=>$itemResp){
												$ana_nome			= $itemResp["ana_nome"];
												$ananat_ativo	= $itemResp["ananat_ativo"];
												$ananat_dte_alteracao = $itemResp["ananat_data"];

												$html_resp .= '
													<tr>
														<td>'. $ana_nome .'</td>
														<td>'. $ananat_ativo .'</td>
														<td>'. $ananat_dte_alteracao .'</td>
													</tr>
												';
											}
											if( !empty($html_resp) )
											{
												$html_itens = '
													<table id="tableList" class="table table-bordered table-hover" style="margin-bottom:5px;">
														<thead>
														<tr>
															<th>Nome</th>
															<th style="width: 80px;">Ativo</th>
															<th style="width: 180px;">Data</th>
														</tr>
														</thead>
														<tbody>
														'. $html_resp .'
														</tbody>
													</table>
												';										
											}



											//$emp_dte_cadastro	= fct_formatdate($item->emp_dte_cadastro, 'd/m/Y H:i');
											//$icon_ativo				= ($item->emp_ativo==1)?"sim":"não";
											//$redirect					= $url["url_form"] ."/". $nat_id;
											//$count_parent			= (int)$item->count_parent;
											//$show_parent			= ($emp_parent_id>0) ? 'line_parent' : '';

											//$listJson .= !($listJson) ? "," : "";  
											//$listJson .= '{"c":[{"v": "'. $nat_titulo .'"}, {"v": '. $quant .'}]},';
		
											$html .= '
												<tr>
													<td>'. $nat_id .'</td>
													<td>'. $nat_titulo .'</td>
													<td>'. $html_itens .'</td>
												</tr>
											';
										};
										echo( $html );
									?>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div><!-- /.box -->
					</div>
					<!-- /.col -->
				</div>

		</section><!-- /.content -->
		<?php
		}
		?> 		
  </div><!-- //content-wrapper -->









	<?php $this->load->view( "includes/footer" ); ?>

	<?php $this->load->view( "includes/scripts" ); ?>

	<?php $this->load->view( "includes/modal" ); ?>


	<?php if( !empty($listJson) ){ ?>
		<!-- Google Chart -->
		<script src="assets/plugins/googlechart/jsapi.js" type="text/javascript"></script>
		<script src="assets/plugins/googlechart/uds_api_contents.js" type="text/javascript"></script>

		<?php
		$listPieChartjson = '{
			"cols":[
				{"id": "lbl", "label": "label", "type": "string"},
				{"id": "val", "label": "values", "type": "number"}
			],
			"rows": [
				'. $listJson .'
			]
		}';
		//echo($json);
		?>
	<?php }// !empty : listJson   ?>

		<script>
		jQuery(document).ready(function($){			

			$('.closeModal').on("click", function(event){
				//$('.modal.in').modal('hide');
				$('#myModal2').modal('hide');

				console.log( $('.modal.in').length );


				if( $('.modal.in').length >=1 ){
					//$('body').css(".modal-open .modal", "overflow-y:scroll;");
					$('body').css("margin-right", "0px");					
					$(document.body).addClass( 'modal-noscrollbar' );
					$(document.body).addClass( 'modal-open' );
					//$('body').css({overflow:'hidden'});
				}

				//$.each(BootstrapDialog.dialogs, function(id, dialog){
					//console.log(id);
					//console.log(dialog);
					////dialog.close();
				//});

			});

			//$(document).bind('cbox_open', function(){
				//$('body').css({overflow:'hidden'});
				////$('.youtube').css({display:'none'});	
			//}).bind('cbox_closed', function(){ 
				//$('body').css({overflow:'auto'});
				////$('.youtube').css({display:'block'});
			//});

			//$('#myModal2').modal({
				//show: true,
				//keyboard: false,
				//backdrop: 'static'
			//});


			$('a[data-toggle="modal"]').on('click', function(e) {
				var target_modal = $(e.currentTarget).data('target');
				var $href = $(e.currentTarget).data('href');
				var remote_content = $href +"/?nat_id="+ $(e.currentTarget).data("code");

				console.log( target_modal );

				// Find the target modal in the DOM
				var modal = $(target_modal);
				var modalBody = $(target_modal + ' .modal-body');

				//modal.on('show.bs.modal', function () {
						//modalBody.load(remote_content);
				//}).modal();

				$(target_modal).modal({
					show: true,
					keyboard: false,
					backdrop: 'static',
					remote: modalBody.load(remote_content)
				});


				// Now return a false (negating the link action) to prevent Bootstrap's JS 3.1.1
				// from throwing a 'preventDefault' error due to us overriding the anchor usage.
				return false;
			});

		});
		</script>

		<script>
		jQuery(document).ready(function($){
			$(document).on('click', '.linkClickPrinter', function (e) {
			//$('.linkClickPrinter').on('click', function(e) {
					var $target = $(this).data('target');
					
					var contents = $("#"+ $target).html();
					var frame1 = $('<iframe />');
					frame1[0].name = "frame1";
					frame1.css({ "position": "absolute", "top": "-1000000px" });
					$("body").append(frame1);
					var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
					frameDoc.document.open();
					//Create a new HTML document.
					frameDoc.document.write('<html><head><title>DIV Contents</title>');
					frameDoc.document.write('</head><body>');

					//Append the external CSS file.
					frameDoc.document.write('<link href="<?php echo(base_url()); ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />');
					frameDoc.document.write('<link href="<?php echo(base_url()); ?>assets/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />');
					frameDoc.document.write('<link href="<?php echo(base_url()); ?>assets/css/custom.css" rel="stylesheet" type="text/css" />');

					//Append the DIV contents.
					frameDoc.document.write(contents);
					frameDoc.document.write('</body></html>');
					frameDoc.document.close();
					setTimeout(function () {
							window.frames["frame1"].focus();
							window.frames["frame1"].print();
							frame1.remove();
					}, 500);
			});
		});
		</script>

		<style>
			.center{
				display:table;
				width:100%;
				margin:0 auto;
				text-align:center;
				border:0px solid #ff0000; 
			}
			.border {
				background-color: #FFFFFF;
				border:3px solid #DDDDDD; 
				border-radius: 3px;
				width:90%;
				margin:10px auto;
				text-align:center;
			}
			.chart {
				width:95%;
				min-height:380px;	
			}

			#myModal2
			{
				overflow: hidden;
			}
			.modal {
				overflow-y: auto;
			}
			/* custom class to override .modal-open */
			.modal-noscrollbar {
				margin-right: 0 !important;
			}
		</style>
	
