<!DOCTYPE html>
<html>
<head>
	<base href="<?php echo(base_url()); ?>" />
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WebOuvidoria | Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
	<!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  
	<!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  
	<!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  
	<!-- Theme style -->
  <link rel="stylesheet" href="assets//css/AdminLTE.css">
  
	<!-- iCheck -->
  <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">



<div class="login-box">

  <div class="login-logo">
	<a href="../../index2.html"><img src="../assets/fonts/img/logo-web-ouvidoria.png" style="max-height:95px;"></a>
  </div>

  <!-- /.login-logo -->
  <div class="login-box-body" style="padding-top:30px;">

		<!--
		<p class="login-box-msg">
			Sign in to start your session
		</p>
		-->

		<?php
		if( isset($message_error) && !empty($message_error) ){
		?>
			<div class="callout callout-danger">
				<p><?php echo($message_error)?></p>
			</div>
		<?php
		}
		?>


		<!--## INI : FORMULARIO -->
		<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmFormulario" id="frmFormulario">
			<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORMULARIO">

			<div class="form-group has-feedback">
        <input type="text" name="str_email" id="str_email" class="form-control" placeholder="Email" value="">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        <input type="password" name="str_senha" id="str_senha" class="form-control" placeholder="Password" value="">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Lembrar
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
        </div>
        <!-- /.col -->
      </div>
    </FORM>
		<!--## END : FORMULARIO -->





    <!--
		<div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div>
		-->

		<br />
		<br />
    <a href="javascript:;">Esqueci a minha senha</a><br>
    <!--
		<a href="register.html" class="text-center">Efetuar novo cadastrro</a>
		-->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->



<!-- jQuery 2.2.3 -->
<script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugins/iCheck/icheck.min.js"></script>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });

		$(document).ready(function(){ 
			//$('#str_email_sss').val('Email');
			//$('#str_senha_sss').val('----');
		}); 
  });
</script>

</body>
</html>