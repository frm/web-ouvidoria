<?php $this->load->view( "includes/doctype" ); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


	<?php $this->load->view( "includes/header" ); ?>

  <?php $this->load->view( "includes/menu" ); ?>



  <div class="content-wrapper">

    <section class="content-header">
      <h1>
        Gerenciar Naturezas
        <small>Listagem</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="javascript:;"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="javascript:;">Naturezas</a></li>
        <li class="active">Listagem</li>
      </ol>
    </section>


		<!--## INI : FORMULARIO -->
		<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmFormulario" id="frmFormulario" enctype="multipart/form-data">
			<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORMULARIO">
			<input type="hidden" name="strAcao" ID="strAcao" value="<?php echo(isset($edit_rows->post_id)? "UPDATE" : "INSERT"); ?>">
			<input type="hidden" name="idregistro" ID="idregistro" value="<?php echo(isset($edit_rows->post_id)? $edit_rows->post_id : ""); ?>">

			<section class="content">


				<!-- botoes de acoes -->
				<div class="row" style="margin-bottom:15px;">
					<div class="col-xs-12" >
						<p style="float:right;">
							<button class="btn btn-info mrBtnAddNovo" rel="<?php echo( $url["url_form"] ); ?>"><i class="fa fa-edit"></i> Criar Novo Registro</button>
							<button class="btn btn-danger mrBtnDelReg" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-trash-o"></i> Excluir</button>
						</p>

						<div class="clear"></div>
						<div style="border-bottom: 1px solid #D8D8D8;"></div>
					</div><!-- /.col -->
				</div><!-- /.row // botoes de acoes-->


				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Lista de naturezas</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<table id="tableList" class="table table-bordered table-hover">
									<thead>
									<tr>
										<th class="center" style="width: 50px;">#</th>
										<th>ID</th>
										<th class="center" style="width: 80px;">Imagem</th>
										<th>Título da natureza</th>
										<th style="width: 110px;">Prazo padrão</th>
										<th style="width: 80px;">É padrão?</th>
										<th class="center" style="width: 140px;">Data</th>
										<th class="center" style="width: 80px;">Ativo</th>
										<th class="center" style="width: 90px;">Ação</th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ( isset($rs_list) ){
										foreach ($rs_list AS $item){
											$nat_id						= (int)$item->nat_id;
											$nat_arquivo			= $item->nat_arquivo;
											$nat_titulo				= $item->nat_titulo;
											$nat_prazo				= $item->nat_prazo;
											$nat_dte_cadastro	= fct_formatdate($item->nat_dte_cadastro, 'd/m/Y H:i');
											$icon_ativo				= ($item->nat_ativo==1)?"sim":"não";
											$icon_padrao			= ($item->nat_padrao==1)?"sim":"não";
											$redirect					= $url["url_form"] ."/". $nat_id;
											$url_ajax					= $url["url_list"] ."/json/EXCLUIR-NATUREZA/";
									?>
									<tr id="tr-row-<?php echo($nat_id); ?>">
										<td class="center">
											<input type="checkbox" name="chk_delete[]" ID="chk_delete_<?php echo($nat_id); ?>" value="<?php echo($nat_id); ?>" class="minimal">
										</td>
										<td><?php echo($nat_id); ?></td>
										<td class="center">
											<?php
												$nat_arquivo_tag = '<img data-src="holder.js" class="rounded" alt="Sem Imagem" style="margin:0 auto; width: 50px; height: 50px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15cc09e90d7%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15cc09e90d7%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2275.5%22%20y%3D%22104.5%22%3EImagem%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">';

												$image_path = $this->config->item('folder_images') .'/naturezas/'. $nat_arquivo;
												if( file_exists($image_path) and is_file($image_path) )
												{
													$load_image = base_url($image_path);
													$nat_arquivo_tag = '<img src="'. $load_image .'" alt="" class="rounded" style="width: 50px; height: 50px;" />';
												};
												print $nat_arquivo_tag;
											?>
										</td>
										<td><a href="<?php echo($redirect); ?>"><?php echo($nat_titulo); ?></a></td>
										<td><?php echo($nat_prazo); ?></td>
										<td class="center"><?php echo($icon_padrao); ?></td>
										<td class="center"><?php echo($nat_dte_cadastro); ?></td>
										<td class="center"><?php echo($icon_ativo); ?></td>
										<td class="center">
											<div style="padding:0 10px; display:inline-block">
												<a href="<?php echo($redirect); ?>"><i class="fa fa-edit fa-ft15px"></i></a>
											</div>
											<div style="padding:0 10px; display:inline-block">
												<a href="javascript:;" class="btnDeleteAjax" data-url="<?php echo($url_ajax); ?>" data-id="<?php echo($nat_id); ?>"><i class="fa fa-trash-o fa-ft15px"></i></a>
											</div>
										</td>
									</tr>
									<?php
										};
									};// isset : rs_list
									?>
									</tbody>
									<!--
									<tfoot>
									<tr>
										<th>ID</th>
										<th>Razão Social</th>
										<th>E-mail</th>
										<th>Data</th>
										<th>Ativo</th>
									</tr>
									</tfoot>
									-->
								</table>
							</div>
							<!-- /.box-body -->
						</div><!-- /.box -->
					</div>
					<!-- /.col -->
				</div>

			</section><!-- /.content -->
		</FORM>
		<!--## END : FORMULARIO -->
    
  </div>



	<?php $this->load->view( "includes/footer" ); ?>

	<?php $this->load->view( "includes/scripts" ); ?>
