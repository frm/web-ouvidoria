<?php $this->load->view( "includes/doctype" ); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


	<?php $this->load->view( "includes/header" ); ?>

  <?php $this->load->view( "includes/menu" ); ?>



  <div class="content-wrapper">


    <section class="content-header">
      <h1>
        Gerenciar Naturezas
        <small>Formulário</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="javascript:;"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="javascript:;">Naturezas</a></li>
        <li class="active">Formulário</li>
      </ol>
    </section>


		<!--## INÍCIO DO FORMULÁRIO -->
		<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmFormulario" id="frmFormulario" enctype="multipart/form-data">
			<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORMULARIO">
			<input type="hidden" name="nat_id" ID="nat_id" value="<?php echo(isset($rs_bd->nat_id)? $rs_bd->nat_id : ""); ?>" />

			<section class="content">

				<!-- botoes de acoes -->
				<div class="row" style="margin-bottom:15px;">
					<div class="col-xs-12" >
						<p style="float:right;">
							<button class="btn btn-info mrBtnAddNovo" rel="<?php echo( $url["url_form"] ); ?>"><i class="fa fa-edit"></i> Criar Novo Registro</button>
							<button class="btn btn-warning mrBtnListReg" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-list-alt"></i> Lista de Registros</button>
							<button class="btn btn-success mrBtnSaveReg2" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-check-square-o"></i> Salvar</button>
						</p>

						<div class="clear"></div>
						<div style="border-bottom: 1px solid #D8D8D8;"></div>
					</div><!-- /.col -->
				</div><!-- /.row // botoes de acoes-->


				<?php if( $this->session->flashdata('message_validate') ) { ?>
					<div class="row">
						<div class="col-lg-12">
							<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<h4><i class="icon fa fa-check"></i></i> Atenção!</h4>
								<?php echo( $this->session->flashdata('message_validate') ); ?>
							</div>
						</div>
					</div>
				<?php } ?>


				<div class="row">
					<div class='col-xs-12'>
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active ">
									<a href="#tab-mod_formulario" data-toggle="tab">Detalhes Principais</a>
								</li>
								<li class="hide">
									<a href="#tab-mod_galeria" data-toggle="tab">Galeria de Fotos</a>
								</li>
							</ul>

							<div class="tab-content">
								
								<!-- tab : tab-mod_formulario -->
								<div class="tab-pane active " id="tab-mod_formulario">
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
									<div class="row">

										<div class="col-md-8">
											<!-- box principal --> 
											<div class="box slim box-primary">
												<div class="box-header with-border">
													<h3 class="box-title">Dados da natureza</h3>
												</div><!-- /.box-header -->

												<div class="box-body">

													<div class="row" >
														<div class="col-xs-6">
															<div class="form-group">
																<label for="nat_titulo">Título</label>
																<input type="text" name="nat_titulo" id="nat_titulo" class="form-control" placeholder="" value="<?php echo(isset($rs_bd->nat_titulo)? $rs_bd->nat_titulo : ""); ?>" />
															</div>
														</div>

														<div class="col-xs-6">
															<div class="form-group">
																<label for="nat_prazo">Prazo</label>
																<input type="text" name="nat_prazo" id="nat_prazo" class="form-control" value="<?php echo(isset($rs_bd->nat_prazo)? $rs_bd->nat_prazo : ""); ?>" />
															</div>
														</div>
													</div><!-- /.row -->

													<div class="row" >
														<div class="col-xs-12">
															<div class="form-group">
																<label for="nat_descricao">Descrição</label>
																<textarea name="nat_descricao" id="nat_descricao" class="fld_ckeditor" rows="10" cols="80" style="width:100%;"><?php echo(isset($rs_bd->nat_descricao)? $rs_bd->nat_descricao : ""); ?></textarea> 
															</div>
														</div>
													</div><!-- /.row -->

												</div><!-- /.box-body -->

												<div class="box-footer hide">
													<button type="submit" class="btn btn-primary">Salvar</button>
												</div><!-- /.box-footer -->

											</div><!-- /.box -->
										</div>

										<div class="col-md-4">
											<div class="box slim box-primary">

												<div class="box-header with-border">
													<h3 class="box-title">Configurações</h3>
												</div><!-- /.box-header -->

												<div class="box-body">
													<div class="form-group smXT">
														<?php
														$nat_ativo = 1;
														$nat_ativo = isset($rs_bd->nat_ativo)?$rs_bd->nat_ativo:1;
														?>
														<label for="nat_ativo" class="lblsm">Ativo</label>
														<select name='nat_ativo' id='nat_ativo' class="form-control">
															<option value="1" <?php echo($nat_ativo=="1"? "selected":""); ?>>Sim</option>
															<option value="0" <?php echo($nat_ativo=="0"? "selected":""); ?>>Não</option>
														</select>
													</div>

													<div class="form-group smXT">
														<?php
														$nat_padrao = 0;
														$nat_padrao = isset($rs_bd->nat_padrao)?$rs_bd->nat_padrao:0;
														?>
														<label for="nat_padrao" class="lblsm">Usar como padrão?</label>
														<select name='nat_padrao' id='nat_padrao' class="form-control">
															<option value="1" <?php echo($nat_padrao=="1"? "selected":""); ?>>Sim</option>
															<option value="0" <?php echo($nat_padrao=="0"? "selected":""); ?>>Não</option>
														</select>
													</div>
												</div><!-- /.box-body -->
											</div>

											<div class="box slim box-primary">

												<div class="box-header with-border">
													<h3 class="box-title">Imagem Relacionada</h3>
												</div><!-- /.box-header -->

												<div class="box-body">
													<div class="form-group smXT">
														<div style="margin:0 auto; border:0px dotted red; text-align:center;">
															<?php
																$nat_arquivo = isset($rs_bd->nat_arquivo)?$rs_bd->nat_arquivo:'';

																$nat_arquivo_tag = '<img data-src="holder.js" class="rounded" alt="Sem Imagem" style="margin:0 auto; width: 75%; height: 200px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15cc09e90d7%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15cc09e90d7%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2275.5%22%20y%3D%22104.5%22%3EImagem%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">';

																$show_file = false;
																$image_path = $this->config->item('folder_images') .'/naturezas/'. $nat_arquivo;
																if( file_exists($image_path) and is_file($image_path) )
																{
																	$load_image = base_url($image_path);
																	$nat_arquivo_tag = '<img src="'. $load_image .'" alt="" class="img-responsive" style="height: 200px;" />';
																};
																print $nat_arquivo_tag;
															?>
														</div>
													</div>

													<div class="form-group smXT">
														<input type="hidden" name="strArqFileUpl" id="strArqFileUpl" value="<?php echo($nat_arquivo); ?>" />
														<input type="file" name="uplArqFileUpl" id="uplArqFileUpl" class="fld_file" />
													</div>
												</div><!-- /.box-body -->
											</div>
										</div>

									</div>
									<div class="clear"></div>
									<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
								</div>

								<!-- tab : tab-content -->
								<div class="tab-pane " id="tab-mod_galeria">
									tab-mod_galeria
								</div>

							</div>

						</div>
					</div>
				</div><!-- /.row -->

			</section><!-- /.content -->
		</FORM>
    
  </div>



	<?php $this->load->view( "includes/footer" ); ?>

	<?php $this->load->view( "includes/scripts" ); ?>


	<script>
		jQuery(document).ready(function ($) {
			$(document).on('click', '.mrBtnSaveReg2', function (e) {
				//e.preventDefault();
				var $form	= $('form#frmFormulario');
				var $msg	= '';

				var $nat_titulo = $form.find("#nat_titulo");
				var $nat_prazo = $form.find("#nat_prazo");

				if( $nat_titulo.val().length == 0 )			{ $msg += "<p>- Preencha corretamente o titulo.</p>"; }
				if( $nat_prazo.val().length == 0 )			{ $msg += "<p>- Preencha corretamente o prazo.</p>"; }

				if( $msg.length > 0)
				{
					$.alert({
						title: 'Atenção',
						confirmButtonClass: 'btn-info',
						cancelButtonClass: 'btn-danger',
						confirmButton: 'OK',
						//cancelButton: 'NO never !',
						content: $msg,
						confirm: function () {
							//$.alert('Confirmed!');
						}
					});
					return false;
				}else{
					$form.submit();
				}
			});
		});
	</script>
