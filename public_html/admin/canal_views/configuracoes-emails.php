<?php $this->load->view( "includes/doctype" ); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">


	<?php $this->load->view( "includes/header" ); ?>

  <?php $this->load->view( "includes/menu" ); ?>



  <div class="content-wrapper">

    <section class="content-header">
      <h1>
        Gerenciar Configurações / Emails
        <small>Listagem</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="javascript:;"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="javascript:;">Emails</a></li>
        <li class="active">Listagem</li>
      </ol>
    </section>


		<!--## INI : FORMULARIO -->
		<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmFormulario" id="frmFormulario" enctype="multipart/form-data">
			<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORMULARIO">
			<input type="hidden" name="cfg_id" ID="cfg_id" value="<?php echo(isset($edit_rows->cfg_id)? $edit_rows->cfg_id : ""); ?>">

			<section class="content">


				<!-- botoes de acoes -->
				<div class="row" style="margin-bottom:15px;">
					<div class="col-xs-12" >
						<p style="float:right;">
							<button class="btn btn-info mrBtnAddNovo" rel="<?php echo( $url["url_form"] ); ?>"><i class="fa fa-edit"></i> Criar Novo Registro</button>
							<button class="btn btn-danger mrBtnDelReg" rel="<?php echo( $url["url_list"] ); ?>"><i class="fa fa-trash-o"></i> Excluir</button>
						</p>

						<div class="clear"></div>
						<div style="border-bottom: 1px solid #D8D8D8;"></div>
					</div><!-- /.col -->
				</div><!-- /.row // botoes de acoes-->


				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Lista de registros</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<table id="tableList" class="table table-bordered table-hover">
									<thead>
									<tr>
										<th class="center" style="width: 50px;">#</th>
										<th style="width: 70px;">ID</th>
										<th style="width: 150px;">Área</th>
										<th>Chave</th>
										<th class="center" style="width: 140px;">Data</th>
										<th class="center" style="width: 80px;">Ativo</th>
										<th class="center" style="width: 90px;">Ação</th>
									</tr>
									</thead>
									<tbody>
									<?php
									if ( isset($rs_list) ){
										foreach ($rs_list AS $item){
											$cfg_id						= (int)$item->cfg_id;
											$cfg_area					= $item->cfg_area;
											$cfg_chave				= $item->cfg_chave;
											$cfg_dte_cadastro	= fct_formatdate($item->cfg_dte_cadastro, 'd/m/Y H:i');
											$icon_ativo				= ($item->cfg_ativo==1)?"sim":"não";
											$redirect					= $url["url_form"] ."/". $cfg_id;
											$url_ajax					= $url["url_list"] ."/json/EXCLUIR-NATUREZA/";
									?>
									<tr id="tr-row-<?php echo($cfg_id); ?>">
										<td class="center">
											<input type="checkbox" name="chk_delete[]" ID="chk_delete_<?php echo($cfg_id); ?>" value="<?php echo($cfg_id); ?>" class="minimal">
										</td>
										<td><?php echo($cfg_id); ?></td>
										<td><a href="<?php echo($redirect); ?>"><?php echo($cfg_area); ?></a></td>
										<td><?php echo($cfg_chave); ?></td>
										<td class="center"><?php echo($cfg_dte_cadastro); ?></td>
										<td class="center"><?php echo($icon_ativo); ?></td>
										<td class="center">
											<div style="padding:0 10px; display:inline-block">
												<a href="<?php echo($redirect); ?>"><i class="fa fa-edit fa-ft15px"></i></a>
											</div>
											<div style="padding:0 10px; display:inline-block">
												<a href="javascript:;" class="btnDeleteAjax" data-url="<?php echo($url_ajax); ?>" data-id="<?php echo($cfg_id); ?>"><i class="fa fa-trash-o fa-ft15px"></i></a>
											</div>
										</td>
									</tr>
									<?php
										};
									};// isset : rs_list
									?>
									</tbody>
									<!--
									<tfoot>
									<tr>
										<th>ID</th>
										<th>Razão Social</th>
										<th>E-mail</th>
										<th>Data</th>
										<th>Ativo</th>
									</tr>
									</tfoot>
									-->
								</table>
							</div>
							<!-- /.box-body -->
						</div><!-- /.box -->
					</div>
					<!-- /.col -->
				</div>

			</section><!-- /.content -->
		</FORM>
		<!--## END : FORMULARIO -->
    
  </div>



	<?php $this->load->view( "includes/footer" ); ?>

	<?php $this->load->view( "includes/scripts" ); ?>
