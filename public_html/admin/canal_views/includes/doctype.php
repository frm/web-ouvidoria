<!DOCTYPE html>
<html>
<head>
	<base href="<?php echo(base_url()); ?>" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WebOuvidoria : </title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo(base_url()); ?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo(base_url()); ?>assets/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo(base_url()); ?>assets/plugins/datepicker/datepicker3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo(base_url()); ?>assets/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo(base_url()); ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo(base_url()); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
	
	<!-- Plugins -->
	<link type="text/css" rel="stylesheet" href="<?php echo(base_url()); ?>assets/plugins/jquery-confirm/css/jquery-confirm.css" />
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo(base_url()); ?>assets/plugins/select2/select2.min.css">

	<link rel="stylesheet" href="<?php echo(base_url()); ?>assets/plugins/datatables/dataTables.bootstrap.css">


  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo(base_url()); ?>assets/bootstrap/css/bootstrap.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo(base_url()); ?>assets/css/AdminLTE.min.css">
	<link rel="stylesheet" href="<?php echo(base_url()); ?>assets/css/custom.css">

  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo(base_url()); ?>assets/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->


