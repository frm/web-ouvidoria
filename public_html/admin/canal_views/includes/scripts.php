
<!-- jQuery 2.2.3 -->
<script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="assets/bootstrap/js/bootstrap.js"></script>
<!-- Select2 -->
<script src="assets/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="assets/plugins/daterangepicker/daterangepicker.js"></script>

<!-- bootstrap datepicker -->
<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>

<!-- datepickerun -->
<script src="assets/plugins/datepickerun/js/lang/pt-br.js" type="text/javascript"></script>
<script src="assets/plugins/datepickerun/js/datepicker.min.js" type="text/javascript"></script>
<link href="<?php echo(base_url()); ?>assets/plugins/datepickerun/css/datepicker.css" rel="stylesheet" type="text/css" />

<!-- bootstrap color picker -->
<script src="assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="assets/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="assets/plugins/fastclick/fastclick.js"></script>

<!-- File Style -->
<script src="assets/plugins/filestyle/bootstrap-filestyle.min.js"></script>

<!-- DataTables -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

<!-- jquery-confirm -->
<script src="assets/plugins/jquery-confirm/js/jquery-confirm.js"></script>

<!-- jquery-printThis -->
<script src="assets/plugins/printer/printThis.js"></script>

<!-- Template -->
<script src="assets/plugins/template/jquery.tmpl.js" type="text/javascript"></script>

<script src="assets/js/common.js"></script>

<!-- AdminLTE App -->
<script src="assets/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="assets/js/demo.js"></script>
<!-- Page script -->
<script>
  jQuery(document).ready(function($){
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    
		//Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    
		//Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

    ////Date picker
    //$('.field-date').datepicker({
      //autoclose: true
    //});

		$('.field-date').each(function(){
			var $id = $(this).attr('id');
			var $idpos = $(this).closest('.input-group').find('.icon-date-picker').attr('id');

			var obj = {};
			obj[$id] = "%d/%m/%Y";

			//console.log( $id );
			//console.log( $idpos );

			//var opts = { formElements:{"cad_dte_inicio":"%d/%m/%Y"}, positioned:"imgdte-cad_dte_inicio" };
			var opts = { 
				formElements:obj, 
				positioned: $idpos,
				clickActivated: true
			};
			//console.log( opts );
			datePickerController.createDatePicker(opts);
		});

		$(document).on('click', '.field-date', function (event) {
			event.preventDefault();
			var $idpos = $(this).closest('.input-group').find('.icon-date-picker').attr('id');
			$( "#"+ $idpos ).find('.date-picker-control ').trigger( "click" );
		});


    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });


    //$('#tableList').DataTable({
      //"paging": true,
      //"lengthChange": false,
      //"searching": false,
      //"ordering": true,
      //"info": true,
      //"autoWidth": false
    //});

			//$('.filestyle').filestyle({
				//buttonName : 'btn-primary',
				//buttonBefore : false
			//});
		


		$('.fld_file').each(function(){
			$('#' + $(this).attr('id')).filestyle({
				icon : false,
				buttonBefore : true,
				buttonName : 'btn-default',
				buttonText : 'Anexar Arquivo'
			});
		});

		$('.fld_ckeditor').each(function(){
			//CKEDITOR.replace( $(this).attr('id'), {} );
			var editor = CKEDITOR.replace( $(this).attr('id'), {
				//allowedContent: true
				//filemanagerImageBrowserURL: 'http://localhost:85/_A99/Intac/dev/index.php/admin/filemanager/listfiles',
				//filemanagerImageBrowserUPL: 'http://localhost:85/_A99/Intac/dev/index.php/admin/filemanager/formupload'
				//customConfig: 'ckeditor_config.js',
				//extraPlugins: 'codesnippet',
				//extraPlugins: 'filemanager-image-browser',
			});
		});

		$('.autotab').keydown(function(event) {
			//save previously entered value prior to keyup event
			$(this).attr('prevValue', $(this).val());
		});
		 
		var $eCur = $('.autotab').keyup(function(event) {
			var newValue = $(this).val();
			//alert( $(this).next('.autotab').attr('id') );
			//see if the textbox had its value changed
			if ($(this).attr('prevValue') != newValue) {
				//see if the number of entered characters is equal or greater
				//than the allowable maxlength
				if (newValue.length >= $(this).attr('maxlength')) {
					var elNext = new Number($eCur.index(this) + 1);
					//var iNext = new Number(iCur+1);
					//alert( iNext +' // '+  $('.autotab').size());
					// muda o cursor para o prÃ³ximo campo
					if(elNext < $('.autotab').size()){ $('.autotab:eq('+ elNext +')').focus(); }
					//set focus on the next field with autotab class
					//$(this).next('.autotab').focus();
				}
				//save newly entered value for the next execution of this event
				$(this).attr('prevValue', newValue);
			}
		});

  });
</script>

<script>
jQuery(document).ready(function($){
	$('a[data-action="print"]').on('click', function(e) {
			var contents = $(".boxprinter").html();
			var frame1 = $('<iframe />');
			frame1[0].name = "frame1";
			frame1.css({ "position": "absolute", "top": "-1000000px" });
			$("body").append(frame1);
			var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
			frameDoc.document.open();
			//Create a new HTML document.
			frameDoc.document.write('<html><head><title>DIV Contents</title>');
			frameDoc.document.write('</head><body>');

			//Append the external CSS file.
			frameDoc.document.write('<link href="<?php echo(base_url()); ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />');
			frameDoc.document.write('<link href="<?php echo(base_url()); ?>assets/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />');
			frameDoc.document.write('<link href="<?php echo(base_url()); ?>assets/css/custom.css" rel="stylesheet" type="text/css" />');

			//Append the DIV contents.
			frameDoc.document.write(contents);
			frameDoc.document.write('</body></html>');
			frameDoc.document.close();
			setTimeout(function () {
					window.frames["frame1"].focus();
					window.frames["frame1"].print();
					frame1.remove();
			}, 500);
	});
});
</script>

</body>
</html>