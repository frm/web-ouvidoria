<?php
	//$str_level = isset($session_level) ? $session_level : "";
?>

	<aside class="main-sidebar">
   
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
		  
			

			<!-- Sidebar user panel -->
			<div class="user-panel" style="background-color: #ecf0f5;">
				
				<div class="user-panel-img-lg">
					<img src="assets/images/logo.png" style="width: 90%;">
				</div>

				<div class="user-panel-img-mini">
					<img src="assets/images/logo-simbolo.png" style="width: 90%;">
				</div>

				<div class="pull-left info hide">
					<p><?php echo( isset($session_username) ? $session_username : "" ); ?></p>
					<i class="fa fa-circle text-success"></i> Online
				</div>
			</div>


			
			<div class="hide">
				<form action="#" method="get" class="sidebar-form">
					<div class="input-group">
						<input type="text" name="q" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</form><!-- /.search form -->
			</div>


			<?php
			switch ($session_level){
			case "analista":
				/**
				 * --------------------------------------------------------
				 * ANALISTA : carrega opções do menu
				 * --------------------------------------------------------
				**/
			?>
				<ul class="sidebar-menu">
					<li class="header">MENU DE NAVEGAÇÃO</li>

					<li class="treeview">
						<a href="javascript:;">
							<i class="fa fa-edit"></i> <span>Gerenciar Denúncias</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo( site_url('denuncias') ); ?>"><i class="fa fa-circle-o text-aqua"></i> Listar</a></li>
						</ul>
					</li>

				</ul>
			<?php
			break;
			case "empresa":
				/**
				 * --------------------------------------------------------
				 * EMPRESA : carrega opções do menu
				 * --------------------------------------------------------
				**/
			?>
				<ul class="sidebar-menu">
					<li class="header">MENU DE NAVEGAÇÃO</li>
					<li class="treeview">
						<a href="javascript:;">
							<i class="fa fa-user"></i> <span>Gerenciar Analistas</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo( site_url('analistas') ); ?>"><i class="fa fa-circle-o text-aqua"></i> Listar</a></li>
							<li><a href="<?php echo( site_url('analistas/form') ); ?>"><i class="fa fa-circle-o text-yellow"></i> Incluir</a></li>
							<li><a href="<?php echo( site_url('analistas/grid') ); ?>"><i class="fa fa-circle-o text-aqua"></i> Grid</a></li>
						</ul>
					</li>

					<li class="treeview">
						<a href="javascript:;">
							<i class="fa fa-edit"></i> <span>Gerenciar Denúncias</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo( site_url('denuncias') ); ?>"><i class="fa fa-circle-o text-aqua"></i> Listar</a></li>
						</ul>
					</li>

				</ul>
			<?php

			break;
			case "administrador":
				/**
				 * --------------------------------------------------------
				 * ADMINISTRADOR : carrega opções do menu
				 * --------------------------------------------------------
				**/
			?>
				<ul class="sidebar-menu">
					<li class="header">MENU DE NAVEGAÇÃO</li>
					<li class="treeview">
						<a href="javascript:;">
							<i class="fa fa-user"></i> <span>Gerenciar Analistas</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo( site_url('analistas') ); ?>"><i class="fa fa-circle-o text-aqua"></i> Listar</a></li>
							<li><a href="<?php echo( site_url('analistas/form') ); ?>"><i class="fa fa-circle-o text-yellow"></i> Incluir</a></li>
							<li><a href="<?php echo( site_url('analistas/grid') ); ?>"><i class="fa fa-circle-o text-aqua"></i> Grid</a></li>
						</ul>
					</li>

					<li class="treeview">
						<a href="javascript:;">
							<i class="fa fa-university"></i> <span>Gerenciar Empresas</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo( site_url('empresas') ); ?>"><i class="fa fa-circle-o text-aqua"></i> Listar</a></li>
							<li><a href="<?php echo( site_url('empresas/form') ); ?>"><i class="fa fa-circle-o text-yellow"></i> Incluir</a></li>
						</ul>
					</li>

					<li class="treeview">
						<a href="javascript:;">
							<i class="fa fa-tags"></i> <span>Gerenciar Naturezas</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo( site_url('naturezas') ); ?>"><i class="fa fa-circle-o text-aqua"></i> Listar</a></li>
							<li><a href="<?php echo( site_url('naturezas/form') ); ?>"><i class="fa fa-circle-o text-yellow"></i> Incluir</a></li>
						</ul>
					</li>

					<li class="treeview">
						<a href="javascript:;">
							<i class="fa fa-edit"></i> <span>Gerenciar Denúncias</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo( site_url('denuncias') ); ?>"><i class="fa fa-circle-o text-aqua"></i> Listar</a></li>
						</ul>
					</li>

					<li class="treeview">
						<a href="javascript:;">
							<i class="fa fa-user"></i> <span>Gerenciar Usuários</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo( site_url('usuarios') ); ?>"><i class="fa fa-circle-o text-aqua"></i> Listar</a></li>
							<li><a href="<?php echo( site_url('usuarios/form') ); ?>"><i class="fa fa-circle-o text-yellow"></i> Incluir</a></li>
						</ul>
					</li>

					<li class="treeview">
						<a href="javascript:;">
							<i class="fa fa-gears"></i> <span>Configurações</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo( site_url('configuracoes/emails') ); ?>"><i class="fa fa-circle-o text-aqua"></i> Emails Templates</a></li>
							<li><a href="<?php echo( site_url('configuracoes/backup') ); ?>"><i class="fa fa-circle-o text-aqua"></i> Backup da Base</a></li>
						</ul>
					</li>

					<li class="treeview">
						<a href="javascript:;">
							<i class="fa fa-bar-chart"></i> <span>Relatórios</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo( site_url('relatorios') ); ?>"><i class="fa fa-circle-o text-aqua"></i> Denúncias</a></li>
							<li><a href="<?php echo( site_url('relatorios') ); ?>"><i class="fa fa-circle-o text-aqua"></i> Analistas</a></li>
						</ul>
					</li>

				</ul>
			<?php
			break;
			} // switch
			?>


    </section>
    <!-- /.sidebar -->
  </aside>