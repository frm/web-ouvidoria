

		<!-- Button trigger modal -->
		<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" style="display:none;">
			Launch demo modal
		</button>

		<a href="<?php echo( $url["url_list"] .'/json' ); ?>" data-remote="true" data-toggle="modal" data-target="#myModal" class="btn btn-default" style="display:none;">
				Launch Modal
		</a>




		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-custom" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Detalhe</h4>
					</div>
					<div class="modal-body">

						<p>Não foi possível carregar os registros solicitados.</p>
						<p>Tente novamente. Por favor!</p>

					</div>
					<div class="modal-footer" style="display:none;">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary">Save changes</button>
					</div>
				</div>
			</div>
		</div>




		<!-- Modal 2 -->
		<div class="modal fade rotate" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
			<div class="modal-dialog modal-custom">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel2">Histórico da Denúncia</h4>
					</div>
					<div class="container"></div>
					<div class="modal-body">
					
						<p>Não foi possível carregar a denúncia.</p>
						<p>Tente novamente. Por favor!</p>

					</div>
				</div>
			</div>
		</div>