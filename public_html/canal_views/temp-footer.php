<?php $this->load->view( "includes/doctype" ); ?>

</head>

  <body>

		<?php $this->load->view( "includes/header" ); ?>


    <!-- Begin page content -->
    <div class="container home">

			<section id="sobre-nos" class="container">
				<!-- Breadcrumbs -->
				<div class="row">
						<div class="col-lg-12">
								<h1 class="page-header"></h1>
								<ol class="breadcrumb">
										<li><a href="<?php echo( site_url() ); ?>">Home</a>
										</li>
										<li class="active">Quem Somos</li>
								</ol>
						</div>
				</div>	  

				<h2>Quem somos</h2>

				<div class="row">
					<img class="img-responsive img-circle col-sm-6" src="assets/fonts/img/qsomos.jpg" alt="">

					<div class="panel-group col-sm-6" id="paineis-sobre">
						<div class="panel panel-default">
							<div class="panel-heading" data-parent="#paineis-sobre">
								<h3 class="panel-title"><i class="fa fa-fw fa-check"></i>Origem</h3>
							</div>

								<div class="panel-body">
									CanalOuvidoria integra a Divisão de Gestão da Ética e Compliance Empresarial do IPGE – Instituto Paulista de Gestão Estratégica. Desde 1994, o IPGE presta serviços de Consultoria Empresarial, Planejamento Estratégico e Governança Integrada, atendendo empresas de pequeno, médio e grande porte, em São Paulo.
								</div>

						</div>

						<div class="panel panel-default">
							<div class="panel-heading" data-parent="#paineis-sobre">
								<h3 class="panel-title"><i class="fa fa-fw fa-check"></i>Missão</h3>
							</div>

							<div class="panel-body">
								Nossa missão é fortalecer o Capital Humano da Empresa, através da comunicação livre e honesta. Dessa forma, as pessoas se sentem respeitadas e confiantes. Com respeito e confiança, as pessoas se tornam engajadas e trabalham melhor. E a Empresa ganha produtividade com sustentabilidade. Sem respeito e confiança, o Clima Organizacional se deteriora e os reflexos disso aparecem na baixa produtividade, nos conflitos, e no turnover elevado, que gera passivos desnecessários.
							</div>					
						</div>
					</div>
				</div>
			</section>


			<section id="servicos" class="container">
				<!-- Breadcrumbs -->
				<div class="row">
						<div class="col-lg-12">
								<h1 class="page-header"></h1>
								<ol class="breadcrumb">
									<li><a href="<?php echo( site_url() ); ?>">Home</a>
									</li>
									<li class="active">Serviços</li>
								</ol>
						</div>
				</div>	

				<h2>Serviços</h2>

				<div class="row">	
					<div class="panel-group col-sm-6" id="paineis-servico">
						<div class="panel panel-default">
							<div class="panel-heading" data-toggle="collapse" data-target="#terc-paragrafo" data-parent="#paineis-servico">
								<h3 class="panel-title">A Ferramenta</h3>
							</div>
							<div id="terc-paragrafo" class="collapse in">
								<div class="panel-body">
									<p>CANALOUVIDORIA é uma ferramenta de comunicação direta, imediata e confidencial para a Alta Administração da Empresa, visando a melhor gestão da Ética e Integridade, dentro dos princípios da Lei Anticorrupção, denominação dada &agrave; lei nº 12.846/2013.<br/></p>
									<p><strong>Vantagens:</strong>
									<ol>
										<li>Leque padrão com 12 naturezas de denúncia, sugestão ou avaliação, com acesso online, via mobile, tablet ou computador.</li>
										<li>A Empresa nomeia os responsáveis pelo tratamento confidencial da denúncia ou informação, que podem ser até 12 pessoas diferentes, conforme a naturezas.</li>
										<li>O leque padrão de 12 naturezas pode ser alterado, visando ajustar-se ao modelo de negócio e/ou modelo de gestão da empresa, mantendo o espírito da Lei Anticorrupção.</li>
										<li>O denunciante/informante pode optar pelo anonimato ou não. Em qualquer caso, ele/ela poderá acompanhar o resultado de sua denúncia pela internet.</li>
										<li>Alertas serão enviados ao responsável pelo tratamento e relatórios mensais serão encaminhados &agrave; Diretoria e/ou Auditoria da Empresa.</li>
									</ol>
									
									<a href="<?php echo( site_url('naturezas') ); ?>">Clique aqui para acessar o leque padrão.</a></p>
								</div>
							</div>
						</div>

					
					<div class="panel panel-default">
						<div class="panel-heading" data-toggle="collapse" data-target="#sexto-paragrafo" data-parent="#paineis-servico">
							<h3 class="panel-title">Custo</h3>
						</div>

						<div id="sexto-paragrafo" class="collapse">
							<div class="panel-body">
								<p>Uma única licença de baixo custo mensal conforme o porte da empresa, partir de R$ 495,00 mensal. A contratação é anual, e o contrato pode ser rescindido pelo cliente com aviso prévio de 30 dias.</p>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading" data-toggle="collapse" data-target="#setimo-paragrafo" data-parent="#paineis-servico">
							<h3 class="panel-title">Treinamento</h3>
						</div>

						<div id="setimo-paragrafo" class="collapse">
							<div class="panel-body">
								<p>Oferecemos serviços de treinamento e material de apoio para a implantação de políticas de Ética e Compliance.</p>
							</div>
						</div>
					</div>
				</div>


				<div class="col-sm-6 ">		
					<img class="img-responsive img-radius" src="assets/fonts/img/ferramentas.jpg" alt="">
				</div>


				<!-- Service Tabs -->
				<div class="row">
					<div class="col-lg-12">
							<h2 class="page-header">Como Funciona?</h2>
					</div>
					<div class="col-lg-12">

						<ul id="myTab" class="nav nav-tabs nav-justified">
								<li class="active"><a href="#service-one" data-toggle="tab"><i class="fa fa-bars"></i> Gestão</a>
								</li>
								<li class=""><a href="#service-two" data-toggle="tab"><i class="fa fa-address-book"></i> Controle</a>
								</li>
								<li class=""><a href="#service-three" data-toggle="tab"><i class="fa fa-support"></i> Praticidade</a>
								</li>
								<li class=""><a href="#service-four" data-toggle="tab"><i class="fa fa-database"></i> Segurança</a>
								</li>
						</ul>

						<div id="myTabContent" class="tab-content">
								<div class="tab-pane fade active in" id="service-one">
									<h4>Gestão</h4><br />
										<p>Registro histórico de todas as denúncias, permitindo avaliar sua evolução no tempo. Consolidação de informações  para a alta administração, com emissão de relatórios nos formatos: tela (HTML), planilha (Excel) ou portátil (PDF).</p>
								</div>
								<div class="tab-pane fade" id="service-two">
									<h4>Controle</h4><br />
										 <p>Controle de prazos e identificação de pendências, colocando-as em destaque e notificando automaticamente os gestores responsáveis pelo tratamento.</p>
								</div>
								<div class="tab-pane fade" id="service-three">
										<h4>Praticidade</h4><br />
										<p>Registro e acompanhamento da denúncia pelo próprio denunciante via internet. Permite enviar anexos: fotos, vídeos, cópia de documentos, etc.</p>
								</div>
								<div class="tab-pane fade" id="service-four">
									<h4>Segurança</h4><br />
										<p>Perfil de acesso diferenciado para cada gestor indicado pela Empresa. Cada empresa possui um codigo de LOGIN que será comunicado aos colaboradores e outros stakeholders definidos pela empresa. Acesso para acompanhamento (denunciante) por meio de número de protocolo (recebido ao denunciar) seguro. Relatórios Executivos incluem geração de log de auditoria, com registro dos relatos e respostas realizadas pelos informantes e gestores. Garantia de anonimato e sigilo das informaçãoes.</p>
								</div>
						</div>

					</div>
				</div>

			</section>


			<section id="contato" class="container">
				<!-- Breadcrumbs -->
				<div class="row">
						<div class="col-lg-12">
								<h1 class="page-header"></h1>
								<ol class="breadcrumb">
										<li><a href="<?php echo( site_url() ); ?>">Home</a>
										</li>
										<li class="active">Contato</li>
								</ol>
						</div>
				</div>
				
				<h2>Contato</h2>

				<?php
				$var_erro_cnt = (int)isset( $var_erro_cnt) ?  $var_erro_cnt : "";
				$msg_erro_cnt = isset( $msg_erro_cnt) ?  $msg_erro_cnt : "";
				if ( $var_erro_cnt != 0 && !empty($msg_erro_cnt) ){
				?>
					<div class="row">
						<div class="col-lg-12">
							<div class="alert alert-danger alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<h4><i class="icon fa fa-ban"></i> Atenção!</h4>
								<h5>Verifique os erros abaixo:</h5>
								<?php
								print_r( $msg_erro_cnt );
								?>
							</div>
						</div>
					</div>
				<?php
				}
				?>

				<?php
				if ( !empty($msg_success) ){
				?>
					<div class="row">
						<div class="col-lg-12">
							<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<h4><i class="icon fa fa-ban"></i> Atenção!</h4>
								<?php echo( $msg_success ); ?>
							</div>
						</div>
					</div>
				<?php
				}
				?>

				<!--## INÍCIO DO FORMULÁRIO -->
				<FORM role="form" action="<?php echo(site_url()); ?>#contato" method="post" name="frmCONTACT" id="frmCONTACT">
					<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORM-FALE-CONOSCO">

						<div class="row">
								<div class="col-md-6">
										<div class="form-group">
												<label for="nome">Nome *</label>
												<input type="text" name="cnt_nome" id="cnt_nome" class="form-control" placeholder="Por favor digite seu nome *" />
										</div>
								</div>

								<div class="col-md-6">
										<div class="form-group">
												<div class="form-group">
													<label for="email">Email *</label>
													<input type="text" name="cnt_email" id="cnt_email" class="form-control" placeholder="Por favor digite seu email *" />
												</div>
										</div>
								</div>
						</div>
								
						<div class="row">
								<div class="col-md-12">
										<div class="form-group">
												<label for="mensagem">Mensagem *</label>
												<textarea name="cnt_mensagem" id="cnt_mensagem" class="form-control" placeholder="Digite a mensagem *" rows="4" style="resize:none; height:200px"></textarea>
										</div>
								</div>
								<div class="col-md-12">
									<input type="submit" class="btn btn-success btn-send" value="Enviar">
								</div>
						</div>

						<div class="row">
								<div class="col-md-12">
										<p class="text-muted"><strong>*</strong> Campos obrigatórios</p>
								</div>
						</div>

				</FORM>
				<!--## TÉRMINO DO FORMULÁRIO -->

			</section>

    </div>

	
		<?php $this->load->view( "includes/footer" ); ?>


  </body>
</html>
