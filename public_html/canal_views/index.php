<?php $this->load->view( "includes/doctype" ); ?>

</head>

  <body>


		<?php $this->load->view( "includes/header" ); ?>


    <!-- Begin page content -->
    <div class="container home">

			<section id="sobre-nos" class="container">
				<!-- Breadcrumbs -->
				<div class="row">
						<div class="col-lg-12">
								<h1 class="page-header"></h1>
								<ol class="breadcrumb">
										<li><a href="<?php echo( site_url() ); ?>">Home</a>
										</li>
										<li class="active">Quem Somos</li>
								</ol>
						</div>
				</div>	  

				<h2>Quem somos</h2>

				<div class="row">
					<div class="col-md-6 col-sm-6 mgBottom15">
						<img src="assets/fonts/img/qsomos.jpg" class="img-responsive img-circle" alt="quem somos">
					</div>

					<div class="panel-group col-sm-6" id="paineis-sobre">
						<div class="panel panel-default">
							<div class="panel-heading" data-parent="#paineis-sobre">
								<h3 class="panel-title"><i class="fa fa-fw fa-check"></i>Origem</h3>
							</div>

								<div class="panel-body">
									WebOuvidoria integra a Divisão de Gestão da Ética e Compliance Empresarial do IPGE – Instituto Paulista de Gestão Estratégica. Desde 1994, o IPGE presta serviços de Consultoria Empresarial, Planejamento Estratégico e Governança Integrada, atendendo empresas de pequeno, médio e grande porte, em São Paulo.
								</div>

						</div>

						<div class="panel panel-default">
							<div class="panel-heading" data-parent="#paineis-sobre">
								<h3 class="panel-title"><i class="fa fa-fw fa-check"></i>Missão</h3>
							</div>

							<div class="panel-body">
								Nossa missão é fortalecer o Capital Humano da Empresa, através da comunicação livre e honesta. Dessa forma, as pessoas se sentem respeitadas e confiantes. Com respeito e confiança, as pessoas se tornam engajadas e trabalham melhor. E a Empresa ganha produtividade com sustentabilidade. Sem respeito e confiança, o Clima Organizacional se deteriora e os reflexos disso aparecem na baixa produtividade, nos conflitos, e no turnover elevado, que gera passivos desnecessários.
							</div>					
						</div>
					</div>
				</div>
			</section>


			<section id="servicos" class="container">
				<!-- Breadcrumbs -->
				<div class="row">
						<div class="col-lg-12">
								<h1 class="page-header"></h1>
								<ol class="breadcrumb">
									<li><a href="<?php echo( site_url() ); ?>">Home</a>
									</li>
									<li class="active">Serviços</li>
								</ol>
						</div>
				</div>	

				<h2>Serviços</h2>

				<div class="row">	
					<div class="panel-group col-sm-6" id="paineis-servico">
						<div class="panel panel-default">
							<div class="panel-heading" data-toggle="collapse" data-target="#primeiro-paragrafo" data-parent="#paineis-servico">
								<h3 class="panel-title">A Ferramenta</h3>
							</div>
							<div id="primeiro-paragrafo" class="collapse in">
								<div class="panel-body">
									<p>WebOuvidoria é uma ferramenta de comunicação direta, imediata e confidencial para a Alta Administração da Empresa, visando a melhor gestão da Ética e Integridade, dentro dos princípios da Lei Anticorrupção, denominação dada &agrave; lei nº 12.846/2013.<br/></p>
									<p><strong>Vantagens:</strong>
									<ol>
										<li>Oferecemos um leque padrão com 12 naturezas de denúncia, sugestão ou avaliação, com acesso online, via mobile, tablet ou computador.</li>
										<li>O leque padrão de 12 naturezas pode ser customizado, visando ajustar-se ao modelo de negócio e/ou modelo de gestão da empresa, mantendo o espírito da Lei Anticorrupção.</li>
										<li>Caso a Empresa prefira ainda, poderá optar por dois modelos diferentes: um Modelo Interno de WebOuvidoria para fins de Gestão Integrada de RH, e/ou um Modelo Externo, para fins de compliance da legislação anticorrupção. Ambos os modelos (Intranet ou Internet) poderão ser customizados conforme a estratégia de comunicação da Empresa. No formato Intranet, a WebOuvidoria pode ainda ser conjugada com um sistema contínuo de RHPesquisa, visando aperfeiçoar a gestão do Capital Humano/ Organizacional da Empresa.</li>
										<li>O denunciante/informante pode optar pelo anonimato ou não. Em qualquer caso, ele/ela poderá acompanhar o resultado de sua denúncia pela internet.</li>
										<li>Seja no Modelo Interno ou Modelo Externo, alertas serão enviados automaticamente pela WebOuvidoria ao responsável da Empresa pelo tratamento das denuncias. Relatórios mensais serão encaminhados à Diretoria e/ou Auditoria Interna da Empresa, conforme os termos do contrato.</li>
									</ol>
									
									<a href="<?php echo( site_url('naturezas') ); ?>">Clique aqui para acessar o leque padrão.</a></p>
								</div>
							</div>
						</div>

					
					<div class="panel panel-default">
						<div class="panel-heading" data-toggle="collapse" data-target="#segundo-paragrafo" data-parent="#paineis-servico">
							<h3 class="panel-title">Custo</h3>
						</div>

						<div id="segundo-paragrafo" class="collapse">
							<div class="panel-body">
								<p>Uma única licença de baixo custo mensal conforme o porte da empresa, a partir de R$ 495,00 mensais no formato online, que inclui:</p>
								<ol>
									<li>Acesso universal para empregados, fornecedores e clientes via internet.</li>
									<li>Direcionamento automático da denuncia conforme a classificação e o responsável.</li>
									<li>Registro controlado de relatos, conforme a data e o tratamento dado.</li>
									<li>Relatórios mensais para auditoria, conforme previsto na Lei Anticorrupção.</li>
									<li>Serviço de email sigiloso e seguro, específico para sua empresa. </li>
								</ol>
								<p>Disponibilizamos ainda serviços opcionais de Treinamento, Atendimento 0800, Atendimento via Whatsapp e Gestão Integrada de Ouvidoria. Veja descrição abaixo.</p>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading" data-toggle="collapse" data-target="#terceiro-paragrafo" data-parent="#paineis-servico">
							<h3 class="panel-title">Treinamento</h3>
						</div>

						<div id="terceiro-paragrafo" class="collapse">
							<div class="panel-body">
								<p>A implantação de um serviço de Ouvidoria funciona melhor com uma etapa prévia de treinamento e conscientização. </p>
								<p>O beneficio desse treinamento está ligado ao desenvolvimento de uma cultura de ética e respeito, que por sua vez resulta em maior engajamento, menor rotatividade e maior produtividade.</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" data-toggle="collapse" data-target="#quarto-paragrafo" data-parent="#paineis-servico">
							<h3 class="panel-title">Atendimento whatsapp</h3>
						</div>

						<div id="quarto-paragrafo" class="collapse">
							<div class="panel-body">
								<p>Como o acesso ao uso de smartfones e desse aplicativo é onipresente hoje no Brasil em todas as camadas sociais, essa opção de atendimento é muito prática para quem não tem acesso fácil a desktops/laptops.</p>
								<p>Serve também para upload arquivos de foto, vídeo ou áudio.</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" data-toggle="collapse" data-target="#quinto-paragrafo" data-parent="#paineis-servico">
							<h3 class="panel-title">Atendimento 0800</h3>
						</div>

						<div id="quinto-paragrafo" class="collapse">
							<div class="panel-body">
								<p>Trata-se de um serviço adequado para locais  sem acesso a internet ou pessoas que não possuem smartfones.</p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading" data-toggle="collapse" data-target="#sexto-paragrafo" data-parent="#paineis-servico">
							<h3 class="panel-title">Gestão Integrada de Ouvidoria</h3>
						</div>

						<div id="sexto-paragrafo" class="collapse">
							<div class="panel-body">
								<p>Serviço recomendado quando a Diretoria da Empresa opta por centralizar 100% do tratamento dos relatos e denuncias sob nossa responsabilidade. </p>
								<p>Essa contratação envolve uma consultoria de Risco e Compliance. 
								<p>Também pode implicar, eventualmente, em visitas ao local da empresa para conduzir observações, entrevistas e/ou auditorias. </p>
								<p>Nossos parceiros de Gestão de Riscos, Consultoria Organizacional e Auditoria Interna estão plenamente capacitados a executar essas avaliações sigilosas com total competência.</p>
							</div>
						</div>
					</div>
				</div>


				<div class="col-sm-6 ">		
					<img class="img-responsive img-radius" src="assets/fonts/img/ferramentas.jpg" alt="">
				</div>


				<!-- Service Tabs -->
				<div class="col-md-12">
					<h2 class="page-header">Como Funciona?</h2>
				</div>
				<div class="col-md-12">

					<ul id="myTab" class="nav nav-tabs nav-justified">
							<li class="active"><a href="#service-one" data-toggle="tab"><i class="fa fa-hand-o-right"></i> Acesso</a></li>
							<li class=""><a href="#service-two" data-toggle="tab"><i class="fa fa-bars"></i> Gestão</a>
							</li>
							<li class=""><a href="#service-three" data-toggle="tab"><i class="fa fa-address-book"></i> Controle</a>
							</li>
							<li class=""><a href="#service-four" data-toggle="tab"><i class="fa fa-support"></i> Praticidade</a>
							</li>
							<li class=""><a href="#service-five" data-toggle="tab"><i class="fa fa-database"></i> Segurança</a>
							</li>
					</ul>

					<div id="myTabContent" class="tab-content">
							<div class="tab-pane fade active in" id="service-one">
								<h4>Acesso</h4><br />
									<ul>
										<li>O acesso para empregados, clientes ou fornecedores fazerem denuncias na tela exclusiva da empresa se dará de duas formas: via link no próprio website da empresa, ou via o nosso website webouvidoria.com.br, clicando em "denuncie" e digitando o nome da empresa.</li>
									</ul>
							</div>
							<div class="tab-pane fade" id="service-two">
								<h4>Gestão</h4><br />
									<ul>
										<li>Registro histórico de todas as denúncias, permitindo avaliar sua evolução no tempo.</li>
										<li>Consolidação de informações  para a alta administração, com emissão de relatórios nos formatos: tela (HTML), planilha (Excel) ou portátil (PDF).</li>
									</ul>
							</div>
							<div class="tab-pane fade" id="service-three">
								<h4>Controle</h4><br />
									<ul>
										<li>Controle de prazos e identificação de pendências, colocando-as em destaque e notificando automaticamente os gestores responsáveis pelo tratamento.</li>
							</div>
							<div class="tab-pane fade" id="service-four">
									<h4>Praticidade</h4><br />
									<ul>
										<li>Registro e acompanhamento da denúncia pelo próprio denunciante via internet.</li>
										<li>Permite enviar anexos: fotos, vídeos, cópia de documentos, etc.</li>
									</ul>
							</div>
							<div class="tab-pane fade" id="service-five">
								<h4>Segurança</h4><br />
									<ul>
										<li>Perfil de acesso diferenciado para cada gestor indicado pela Empresa.</li>
										<li>Cada empresa possui um codigo de LOGIN que será comunicado aos colaboradores e outros stakeholders definidos pela empresa.</li>
										<li>Acesso para acompanhamento (denunciante) por meio de número de protocolo (recebido ao denunciar) seguro.</li>
										<li>Relatórios Executivos incluem geração de log de auditoria, com registro dos relatos e respostas realizadas pelos informantes e gestores.</li>
										<li>Garantia de anonimato e sigilo das informaçãoes.</li>
										<li>WebOuvidoria usa HTTPS, cujo símbolo é o CADEADO. Trata-se de um protocolo de criptografia que protege os dados.</li>
										<li>Em caso de informações sigilosas serem fornecidas ou auditadas através do WebOuvidoria, essa segurança faz a diferença.</li>
									</ul>
							</div>
					</div>

				</div>

			</section>


			<section id="contato" class="container">
				<!-- Breadcrumbs -->
				<div class="row">
						<div class="col-lg-12">
								<h1 class="page-header"></h1>
								<ol class="breadcrumb">
										<li><a href="<?php echo( site_url() ); ?>">Home</a>
										</li>
										<li class="active">Contato</li>
								</ol>
						</div>
				</div>
				
				<?php
				$var_erro_cnt = (int)isset( $var_erro_cnt) ?  $var_erro_cnt : "";
				$msg_erro_cnt = isset( $msg_erro_cnt) ?  $msg_erro_cnt : "";
				if ( $var_erro_cnt != 0 && !empty($msg_erro_cnt) ){
				?>
					<div class="row">
						<div class="col-lg-12">
							<div class="alert alert-danger alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<h4><i class="icon fa fa-ban"></i> Atenção!</h4>
								<h5>Verifique os erros abaixo:</h5>
								<?php
								print_r( $msg_erro_cnt );
								?>
							</div>
						</div>
					</div>
				<?php
				}
				?>

				<?php
				if ( !empty($msg_success) ){
				?>
					<div class="row">
						<div class="col-lg-12">
							<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<h4><i class="icon fa fa-ban"></i> Atenção!</h4>
								<?php echo( $msg_success ); ?>
							</div>
						</div>
					</div>
				<?php
				}
				?>


				<div class="realizacao">
					<div class="row">

						<div class="col-md-6 col-xs-12" align="center">
							<div>
								<h3 class="title">UMA DIVISÃO DO IPGE</h3>
							</div>
							<div class="image" style="min-height:140px;">
								<div>
									<a href="http://www.webouvidoria.com.br/" target="parent"><img src="assets/fonts/img/logo-web-ouvidoria-red.png" style="width:55%" title="Webouvidoria"  /></a>
									<!--<a href="#" target="parent"><img src="img/logopesq-red.png" title="RHPesquisa"  /></a>-->
								</div>
							</div>
						</div>

						<div class="col-md-6 col-xs-12" align="center">
							<div>
								<h3 class="title">&nbsp;</h3>
							</div>
							<div class="image" style="min-height:140px;">
								<a href="http://www.ipge.com.br/" target="parent"><img src="assets/fonts/img//logo_fundo.azul.png" style="width:70%" title="IPGE" /></a>
								
							</div>
						</div>
					</div>
					<hr>

					<div class="row">
						<div class="col-md-12 col-xs-12" align="center">
							<div>
								<h3 class="title">ENTRE EM CONTATO</h3>
							</div>
						</div>
						<!--<div class="col-md-5 col-xs-12" align="center">
							<div>
								<h3 class="title">REALIZAÇÃO</h3>
							</div>
							<div class="image" style="min-height:140px;">
								<a href="http://www.ipge.com.br/" target="parent"><img src="img/ipge_red_blue.png" title="IPGE" /></a>
								<div class="row" style="margin-top:15px;">
									<div class="col-md-12 col-xs-12" align="center">
										<ul class="none-style work-time">
											<li style="padding-left: 0px;"><i class="fa fa-phone fa-lg tam"></i><strong> Fone:</strong>  4004-0000</li>
											<li style="padding-left: 0px;"><i class="fa fa-whatsapp fa-lg tam"></i> <strong>Whatsapp:</strong>
											(11) 942677335</li>
										</ul>
									</div>
								</div>
							</div>
						</div>-->
						<div class="col-md-12 col-xs-12">
							<div class="image">
								<div class="col-md-8 col-xs-12" >

									<!--## INÍCIO DO FORMULÁRIO -->
									<FORM role="form" action="<?php echo(site_url()); ?>#contato" method="post" name="frmCONTACT" id="frmCONTACT">
										<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORM-FALE-CONOSCO">

										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="cnt_nome">Nome *</label>
													<input type="text" name="cnt_nome" id="cnt_nome" class="form-control input-sm" value="" placeholder="Por favor digite seu nome *" />
												</div>
												<div class="form-group">
													<label for="cnt_email">Email *</label>
													<input type="text" name="cnt_email" id="cnt_email" class="form-control input-sm" value="" placeholder="Por favor digite seu email *" />
												</div>
											</div>									
											<div class="col-md-6">
												<div class="form-group">
													<label for="cnt_mensagem">Mensagem *</label>
													<textarea id="cnt_mensagem" name="cnt_mensagem" class="form-control input-sm" placeholder="Por favor, deixe sua mensagem *" style="resize:none; height:100px"></textarea>
												</div>
												<div class="form-group">
													<input type="submit" class="btn btn-success btn login-btn btn-send pull-right" value="Enviar">
												</div>
											</div>
										</div>
									</FORM>
									<!--## END : FORMULARIO -->

									<div class="clearfix"></div>
								</div>   
								<div class="col-md-4 col-sm-12">
									<div class="clearfix"></div>
									<ul class="contact">
										<li>
											<i class="fa fa-map-marker"></i>
											 Rua Tabapuã, 82, Conj. 501 - Itaim Bibi 
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;São Paulo - SP
										</li>
										<li>
											<i class="fa fa-whatsapp"></i>
											(11) 94267 7335 
										</li>
										<li>
											<i class="fa fa-envelope-o"></i>
											contato@webouvidoria.com.br
										</li>
									</ul> 
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>						
				</div>

			</section>

    </div>


	
		<?php $this->load->view( "includes/footer" ); ?>


		<script>
			jQuery(document).ready(function ($) {
				$('form#frmCONTACT').submit( function(e){
					//e.preventDefault();
					var $form	= $(this);
					var $msg	= '';
					var $erro = false;

					var $nome = $form.find("#cnt_nome");
					var $email = $form.find("#cnt_email");
					var $mensagem = $form.find("#cnt_mensagem");

					if( $nome.val().length == 0 ) { 
						$msg += "<p>- Preencha corretamente seu nome.</p>";
						$erro = true; 
					}
					if( $email.val().length == 0 ) { 
						$msg += "<p>- Preencha corretamente seu e-mail.</p>";
						$erro = true; 
					}
					if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test( $email.val() )) {
						$msg += "<p>- O e-mail informado é inválido.</p>";
						$erro = true; 
					}
					if( $mensagem.val().length == 0 ) { 
						$msg += "<p>- Preencha corretamente a mensagem.</p>";
						$erro = true; 
					}

					if( $erro == true)
					{
						$.alert({
							title: 'Atenção',
							confirmButtonClass: 'btn-info',
							cancelButtonClass: 'btn-danger',
							confirmButton: 'OK',
							//cancelButton: 'NO never !',
							content: $msg,
							confirm: function () {
								//$.alert('Confirmed!');
							}
						});
						return false;
					}else{
						$form.submit();
					}
				});
			});
		</script>


  </body>
</html>