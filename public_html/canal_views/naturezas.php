<?php $this->load->view( "includes/doctype" ); ?>

</head>

	<body>

		<?php $this->load->view( "includes/header-categ" ); ?>


		<section id="categorias" class="container">	
				
				<!-- Page Heading/Breadcrumbs -->
				<div class="row">
						<div class="col-lg-12">
								<h1 class="page-header"></h1>
								<ol class="breadcrumb">
										<li><a href="<?php echo( site_url() ); ?>">Home</a>
										</li>
										<li class="active">Naturezas</a></li>
								</ol>
						</div>
				</div>		 


				<div class="row">
						<div class="col-lg-12">
								<h2 class="page-header">Naturezas</h2>
						</div>
				</div>
					
				
				<?php
				if ( isset($rs_list_nat) ){
					$x_nat = 0;
					$x_col = 0;
					foreach ($rs_list_nat AS $item){
						$x_nat++;
						$x_col++;
						$nat_id						= (int)$item->nat_id;
						$nat_titulo				= $item->nat_titulo;
						$nat_prazo				= $item->nat_prazo;
						$nat_descricao		= $item->nat_descricao;
						$nat_arquivo			= $item->nat_arquivo;
						$nat_dte_cadastro	= fct_formatdate($item->nat_dte_cadastro, 'd/m/Y H:i');
						$icon_ativo				= ($item->nat_ativo==1)?"sim":"não";
						//$redirect					= $url["url_form"] ."/". $nat_id;

						$nat_arquivo_tag = '<img data-src="holder.js" class="img-responsive img-circle" alt="Sem Imagem" style="margin:0 auto; width: 100%; max-width: 100%; height: 250px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15cc09e90d7%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15cc09e90d7%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2275.5%22%20y%3D%22104.5%22%3E%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">';

						$image_path = $this->config->item('folder_images') .'/naturezas/'. $nat_arquivo;
						if( file_exists($image_path) and is_file($image_path) )
						{
							$nat_arquivo_tag = '<img src="'. base_url($image_path) .'" alt="" class="img-responsive img-circle" style="margin:0 auto; width: 100%; max-width: 100%; height: 250px;" />';
						}
				?>
				<?php if( ($x_col == 1) ){ ?>
				<div class="row">
				<?php } ?>
				
					<!-- <?php echo($nat_titulo); ?> -->
					<div class="col-md-4 col-sm-6">
							<div class="panel panel-default">
									<div class="panel-heading">
											<span class="">
												<?php echo($nat_arquivo_tag); ?>
											</span>
									</div>
									<div class="panel-body">
										<h4><?php echo($x_nat); ?>. <?php echo($nat_titulo); ?></h4>
											<p>
											<?php echo($nat_descricao); ?>
											</p>
									</div>
							</div>
					</div>
					<!-- /.<?php echo($nat_titulo); ?> -->

				<?php if( ($x_col % 3) == 0 ||  $x_nat >= count($rs_list_nat) ){ $x_col = 0; ?>
				</div>
					<?php if( $x_nat < count($rs_list_nat) ){ ?>
					<hr>
					<?php } ?>
				<?php } ?>


				<?php
					}; // foreach : rs_list_nat
				}; // if isset : rs_list_nat
				?>

				<div class="clear" style="margin-bottom:60px;"></div>
		</section>


		<?php $this->load->view( "includes/footer" ); ?>


  </body>
</html>