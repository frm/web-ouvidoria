		
		<header class="no-bg">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
					<div class="container">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse-navbar">
									<span class="sr-only"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<a class="navbar-brand logo navbar-left" href="<?php echo( site_url() ); ?>"><img src="assets/fonts/img/logo-web-ouvidoria.png" alt="" style="max-height:65px;"></a>
							</div>
							<!-- menu -->
							<div class="collapse navbar-collapse" id="collapse-navbar">
								<ul class="nav navbar-nav navbar-right">
									<li><a href="<?php echo( site_url() ); ?>/#sobre-nos">Quem Somos</a></li>
									<li><a href="<?php echo( site_url() ); ?>/#servicos">Serviços</a></li>
									<!--<li><a href="<?php echo( site_url() ); ?>#ebook">E-Books</a></li>
									<!--<li><a href="http://www.grc.blog.br">Blog</a></li>
									<li><a href="<?php echo( site_url('naturezas') ); ?>">Naturezas</a></li>-->
									<li><a href="<?php echo( site_url() ); ?>/#contato">Contato</a></li>
									<li><a href="<?php echo( site_url('denunciar') ); ?>">Denuncie</a></li>	
									<li><a href="../assets/pdf/programa-de-integridade-diretrizes-para-empresas-privadas.pdf" target="_blank">Legislação</a></li>
								</ul>
							</div>
					</div>      
			</nav>

			<div class="container canalOuvidoria-bannerWrapper hide-mobile">
				<div class="canalOuvidoria-banner">
					<h1><span class="logo">WebOuvidoria</span></h1>
					<p><span class="banner">A ferramenta online que faltava na sua Empresa para a Gestão da Ética & Integridade, com total eficiência e baixo custo.</span></p>			
					<a class="bto btn btn-warning btn-block" href="#contato">Contate-nos agora</a>
				</div>
			</div>
		</header>
