<!DOCTYPE html>
<html lang="pt-br">
  <head>
		<base href="<?php echo(base_url()); ?>" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

		<title>WebOuvidoria</title>

		<meta name="author" content="Ageu Barros">
		<meta name="keywords" content="Conformidade, Ética, Auditoria, Recursos Humanos, Corrupção, Assédio Moral, Assédio Sexual, Fraude, Fornecedor, Favorecimento, Clima Organizacional, Pesquisa, Engajamento, Risco, Insatisfação, Network, Confidencial, Pesquisa Anônima, Serviço de Ouvidoria, Ouvidoria Terceirizada, Queixas de Empregados, Demissão, Entrevista de Demissão, Autoritarismo, Reclamação, CLT, Reclamação Trabalhista, Justa Causa">
		<meta name="description" content="Se você quer se aventurar com o HTML5, entenda aqui a sua estrutura básica, a semântica das principais marcações novas e algumas ferramentas.">


    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" />


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="http://getbootstrap.com/docs/3.3/assets/css/ie10-viewport-bug-workaround.css" />

    <!-- Custom styles for this template -->
    <link href="assets/css/sticky-footer-navbar.css" rel="stylesheet">
    <link href="assets/css/custom.css" rel="stylesheet">
		<link href="assets/css/responsive.css" rel="stylesheet">


		<!-- Customização Fontes -->
		<link type="text/css" rel="stylesheet" href="assets/css/font-awesome/css/font-awesome.min.css" />


		<!-- Plugins -->
		<link type="text/css" rel="stylesheet" href="assets/plugins/jquery-confirm/css/jquery-confirm.min.css" />


    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="http://getbootstrap.com/docs/3.3/assets//js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="http://getbootstrap.com/docs/3.3/assets/js/ie-emulation-modes-warning.js"></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112770218-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-112770218-1');
		</script>




