<?php

$den_status	= (isset($rs_den->den_status)? $rs_den->den_status : '');

?>
<?php $this->load->view( "includes/doctype" ); ?>

</head>

	<body>

		<?php $this->load->view( "includes/header-categ" ); ?>

    
		<section id="sobre-nos" class="container">
				<!-- Breadcrumbs -->
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header"></h1>
						<ol class="breadcrumb">
							<li><a href="<?php echo ( site_url('index.php') ); ?>">Home</a></li>
							<li class="active">Resposta</li>
						</ol>
					</div>
				</div>


				</br>


				<?php if($den_status == 'finalizado'){ ?>
					<div class="row">
						<div class="col-lg-12">
							<div class="alert alert-success alert-dismissible">
								<h4><i class="icon fa fa-check"></i> Atenção!</h4>
								<h5>Esta denúncia está finalizada</h5>
							</div>
						</div>
					</div>
				<?php } // if den_status == finalizado  ?>


				<?php
				$parar = false;
				$lastAprov = false;
				$xx = 0;
				//$status_prev = '';
				$aprovad_prev = '';
				$texto_prev = false;
				$list_show = false;
				$aqui = '';
				if ( isset($rs_resp) ){
					foreach ($rs_resp AS $item){
						$xx++;
						$dresp_id						= (int)$item->dresp_id;
						$dresp_descricao		= trim($item->dresp_descricao);
						$dresp_status				= $item->dresp_status;
						$dresp_aprovada			= $item->dresp_aprovada;
						$dresp_dte_cadastro	= fct_formatdate($item->dresp_dte_cadastro, 'd/m/Y H:i');
						$list_show = false;


						$label_resposta = ($dresp_status == $this->cfg_status_definidos["replica"]) ? "Réplica do Autor" : "Resposta";
						$bd_panel_head = ($dresp_status == $this->cfg_status_definidos["replica"]) ? "background-color: #FFA88A; border-color: #FFA88A;" : "";
						$bd_panel = ($dresp_status == $this->cfg_status_definidos["replica"]) ? "border-color: #FFA88A;" : "";


						if( $dresp_aprovada == '0' && $dresp_status == $this->cfg_status_definidos["replica"] ){
							$list_show = true;
							$aqui = 'replica';
						}

						if( $dresp_aprovada == '1' && $dresp_status == $this->cfg_status_definidos["resp_admin"] ){ 
							//$lastAprov = true; 
							$list_show = !empty(trim($dresp_descricao)) ? true : false;
							$texto_prev = !empty(trim($dresp_descricao)) ? true : false;

							//$status_prev = '';
							$aprovad_prev = '1';
							$aqui = 'admin';
						}

						if( $dresp_aprovada == '0' && $dresp_status == $this->cfg_status_definidos["resp_analista"] ){ 
							if( $aprovad_prev == '1' && $texto_prev != true ){
								$list_show = true;
								$aqui = 'analista';
							}						
						}
						
						
						
						//if( $dresp_aprovada == 1 && $xx == 1 && !empty($dresp_descricao)){
							////$parar = true;
						//}else{
							//if($lastAprov == true && $dresp_status == 'em-aprovacao' && !empty($dresp_descricao) ){
								////$parar = true;	
							//}
						//}
						//if( $dresp_aprovada != 1 && $xx == 1){ $parar = true; break; }



						if( !empty($dresp_descricao) && $list_show == true){
							//var_dump( $list_show );
							//var_dump( $aqui );
				?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default" style="<?php echo( $bd_panel ); ?>">
								<div class="panel-heading" style="<?php echo( $bd_panel_head ); ?>">
									<h4>
										<i class="fa fa-fw fa-check"></i> <?php echo( $label_resposta ); ?> &nbsp| &nbsp  Data: <?php echo( $dresp_dte_cadastro ); ?> 
										<!-- &nbsp| &nbsp &nbsp<i class="fa fa-paperclip"></i> -->
									</h4>
								</div>
								<div class="panel-body">
									<p><?php echo( nl2br($dresp_descricao) ); ?></p>

									<?php
										/**
										 * --------------------------------------------------------
										 * anexos
										 * --------------------------------------------------------
										**/
										$this->db->select(" * ")
											->from('tbl_denunc_resp_anexos')
											->where( array('dresp_id' => $dresp_id) )
											->order_by( 'dranx_id', 'DESC' );
										$query = $this->db->get();
										$rs_resp_anexo = $query->result();
										//fct_print_debug( $data['rs_den_anexo'] );

										/**
										 * --------------------------------------------------------
										 * anexos
										 * --------------------------------------------------------
										**/
										if ( isset($rs_resp_anexo)  && count($rs_resp_anexo)>=1 ){
											$list_anexos = '';
											print '<h5 style="margin-top:30px;">Anexos:</h5>';
											foreach ($rs_resp_anexo AS $itemRespAnexo){
													$dranx_id				= (int)$itemRespAnexo->dranx_id;
													$dranx_arquivo		= $itemRespAnexo->dranx_arquivo;
													
													$image_path = $this->config->item('folder_images') .'denuncias/'. $dranx_arquivo;
													if( file_exists($image_path) and is_file($image_path) )
													{
														$load_image = base_url($image_path);
														//$list_anexos .= '<a href="'. $load_image .'" target="_blank" class="btn btn-primary">'. $dranx_arquivo .'</a>';
														print '<a href="'. $load_image .'" target="_blank" class="btn btn-primary">'. $dranx_arquivo .'</a>';
													};
											}
										}
										?>
								</div>
							</div>
						</div>
					</div>
				<?php
						}
						//print_r( $parar );
						if( $parar == true ){ break; }
					} // if rs_resp
				} // for rs_resp
				?>


				<div class="row">
						<div class="col-md-12">
								<div class="panel panel-default" style="border-color: #82B4FF;">
										<div class="panel-heading" style="background-color: #82B4FF; border-color: #82B4FF;">
											<h4>
											<i class="fa fa-fw fa-check"></i> Sua Denúncia | Data: <?php 
											$den_dte_cadastro	= (isset($rs_den->den_dte_cadastro)? $rs_den->den_dte_cadastro : '');
											$den_dte_cadastro	= fct_formatdate($den_dte_cadastro, 'd/m/Y H:i');
											echo( $den_dte_cadastro );
											?>

											<?php 
											$den_num_protocolo_relac = (isset($rs_den->den_num_protocolo_relac)? $rs_den->den_num_protocolo_relac : '');
											if( !empty($den_num_protocolo_relac) ){
											?>
												<span class="pull-right">protocolo relacionado: <?php echo( $den_num_protocolo_relac ); ?></span>
											<?php } // if !empty den_num_protocolo_relac ?>
											</h4>
										</div>
										<div class="panel-body">
											<p><?php echo( (isset($rs_den->den_descricao)? $rs_den->den_descricao : '') ); ?></p>

											<?php
											/**
											 * --------------------------------------------------------
											 * anexos
											 * --------------------------------------------------------
											**/
											if ( isset($rs_den_anexo) ){
												if( count($rs_den_anexo)>=1){
													print '<h5 style="margin-top:30px;">Anexos:</h5>';
													foreach ($rs_den_anexo AS $itemAnexo){
														$danx_id				= (int)$itemAnexo->danx_id;
														$danx_arquivo		= $itemAnexo->danx_arquivo;

														$image_path = $this->config->item('folder_images') .'denuncias/'. $danx_arquivo;
														if( file_exists($image_path) and is_file($image_path) )
														{
															$load_image = base_url($image_path);
															//$nat_arquivo_tag = '<img src="'. $load_image .'" alt="" class="img-responsive" style="height: 200px;" />';
															print '<a href="'. $load_image .'" target="_blank" class="btn btn-primary">'. $danx_arquivo .'</a>';
														};
													}
												}
											}
											?>

										</div>
								</div>
						</div>
				</div>


			<?php if($den_status != 'finalizado') { ?>

				<?php
				$show_form = 'display:none;';
				if ( $var_erro != 0 && !empty($msg_erro) ){
					$show_form = 'display:block;';
				?>
					<div class="row">
						<div class="col-lg-12">
							<div class="alert alert-danger alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<h4><i class="icon fa fa-ban"></i> Atenção!</h4>
								<h5>Verifique os erros abaixo:</h5>
								<?php
								print_r( $msg_erro );
								?>
							</div>
						</div>
					</div>
				<?php
				}
				?>
			

				<?php if($den_status == 'respondido') { ?>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>Satisfeito com a resposta?</h3>
								<a href="javascript:void(0);" class="finalizar-link btn btn-primary btn-lg active" role="button" aria-pressed="true">Sim</a>
								<a href="#" class="content-link btn btn-primary btn-lg active" role="button" aria-pressed="true">Não</a>

								<div style="display:none;">
									<!--## INÍCIO DO FORMULÁRIO -->
									<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmDadosFinalizar" id="frmDadosFinalizar" >
										<input type="text" name="baseAcao" id="baseAcao" value="SEND-FORM-DENUNCIA-FINALIZAR">
										<input type="text" name="den_id" id="den_id" value="<?php echo( (isset($rs_den->den_id)? $rs_den->den_id : '') ); ?>">
										<button type="submit" class="btn btn-default btn-success ">Salvar</button>
									</FORM>
									<!--## TÉRMINO DO FORMULÁRIO -->
								</div>

							</div>
						</div>
					</div>
				</div>

				<div id="content" style="<?php echo($show_form); ?>">
			 
					<!--## INÍCIO DO FORMULÁRIO -->
					<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmDadosResposta" id="frmDadosResposta" >
						<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORM-DENUNCIA-RESPOSTA">

						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
										<label for="descricao">Deixe seu comentário *</label>
										 <textarea id="dresp_descricao" name="dresp_descricao" class="form-control" rows="8"></textarea>
										<div class="help-block with-errors"></div>
								</div>
							</div>
						</div>
						<div class="row" style="display:none;">
							<div class="col-md-4">
								<div class="form-group">
									<input type="file" name="uplArqFileUpl" id="uplArqFileUpl" class="fld_file">
								</div><!--// form-group -->
							</div>
							<div class="col-md-6">&nbsp;</div>
						</div>

						<div class="row" style="margin-top:15px;">
							<div class="col-md-12">
								<button type="submit" class="btn btn-default btn-success ">Salvar</button>
								<button type="reset" class="btn btn-default btn-danger ">Cancelar</button>
							</div>
						</div>

					</FORM>
					<!--## TÉRMINO DO FORMULÁRIO -->

				</div>
				<?php
				}
				?>



			<?php } // if den_status != finalizado ?>
		</section> 


		<?php $this->load->view( "includes/footer" ); ?>


		<script>
			jQuery(document).ready(function ($) {
				$(document).on("click",".content-link",function(event) {
					event.preventDefault();	

					$('#content').toggle();

				});
			});
		</script> 

		<script>
			jQuery(document).ready(function ($) {

				$(document).on("click",".finalizar-link",function(event) {
					event.preventDefault();
					console.log('finalizar-link');
					$('form#frmDadosFinalizar').submit();
				});

				$('form#frmDadosResposta').submit( function(e){
					//e.preventDefault();
					var $form	= $(this);
					var $msg	= '';
					var $error = 0;

					var $dresp_descricao = $form.find("#dresp_descricao");

					if( $dresp_descricao.val().length == 0 ) { 
						$msg += "<p>- Preencha a Descrição.</p>";
						$error = 1;
					}

					if( $error != 0 )
					{
						$.alert({
							title: 'Atenção',
							confirmButtonClass: 'btn-info',
							cancelButtonClass: 'btn-danger',
							confirmButton: 'OK',
							//cancelButton: 'NO never !',
							content: $msg,
							confirm: function () {
								//$.alert('Confirmed!');
							}
						});
						return false;
					}else{
						$form.submit();
					}
				});
			});
		</script>


  </body>
</html>