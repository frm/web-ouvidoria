<?php $this->load->view( "includes/doctype" ); ?>

</head>

	<body>

		<?php $this->load->view( "includes/header-categ" ); ?>


		<?php 
		$_emp_id = isset($session_emp_id) ? $session_emp_id : "";
		$_emp_id = (int)$_emp_id;
		$_emp_codigo = isset($session_emp_codigo) ? $session_emp_codigo : "";
		$_emp_raza_social = isset($session_emp_raza_social) ? $session_emp_raza_social : ""; 
		?>
		<section id="categorias" class="container">	
				<!-- Page Heading/Breadcrumbs -->
				<div class="row">
						<div class="col-lg-12">
								<h1 class="page-header"></h1>
								<ol class="breadcrumb">
										<li><a href="<?php echo( site_url() ); ?>">Home</a></li>
										<li><a href="<?php echo( site_url('denunciar') ); ?>">Denunciar</a></li>
										<li><a href="<?php echo( site_url('empresa/'. $_emp_codigo) ); ?>"><?php echo( $_emp_raza_social ); ?></a></li>
										<li class="active">Relatar Incidente</a></li>
								</ol>
						</div>
				</div>		 

			<div class="row">
					<div class="col-lg-12">
						<h2 class="page-header">Relatar Incidente</h2>
					</div>
					<div class="col-lg-12">
						<h4>Você pode fazer seu registro de forma anônima ou identificada, como preferir.</h3>

						<h4>De qualquer forma, sua identidade estará protegida. As informações aqui registradas serão recebidas por uma empresa independente e especializada e o tratamento de cada incidente será supervisionado pela alta administração da EMPRESA X.</h4>

						<h3>Você deseja se identificar?</h3><br /><br />
						<a href="#" class="content-link btn btn-primary btn-lg active" role="button" aria-pressed="true">Sim</a>
						<a href="<?php echo( site_url('registro') ); ?>" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Não</a>     
					</div>
			</div>


			<hr>


			<?php
			$show_form = 'display:none;';
			if ( $var_erro != 0 && !empty($msg_erro) ){
				$show_form = 'display:block;';
			?>
				<div class="row">
					<div class="col-lg-12">
						<div class="alert alert-danger alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<h4><i class="icon fa fa-ban"></i> Atenção!</h4>
							Preencha corretamente todos os campos do formulário.
						</div>
					</div>
				</div>
			<?php
			}
			?>

			<div id="content" style="<?php echo($show_form); ?>">

				<!--## INÍCIO DO FORMULÁRIO -->
				<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmDadosDenunciante" id="frmDadosDenunciante" >
					<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORM-DADOS-DENUNCIANTE">

					<div class="row">
							<div class="col-md-6">
									<div class="form-group">
											<label for="dnt_nome">Nome *</label>
											<input type="text" id="dnt_nome" name="dnt_nome" class="form-control" placeholder="Por favor digite seu nome *" />
											<div class="help-block with-errors"></div>
									</div>
							</div>

							 <div class="col-md-6">
									<div class="form-group">
											<label for="nome">Cargo*</label>
											<input type="text" name="dnt_cargo" id="dnt_cargo" class="form-control" placeholder="Por favor digite seu cargo *" />
											<div class="help-block with-errors"></div>
									</div>
							</div>
					</div>
									
					<div class="row">
							<div class="col-md-6">
									<div class="form-group">
											<div class="form-group">
												<label for="dnt_email">Email *</label>
												<input type="text" name="dnt_email" id="dnt_email" class="form-control" placeholder="Por favor digite seu email *" />
												<div class="help-block with-errors"></div>
										</div>
									</div>
							</div>

							<div class="col-md-6">
									<div class="form-group">
											<div class="form-group">
												<label for="email">Telefone </label>
												<input type="text" name="dnt_telefone" id="dnt_telefone" class="form-control" placeholder="Por favor digite seu telefone" />
												<div class="help-block with-errors"></div>
										</div>
									</div>
							</div>

							<div class="col-md-12">
								<button type="submit" class="btn btn-default btn-success ">Prosseguir</button>
								<button type="reset" class="btn btn-default btn-danger ">Cancelar</button>
							</div>
					</div>

					<div class="row">
							<div class="col-md-12">
									<p class="text-muted"><strong>*</strong> Campos obrigatórios</p>
							</div>
					</div>

				</FORM>
				<!--## TÉRMINO DO FORMULÁRIO -->

			</div>
		</section>


		<?php $this->load->view( "includes/footer" ); ?>


		<script>
			jQuery(document).ready(function ($) {
				$(document).on("click",".content-link",function(event) {
					event.preventDefault();	

					$('#content').toggle();

				});
			});
		</script>

		<script>
			jQuery(document).ready(function ($) {
				$('form#frmDadosDenunciante').submit( function(e){
					//e.preventDefault();
					var $form	= $(this);
					var $msg	= '';
					var $error = 0;

					var $dnt_nome		= $form.find("#dnt_nome");
					var $dnt_cargo	= $form.find("#dnt_cargo");
					var $dnt_email	= $form.find("#dnt_email");

					if( $dnt_nome.val().length == 0 ) { 
						$msg += "<p>- Informe corretamente seu nome.</p>";
						$error = 1;
					}
					if( $dnt_cargo.val().length == 0 ) { 
						$msg += "<p>- Informe corretamente seu cargo.</p>";
						$error = 1;
					}
					if( $dnt_email.val().length == 0 ) { 
						$msg += "<p>- Informe corretamente seu e-mail.</p>";
						$error = 1;
					}

					if( $error != 0 )
					{
						$.alert({
							title: 'Atenção',
							confirmButtonClass: 'btn-info',
							cancelButtonClass: 'btn-danger',
							confirmButton: 'OK',
							//cancelButton: 'NO never !',
							content: $msg,
							confirm: function () {
								//$.alert('Confirmed!');
							}
						});
						return false;
					}else{
						$form.submit();
					}
				});
			});
		</script>


  </body>
</html>