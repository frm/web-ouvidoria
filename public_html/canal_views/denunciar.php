<?php $this->load->view( "includes/doctype" ); ?>

</head>

	<body>

		<?php $this->load->view( "includes/header-categ" ); ?>


		<section id="sobre-nos" class="container">
					<!-- Breadcrumbs -->
					<div class="row">
						 <div class="col-lg-12">
									<h1 class="page-header"></h1>
									<ol class="breadcrumb">
											<li><a href="<?php echo( site_url() ); ?>">Home</a></li>
											<li class="active">Denunciar</a></li>
									</ol>
							</div>
					</div>	  

				 
			<br />
			<div class="row">
				
				<div class="col-md-4">
					<div class="well">

						<!--## INÍCIO DO FORMULÁRIO -->
						<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmDenEmpresa" id="frmDenEmpresa" >
							<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-DENUNCIAR">

							<h2>Denunciar</h2>
							<div class="input-group">
								<input type="text" class="form-control" name="emp_codigo" id="emp_codigo" placeholder="Digite o nome ou código da Empresa">
								<span class="input-group-btn">
									<button type="submit" class="btn btn-default active">ok</button>
								</span>
							</div><!-- /.input-group -->

						</FORM>
						<!--## TÉRMINO DO FORMULÁRIO -->

					</div>
				</div>

		
				<div class="col-md-8">
					<div class="well">
						<!--## INÍCIO DO FORMULÁRIO -->
						<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmDenAcompanhar" id="frmDenAcompanhar" >
							<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-ACOMPANHAR-DENUNCIAR">

							<h2>Acompanhar sua Denúncia</h2>
							<div class="input-group">
								<input type="text" class="form-control" name="den_num_protocolo" id="den_num_protocolo" placeholder="Digite o protocolo...">
								<span class="input-group-btn"></span>
								<input type="password" class="form-control" name="den_senha" id="den_senha" placeholder="Digite a senha...">
								<span class="input-group-btn">
									<button class="btn btn-default active" type="submit">ok</button>
								</span>
							</div><!-- /input-group -->

						</FORM>
						<!--## TÉRMINO DO FORMULÁRIO -->

					</div><!-- /.well -->
				</div>

			</div><!-- /.row -->

		</section> 


		<?php $this->load->view( "includes/footer" ); ?>


		<script>
			jQuery(document).ready(function ($) {
				$('form#frmDenAcompanhar').submit( function(e){
					//e.preventDefault();
					var $form	= $(this);
					var $msg	= '';

					var $den_num_protocolo = $form.find("#den_num_protocolo");
					var $den_senha = $form.find("#den_senha");

					if( $den_num_protocolo.val().length == 0 ) { 
						$msg += "<p>- Informe corretamente o número do protocolo.</p>";
					}

					if( $den_senha.val().length == 0 ) { 
						$msg += "<p>- Informe corretamente a senha.</p>";
					}

					if( $msg.length > 0)
					{
						$.alert({
							title: 'Atenção',
							confirmButtonClass: 'btn-info',
							cancelButtonClass: 'btn-danger',
							confirmButton: 'OK',
							//cancelButton: 'NO never !',
							content: $msg,
							confirm: function () {
								//$.alert('Confirmed!');
							}
						});
						return false;
					}else{
						$form.submit();
					}

					//var formData = new FormData( this );
					//$.ajax({
						//url: $url,
						//type: 'POST',
						//data: formData,
						////async: false,
						//processData: false,
						//contentType: false,
						//beforeSend: function(data)	{ 
							//// console.log( data );
						//},
						//complete: function(data) { 
							//// console.log( data ); 
						//},
						//success: function(data) {
							//console.log( data ); // return false;
							//var json = JSON.parse(data);
							//if( json.report.success == "true" ){
								//window.location.href = json.report.redirect;
								//return false;
							//}else{
								////alert( json.report.message );
							//}
							//$.alert({
								//title: 'Atenção',
								//confirmButtonClass: 'btn-info',
								//cancelButtonClass: 'btn-danger',
								//confirmButton: 'OK',
								////cancelButton: 'NO never !',
								//content: json.report.message,
								//confirm: function () {
									////$.alert('Confirmed!');
								//}
							//});
							///*
							//var $el = $form.find(".tagitem");
							//$el.remove();
							//$form.find("#cmty_invite_description").val('');

							//fct_close_dialogs();

							//var json = JSON.parse(data);
							//var $modal = fct_modal_alert({
								//title: 'OBRIGADO!',
								//message: json.message
							//});
							//*/
							//// $this->session->set_userdata( "#SESSION-WEBSITE#", $session);
							//return false;
						//}
					//});
				});
			});
		</script>

  </body>
</html>
