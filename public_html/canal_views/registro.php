<?php $this->load->view( "includes/doctype" ); ?>

</head>

	<body>

		<?php $this->load->view( "includes/header-categ" ); ?>


		<?php 
		$_emp_id = isset($session_emp_id) ? $session_emp_id : "";
		$_emp_id = (int)$_emp_id;
		$_emp_codigo = isset($session_emp_codigo) ? $session_emp_codigo : "";
		$_emp_raza_social = isset($session_emp_raza_social) ? $session_emp_raza_social : ""; 
		?>
		<section id="categorias" class="container">	
				<!-- Page Heading/Breadcrumbs -->
				<div class="row">
						<div class="col-lg-12">
								<h1 class="page-header"></h1>
								<ol class="breadcrumb">
										<li><a href="<?php echo( site_url() ); ?>">Home</a></li>
										<li><a href="<?php echo( site_url('denunciar') ); ?>">Denunciar</a></li>
										<li><a href="<?php echo( site_url('empresa/'. $_emp_codigo) ); ?>"><?php echo( $_emp_raza_social ); ?></a></li>
										<li><a href="<?php echo( site_url('relatar-incidente') ); ?>">Relatar Incidente</a></li>
										<li class="active">Registro</a></li>
								</ol>
						</div>
				</div>


				<div class="row">
					<div class="col-lg-12">
						<h2 class="page-header">Registro</h2>
					</div>
					<div class="col-lg-12">
						<h4>Aqui você deve descrever qual foi a situação em que houve o descumprimento do nosso Código de Ética e Conduta, o Código Anticorrupção e/ou de qualquer lei, política ou norma interna da Empresa.</h4>

						<p><strong>É importante que o relato seja completo e detalhado, pois isso auxiliará o processo de averiguação. Não se esqueça de incluir no relato::</strong>
						<ol>
							<li>O quê (descrição da situação).</li>
							<li>Quem (nome das pessoas envolvidas, inclusive testemunhas).</li>
							<li>Quando (data em que aconteceu, acontece ou acontecerá a situação).</li>
							<li>Por que (a causa ou motivo).</li>
							<li>Quanto (se for possível medir).</li>
							<li>Provas (se elas existem e onde podem ser encontradas).</li>
						</ol>
						<p>Para acompanhar o andamento de seu relato, guarde o número do protocolo que lhe será fornecido após o registro do incidente.</p>
						<p>Obrigado pela iniciativa e confiança.</p>
					</div>
				</div>
					

			<hr>


			<?php
			if ( $var_erro != 0 && !empty($msg_erro) ){
			?>
				<div class="row">
					<div class="col-lg-4">&nbsp;</div>
					<div class="col-lg-8">
						<div class="alert alert-danger alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<h4><i class="icon fa fa-ban"></i> Atenção!</h4>
							<h5>Verifique os erros abaixo:</h5>
							<?php
							print_r( $msg_erro );
							?>
						</div>
					</div>
				</div>
			<?php
			}
			?>


			<div class="row">
				<div class="col-xs-12">
								 
					<!--## INÍCIO DO FORMULÁRIO -->
					<FORM role="form" action="<?php echo(current_url()); ?>" method="post" name="frmREGISTRO" id="frmREGISTRO" enctype="multipart/form-data" class="form-horizontal" >
						<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORM-DENUNCIA-REGISTRO">

						<?php 
						$session_nat_id = isset($session_nat_id) ? $session_nat_id : "";
						$session_nat_id = (int)$session_nat_id;
						?>
						<div class="form-group">
							<label for="tipo" class="col-xs-4 control-label">Classificação do Incidente *:</label>
							<div class="col-xs-8">
								<select name="nat_id" id="nat_id" class="form-control">
									<option value="">- selecione -</option>
									<?php
									if ( isset($rs_list_nat) ){
										foreach ($rs_list_nat AS $item){
											$selected = ($session_nat_id == (int)$item->nat_id) ? ' selected ' : ''; 
									?>
										<option value="<?php echo((int)$item->nat_id); ?>" <?php echo($selected); ?> ><?php echo($item->nat_titulo); ?></option>
									<?php
										}// foreach
									}// if
									?>
									<!--
									<option>Corrupção</option>
									<option>Roubo</option>
									<option>Risco</option>
									<option>Fraude</option>
									<option>Compliance</option>
									<option>Assédio Moral</option>
									<option>Assédio Sexual</option>
									<option>Insatisfação</option>
									<option>Favorecimento</option>
									<option>Desídia</option>
									<option>Demissão: queixas e relatos</option>
									<option>Outras Queixas ou Sugestões</option>
									-->
								</select>
							</div>
						</div><!--// form-group -->

						<div class="form-group hide">
								<label for="tipo" class="col-xs-4 control-label">Localidade/Área *:</label>
								<div class="col-xs-8">
									<select class="form-control">
										<option>Selecionar</option>
										<option>Opçoes</option>
									</select>
								</div>
						</div><!--// form-group -->

						<div class="form-group">
								<label for="tipo" class="col-xs-4 control-label">Descrição *:</label>
								<div class="col-xs-8">
									<textarea id="den_descricao" name="den_descricao" class="form-control" rows="15"></textarea>
								</div>
						</div><!--// form-group -->

						<div class="form-group">
								<label for="nome" class="col-xs-8 control-label">
									Este relato está ligado a outro incidente já denunciado? <br />
									Em caso positivo, por favor, informe o número do protocolo:
								</label>
								<div class="col-xs-4">
									<input type="text" name="den_num_protocolo_relac" id="den_num_protocolo_relac" class="form-control" id="protocolo">
								</div>
						</div><!--// form-group -->

						<div class="form-group">
								<label for="nome" class="col-xs-8 control-label">
									Se você quiser anexar arquivos à esta ocorrência, como fotos e documentos, adicione-os aqui. <br />
									O tamanho máximo permitido para cada arquivo é de 100 Mb.
								</label>
								<div class="col-xs-4">
									<input type="file" name="uplArqFileUpl" id="uplArqFileUpl" class="fld_file">
								</div>
						</div><!--// form-group -->
																							 
						
						<div class="form-group" style="margin-top:50px;">
								<div class="col-md-6 col-md-offset-3 col-centered">
									<button type="submit" class="btn btn-default btn-success col-centered">Enviar relato</button>
									<button type="reset" class="btn btn-default btn-danger col-centered">Cancelar</button>
								</div>
						</div><!--// form-group -->
						
					</FORM>
					<!--## TÉRMINO DO FORMULÁRIO -->
				
				</div>
			</div><!--// col-xs-12 -->

		</section>


		<?php $this->load->view( "includes/footer" ); ?>


		<script>
			jQuery(document).ready(function ($) {
				$('form#frmREGISTRO').submit( function(e){
					//e.preventDefault();
					var $form	= $(this);
					var $msg	= '';

					var $nat_id = $form.find("#nat_id");
					var $den_descricao = $form.find("#den_descricao");

					if( $nat_id.val().length == 0 ) { 
						$msg += "<p>- Selecione a Classificação do Incidente.</p>";
					}

					if( $den_descricao.val().length == 0 ) { 
						$msg += "<p>- Preencha a Descrição.</p>";
					}

					if( $msg.length > 0)
					{
						$.alert({
							title: 'Atenção',
							confirmButtonClass: 'btn-info',
							cancelButtonClass: 'btn-danger',
							confirmButton: 'OK',
							//cancelButton: 'NO never !',
							content: $msg,
							confirm: function () {
								//$.alert('Confirmed!');
							}
						});
						return false;
					}else{
						$form.submit();
					}

					//var formData = new FormData( this );
					//$.ajax({
						//url: $url,
						//type: 'POST',
						//data: formData,
						////async: false,
						//processData: false,
						//contentType: false,
						//beforeSend: function(data)	{ 
							//// console.log( data );
						//},
						//complete: function(data) { 
							//// console.log( data ); 
						//},
						//success: function(data) {
							//console.log( data ); // return false;
							//var json = JSON.parse(data);
							//if( json.report.success == "true" ){
								//window.location.href = json.report.redirect;
								//return false;
							//}else{
								////alert( json.report.message );
							//}
							//$.alert({
								//title: 'Atenção',
								//confirmButtonClass: 'btn-info',
								//cancelButtonClass: 'btn-danger',
								//confirmButton: 'OK',
								////cancelButton: 'NO never !',
								//content: json.report.message,
								//confirm: function () {
									////$.alert('Confirmed!');
								//}
							//});
							///*
							//var $el = $form.find(".tagitem");
							//$el.remove();
							//$form.find("#cmty_invite_description").val('');

							//fct_close_dialogs();

							//var json = JSON.parse(data);
							//var $modal = fct_modal_alert({
								//title: 'OBRIGADO!',
								//message: json.message
							//});
							//*/
							//// $this->session->set_userdata( "#SESSION-WEBSITE#", $session);
							//return false;
						//}
					//});
				});
			});
		</script>


  </body>
</html>