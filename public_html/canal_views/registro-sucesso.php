<?php $this->load->view( "includes/doctype" ); ?>

</head>

	<body>

		<?php $this->load->view( "includes/header-categ" ); ?>


		<?php 
		$_emp_id = isset($session_emp_id) ? $session_emp_id : "";
		$_emp_id = (int)$_emp_id;
		$_emp_codigo = isset($session_emp_codigo) ? $session_emp_codigo : "";
		$_emp_raza_social = isset($session_emp_raza_social) ? $session_emp_raza_social : ""; 
		?>
		<section id="categorias" class="container">	
				<!-- Page Heading/Breadcrumbs -->
				<div class="row">
						<div class="col-lg-12">
								<h1 class="page-header"></h1>
								<ol class="breadcrumb">
										<li><a href="<?php echo( site_url() ); ?>">Home</a></li>
										<li><a href="<?php echo( site_url('denunciar') ); ?>">Denunciar</a></li>
										<li><a href="<?php echo( site_url('empresa/'. $_emp_codigo) ); ?>"><?php echo( $_emp_raza_social ); ?></a></li>
										<li><a href="<?php echo( site_url('relatar-incidente') ); ?>">Relatar Incidente</a></li>
										<li>Registro</a></li>
								</ol>
						</div>
				</div>


				<div class="row">
					<div class="col-lg-12">
						<h2 class="page-header">Registro</h2>
					</div>
				</div>
					

				<div class="row">
					<div class="col-lg-12">
						
						<p>Sua denúncia foi cadastrada com sucesso!</p>

					</div>
				</div><!--// row -->


				<div class="row">
					<div class="col-lg-6">
						<div class="alert alert-success alert-dismissible">
							<h4><i class="icon fa fa-ban"></i> Atenção!</h4>
							<h5>Guarde seu protocolo e senha para acompanhar sua denúncia</h5>
							<h4>
								Número do Protocolo: 
								<?php 
								$den_num_protocolo = (isset($den_num_protocolo) ? $den_num_protocolo : "");
								echo( $den_num_protocolo ); 
								?>
							</h4>
						</div>
					</div>
					<div class="col-lg-6">&nbsp;</div>
				</div>


		</section>


		<?php $this->load->view( "includes/footer" ); ?>


		<script>
			jQuery(document).ready(function ($) {
				$('form#frmREGISTRO').submit( function(e){
					//e.preventDefault();
					var $form	= $(this);
					var $msg	= '';

					var $nat_id = $form.find("#nat_id");
					var $den_descricao = $form.find("#den_descricao");

					if( $nat_id.val().length == 0 ) { 
						$msg += "<p>- Selecione a Classificação do Incidente.</p>";
					}

					if( $den_descricao.val().length == 0 ) { 
						$msg += "<p>- Preencha a Descrição.</p>";
					}

					if( $msg.length > 0)
					{
						$.alert({
							title: 'Atenção',
							confirmButtonClass: 'btn-info',
							cancelButtonClass: 'btn-danger',
							confirmButton: 'OK',
							//cancelButton: 'NO never !',
							content: $msg,
							confirm: function () {
								//$.alert('Confirmed!');
							}
						});
						return false;
					}else{
						$form.submit();
					}

					//var formData = new FormData( this );
					//$.ajax({
						//url: $url,
						//type: 'POST',
						//data: formData,
						////async: false,
						//processData: false,
						//contentType: false,
						//beforeSend: function(data)	{ 
							//// console.log( data );
						//},
						//complete: function(data) { 
							//// console.log( data ); 
						//},
						//success: function(data) {
							//console.log( data ); // return false;
							//var json = JSON.parse(data);
							//if( json.report.success == "true" ){
								//window.location.href = json.report.redirect;
								//return false;
							//}else{
								////alert( json.report.message );
							//}
							//$.alert({
								//title: 'Atenção',
								//confirmButtonClass: 'btn-info',
								//cancelButtonClass: 'btn-danger',
								//confirmButton: 'OK',
								////cancelButton: 'NO never !',
								//content: json.report.message,
								//confirm: function () {
									////$.alert('Confirmed!');
								//}
							//});
							///*
							//var $el = $form.find(".tagitem");
							//$el.remove();
							//$form.find("#cmty_invite_description").val('');

							//fct_close_dialogs();

							//var json = JSON.parse(data);
							//var $modal = fct_modal_alert({
								//title: 'OBRIGADO!',
								//message: json.message
							//});
							//*/
							//// $this->session->set_userdata( "#SESSION-WEBSITE#", $session);
							//return false;
						//}
					//});
				});
			});
		</script>


  </body>
</html>