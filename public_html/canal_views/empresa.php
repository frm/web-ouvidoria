<?php $this->load->view( "includes/doctype" ); ?>

</head>

	<body>

		<?php $this->load->view( "includes/header-categ" ); ?>


		<section id="empresa" class="container">
				<!-- Breadcrumbs -->
				<div class="row">
					 <div class="col-lg-12">
								<h1 class="page-header"></h1>
								<ol class="breadcrumb">
										<li><a href="<?php echo( site_url() ); ?>">Home</a></li>
										<li><a href="<?php echo( site_url('denunciar') ); ?>">Denunciar</a></li>
										<li class="active"><?php echo( (isset($rs_emp->emp_razao_social)? $rs_emp->emp_razao_social : '') ); ?></a></li>
								</ol>
						</div>
				</div>	  

				<br />
				<div class="row">
						<div class="col-md-12">
								<h3>Canal Confidencial da <?php echo( (isset($rs_emp->emp_razao_social)? $rs_emp->emp_razao_social : '') ); ?></h3>
								<?php echo( (isset($rs_emp->emp_descricao)? $rs_emp->emp_descricao : '') ); ?>
						</div>
				</div>
		</section> 


		<section id="categorias" class="container">	

				<?php
				if ( isset($rs_emp_nat) ){
					$x_nat = 0;
					$x_col = 0;
					foreach ($rs_emp_nat AS $item){
						$x_nat++;
						$x_col++;
						$nat_id						= (int)$item->nat_id;
						$nat_titulo				= $item->nat_titulo;
						$nat_prazo				= $item->nat_prazo;
						$nat_descricao		= $item->nat_descricao;
						$nat_arquivo			= $item->nat_arquivo;
						$natemp_arquivo		= $item->natemp_arquivo;
						$nat_dte_cadastro	= fct_formatdate($item->nat_dte_cadastro, 'd/m/Y H:i');
						$icon_ativo				= ($item->nat_ativo==1)?"sim":"não";
						//$redirect					= $url["url_form"] ."/". $nat_id;

						$form_name				= "frmNatureza_". $x_nat;

						$nat_arquivo_tag = '<img data-src="holder.js" class="img-responsive img-circle" alt="Sem Imagem" style="margin:0 auto; width: 100%; max-width: 100%; height: 250px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15cc09e90d7%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15cc09e90d7%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2275.5%22%20y%3D%22104.5%22%3E%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">';

						$image_path = $this->config->item('folder_images') .'/naturezas/'. $natemp_arquivo;
						if( file_exists($image_path) and is_file($image_path) )
						{
							$nat_arquivo_tag = '<img src="'. base_url($image_path) .'" alt="" class="img-responsive img-circle" style="margin:0 auto; width: 100%; max-width: 100%; height: 250px;" />';
						}else{
							$image_path = $this->config->item('folder_images') .'/naturezas/'. $nat_arquivo;
							if( file_exists($image_path) and is_file($image_path) )
							{
								$nat_arquivo_tag = '<img src="'. base_url($image_path) .'" alt="" class="img-responsive img-circle" style="margin:0 auto; width: 100%; max-width: 100%; height: 250px;" />';
							}
						};

				?>
				<?php if( ($x_col == 1) ){ ?>
				<div class="row">
				<?php } ?>
				
					<!-- <?php echo($nat_titulo); ?> -->
					<div class="col-md-4 col-sm-6">
							<FORM name="<?php echo($form_name); ?>" id="<?php echo($form_name); ?>" method="POST" action="<?php echo(current_url()); ?>" >
								<input type="hidden" name="baseAcao" id="baseAcao" value="SEND-FORMULARIO-NATUREZA">
								<input type="hidden" id="nat_id" name="nat_id" value="<?php echo($nat_id); ?>" />
								<input type="hidden" id="nat_titulo" name="nat_titulo" value="<?php echo($nat_titulo); ?>" />

								<div class="panel panel-default">
										<div class="panel-heading">
											<span class="">
												<?php echo($nat_arquivo_tag); ?>
											</span>
										</div>
										<div class="panel-body" style="min-height:280px; position:relative">
											<h4><?php echo($x_nat); ?>. <?php echo($nat_titulo); ?></h4>
												<p>
													<?php echo($nat_descricao); ?>
												</p>
												<div class="push-to-bottom btn-block row">
													<div class="col-md-12">
														<input type="submit" value="Relatar" name="relatar" class="btn btn-warning btn-block">
													</div>
												</div>
										</div>
								</div>
							</FORM>
					</div>
					<!-- /.<?php echo($nat_titulo); ?> -->

				<?php if( ($x_col % 3) == 0 ||  $x_nat >= count($rs_emp_nat) ){ $x_col = 0; ?>
				</div>
					<?php if( $x_nat < count($rs_emp_nat) ){ ?>
					<hr>
					<?php } ?>
				<?php } ?>


				<?php
					}; // foreach : rs_emp_nat
				}; // if isset : rs_emp_nat
				?>

			<div class="clear"></div>
		</section>


		<section id="categorias" class="container hide">	
		
			<div class="row">        
						<!-- Bloco 1-->

						<!-- Corrupção-->
						<div class="col-md-4 col-sm-6">
								<div class="panel panel-default">
										<div class="panel-heading">
												<span class="">
												<img class="img-responsive img-circle" src="assets/fonts//img/corrupcao.jpg" alt="">
												 
												</span>
										</div>
										<div class="panel-body">
											 <form id="form1" method="post" action="<?php echo( site_url('relatar-incidente') ); ?>" >
										<input type="hidden" id="corrupcao" name="corrupcao" class="campo" value="Corrupção"/>
										<h4>1. Corrupção</h4>
													<p><br />Casos envolvendo fornecedores, clientes ou agentes públicos, em que você suspeita de qualquer relacionamento inadequado ou negociação anormal por parte de qualquer pessoa da empresa.</p>
													<input type="submit" value="Relatar" name="relatar" class="btn btn-warning btn-block">
										<!--	<a href="relatar_incidente.php" type="submit" value="relatar" class="btn btn-warning btn-block">Relatar</a>
								</form>  -->
										</div>
								</div>
						</div>
						<!-- /.Corrupção-->

						<!-- Roubo-->            
						<div class="col-md-4 col-sm-6">
								<div class="panel panel-default text-justify">
										<div class="panel-heading">
												<span class="">
												<img class="img-responsive img-circle" src="assets/fonts//img/roubo.jpg" alt="">
												 
												</span>
										</div>
										<div class="panel-body">
												<h4>2. Furto</h4>
												<p><br />Casos em que você observou (ou soube de) pessoa na empresa (de dentro ou de fora) que tenha se apropriado de dinheiro, estoque, objetos ou informações.</p> <br />    
												<a href="#" class="btn btn-warning btn-block">Relatar</a>       
										</div>
								</div>
						</div>
						<!-- /.Roubo -->

						<!-- Risco-->    
						 <div class="col-md-4 col-sm-6">
								<div class="panel panel-default text-justify">
										<div class="panel-heading">
												<span class="">
												<img class="img-responsive img-circle" src="assets/fonts//img/risco.jpg" alt="">
												 
												</span>
										</div>
										<div class="panel-body">
												<h4>3. Risco</h4>
												<p>Casos em que você percebeu situação de risco envolvendo algum projeto ou propriedade da empresa. Exemplos: equipamentos ligados e abandonados, mercadoria em local impróprio, documentos abandonados, pessoas estranhas ao local, etc.</p> 
												<a href="#" class="btn btn-warning btn-block">Relatar</a>           
										</div>
							</div>
						</div>
						<!-- /.Risco -->
				</div>
				<!-- /.row -->

			<hr>

				<!-- Bloco 2-->        
				 <div class="row">
						<!-- Fraude-->
								<div class="col-md-4 col-sm-6">
										<div class="panel panel-default text-justify">
												<div class="panel-heading">
														<span class="">
														<img class="img-responsive img-circle" src="assets/fonts//img/fraude.jpg" alt="">
														 
														</span>
												</div>
												<div class="panel-body">
														<h4>4. Fraude</h4>
														<p><br />Casos envolvendo documentos ou arquivos eletrônicos, em que você suspeita que alguém alterou documentos da empresa com qualquer objetivo. Exemplos:  emissão de recibos sem comprovação de recebimento, baixa contábil sem autorização, fraude em documento de recebimento ou embarque de mercadoria, etc.</p>   
														<a href="#" class="btn btn-warning btn-block">Relatar</a>            
												</div>
										</div>
								</div>
								<!-- /.Fraude -->

							<!-- Compliance-->
							<div class="col-md-4 col-sm-6">
								<div class="panel panel-default text-justify">
										<div class="panel-heading">
												<span class="">
												<img class="img-responsive img-circle" src="assets/fonts//img/compliance.jpg" alt="">
												 
												</span>
										</div>
										<div class="panel-body">
												<h4>5. Compliance</h4>
												<p><br /><br />Casos envolvendo regras da empresa que não foram cumpridas.   Exemplos: falha de procedimento, falha no trato com clientes, omissão de informações,  descumprimento de prazos fiscais, erro na guarda de documentos, falha na segurança da informação,  conflito de interesses, etc.</p><br />
												<a href="#" class="btn btn-warning btn-block">Relatar</a>
										</div>
								</div>
						</div>
						<!-- /.Compliance -->

						<!-- AssedioMoral-->
						<div class="col-md-4 col-sm-6">
								<div class="panel panel-default text-justify">
										<div class="panel-heading">
												<span class="">
												<img class="img-responsive img-circle" src="assets/fonts//img/assediom.jpg" alt="">
												 
												</span>
										</div>
										<div class="panel-body">
												<h4>6. Assédio Moral</h4>
												 <p>Casos em que o chefe direto ou outro gestor demonstrou desrespeito a você ou colega. Pode ter sido com palavras, textos, piadas, riso, gestos, recusa, etc.. A recusa pode ter sido de atendimento pessoal, telefônico ou por email.</p>
							<p>Nesse caso, é importante incluir o nome de testemunhas.</p><br />
												<a href="#" class="btn btn-warning btn-block">Relatar</a>
										</div>
								</div>
						</div>
						<!-- /.AssedioMoral- -->
				</div>
				<!-- /.row -->

			 <hr>

				<!-- Bloco 3-->
			 <div class="row">
						<!-- AssedioSexual-->
								<div class="col-md-4 col-sm-6">
										<div class="panel panel-default text-justify">
												<div class="panel-heading">
														<span class="">
														<img class="img-responsive img-circle" src="assets/fonts//img/assedios.jpg" alt="">
														 
														</span>
												</div>
												<div class="panel-body">
														<h4>7. Assédio Sexual</h4><br />
														<p>Casos em que o/a chefe ou outro/a superior hierárquico, de forma clara ou não, demonstrou interesse sexual por você ou colega. </p>
														<p>Nesse caso, é importante incluir o nome de testemunhas.</p>
														<a href="#" class="btn btn-warning btn-block">Relatar</a> 
												</div>
										</div>
								</div>
								<!-- /.AssedioSexual-->

								<!-- Insatisfação-->
								<div class="col-md-4 col-sm-6">
										<div class="panel panel-default text-justify">
												<div class="panel-heading">
														<span class="">
														<img class="img-responsive img-circle" src="assets/fonts//img/insatisfacao.jpg" alt="">
														 
														</span>
												</div>
												<div class="panel-body">
														<h4>8. Insatisfação</h4><br />
														<p><br />Exemplos de insatisfação no ambiente de trabalho: condições insalubres, desconforto físico, equipamento inadequado, tarefas desgastantes, falta de apoio, falta de orientação, colegas que não passam informação, descaso do chefe,  intolerância do chefe, etc.</p><br />
														<a href="#" class="btn btn-warning btn-block">Relatar</a>
												</div>
										</div>
								</div>
								<!-- /.Insatisfação-->

								<!-- favorecimento-->
								<div class="col-md-4 col-sm-6">
										<div class="panel panel-default text-justify">
												<div class="panel-heading">
														<span class="">
														<img class="img-responsive img-circle" src="assets/fonts//img/favorecimento.jpg" alt="">
														 
														</span>
												</div>
												<div class="panel-body">
														<h4>9. Favorecimento</h4><br />
														<p><br />Casos em que você observou qualquer privilégio ou vantagem indevida. Exemplos: nepotismo, admissão ou promoção de pessoa desqualificada para o cargo, atitude de favorecimento, benefícios extraordinários, etc.</p>  
														<a href="#" class="btn btn-warning btn-block">Relatar</a>
												</div>
										</div>
								</div>
								<!-- /.Insatisfação-->
				</div>
				<!-- /.row -->

				<hr>

				<!-- Bloco 4-->
			 <div class="row">
						<!-- Desídia-->
								<div class="col-md-4 col-sm-6">
										<div class="panel panel-default text-justify">
												<div class="panel-heading">
														<span class="">
														<img class="img-responsive img-circle" src="assets/fonts//img/des.jpg" alt="">
														 
														</span>
												</div>
												<div class="panel-body">
														<h4>10. Desídia</h4>
														<p>Casos frequentes de abandono do posto, desinteresse pelo trabalho ou falta de atendimento aos clientes. Outros exemplos: atrasos e ausências frequentes, recusa a tratar com  clientes, prazos não cumpridos, etc.</p>  
														<a href="#" class="btn btn-warning btn-block">Relatar</a>
												</div>
										</div>
								</div>
								<!-- /.Desídia-->

								<!-- Demissão-->
								<div class="col-md-4 col-sm-6">
										<div class="panel panel-default text-justify">
												<div class="panel-heading">
														<span class="">
														<img class="img-responsive img-circle" src="assets/fonts//img/demissao.jpg" alt="">
														 
														</span>
												</div>
												<div class="panel-body"><br />
														<h4>11. Demissão: entrevista de saída</h4>
														<p>Todas as pessoas demitidas (ou que pediram demissão) podem oferecer sua contribuição aos colegas que ficaram, comentando sobre sua passagem pela empresa. Seu relato será encaminhado à Diretoria da Empresa. </p><br />
														<a href="#" class="btn btn-warning btn-block">Relatar</a>
												</div>
										</div>
								</div>
								<!-- /.Demissao-->

								<!-- Outros-->
								<div class="col-md-4 col-sm-6">
										<div class="panel panel-default text-justify">
												<div class="panel-heading">
														<span class="">
														<img class="img-responsive img-circle" src="assets/fonts//img/outros.jpg" alt="">
														 
														</span>
												</div>
												<div class="panel-body">
														<h4><br />12. Outras Queixas ou Sugestões</h4><br /><br />
														<p><br />Gostaria de sugerir mudanças?  </p>
														<p>Acha que alguma coisa pode melhorar? </p>
								<p>Tem queixas não definidas acima? </p>
													
														<a href="#" class="btn btn-warning btn-block">Relatar</a>
												</div>
										</div>
								</div>
								<!-- /.Outros-->
				</div>

				 <!-- /.row -->
		</section>


		<?php $this->load->view( "includes/footer" ); ?>


  </body>
</html>