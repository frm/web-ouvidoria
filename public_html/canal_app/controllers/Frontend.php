<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends MY_Controller {

	function __construct()
	{
		parent::__construct();

		/**
		 * AUTH
		**/
		//$this->authlib->logged_in();
	}

	public function index()
	{
		$data = array();

		$var_erro_cnt	= 0;
		$msg_erro_cnt	= '';
		$msg_success = '';


		/**
		 * -------------------------------------------------
		 * processando formuário fale conosco
		 * -------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		if($baseAcao == "SEND-FORM-FALE-CONOSCO")
		{
			if( fct_validate("fale_conosco") )
			{
					$tpl_email		= "";
					$cnt_nome			= utf8_decode($this->input->post('cnt_nome'));
					$cnt_email		= utf8_decode($this->input->post('cnt_email'));
					$cnt_mensagem	= utf8_decode($this->input->post('cnt_mensagem'));

					$enviar_para = array(
						'cleliapuc@gmail.com',
						'ab@ipge.com.br',
					);

					/**
					 * -------------------------------------------------
					 * recuperamos o html configurado no admin
					 * -------------------------------------------------
					**/
					$this->db->select(" * ")
						->from('tbl_configuracoes')
						->where(array(
								'cfg_area'	=> 'template-emails',
								'cfg_chave' => 'email-contato'
							)	
						)
						->limit(1);
					$query = $this->db->get();
					if( (int)$query->num_rows() >=1 )
					{
						$rs_email = $query->row();
						//fct_print_debug( $rs_email );
						$tpl_email = $rs_email->cfg_valor;
						$cfg_json = json_decode($rs_email->cfg_json, true);
					
						$enviar_para = preg_split('/[,;]+/', $cfg_json, -1, PREG_SPLIT_NO_EMPTY);
						$enviar_para = array_map('trim',$enviar_para);
					}
					//fct_print_debug( $enviar_para );


					/**
					 * -------------------------------------------------
					 * parsemos os dados com o template
					 * -------------------------------------------------
					**/
					$dataMail = array(
						'cnt_nome'			=> $cnt_nome,
						'cnt_email'			=> $cnt_email,
						'cnt_mensagem'	=> nl2br($cnt_mensagem) ." enviando para: ". json_encode($enviar_para),
					);
					$tpl_email_parse = $this->parser->parse_string($tpl_email, $dataMail, TRUE);

					//$tpl_email_parse = $this->parser->parse('email/contato', $dataMail, TRUE);

					$dataMail = array(
						'titulo_pagina'			=> 'Web Ouvidoria',
						'conteudo_template'	=> $tpl_email_parse,
					);
					$body = $this->parser->parse('email/template-padrao', $dataMail, TRUE);


					/**
					 * -------------------------------------------------
					 * configuracao do email
					 * -------------------------------------------------
					**/
					$arrInfos = array(
						'from_nome'		=> 'Web Ouvidoria',
						'from_email'	=> 'no-reply@webouvidoria.com.br',
						'replyto'			=> 'atendimento@webouvidoria.com.br ',
						'to'					=> $enviar_para,
						'to_cc'				=> '',
						//'to_bcc'			=> array('listasguardiao@gmail.com'),
						'to_bcc'			=> '',
						'subject'			=> utf8_decode('[Web Ouvidoria] : Contato'),
						'message'			=> $body
					);
					self::sendMailExec($arrInfos);
					//fct_print_debug( $body );


					$msg_success = "Sua mensagem foi enviada com sucesso!";
			}
			else
			{
				$msg_erro_cnt = validation_errors();
				$var_erro_cnt = 1;
			};
		}
		$data['var_erro_cnt']	= $var_erro_cnt;
		$data['msg_erro_cnt']	= $msg_erro_cnt;

		$data['msg_success']	= $msg_success;



		$this->load->view('index', $data);
	}


	public function denunciar()
	{
		$data = array();


		$var_erro						= 0;
		$msg_erro						= '';
		$emp_codigo					= '';

		/**
		 * -------------------------------------------------
		 * verificamos se o código da empresa existe
		 * -------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		if($baseAcao == "SEND-DENUNCIAR")
		{
			$emp_codigo = $this->input->post('emp_codigo');

			$var_erro = empty($emp_codigo) ? 1 : $var_erro;

			if( $var_erro == 0 )
			{
				$this->db->select(" * ")
					->from('tbl_empresas')
					->where(array(
							'emp_codigo' => $emp_codigo
						)	
					)
					->limit(1);
				$query = $this->db->get();
				if( (int)$query->num_rows() >=1 )
				{
					$rs_emp = $query->row();
					$data['rs_bd'] = $rs_emp;

					$arr_session_data = array(
						"den_hashkey"			=> md5(date("Y-m-d H:i:s")),
						"emp_select"			=> 'true',
						"emp_id"					=> (int)$rs_emp->emp_id,
						"emp_codigo"			=> $rs_emp->emp_codigo,
						"emp_raza_social"	=> $rs_emp->emp_razao_social,
					);
					$this->session->set_userdata( $this->var_session, $arr_session_data);

					//redirect( site_url('empresa/'. $emp_codigo) );
					redirect( site_url($emp_codigo) );
					exit();
				}
				else
				{
					$msg_erro = 'Código inexistente.';	
				}
				//fct_print_debug( $data['rs_bd'] );
				//$data_bd = array(
					//'dnt_nome'					=> $dnt_nome,
					//'dnt_cargo'					=> $dnt_cargo,
					//'dnt_email'					=> $dnt_email,
					//'dnt_telefone'			=> $dnt_telefone,
					//'dnt_dte_cadastro'	=> date("Y-m-d H:i:s"),
					//'dnt_dte_alteracao'	=> date("Y-m-d H:i:s"),
				//);		
				//$this->db->insert('tbl_autores', $data_bd); 
				////$den_id = (int)$this->db->insert_id();	
			}
			else
			{
				$msg_erro = 'Preencha corretamente todos os campos do formulário.';
			}
		};




		/**
		 * -------------------------------------------------
		 * acompanhamento de denuncia
		 * -------------------------------------------------
		**/
		if($baseAcao == "SEND-ACOMPANHAR-DENUNCIAR")
		{
			if( fct_validate("denuncia_acompanhar") )
			{
				$den_num_protocolo = $this->input->post('den_num_protocolo');
				$den_senha = $this->input->post('den_senha');

				$this->db->select(" * ")
					->from('tbl_denuncias')
					->where(array(
							'den_num_protocolo' => $den_num_protocolo,
							//'den_senha' => fct_encripta($den_senha),
						)	
					)
					->limit(1);
				$query = $this->db->get();
				if( (int)$query->num_rows() >=1 )
				{
					$rs_den = $query->row();

					$arr_session_data = array(
						"den_id"						=> (int)$rs_den->den_id,	
						"den_hashkey"				=> $rs_den->den_hashkey,
						"den_num_protocolo"	=> $den_num_protocolo,
					);
					$this->session->set_userdata( $this->var_session, $arr_session_data);

					redirect( site_url('resposta') );
					exit();
				}
			}
		}		



		$data['emp_codigo']			= $emp_codigo;

		$data['var_erro']				= $var_erro;
		$data['msg_erro']				= $msg_erro;
		//fct_print_debug( $data );
		// -------------------------------------------------


		$this->load->view('denunciar', $data);
	}


	public function empresa($emp_codigo="")
	{
		$data = array();

		if( empty($emp_codigo) )
		{
			exit('código inválido');
		}

		//$var_erro						= 0;
		//$msg_erro						= '';
		//$emp_codigo					= '';

		/**
		 * -------------------------------------------------
		 * verificamos se o código da empresa existe
		 * -------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		if($baseAcao == "SEND-FORMULARIO-NATUREZA")
		{
			if( fct_validate("denuncia_natureza") )
			{
				$nat_id = (int)$this->input->post('nat_id');
				$nat_titulo = $this->input->post('nat_titulo');

				//fct_print_debug( $this->session->userdata( $this->var_session ) );

				$arr_session_data = array(
					"nat_id"			=> (int)$nat_id,
					"nat_titulo"	=> $nat_titulo,
				);
				$arr_session_data_new = array_merge($this->session->userdata( $this->var_session ), $arr_session_data);
				$this->session->set_userdata( $this->var_session, $arr_session_data_new);
				//fct_print_debug( $this->session->userdata( $this->var_session ) );

				redirect( site_url('relatar-incidente') );
				exit();
			}
			else
			{
				//fct_print_debug( validation_errors() );
				$msg_erro = validation_errors();
				$var_erro = 1;
			};
			
		}


		/**
		 * -------------------------------------------------
		 * verificamos se o código da empresa existe
		 * -------------------------------------------------
		**/
		$this->db->select(" * ")
			->from('tbl_empresas')
			->where(array(
					'emp_codigo' => $emp_codigo,
					'emp_ativo' => '1'
				)	
			)
			->limit(1);
		$query = $this->db->get();
		if( (int)$query->num_rows() >= 1 )
		{
			$rs_emp = $query->row();
			$data['rs_emp'] = $rs_emp;
			//fct_print_debug( $data['rs_emp'] );
			//fct_print_debug( $this->session->userdata( $this->var_session ) );

			/**
			 * -------------------------------------------------
			 * naturezas relacionadas a empresa
			 * -------------------------------------------------
			**/
			$this->db->select(" * ")
				->select(" COALESCE(REL.natemp_ordem, 999999999) AS Ordenar ")
				->select(" REL.natemp_arquivo ")
				->from('tbl_naturezas NAT')
				->join('tbl_rel_empresas_x_naturezas REL', 'REL.nat_id = NAT.nat_id', 'INNER JOIN')
				->where(array(
						'REL.emp_id' => $data['rs_emp']->emp_id
					)	
				)

				->order_by( 'Ordenar', 'ASC' )
				->order_by( 'NAT.nat_titulo', 'ASC' );
			$qry_nat = $this->db->get();
			//fct_print_debug( $this->db->last_query() );
			if( (int)$qry_nat->num_rows() >= 1 )
			{	
				$data['rs_emp_nat'] = $qry_nat->result();
				//fct_print_debug( $data['rs_emp_nat'] );
			}
			
		}
		else
		{
			$msg_erro = 'Código inexistente.';	
		}


		$this->load->view('empresa', $data);
	}


	public function relatar_incidente()
	{
		$data = array();


		$var_erro						= 0;
		$msg_erro						= '';
		$dnt_nome						= '';
		$dnt_cargo					= '';
		$dnt_email					= '';
		$dnt_telefone				= '';


		$arr_session_data = array(
			"identificar"		=> '0',
			"dnt_nome"			=> '',
			"dnt_cargo"			=> '',
			"dnt_email"			=> '',
			"dnt_telefone"	=> '',
		);
		$arr_session_data_new = array_merge($this->session->userdata( $this->var_session ), $arr_session_data);
		$this->session->set_userdata( $this->var_session, $arr_session_data_new);


		/**
		 * -------------------------------------------------
		 * verifica se dados são válidos
		 * -------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		$acao = 'INSERT';
		if($baseAcao == "SEND-FORM-DADOS-DENUNCIANTE")
		{
			$dnt_nome						= $this->input->post('dnt_nome');
			$dnt_cargo					= $this->input->post('dnt_cargo');
			$dnt_email					= $this->input->post('dnt_email');
			$dnt_telefone				= $this->input->post('dnt_telefone');

			$var_erro = empty($dnt_nome) ? 1 : $var_erro;
			$var_erro = empty($dnt_email) ? 2 : $var_erro;

			if( $var_erro == 0 )
			{
				$arr_session_data = array(
					"identificar"		=> '1',
					"dnt_nome"			=> $dnt_nome,
					"dnt_cargo"			=> $dnt_cargo,
					"dnt_email"			=> $dnt_email,
					"dnt_telefone"	=> $dnt_telefone,
				);
				$arr_session_data_new = array_merge($this->session->userdata( $this->var_session ), $arr_session_data);
				$this->session->set_userdata( $this->var_session, $arr_session_data_new);
				//fct_print_debug( $this->session->userdata( $this->var_session ) );

				//$data_bd = array(
					//'dnt_nome'					=> $dnt_nome,
					//'dnt_cargo'					=> $dnt_cargo,
					//'dnt_email'					=> $dnt_email,
					//'dnt_telefone'			=> $dnt_telefone,
					//'dnt_dte_cadastro'	=> date("Y-m-d H:i:s"),
					//'dnt_dte_alteracao'	=> date("Y-m-d H:i:s"),
				//);		
				//$this->db->insert('tbl_autores', $data_bd); 
				////$den_id = (int)$this->db->insert_id();	

				redirect( site_url('registro') );
				exit();
			}
			else
			{
				$msg_erro = 'Preencha corretamente todos os campos do formulário.';
			}
		};

		$data['dnt_nome']				= $dnt_nome;
		$data['dnt_cargo']			= $dnt_cargo;
		$data['dnt_email']			= $dnt_email;
		$data['dnt_telefone']		= $dnt_telefone;

		$data['var_erro']				= $var_erro;
		$data['msg_erro']				= $msg_erro;
		// -------------------------------------------------


		$this->load->view('relatar-incidente', $data);
	}


	public function registro()
	{
		$data = array();

		$this->authlib->logged_in();
		$this->authSession = $this->authlib->get_session_by_value();
		//fct_print_debug( $this->session->userdata( $this->var_session ) );


		/**
		 * --------------------------------------------------------
		 * upload do arquivo
		 * --------------------------------------------------------
		**/
		$config_upl['upload_path']		= $this->config->item('folder_images') .'/denuncias/';
		$config_upl['allowed_types']	= 'gif|jpg|jpeg|png|xls|xlsx|ppt|pptx|mp4|m4v|mov|avi|flv|mpg|wmv|3gp';
		$config_upl['max_size']				= (15*1024); // 15mb				=> '102048'; // 2mb = 2048
		$config_upl['max_width']			= '0';
		$config_upl['max_height']			= '0';


		$var_erro						= 0;
		$msg_erro						= '';
		$emp_id							= (int)0;
		$nat_id							= (int)0;
		$dnt_id							= (int)0;
		$den_descricao			= '';
		$den_num_protocolo	= '';
		$dnt_json						= '';


		/**
		 * -------------------------------------------------
		 * verifica se dados são válidos
		 * -------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		$acao = 'INSERT';
		if($baseAcao == "SEND-FORM-DENUNCIA-REGISTRO")
		{
			if( fct_validate("denuncia_registro") )
			{
				$dnt_id = 0;

				/*
				 * gravamos primeiro os dados do denunciante caso ele queira se identificar
				 * -------------------------------------------------
				**/
				if( (int)$this->authSession["identificar"] == 1 )
				{
					$data_json = array(
						'dnt_nome'					=> $this->authSession["dnt_nome"],
						'dnt_cargo'					=> $this->authSession["dnt_cargo"],
						'dnt_email'					=> $this->authSession["dnt_email"],
						'dnt_telefone'			=> $this->authSession["dnt_telefone"],
					);
					$dnt_json = json_encode($data_json);

					$this->db->select(" * ")
						->from('tbl_autores')
						->where(array(
								'dnt_email' => $this->authSession["dnt_email"],
							)	
						)
						->order_by( 'dnt_id', 'DESC' )
						->limit(1);
					$query = $this->db->get();
					if( (int)$query->num_rows() >= 1 )
					{
						$rs_dnt = $query->row();
						$dnt_id = (int)$rs_dnt->dnt_id;
					}
					else
					{
						$data_bd = array(
							'dnt_nome'					=> $this->authSession["dnt_nome"],
							'dnt_cargo'					=> $this->authSession["dnt_cargo"],
							'dnt_email'					=> $this->authSession["dnt_email"],
							'dnt_telefone'			=> $this->authSession["dnt_telefone"],
							'dnt_dte_cadastro'	=> date("Y-m-d H:i:s"),
							'dnt_dte_alteracao'	=> date("Y-m-d H:i:s"),
						);		
						$this->db->insert('tbl_autores', $data_bd); 
						$dnt_id = (int)$this->db->insert_id();
					}
				}


				/*
				 * -------------------------------------------------
				 * gravamos os dados da denuncia
				 * -------------------------------------------------
				**/
				$den_parent_id			= 0;
				$nat_id							= (int)$this->input->post('nat_id');
				$den_descricao			= $this->input->post('den_descricao');
				$den_num_protocolo_relac = $this->input->post('den_num_protocolo_relac');
				$den_ativo					= (int)1;


				/*
				 * -------------------------------------------------
				 * verificamos se existe uma denuncia relacionada
				 * -------------------------------------------------
				**/
				$this->db->select(" * ")
					->from('tbl_denuncias')
					->where(array(
							'den_num_protocolo' => $den_num_protocolo_relac,
						)	
					)
					->order_by( 'den_id', 'DESC' )
					->limit(1);
				$query = $this->db->get();
				if( (int)$query->num_rows() >= 1 )
				{
					$rs_den = $query->row();
					$den_parent_id = (int)$rs_den->den_id;
				}


				/*
				 * -------------------------------------------------
				 * verifica se a denuncia já existe no banco
				 * -------------------------------------------------
				**/
				$this->db->select(" * ")
					->from('tbl_denuncias')
					->where(array(
							'den_hashkey' => $this->authSession["den_hashkey"],
						)	
					)
					->order_by( 'den_id', 'DESC' )
					->limit(1);
				$query = $this->db->get();
				if( (int)$query->num_rows() == 0 )
				{
					$data_bd = array(
						'den_hashkey'				=> $this->authSession["den_hashkey"],
						'emp_id'						=> (int)$this->authSession["emp_id"],
						'den_parent_id'			=> $den_parent_id,
						'nat_id'						=> $nat_id,
						'dnt_id'						=> $dnt_id,
						'den_descricao'			=> $den_descricao,
						'den_status'				=> 'em-analise',
						'den_status_admin'	=> 'em-analise',
						'dnt_json'					=> $dnt_json,
						//den_num_protocolo'	=> $den_num_protocolo,
						'den_num_protocolo_relac' => $den_num_protocolo_relac,
						'den_dte_cadastro'	=> date("Y-m-d H:i:s"),
						'den_dte_alteracao'	=> date("Y-m-d H:i:s"),
						'den_ativo'					=> $den_ativo,
					);		
					$this->db->insert('tbl_denuncias', $data_bd); 
					$den_id = (int)$this->db->insert_id();
				}

				if( $den_id > 0 )
				{
					$den_num_protocolo = date("Ymd") .'-'. str_pad($den_id, 5, "0", STR_PAD_LEFT); 
					$data_bd = array(
						'den_num_protocolo' => $den_num_protocolo,
						'den_dte_alteracao'	=> date("Ym-d H:i:s")
					);		
					$where_bd = array(
						'den_id' => (int)$den_id,
					);
					$this->db->where($where_bd);
					$this->db->update('tbl_denuncias', $data_bd);

					// registro/num:20070709-00012/hashkey:3de4984ecb06ba9fffc11892ebd0f611
					//$link_redirect = site_url('registro/num:'. $den_num_protocolo .'/hashkey:'. $this->authSession["den_hashkey"]);
					$link_redirect = site_url('registro/'. $den_num_protocolo .'/'. $this->authSession["den_hashkey"]);


					$dataMail = array(
					);
					$body = $this->parser->parse('email/denuncia-registro', $dataMail, TRUE);

					$arrInfos = array(
						'from_nome'		=> 'Web Ouvidoria',
						'from_email'	=> 'no-reply@webouvidoria.com.br ',
						'replyto'			=> 'atendimento@webouvidoria.com.br ',
						'to' => array(
							'cleliapuc@gmail.com',
							'ab@webouvidoria.com.br',
						),
						'to_cc'				=> '',
						//'to_bcc'			=> array('listasguardiao@gmail.com'),
						'to_bcc'			=> '',
						'subject'			=> utf8_decode('[Web Ouvidoria] : Nova Denúncia'),
						'message'			=> $body
					);
					self::sendMailExec($arrInfos);




					/*
					 * -------------------------------------------------
					 * enviamos o email para o analista
					 * -------------------------------------------------
					**/
					/*
					SELECT 
						EMP.emp_id,
						EMP.emp_razao_social,
						NAT.nat_titulo, 
						NAT.nat_id,
						ANA.ana_email,
						REL_AN.ananat_ativo
					FROM tbl_empresas EMP 
						INNER JOIN tbl_rel_empresas_x_naturezas REL_EMP_NAT ON REL_EMP_NAT.emp_id = EMP.emp_id
						INNER JOIN tbl_naturezas NAT ON NAT.nat_id = REL_EMP_NAT.nat_id
						INNER JOIN tbl_rel_analistas_x_naturezas REL_AN ON REL_AN.nat_id = NAT.nat_id
						INNER JOIN tbl_analistas ANA ON ANA.ana_id = REL_AN.ana_id 
					WHERE EMP.emp_id = 1
					AND NAT.nat_id = 5
					AND REL_AN.ananat_ativo = 1
					ORDER BY ANA.ana_id DESC
					LIMIT 10
					*/

					$this->db->select(" ANA.ana_email ")
						->from('tbl_empresas EMP')
						->join('tbl_rel_empresas_x_naturezas REL_EMP_NAT', 'REL_EMP_NAT.emp_id = EMP.emp_id', 'INNER JOIN')
						->join('tbl_naturezas NAT', 'NAT.nat_id = REL_EMP_NAT.nat_id', 'INNER JOIN')
						->join('tbl_rel_analistas_x_naturezas REL_AN', 'REL_AN.nat_id = NAT.nat_id', 'INNER JOIN')
						->join('tbl_analistas ANA', 'ANA.ana_id = REL_AN.ana_id', 'INNER JOIN')
						->where(array(
								'EMP.emp_id' => (int)$this->authSession["emp_id"],
								'NAT.nat_id' => (int)$nat_id,
								//'REL_AN.ananat_ativo' => 1,
							)	
						)
						->order_by( 'ANA.ana_id', 'DESC' );
					$qry_relempnat = $this->db->get();
					//fct_print_debug( $this->db->last_query() );

					if( (int)$qry_relempnat->num_rows() >= 1 )
					{
							$rs_relempnat = $qry_relempnat->result();
							//fct_print_debug( $rs_relempnat );

							foreach ($rs_relempnat AS $itemEmpNat){
								$ana_email = $itemEmpNat->ana_email;
								if( !empty($ana_email) )
								{
									$arrInfos = array(
										'from_nome'		=> 'Web Ouvidoria',
										'from_email'	=> 'no-reply@webouvidoria.com.br',
										'replyto'			=> 'atendimento@webouvidoria.com.br',
										'to'					=> array($ana_email),
										'to_cc'				=> '',
										//'to_bcc'			=> array('listasguardiao@gmail.com'),
										'to_bcc'			=> '',
										'subject'			=>  utf8_decode('[Web Ouvidoria] : Denúncia : Analista'),
										'message'			=> $this->parser->parse('email/denuncia-registro', array(), TRUE)
									);
									self::sendMailExec($arrInfos);
								}
							}
					}




					/*
					 * -------------------------------------------------
					 * enviamos o email para o autor da denúncia
					 * -------------------------------------------------
					**/
					if( (int)$this->authSession["identificar"] == 1 && !empty($this->authSession["dnt_email"]) )
					{
						$dataMail = array(
							"dnt_nome"						=> utf8_decode($this->authSession["dnt_nome"]),
							"dnt_email"						=> utf8_decode($this->authSession["dnt_email"]),
							"den_num_protocolo"		=> $den_num_protocolo,
						);
						$body = $this->parser->parse('email/denuncia-registro-autor', $dataMail, TRUE);

						$arrInfos = array(
							'from_nome'		=> 'Web Ouvidoria',
							'from_email'	=> 'no-reply@webouvidoria.com.br',
							'replyto'			=> 'atendimento@webouvidoria.com.br',
							'to'					=> array($this->authSession["dnt_email"]),
							'to_cc'				=> '',
							//'to_bcc'			=> array('listasguardiao@gmail.com'),
							'to_bcc'			=> '',
							'subject'			=> utf8_decode('[Web Ouvidoria] : Denúncia'),
							'message'			=> $body
						);
						self::sendMailExec($arrInfos);
					}






					//login: no-reply@webmanager.com.br
					//senha: julho2017

					//login: atendimento@webmanager.com.br
					//senha: julho2017

					//$den_num_protocolo
					//$data_bd = array(
						//'emp_id'						=> (int)$this->authSession["emp_id"],
						//'nat_id'						=> $nat_id,
						//'dnt_id'						=> $dnt_id,
						//'den_descricao'			=> $den_descricao,
						//'den_status'				=> 'em-analise',
						//'den_status_admin'	=> 'em-analise',
						//'den_num_protocolo'	=> $den_num_protocolo,
						//'den_dte_cadastro'	=> date("Y-m-d H:i:s"),
						//'den_dte_alteracao'	=> date("Y-m-d H:i:s"),
						//'den_ativo'					=> $den_ativo,
					//);		
					//$this->db->insert('tbl_denuncias', $data_bd); 
					//$den_id = (int)$this->db->insert_id();				
				}


				/**
				 * -------------------------------------------------
				 * ini : upload dos anexos
				 * -------------------------------------------------
				**/
				if( isset($_FILES["uplArqFileUpl"]["name"]) )
				{
					$images = fct_upload_files_unico("uplArqFileUpl", $config_upl);
					if(is_array($images))
					{
						if( !empty($images["orig_name"]) )
						{
							$data_bd = array(
								'den_id'						=> $den_id,
								'danx_arquivo'			=> $images["orig_name"],
								'danx_dte_cadastro'	=> date("Y-m-d H:i:s"),
								'danx_dte_alteracao'	=> date("Y-m-d H:i:s"),
							);		
							$this->db->insert('tbl_denunc_anexos', $data_bd); 
						}

					} // is_array images
				};



				redirect( $link_redirect );
				exit();
				/**
				 * -------------------------------------------------
				 * gera o número de protocolo
				 * -------------------------------------------------
				**/


				/**
				 * -------------------------------------------------
				 * enviar email
				 * -------------------------------------------------
				**/




			}
			else
			{
				//fct_print_debug( validation_errors() );
				$msg_erro = validation_errors();
				$var_erro = 1;
			};



			//$var_erro = ($emp_id == 0) ? 1 : $var_erro;
			//$var_erro = ($nat_id == 0) ? 1 : $var_erro;
			//$var_erro = empty($den_descricao) ? 1 : $var_erro;

			//if( $var_erro == 0 )
			//{
			//}
			//else
			//{				
			//}
		};

		$data['emp_id']							= $emp_id;
		$data['nat_id']							= $nat_id;
		$data['dnt_id']							= $dnt_id;
		$data['den_descricao']			= $den_descricao;
		$data['den_num_protocolo']	= $den_num_protocolo;

		$data['var_erro']						= $var_erro;
		$data['msg_erro']						= $msg_erro;
		// -------------------------------------------------






		/**
		 * -------------------------------------------------
		 * naturezas relacionadas a empresa
		 * -------------------------------------------------
		**/
		$this->db->select(" * ")
			->from('tbl_naturezas NAT')
			->join('tbl_rel_empresas_x_naturezas REL', 'REL.nat_id = NAT.nat_id', 'INNER JOIN')
			->where(array(
					'REL.emp_id' => (int)$this->authSession["emp_id"]
				)	
			)
			->order_by( 'NAT.nat_titulo', 'ASC' );
		$qry_nat = $this->db->get();
		if( (int)$qry_nat->num_rows() >= 1 )
		{	
			$data['rs_list_nat'] = $qry_nat->result();
			//fct_print_debug( $data['rs_emp_nat'] );
		}

		$this->load->view('registro', $data);
	}

	public function registro_protocolo($den_num_protocolo, $den_hashkey)
	{
		$data = array();

		$var_erro		= 0;
		$msg_erro		= '';
		$template		= 'registro-protocolo';

		if( empty($den_num_protocolo) ||  empty($den_hashkey) ){
		
			exit('erro');
		}

		$data['den_num_protocolo'] = $den_num_protocolo;
		$data['den_hashkey'] = $den_hashkey;

		//$data['den_num_protocolo'] = (isset($this->uri->named->num) ? $this->uri->named->num : "");
		//$data['den_hashkey'] = (isset($this->uri->named->hashkey) ? $this->uri->named->hashkey : "");


		/**
		 * -------------------------------------------------
		 * verifica se dados são válidos
		 * -------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		$acao = 'INSERT';
		if($baseAcao == "SEND-FORM-REGISTRO-PROTOCOLO")
		{
			if( fct_validate("denuncia_protocolo") )
			{
				/*
				 * gravamos os dados da denuncia
				 * -------------------------------------------------
				**/
				$den_senha = (int)$this->input->post('den_senha');

				$this->db->select(" * ")
					->from('tbl_denuncias')
					->where(array(
							'den_num_protocolo' => $data['den_num_protocolo'],
							'den_hashkey' => $data['den_hashkey']
						)	
					)
					->order_by( 'den_id', 'DESC' );
				$query = $this->db->get();
				if( (int)$query->num_rows() >= 1 )
				{
					$data_bd = array(
						'den_senha' => fct_encripta($den_senha),
						'den_dte_alteracao'	=> date("Y-m-d H:i:s")
					);		
					$where_bd = array(
						'den_num_protocolo' => $data['den_num_protocolo'],
						'den_hashkey' => $data['den_hashkey']
					);
					$this->db->where($where_bd);
					$this->db->update('tbl_denuncias', $data_bd);

					$template = 'registro-sucesso';
				}
			}
			else
			{
				//fct_print_debug( validation_errors() );
				$msg_erro = validation_errors();
				$var_erro = 1;
			};
		}



		$data['var_erro']		= $var_erro;
		$data['msg_erro']		= $msg_erro;
		// -------------------------------------------------
	
		$this->load->view($template, $data);
	}


	public function naturezas()
	{
		$data = array();


		$this->db->select(" * ")
			->from('tbl_naturezas')
			->where(array(
					'nat_ativo' => '1',
					'nat_padrao' => '1'
				)	
			)
			->order_by( 'nat_id', 'ASC' )
			->order_by( 'nat_titulo', 'ASC' );
			//->order_by( 'nat_id', 'DESC' );
		$query = $this->db->get();
		$data['rs_list_nat'] = $query->result();
		//fct_print_debug( $data['rs_list_nat'] );


		$this->load->view('naturezas', $data);
	}


	public function resposta($acao="")
	{
		$data = array();

		$this->authlib->logged_in();
		$this->authSession = $this->authlib->get_session_by_value();
		$session = $this->session->userdata( $this->var_session );
		//fct_print_debug( $this->session->userdata( $this->var_session ) );



		/**
		 * --------------------------------------------------------
		 * upload do arquivo
		 * --------------------------------------------------------
		**/
		$config_upl['upload_path']		= $this->config->item('folder_images') .'/denuncias/';
		$config_upl['allowed_types']	= 'gif|jpg|jpeg|png|xls|xlsx';
		$config_upl['max_size']				= '102048';
		$config_upl['max_width']			= '5000';
		$config_upl['max_height']			= '5000';


		$var_erro						= 0;
		$msg_erro						= '';
		$msg_success				= '';

		//fct_print_debug( $this->cfg_status_definidos["replica"] );

		/**
		 * -------------------------------------------------
		 * verifica se dados são válidos
		 * -------------------------------------------------
		**/
		$baseAcao = $this->input->post('baseAcao');
		$acao = 'INSERT';
		if($baseAcao == "SEND-FORM-DENUNCIA-RESPOSTA")
		{
			if( fct_validate("denuncia_resposta") )
			{
				/*
				 * -------------------------------------------------
				 * gravamos os dados da resposta
				 * -------------------------------------------------
				**/
				$dresp_descricao = $this->input->post('dresp_descricao');

				$data_bd = array(
					'den_id'							=> $session["den_id"],
					'dresp_descricao'			=> $dresp_descricao,
					'dresp_status'				=> $this->cfg_status_definidos["replica"],
					'dresp_aprovada'			=> 0,
					'dresp_autor'					=> $this->cfg_status_definidos["label_autor"],
					'dresp_dte_cadastro'	=> date("Y-m-d H:i:s"),
					'dresp_dte_alteracao'	=> date("Y-m-d H:i:s"),
				);		
				$this->db->insert('tbl_denunc_respostas', $data_bd); 
				$dresp_id = (int)$this->db->insert_id();

				/**
				 * -------------------------------------------------
				 * ini : upload dos anexos
				 * -------------------------------------------------
				**/
				if( isset($_FILES["uplArqFileUpl"]["name"]) )
				{
					$images = fct_upload_files_unico("uplArqFileUpl", $config_upl);
					if(is_array($images))
					{
						if( !empty($images["orig_name"]) )
						{
							$data_bd = array(
								'den_id'						=> $session["den_id"],
								'danx_arquivo'			=> $images["orig_name"],
								'danx_dte_cadastro'	=> date("Y-m-d H:i:s"),
								'danx_dte_alteracao'	=> date("Y-m-d H:i:s"),
							);		
							$this->db->insert('tbl_denunc_anexos', $data_bd); 
						}

					} // is_array images
				};


				/*
				 * -------------------------------------------------
				 * gravamos os dados da resposta
				 * -------------------------------------------------
				**/
				$den_id = (int)$this->input->post('den_id');

				$data_bd = array(
					'den_status'				=> $this->cfg_status_definidos["replica"],
					'den_status_admin' => $this->cfg_status_definidos["replica"],
				);	
				$where_bd = array(
					'den_id' => $session["den_id"],
					'den_num_protocolo' => $session["den_num_protocolo"],
					'den_hashkey' => $session["den_hashkey"]
				);
				$this->db->where($where_bd);
				$this->db->update('tbl_denuncias', $data_bd);


				/**
				 * --------------------------------------------------------
				 * configuracao do email
				 * --------------------------------------------------------
				**/
					$this->db->select(" * ")
						->from('tbl_denuncias')
						->where(array(
								'den_id' => $session["den_id"],
								'den_num_protocolo' => $session["den_num_protocolo"],
								'den_hashkey' => $session["den_hashkey"]
							)	
						)
						->order_by( 'den_id', 'DESC' )
						->limit(1);
					$qry_denuncia = $this->db->get();

					if( (int)$qry_denuncia->num_rows() >= 1 )
					{
						$rs_denuncia = $qry_denuncia->row();

						$this->db->select(" ANA.ana_email ")
							->from('tbl_empresas EMP')
							->join('tbl_rel_empresas_x_naturezas REL_EMP_NAT', 'REL_EMP_NAT.emp_id = EMP.emp_id', 'INNER JOIN')
							->join('tbl_naturezas NAT', 'NAT.nat_id = REL_EMP_NAT.nat_id', 'INNER JOIN')
							->join('tbl_rel_analistas_x_naturezas REL_AN', 'REL_AN.nat_id = NAT.nat_id', 'INNER JOIN')
							->join('tbl_analistas ANA', 'ANA.ana_id = REL_AN.ana_id AND ANA.emp_id = EMP.emp_id', 'INNER JOIN')
							->where(array(
									'EMP.emp_id' => (int)$rs_denuncia->emp_id,
									'NAT.nat_id' => (int)$rs_denuncia->nat_id,
								)	
							)
							->order_by( 'ANA.ana_id', 'DESC' );
						$qry_relempnat = $this->db->get();
						fct_print_debug( $this->db->last_query() );

						if( (int)$qry_relempnat->num_rows() >= 1 )
						{
								//$rs_relempnat = $qry_relempnat->result();
								$rs_relempnat = $qry_relempnat->result_array();
								$arr_emails = array_column($rs_relempnat, 'ana_email');

								if( is_array($arr_emails) && count($arr_emails)>=1 )
								{
										$data_mail = array();
										$args_email = array(
											"data_parser"	=> $data_mail,
											"cfg_chave"		=> 'denuncia-replica',
										);
										$arr_retorno = fct_gera_html_email($args_email);

										$arrInfos = array(
											'from_nome'		=> 'Web Ouvidoria',
											'from_email'	=> 'no-reply@webouvidoria.com.br',
											'replyto'			=> 'atendimento@webouvidoria.com.br',
											'to'					=> $arr_emails,
											'to_cc'				=> '',
											//'to_bcc'			=> array('listasguardiao@gmail.com'),
											'to_bcc'			=> '',
											'subject'			=> utf8_decode('[Web Ouvidoria] : Denúncia : Réplica'),
											'message'			=> $arr_retorno["body"]
										);
										fct_sendmailexec($arrInfos);
								}
						}
					}
				// --------------------------------------------------------


				//$msg_success = "Sua resposta foi gravada com sucesso!";
				
				$this->session->set_flashdata('message_success', 'Sua resposta foi gravada com sucesso!');
				redirect( site_url('resposta/mensagem') );
			}
			else
			{
				//fct_print_debug( validation_errors() );
				$msg_erro = validation_errors();
				$var_erro = 1;
			};
		}


		if($baseAcao == "SEND-FORM-DENUNCIA-FINALIZAR")
		{
			/*
			 * -------------------------------------------------
			 * gravamos os dados da resposta
			 * -------------------------------------------------
			**/
			$den_id = (int)$this->input->post('den_id');

			$data_bd = array(
				'den_status' => 'finalizado',
				'den_status_admin' => 'finalizado',
			);	
			$where_bd = array(
				'den_id' => $session["den_id"],
				'den_num_protocolo' => $session["den_num_protocolo"],
				'den_hashkey' => $session["den_hashkey"]
			);
			$this->db->where($where_bd);
			$this->db->update('tbl_denuncias', $data_bd);

			$msg_success = "Denúncia finalizada com sucesso!";
		}


		//$data['emp_id']							= $emp_id;
		//$data['nat_id']							= $nat_id;
		//$data['dnt_id']							= $dnt_id;
		//$data['den_descricao']			= $den_descricao;
		//$data['den_num_protocolo']	= $den_num_protocolo;

		$data['var_erro']						= $var_erro;
		$data['msg_erro']						= $msg_erro;
		$data['msg_success']				= $msg_success;
		// -------------------------------------------------



		$this->db->select(" * ")
			->from('tbl_denuncias')
			->where(array(
					'den_id' => $session["den_id"],
					'den_num_protocolo' => $session["den_num_protocolo"],
					'den_hashkey' => $session["den_hashkey"]
				)	
			)
			->order_by( 'den_id', 'DESC' );
		$query = $this->db->get();
		//fct_print_debug( $this->db->last_query() );
		//fct_print_debug( $query->num_rows() );

		if( (int)$query->num_rows() >= 1 )
		{
			$rs_den = $query->row();
			$data['rs_den'] = $rs_den;
			//fct_print_debug( $data['rs_den'] );


			/**
			 * --------------------------------------------------------
			 * anexos
			 * --------------------------------------------------------
			**/
			$this->db->select(" * ")
				->from('tbl_denunc_anexos')
				->where(array(
						'den_id' => $session["den_id"]
					)	
				)
				->order_by( 'danx_id', 'DESC' );
			$query = $this->db->get();
			$data['rs_den_anexo'] = $query->result();
			//fct_print_debug( $data['rs_den_anexo'] );


			/**
			 * --------------------------------------------------------
			 * ini : respostas
			 * --------------------------------------------------------
			**/
			$status = array(
				'em-aprovacao', 
				'respondido', 
				$this->cfg_status_definidos["replica"]
			);

			$this->db->select(" * ")
				->from('tbl_denunc_respostas')
				->where(array(
						'den_id' => $session["den_id"],
					)	
				);

				$this->db->group_start();
					$this->db->or_where('dresp_status', $this->cfg_status_definidos["replica"]);

					$this->db->or_group_start();
						$this->db->where('dresp_status', $this->cfg_status_definidos["resp_admin"]);
						$this->db->where('dresp_aprovada', '1');
					$this->db->group_end();

					$this->db->or_group_start();
						$this->db->where('dresp_status', $this->cfg_status_definidos["resp_analista"]);
						$this->db->where('dresp_aprovada', '0');
					$this->db->group_end();


				$this->db->group_end();



				//->where_in('dresp_status', $status)
				//->where_in('dresp_aprovada', array(0,1))


			$this->db->order_by( 'dresp_id', 'DESC' );
			//	->limit(4);
			$qry_resp = $this->db->get();
			//fct_print_debug( $this->db->last_query() );
			//fct_print_debug( $query->num_rows() );
			if( (int)$qry_resp->num_rows() >= 1 )
			{
				$rs_resp = $qry_resp->result();
				$data['rs_resp'] = $rs_resp;
				//fct_print_debug( $data['rs_resp'] );
			}

		}
		

		$this->load->view('resposta', $data);
	}


	public function mensagem_amigavel($acao="")
	{
		$data = array();

		$this->load->view('mensagem-amigavel', $data);
	}

	public function enviaremail()
	{
		$dataMail = array(
		);
		$body = $this->parser->parse('email/denuncia-registro', $dataMail, TRUE);
		$arrInfos = array(
			'from_nome'		=> 'Web Ouvidoria',
			'from_email'	=> 'no-reply@webouvidoria.com.br',
			'replyto'			=> 'atendimento@webouvidoria.com.br ',
			'to' => array(
				'cleliapuc@gmail.com',
				'ab@webouvidoria.com.br',
				'abipge@gmail.com',
			),
			'to_cc'				=> '',
			//'to_bcc'			=> array(''),
			'to_bcc'			=> '',
			'subject'			=> utf8_decode('[Web Ouvidoria] : Nova Denúncia'),
			'message'			=> $body
		);
		self::sendMailExec($arrInfos);	
	
	}

	public function logout()
	{
		$this->session->unset_userdata( $this->var_session );

		$this->session->sess_destroy();
		redirect( );	
	}


	public function paginatemp($acao="")
	{
		$data = array();

		$this->load->view('temp-footer', $data);
	}
	

	public function sendMailExec($arrInfos)
	{
		$_CONST_SERVER_NAME = $_SERVER['SERVER_NAME'];
		if($_CONST_SERVER_NAME == "localhost")
		{
		}
		else
		{
			// Enviando email para o administrador
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			//$subject		= utf8_decode('[Web Ouvidoria] : Nova Denúncia');
			//$emailsender	= "CanalOuvidoria<no-reply@webmanager.com.br>";
			//$corpo = "TESTE : ". date("Y-m-d H:i:s");

			//$_br = "\n";	if(PHP_OS == "WINNT") { $_br = "\r\n"; }
			//$headers = "MIME-Version: 1.1". $_br;
			//$headers .= "Content-type: text/html; charset=iso-8859-1". $_br;
			//$headers .= "From: ". $emailsender . $_br;
			//$headers .= "Return-Path: ". $emailsender . $_br;
			//$headers .= "Reply-To: ". $emailsender . $_br;

			//@mail("Marcio<listasguardiao@gmail.com>", $subject, $corpo, $headers);
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

			/*
			$config['charset'] = 'utf-8';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = 'html';
			$this->email->initialize($config);
			$this->email->from('marcio@3dgarage.com.br', 'Marcio');
			$this->email->to('marcio@3dgarage.com.br'); 
			//$this->email->cc('sergio@3dgarage.com.br'); 
			//$this->email->bcc('them@their-example.com'); 
			$this->email->subject('[Albuka] : Status do seu pedido : '. $idPedido);
			$this->email->message($body);	
			$this->email->send();
			echo $this->email->print_debugger();
			*/
			if(is_array($arrInfos)){
				$strFromName	= $arrInfos["from_nome"];
				$strFromEmail	= $arrInfos["from_email"];
				$strReplyTo		= isset($arrInfos["replyto"]) ? $arrInfos["replyto"] : "";
				$strTo				= $arrInfos["to"];
				$strToCC			= $arrInfos["to_cc"];
				$strToBCC			= $arrInfos["to_bcc"];
				$strSubject		= $arrInfos["subject"];
				$strMessage		= $arrInfos["message"];
				$strAttach		= isset($arrInfos["attach"]) ? $arrInfos["attach"] : '';

				foreach ($strTo as $userEmail) : 
					//$config['charset']		= 'utf-8';
					$config['charset']			= 'iso-8859-1';
					$config['crlf']					= "\r\n";
					$config['newline']			= "\r\n";
					$config['wordwrap']			= TRUE;
					$config['mailtype']			= 'html';
					$config['protocol']			= "mail";

					//$config['useragent']		= 'CodeIgniter';
					//$config['protocol']			= "smtp";
					//$config['smtp_host']		= "smtp.webmanager.com.br";
					//$config['smtp_port']		= "465";
					//$config['smtp_user']		= "no-reply@webmanager.com.br"; 
					//$config['smtp_pass']		= "julho2017";
					//$config['smtp_timeout'] = "10";
					//$config['validate']			= TRUE;

					/*
						$mailer->Host = 'servidor_de_saida'; //Onde em 'servidor_de_saida' deve ser alterado por um dos hosts abaixo:
						//Para cPanel: 'localhost';
						//Para Plesk 11 / 11.5: 'smtp.dominio.com.br';
						 
						//Descomente a linha abaixo caso revenda seja 'Plesk 11.5 Linux'
						//$mailer->SMTPSecure = 'tls';
					*/

					$this->email->clear();
					$this->email->clear(TRUE);
					$this->email->initialize($config);
					$this->email->from( $strFromEmail, $strFromName );
					if( !empty($strReplyTo) ) { $this->email->reply_to($strReplyTo, $strFromName); }
					//$this->email->to( $strTo );
					$this->email->to( $userEmail );
					if(!empty($strToCC)): $this->email->cc($strToCC); endif;
					if(!empty($strToBCC)): $this->email->bcc($strToBCC); endif;
					$this->email->subject( $strSubject);
					$this->email->message( $strMessage );	
					
					if( is_array($strAttach) ):
						foreach ($strAttach as $Attach) :
							$this->email->attach($Attach);	
						endforeach;
					endif;

					//print $userEmail ."<hr>";
					if( $this->email->send() )
					{
						//print 'true';
					}
					else
					{
						//print 'false';
					}
					//echo $this->email->print_debugger();
				endforeach;

			}
		}
	}


}
