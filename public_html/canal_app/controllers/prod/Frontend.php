<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends MY_Controller {

	function __construct()
	{
		parent::__construct();

		/**
		 * AUTH
		**/
		//$this->authlib->logged_in();
	}

	public function index()
	{
		$data = array();

		$this->load->view('index', $data);
	}


	public function denunciar()
	{
		$data = array();

		$this->load->view('denunciar', $data);
	}


	public function empresa()
	{
		$data = array();

		$this->load->view('empresa', $data);
	}


	public function relatar_incidente()
	{
		$data = array();

		$this->load->view('relatar-incidente', $data);
	}


	public function registro()
	{
		$data = array();

		$this->load->view('registro', $data);
	}


	public function naturezas()
	{
		$data = array();


		$this->db->select(" * ")
			->from('tbl_naturezas')
			->order_by( 'nat_id', 'DESC' );
		$query = $this->db->get();
		$data['rs_list_nat'] = $query->result();
		//fct_print_debug( $data['rs_list_nat'] );


		$this->load->view('naturezas', $data);
	}

	public function resposta()
	{
		$data = array();

		$this->load->view('resposta', $data);
	}


}
