<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['cfg_validate'] = array(

	'fale_conosco' => array(
		array( 'field' => 'cnt_nome', 'label' => 'Nome', 'rules' => 'required',
			'errors' => array(
				'required' => 'Preencha corretamente seu nome.',
			),
		),
		array( 'field' => 'cnt_email', 'label' => 'E-mail', 'rules' => 'required',
			'errors' => array(
				'required' => 'Preencha corretamente seu e-mail.',
			),
		),
		array( 'field' => 'cnt_mensagem', 'label' => 'Mensagem', 'rules' => 'required',
			'errors' => array(
				'required' => 'Preencha corretamente a mensagem.',
			),
		),
	),

	'denuncia_empresa' => array(
		array( 'field' => 'nat_id', 'label' => 'Natureza', 'rules' => 'required',
			'errors' => array(
				'required' => 'Selecione classificação do incidente.',
			),
		),
	),

	'denuncia_natureza' => array(
		array( 'field' => 'nat_id', 'label' => 'Natureza', 'rules' => 'required',
			'errors' => array(
				'required' => 'Selecione classificação do incidente.',
			),
		),
	),

	'denuncia_registro' => array(
		array( 'field' => 'nat_id', 'label' => 'Natureza', 'rules' => 'required',
			'errors' => array(
				'required' => 'Selecione classificação do incidente.',
				//'user_name_check' => 'Preencha corretamente o campo nome 2 %s.',
			),
		),
		array( 'field' => 'den_descricao', 'label' => 'Descrição', 'rules' => 'required',
			'errors' => array(
				'required' => 'Preencha corretamente a descrição.',
				//'user_name_check' => 'Preencha corretamente o campo nome 2 %s.',
			),
		),
		//array( 'field' => 'nat_id', 'label' => 'Natureza', 'rules' => 'required|callback_user_name_check',
			////'errors' => array(
				////'required' => 'Preencha corretamente o campo nome %s.',
				////'user_name_check' => 'Preencha corretamente o campo nome 2 %s.',
			////),
		//),
		/*
		array(
			'field' => 'user_cpf',
			'label' => 'CPF',
			'rules' => 'required|min_length[11]|valid_cpf'
		),		
		array(
			'field' => 'user_phone',
			'label' => 'Telefone',
			'rules' => 'required'
		),
		array(
			'field' => 'user_login',
			'label' => 'Login',
			'rules' => 'required|min_length[8]'
		),
		*/
	),

	'denuncia_resposta' => array(
		array( 'field' => 'dresp_descricao', 'label' => 'Descrição', 'rules' => 'required',
			'errors' => array(
				'required' => 'Preencha corretamente a descrição.',
				//'user_name_check' => 'Preencha corretamente o campo nome 2 %s.',
			),
		),
	),

	'denuncia_protocolo' => array(
		array( 'field' => 'den_senha', 'label' => 'Senha', 'rules' => 'required|min_length[8]',
			'errors' => array(
				'required' => 'Informe a senha.',
				'min_length' => 'A senha deve ter mínimo de 8 caracteres.',
				//'user_name_check' => 'Preencha corretamente o campo nome 2 %s.',
			),
		),
		array( 'field' => 'den_senha_confirm', 'label' => 'Confirme a senha', 'rules' => 'required|min_length[8]|matches[den_senha]',
			'errors' => array(
				'required' => 'Confirme corretamente a senha.',
				'min_length' => 'A confirmação da senha deve ter mínimo de 8 caracteres.',
				'matches' => 'A confirmação está diferente a senha informada.',
				//'user_name_check' => 'Preencha corretamente o campo nome 2 %s.',
			),
		),
		//array( 'field' => 'nat_id', 'label' => 'Natureza', 'rules' => 'required|callback_user_name_check',
			////'errors' => array(
				////'required' => 'Preencha corretamente o campo nome %s.',
				////'user_name_check' => 'Preencha corretamente o campo nome 2 %s.',
			////),
		//),
	),

	'denuncia_acompanhar' => array(
		array( 'field' => 'den_num_protocolo', 'label' => 'Senha', 'rules' => 'required',
			'errors' => array(
				'required' => 'Informe corretamente o número do protocolo.',
			),
		),
		array( 'field' => 'den_senha', 'label' => 'Senha', 'rules' => 'required',
			'errors' => array(
				'required' => 'Informe corretamente a senha.',
			),
		),
	),

	'user_register_old' => array(
		array( 'field' => 'user_name', 'label' => 'Nome', 'rules' => 'required|callback_user_name_check',
			//'errors' => array(
				//'required' => 'Preencha corretamente o campo nome %s.',
				//'user_name_check' => 'Preencha corretamente o campo nome 2 %s.',
			//),
		),
		/*
		array(
			'field' => 'user_cpf',
			'label' => 'CPF',
			'rules' => 'required|min_length[11]|valid_cpf'
		),		
		array(
			'field' => 'user_phone',
			'label' => 'Telefone',
			'rules' => 'required'
		),
		array(
			'field' => 'user_login',
			'label' => 'Login',
			'rules' => 'required|min_length[8]'
		),
		*/
		array( 'field' => 'user_email',			'label' => 'E-mail',		'rules' => 'required|matches[user_email_confirm]', ),
		array( 'field' => 'user_email_confirm', 'label' => 'Confirme seu e-mail',	'rules' => 'required',
			//'rules' => 'required|matches[passconf]callback_user_email_confirm_check',
		),
		array( 'field' => 'user_password',		'label' => 'Senha',			'rules' => 'required|matches[user_password_confirm]', ),
		array( 'field' => 'user_password_confirm', 'label' => 'Confirme sua senha', 'rules' => 'required', ),
	),
	'user_info_access' => array(
		array( 
			'field' => 'user_email',
			'label' => 'E-mail',
			'rules' => 'required|valid_email|matches[user_email_confirm]', 
		),
		array(
			'field' => 'user_email_confirm',
			'label' => 'Confirme seu e-mail',
			'rules' => 'required|valid_email', 
		),
		array( 
			'field' => 'user_password',
			'label' => 'Senha',
			'rules' => 'required|min_length[6]|matches[user_password_confirm]',
			//'errors' => array( 'required' => 'Preencha corretamente o %s.', )
		),
		array( 
			'field' => 'user_password_confirm',
			'label' => 'Confirme sua senha',
			'rules' => 'required|min_length[6]',
		),
	),
	'user_forgot_password' => array(
		array( 'field' => 'user_email',			'label' => 'E-mail',		'rules' => 'required', ),
	),
	'user_generate_new_password' => array(
		array( 'field' => 'user_email',			'label' => 'E-mail',		'rules' => 'required', ),
		array( 'field' => 'user_new_hashkey',	'label' => 'Hashkey',		'rules' => 'required', ),
		array( 'field' => 'user_password',		'label' => 'Senha',			'rules' => 'required', ),
	),
	'user_login' => array(
		array( 'field' => 'user_email',			'label' => 'E-mail',		'rules' => 'required', ),
		array( 'field' => 'user_password',		'label' => 'Senha',			'rules' => 'required', ),
	),
	'user_avatar' => array(
		array( 'field' => 'user_file_avatar',	'label' => 'Avatar',		'rules' => 'required', ),
		array( 'field' => 'user_file_banner',	'label' => 'Banner',		'rules' => 'required', ),
	),
	'user_address' => array(
		array( 'field' => 'adds_zipcode',		'label' => 'CEP',			'rules' => 'required', ),
		array( 'field' => 'adds_logradouro',	'label' => 'Endereço',		'rules' => 'required', ),
		array( 'field' => 'adds_number',		'label' => 'Número',		'rules' => 'required', ),
		array( 'field' => 'adds_district',		'label' => 'Bairro',		'rules' => 'required', ),
		array( 'field' => 'adds_city',			'label' => 'Cidade',		'rules' => 'required', ),
		array( 'field' => 'adds_state',			'label' => 'Estado',		'rules' => 'required', ),
	),
	'user_community' => array(
		array( 'field' => 'cmty_file_avatar',	'label' => 'Avatar',		'rules' => 'required', 
			'errors' => array( 'required' => 'Selecione um %s.', )
		),
		array( 'field' => 'cmty_file_banner',	'label' => 'Banner',		'rules' => 'required', 
			'errors' => array( 'required' => 'Selecione um %s.', )
		),
		array( 'field' => 'cmty_title',			'label' => 'Título',		'rules' => 'required', ),
		array( 'field' => 'cmty_description',	'label' => 'Descrição',		'rules' => 'required', ),
	),
	'community_message' => array(
		array( 'field' => 'cmty_msg_description',	'label' => 'Descrição',		'rules' => 'required', ),
	),
	'community_invites' => array(
		array( 'field' => 'cmty_invite_description',	'label' => 'Descrição',		'rules' => 'required', ),
	),

	'feedback_register' => array(
		array( 'field' => 'rent_hashkey',	'label' => 'HashKey',				'rules' => 'required', ),
		array( 'field' => 'rent_number',	'label' => 'Número da Locação',		'rules' => 'required', ),
		array( 'field' => 'feed_avalicao',	'label' => 'Status de Avaliação',	'rules' => 'required', 
			'errors' => array( 'required' => 'Selecione um %s.', )
		),
		/*
		array( 'field' => 'ratting-item[id]',	'label' => 'Classificação de Avaliação',	'rules' => 'required', 
			'errors' => array( 'required' => 'Selecione um %s.', )
		),
		*/
	),


	'form_contato' => array(
		array(
			'field' => 'cnt_name',
			'label' => 'Nome',
			'rules' => 'required',
		),
		array(
			'field' => 'cnt_email',
			'label' => 'E-mail',
			'rules' => 'required',
		),
		array(
			'field' => 'cnt_phone',
			'label' => 'Telefone',
			'rules' => 'required',
		),
		array(
			'field' => 'cnt_message',
			'label' => 'Mensagem',
			'rules' => 'required',
		),
	),

	'product_by_user' => array(
		array(
			'field' => 'prod_title',
			'label' => 'Nome do Produto',
			'rules' => 'required',
		),
		array(
			'field' => 'prod_description',
			'label' => 'Descrição do produto',
			'rules' => 'required',
		),
	),

	'comment_product' => array(
		/*
		array(
			'field' => 'prod_title',
			'label' => 'Nome do Produto',
			'rules' => 'required',
		),
		*/
		array(
			'field' => 'comm_description',
			'label' => 'Descrição do comentário',
			'rules' => 'required',
		),
	),
	'comment_parents' => array(
		array(
			'field' => 'cmty_msg_description',
			'label' => 'Descrição do comentário',
			'rules' => 'required',
		),
	),
	'complaints_product' => array(
		/*
		array(
			'field' => 'prod_title',
			'label' => 'Nome do Produto',
			'rules' => 'required',
		),
		*/
		array(
			'field' => 'cpnt_description',
			'label' => 'Descrição da denúncia',
			'rules' => 'required',
		),
	),

	




);

/*
// usage:

	$this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
	$this->config->load('form_validation');
	$cfg_validate = $this->config->item('cfg_validate');

	$this->form_validation->set_rules( $cfg_validate["user_register"] );
	if ($this->form_validation->run() == TRUE):

	$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[12]|xss_clean');
	$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[passconf]|md5');
	$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required');
	$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
*/

/* End of file form_validation.php */
/* Location: ./application/config/form_validation.php */
