<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
|--------------------------------------------------------------------------
| CONFIGURACOES DIVERSAS
|--------------------------------------------------------------------------
|
*/

$config['cfg_constantes'] = array (
	'SITE_NAME'				=> "",
	'SITE_URL'				=> site_url(),
	'TEMPLATE'				=> "default",
	//'SESSION'					=> "##_SESSION_WEBSITE_##",
	//'SESSION_CLIE'		=> "##_SESSION_CLIENTE_##",
	'SESSION_EMPRESA'	=> "##_SESSION_EMPRESA_##",
);


$config['cfg_den_status'] = array (
	/*
	"Em análise" 
	"Respondido"
	"Aguardando"
	"Finalizado"
	*/
	'em-analise' => array("abrev" => 'em-analise', "titulo" => 'Em Análise'),
	'respondido' => array("abrev" => 'respondido', "titulo" => 'Respondido'),


	'aguardando' => array("abrev" => 'aguardando', "titulo" => 'Aguardando'),
	'finalizado' => array("abrev" => 'finalizado', "titulo" => 'Finalizado'),
);

$config['cfg_den_status'] = array (
	/*
	"Em análise" 
	"Em Aprovação"
	"Responiddo"
	"Pendente de Réplica"
	"Segunda Resposta Enviada"
	"Finalizado"

	// label-danger
	*/
	'em-analise'				=> array("abrev" => 'em-analise',				"titulo" => 'Em Análise',								"color" => 'label-warning'),
	'em-aprovacao'			=> array("abrev" => 'em-aprovacao',			"titulo" => 'Em Aprovação',							"color" => 'label-primary'),
	'respondido'				=> array("abrev" => 'respondido',				"titulo" => 'Respondido',								"color" => 'label-success'),
	'respondido'				=> array("abrev" => 'respondido',				"titulo" => 'Respondido',								"color" => 'label-success'),
	'pendente-replica'	=> array("abrev" => 'pendente-replica',	"titulo" => 'Pendente de Réplica',			"color" => 'label-success'),
	'segunda-resposta'	=> array("abrev" => 'respondido',				"titulo" => 'Segunda Resposta Enviada',	"color" => 'label-success'),
	'finalizado'				=> array("abrev" => 'finalizado',				"titulo" => 'Finalizado',								"color" => 'label-info'),
);

$config['cfg_status_definidos'] = array (
	'default'					=> 'em-analise',
	'analista'				=> 'em-aprovacao',
	'administrador'		=> 'respondido',
	'empresa'					=> 'respondido',

	'replica'					=> 'pendente-replica',
	'label_autor'			=> 'autor da denuncia',
	'resp_admin'			=> 'respondido',
	'resp_analista'		=> 'em-aprovacao',
	
);

$config['cfg_meses'] = array (
	/*
	'1'	=> array('jan','Janeiro'),
	'2'	=> array('fev','Fevereiro'),
	'3'	=> array('mar','Março'),
	'4'	=> array('abr','Abril'),
	'5'	=> array('mai','Maio'),
	'6'	=> array('jun','Junho'),
	'7'	=> array('jul','Julho'),
	'8'	=> array('ago','Agosto'),
	'9'	=> array('set','Setembro'),
	'10'=> array('out','Outubro'),
	'11'=> array('nov','Novembro'),
	'12'=> array('dez','Dezembro'),
	*/
	'1' => array ('jan', 'January'),
	'2' => array ('feb', 'February'),
	'3' => array ('mar', 'March'),
	'4' => array ('apr', 'April'),
	'5' => array ('may', 'May'),
	'6' => array ('jun', 'June'),
	'7' => array ('jul', 'July'),
	'8' => array ('aug', 'August'),
	'9' => array ('sep', 'September'),
	'10' => array ('oct', 'October'),
	'11' => array ('nov', 'November'),
	'12' => array ('dec', 'December'),
);

$config['cfg_meses_txt'] = array (
	/*
	'jan'	=> array('1','Janeiro'),
	'fev'	=> array('2','Fevereiro'),
	'mar'	=> array('3','Março'),
	'abr'	=> array('4','Abril'),
	'mai'	=> array('5','Maio'),
	'jun'	=> array('6','Junho'),
	'jul'	=> array('7','Julho'),
	'ago'	=> array('8','Agosto'),
	'set'	=> array('9','Setembro'),
	'out'	=> array('10','Outubro'),
	'nov'	=> array('11','Novembro'),
	'dev'	=> array('12','Dezembro'),
	*/
	'jan' => array ('1', 'January'),
	'feb' => array ('2', 'February'),
	'mar' => array ('3', 'March'),
	'apr' => array ('4', 'April'),
	'may' => array ('5', 'May'),
	'jun' => array ('6', 'June'),
	'jul' => array ('7', 'July'),
	'aug' => array ('8', 'August'),
	'sep' => array ('9', 'September'),
	'oct' => array ('10', 'October'),
	'nov' => array ('11', 'November'),
	'dec' => array ('12', 'December'),
);

$config['cfg_day_week'] = array (
	/*
	0	=> array('dom','Domingo'),
	1	=> array('seg','Segunda-feira'),
	2	=> array('ter','Terça-feira'),
	3	=> array('qua','Quarta-feira'),
	4	=> array('qui','Quinta-feira'),
	5	=> array('sex','Sexta-feira'),
	6	=> array('sac','Sábado')
	*/
	0 => array ('sun', 'Sunday'),
	1 => array ('mon', 'Monday'),
	2 => array ('tue', 'Tuesday'),
	3 => array ('wed', 'Wednesday'),
	4 => array ('thu', 'Thursday'),
	5 => array ('fri', 'Friday'),
	6 => array ('sat', 'Saturday')
);


/* End of file frontend_settings.php */
/* Location: ./application/config/frontend_settings.php */
