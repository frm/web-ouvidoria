<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_URI extends CI_URI {
	var $named;

	function  __construct() {
		parent::__construct();

		$uri = $this->segment_array();
		$arr_property = array();
		foreach($uri as $val) {
			if (strpos($val, ':')) {
				$named_key = substr($val, 0, strpos($val, ':'));
				$named_val = substr($val, strpos($val, ':')+1);
				//$this->named->{substr($val, 0, strpos($val, ':'))} = substr($val, strpos($val, ':')+1);

				$arra_temp = array( $named_key => $named_val );
				$arr_property = array_merge($arr_property, $arra_temp);
			}
		}

		$this->named = (object) $arr_property;
	}
}