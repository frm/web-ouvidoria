<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

Class AuthLib {

	public $CI;
	public $var_session;
	public $cfg;

	public function __construct(){
		$this->CI =& get_instance();

		$this->CI->config->load('cfg_variaveis');
		$this->cfg = $this->CI->config->item('cfg_constantes');
		//$session = $ci->session->userdata( $ci->cfg["SESSION"] );
		$this->var_session = $this->cfg["SESSION_EMPRESA"];
	}


	public function logged_in()
	{
		$array_session = $this->CI->session->userdata( $this->var_session );
		//fct_print_debug( $array_session );

		if( count($array_session) == 0)
		{
			redirect( site_url() );
			exit();
		}

		//self::get_session();
	}


	public function get_session()
	{
		$array_session = $this->CI->session->userdata( $this->var_session );

		if( isset($array_session["emp_select"]) )
		{
			if( $array_session["emp_select"] == "true" )
			{

				$this->CI->load->vars(array('session_emp_id' => $array_session["emp_id"]));
				$this->CI->load->vars(array('session_emp_codigo' => $array_session["emp_codigo"]));
				$this->CI->load->vars(array('session_emp_raza_social' => $array_session["emp_raza_social"]));

				if( isset($array_session["nat_id"]) ){
					$this->CI->load->vars(array('session_nat_id' => $array_session["nat_id"]));
				}
				if( isset($array_session["nat_titulo"]) ){
					$this->CI->load->vars(array('session_nat_titulo' => $array_session["nat_titulo"]));
				}

			}
		}
	}


	public function get_session_by_value()
	{
		$array_session = $this->CI->session->userdata( $this->var_session );
		//fct_print_debug( $array_session );

		if( isset($array_session["emp_select"]) )
		{
			if( $array_session["emp_select"] == "true" )
			{
				//fct_print_debug( $array_session );
				return $array_session;
			}
		}
	}



}// end MrsAuth

/* End of file AuthLib.php */
/* Location: ./application/libraries/AuthLib.php */