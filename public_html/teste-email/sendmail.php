<?php
header("Content-Type: text/html; charset=UTF-8");
error_reporting(E_ALL);
ini_set('display_errors','On');
setlocale(LC_TIME, "portuguese");
date_default_timezone_set('America/Sao_Paulo');


/*
 * ---------------------------------------------------------------------
 * INCLUDES
 * ---------------------------------------------------------------------
**/
require_once 'PHPMailer/PHPMailerAutoload.php';


	/*
	 * ---------------------------------------------------------------------
	 * REQUEST
	 * ---------------------------------------------------------------------
	**/
	$type_input = INPUT_POST;
	//$type_input = INPUT_GET;

	$baseAcao = filter_input($type_input, 'baseAcao', FILTER_SANITIZE_STRING);
	switch ($baseAcao) {
	case "SEND-FORM-INSCRICAO" :
			/*
			 * ---------------------------------------------------------------------
			 * INSCRICAO
			 * sendmail.php?baseAcao=SEND-FORM-INSCRICAO&strnome=M%C3%A1rcio+Lima&stremail=marcio%40listasguardiao.com&strtelefone=123&txtmensagem=testemensagem
			 * ---------------------------------------------------------------------
			**/
		

			$_error = 0;



			/*
			 * ---------------------------------------------------------------------
			 * definições
			 * ---------------------------------------------------------------------
			**/
				$subject = "[IPGE] : Inscrição";			// Assunto do Email



			/*
			 * ---------------------------------------------------------------------
			 * emails para onde deverá ser enviado
			 * ---------------------------------------------------------------------
			**/
				$enviar_para = array(
					'listasguardiao@gmail.com',
					'cleliapuc@gmail.com',
				);


			
			/*
			 * ---------------------------------------------------------------------
			 * validação dos campos do formulário
			 * ---------------------------------------------------------------------
			**/
				$cnt_nome = filter_input($type_input, 'cnt_nome', FILTER_SANITIZE_STRING);
				$_error = empty($cnt_nome) ? 1 : $_error;

				$cnt_email = filter_input($type_input, 'cnt_email', FILTER_VALIDATE_EMAIL);
				$_error = empty($cnt_email) ? 2 : $_error;
				//$_error = ($cnt_email) ? 3 : $_error;

				//if( $cnt_email ){
					//$cnt_email = sanitize_text_field( $cnt_email );
				//}else{
					//$_error = 1;
				//}
				//if( empty($cnt_email) ){ $_error = 1; }

				$cnt_mensagem = filter_input($type_input, 'cnt_mensagem', FILTER_SANITIZE_STRING);
				$_error = empty($cnt_mensagem) ? 4 : $_error;


			if($_error != 0){

				$obj["error"]			= $_error;
				$obj["err_msg"]		= 'Preencha corretamente os campos.';

			}else{

				/*
   			 * ---------------------------------------------------------------------
				 * cria log com dados enviados pelos formulários
			   * ---------------------------------------------------------------------
				**/
					//$filename = (isset($args["filename"]) ? $args["filename"] : date("m") ."_". date("Y") .".log"); 
					// ". $baseAcao ."_". date("m") ."_". date("Y") .".log"
					$dataJSON = json_encode($_POST);

					$folder		= "dados/__xx-INSCRICOES-IPGE-xx_";
					$dir_root	= dirname( $_SERVER['DOCUMENT_ROOT'] ) . DIRECTORY_SEPARATOR . $folder;
					if( !is_dir($dir_root) ) mkdir($dir_root, 0777, TRUE);
					
					$fp = fopen($dir_root . "/inscricoes_ipge.txt","a+");
					fwrite($fp,"\n---- ". date("d/m/Y H:i:s")  ." - informacoes da mensagem ---- \n");
					fwrite($fp,$dataJSON);



				/*
   			 * ---------------------------------------------------------------------
				 * html do email
			   * ---------------------------------------------------------------------
				**/
					$body = '<table >
						<tr>
							<td>nome: '. $cnt_nome .'</td>
						</tr>
						<tr>
							<td>e-mail: '. $cnt_email .'</td>
						</tr>
						<tr>
							<td>mensagem: '. $cnt_mensagem .'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								data: '. date("d/m/Y H:i:s") .' 
								&nbsp;&nbsp;&nbsp;
								IP: '. $_SERVER["REMOTE_ADDR"] .' 
							</td>
						</tr>
					</table>';
				


				/*
				 * ---------------------------------------------------------------------
				 * eviando o email
				 * ---------------------------------------------------------------------
				**/
					$args = array(
						'from_nome'		=> 'Canal Pesquisa',
						'from_email'	=> 'no-reply@webmanager.com.br',
						'replyto'			=> 'atendimento@webmanager.com.br',
						'to'					=> $enviar_para,
						'to_cc'				=> '',
						//'to_bcc'			=> array('listasguardiao@gmail.com'),
						'to_bcc'			=> '',
						'attach'			=> '',
						//'subject'			=> utf8_encode('[Canal Pesquisa] : Inscrição'),
						'subject'			=> $subject,
						'message'			=> $body 
					);
					fct_sendmail($args);


				/*
				 * ---------------------------------------------------------------------
				 * mensagem de sucesso
				 * ---------------------------------------------------------------------
				**/
					$obj["error"]			= $_error;
					$obj["err_msg"]		= 'Mensagem enviada com sucesso.';
		
			}


			/*
			 * ---------------------------------------------------------------------
			 * impressao da mensagem em json
			 * ---------------------------------------------------------------------
			**/
				$json = json_encode( $obj );
				exit($json);
	
	break;
	}




	/*
	 * ---------------------------------------------------------------------
	 * funcao para enviar o email
	 * nao alterar nada nesta função
	 * a nao ser que tenha certeza do que esteja alterando
	 * ---------------------------------------------------------------------
	**/
	function fct_sendmail($args){
		if( empty($args) ) return;

		/*
		 * ---------------------------------------------------------------------
		 * config do email para autenticacao
		 * ---------------------------------------------------------------------
		**/
			$isSMTP				= true;
			$SMTPhost			= "smtp.canalouvidoria.com.br";
			$username			= "no-reply@canalouvidoria.com.br";
			$password			= "C@nal20170uvidoria";
			$SMTPport			= "465"; // 587 // 465

			$config = array ( 
				"sender_name"			=> "IPGE",
				"sender_mail"			=> "no-reply@ipge.com.br",
				"smtp_auth"				=> false, 
				"smtp_host"				=> "smtp.webmanager.com.br", 
				"smtp_port"				=> "465",
				"smtp_user"				=> "no-reply@webmanager.com.br",
				"smtp_pass"				=> "julho2017",
				"smtp_auth"				=> false,
				"smtp_tls"				=> false, // usado quando for dados do gmail
				"mail_debug"			=> true, // mostra debug na tela
			);


		/*
		 * ---------------------------------------------------------------------
		 * parametros para envio
		 * ---------------------------------------------------------------------
		**/
			$strFromName	= (isset($args["from_nome"])	? $args["from_nome"] : "");
			$strFromEmail	= (isset($args["from_email"]) ? $args["from_email"] : "");
			$strTo				= (isset($args["to"])					? $args["to"] : "");
			$strToCC			= (isset($args["to_cc"])			? $args["to_cc"] : "");
			$strToBCC			= (isset($args["to_bcc"])			? $args["to_bcc"] : "");
			$strSubject		= (isset($args["subject"])		? $args["subject"] : "");
			$strMessage		= (isset($args["message"])		? $args["message"] : "");
			$strAttach		= (isset($args["attach"])			? $args["attach"] : "");


			foreach ($strTo as $userEmail) :
				$mail = new PHPMailer();
				$mail->clearAllRecipients();
				//$mail->CharSet = 'iso-8859-1';
				$mail->CharSet = 'UTF-8';
				$mail->isHTML = true;

				if( $config['smtp_auth'] == true ){
					$mail->isSMTP();
					$mail->SMTPAuth = true;
					if( $config['smtp_tls'] == true){ $mail->SMTPSecure = 'tls'; };
					$mail->Host = $config['smtp_host'];
					$mail->Port = $config['smtp_port'];

					$mail->Username = $config['smtp_user'];
					$mail->Password = $config['smtp_pass'];
				}else{
					$mail->isMail();
				}

				//Enable SMTP debugging
				// 0 = off (for production use)
				// 1 = client messages
				// 2 = client and server messages
				$mail->SMTPDebug = 2;	//(($pDebug == true) ? 2 : 0);
				$mail->Debugoutput = 'html';

				$mail->setFrom($config['sender_mail'], $config['sender_name']);
				$mail->addReplyTo($config['sender_mail'], $config['sender_mail']);
				$mail->addAddress($userEmail, $userEmail);
				//if(!empty($strToCC)): $ci->email->cc($strToCC); endif;
				//if(!empty($strToBCC)): $ci->email->bcc($strToBCC); endif;

				$mail->Subject = $strSubject;
				$mail->Body = $strMessage;
				$mail->AltBody = $strMessage;		//Replace the plain text body with one created manually

				if( !empty($strAttach) ):
					$mail->addAttachment($strAttach);
				endif;

				if (!$mail->send()) {
						//echo "Mailer Error: " . $mail->ErrorInfo;
						//echo "<hr>";
				} else {
						//echo "<h2>Message sent! ". $userEmail ."</h2>";
						//echo "<hr>";
				}
				unset($mail);
			endforeach;
	}
?>