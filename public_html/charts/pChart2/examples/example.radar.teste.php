<?php   
 /* CAT:Polar and radars */

 /* pChart library inclusions */
 include("../class/pData.class.php");
 include("../class/pDraw.class.php");
 include("../class/pRadar.class.php");
 include("../class/pImage.class.php");

	//CULTURA	6,90
	//COMUNICAÇÃO	5,80
	//CONHECIMENTO	5,36
	//CONDIÇÕES	6,20
	//ETICA	6,62
	//MEDIA GERAL	6,18

 $arr_data = array(
		"title" => "Families", 
		"dados" => array(
			array("value"=> 4.9,		"label"=> "Cultura"),
			array("value"=> 5.8,		"label"=> "Comunicação"),
			array("value"=> 5.36,		"label"=> "Conhecimento"),
			array("value"=> 6.2,		"label"=> "Condições"),
			array("value"=> 6.62,		"label"=> "Ética"),
			array("value"=> 3.62,		"label"=> "Disciplina"),
		)
 );
 $arr_data_title = $arr_data["title"];
 $arr_data_dados = array_column($arr_data["dados"], 'value');
 $arr_data_label = array_column($arr_data["dados"], 'label');

 //print '<pre>';
 //print_r( $arr_data["title"] );
 //print '</pre>';
 
 //print '<br />';
 
 //print '<pre>';
 //print_r( $arr_data["dados"] );
 //print '</pre>';

 //$arr_data_dados = array_column($arr_data["dados"], 'value');
 //print '<pre>';
 //print_r( $arr_data_dados );
 //print '</pre>';

 //$arr_data_label = array_column($arr_data["dados"], 'label');
 //print '<pre>';
 //print_r( $arr_data_label );
 //print '</pre>';

 //exit();

 /* Create and populate the pData object */
 $MyData = new pData();   
 $MyData->addPoints($arr_data_dados,"Score");  
 $MyData->setSerieDescription("Score","Application A");
 $MyData->setPalette("Score",array("R"=>157,"G"=>196,"B"=>22));

 /* Define the absissa serie */
 $MyData->addPoints($arr_data_label,$arr_data_title);
 $MyData->setAbscissa($arr_data_title);

 /* Create the pChart object */
 $myPicture = new pImage(300,300,$MyData);
 $myPicture->drawGradientArea(0,0,300,300,DIRECTION_VERTICAL,array("StartR"=>200,"StartG"=>200,"StartB"=>200,"EndR"=>240,"EndG"=>240,"EndB"=>240,"Alpha"=>100));
 $myPicture->drawGradientArea(0,0,300,20,DIRECTION_HORIZONTAL,array("StartR"=>30,"StartG"=>30,"StartB"=>30,"EndR"=>100,"EndG"=>100,"EndB"=>100,"Alpha"=>100));
 $myPicture->drawLine(0,20,300,20,array("R"=>255,"G"=>255,"B"=>255));
 $RectangleSettings = array("R"=>180,"G"=>180,"B"=>180,"Alpha"=>30);

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,299,299,array("R"=>255,"G"=>255,"B"=>255));

 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"../fonts/Silkscreen.ttf","FontSize"=>8));
 $myPicture->drawText(10,13,"pRadar - Draw radar charts",array("R"=>255,"G"=>255,"B"=>255));

 /* Set the default font properties */ 
 $myPicture->setFontProperties(array("FontName"=>"../fonts/Verdana.ttf","FontSize"=>10,"R"=>80,"G"=>80,"B"=>80));

 /* Enable shadow computing */ 
 $myPicture->setShadow(TRUE,array("X"=>2,"Y"=>2,"R"=>255,"G"=>255,"B"=>255,"Alpha"=>10));

 /* Create the pRadar object */ 
 $SplitChart = new pRadar();

 /* Draw a radar chart */ 
 $myPicture->setGraphArea(10,25,290,290);
 $Options = array(
	"FixedMax"					=> 10, 
	"AxisRotation"			=> 0,			//-60,
	"DrawPoly"					=> TRUE,
	"WriteValues"				=> TRUE,
	//"ValueFontSize"		=> 6, // ANTERIOR
	"ValueFontSize"			=> 6,

	//"SkipLabels"=>3, 
	//"LabelMiddle"=>TRUE, 

	"Layout"						=> RADAR_LAYOUT_STAR,
	"BackgroundGradient"=>array(
			"StartR"=>255,
			"StartG"=>255,
			"StartB"=>255,
			"StartAlpha"=>100,
			"EndR"=>207,
			"EndG"=>227,
			"EndB"=>125,
			"EndAlpha"=>5
		)
	);
 $SplitChart->drawRadar($myPicture,$MyData,$Options);

 /* Render the picture (choose the best way) */
 $myPicture->autoOutput("pictures/example.radar.values.png");
 //print '<img src="'. $myPicture->autoOutput("pictures/example.radar.values.png"); .'" />';
?>